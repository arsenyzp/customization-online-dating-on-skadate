<?php

class component_ThisIsMe extends SK_Component {

    private $list_type;

    /**
     * Component VideoList constructor.
     *
     * @return component_VideoList
     */
    public function __construct(array $params = null) {
        parent::__construct('this_is_me');

    }

    public function render(SK_Layout $Layout) {
       
        $tabs = intval(SK_HttpRequest::$GET['tabs']);
        $profile_id = SK_HttpUser::profile_id();
        $page = isset(SK_HttpRequest::$GET['page']) ? SK_HttpRequest::$GET['page'] : 1;
        $list = app_ThisMe::getMediaList($profile_id);
        $page_limit = SK_Config::Section('video')->Section('other_settings')->display_media_list_limit;
        $list_p = array();
        foreach ($list as $k=>$v){
            //пределать пагинацию
            if($k > $page_limit*$page || $k < $page_limit*$page-$page_limit)
                continue;
            
            if(isset($v['photo_id'])){
                $list[$k]['photo_url']=app_ProfilePhoto::getUrl($v['photo_id']);
            }
             if(isset($v['video_id'])){
                $list[$k]['url'] = app_ProfileVideo::getVideoURL($v['hash'], $v['extension']);
                $list[$k]['thumb_img'] = app_ProfileVideo::getVideoThumbnail($v);
                $list[$k]['video_page'] = app_ProfileVideo::getVideoViewURL($v['hash']);
             }
             array_push($list_p, $list[$k]);
        }


        $Layout->assign('paging',array(
                'total'=> count($list),
                'on_page'=> SK_Config::Section('video')->Section('other_settings')->display_media_list_limit,
                'pages'=> SK_Config::Section('site')->Section('additional')->Section('profile_list')->nav_per_page,
        ));
        
         $Layout->assign('profile_id', SK_HttpUser::profile_id());
         
         $Layout->assign('list', $list_p);
        return parent::render($Layout);
    }
    
        function prepare(SK_Layout $Layout, SK_Frontend $Frontend) {
            $Frontend->include_js_file(URL_STATIC . 'this_is_me.js');
             return parent::render($Layout);
        }

}

