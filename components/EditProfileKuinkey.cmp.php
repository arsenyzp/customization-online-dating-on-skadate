<?php

class component_EditProfileKuinkey extends SK_Component {

    public $profile_id;
    public $completeMode = false;

    public function __construct(array $params = null) {
        parent::__construct('edit_profile');
    }

    public function prepare(SK_Layout $Layout, SK_Frontend $Frontend) {

        $Frontend->include_js_file(URL_STATIC . 'jquery.tokeninput.js');
        $Frontend->include_js_file(URL_STATIC . 'foto_upload.js');
        $Frontend->include_js_file(URL_STATIC . 'select_plugin.js');
        $this->profile_id = SK_HttpUser::profile_id();
        $Layout->assign('form_tab', SK_HttpRequest::$GET["form_tab"]);

        parent::prepare($Layout, $Frontend);
    }

    public function render(SK_Layout $Layout, SK_Frontend $Frontend) {

        $user_fields = app_Profile::getFieldValues($this->profile_id, array('real_name', 'tag', 'last_name', 'last_name', 'email', 'age', 'sex', 'country_id', 'interested_in', 'match_sex', 'general_description', 'username',
                    'contract_updated',
                    'received_keys',
                    'new_follower',
                    'fantasy_completed',
                    'received_new_comment_performance',
                    'you_rated',
                    'added_favorites_video',
                    'posted_fantasy',
                    'new_message'));
        ////TAG
        $tag_list = array();
        $str_tag = explode(',', $user_fields['tag']);
        foreach ($str_tag as $k => $tag_id) {
            $tag_list[$k]['id'] = $tag_id;
            $tag_list[$k]['name'] = app_Tags::GetPerformerTagById($tag_id);
        }
        $Layout->assign('tag', json_encode($tag_list));
        $user_fields['tag'] = json_encode($tag_list);

        $Layout->assign('profile_id', $this->profile_id);

        //Location
        /*    invited_to_fantasize
          contract_updated
          received_keys
          new_follower
          fantasy_completed
          received_new_comment_performance
          you_rated
          added_favorites_video
          posted_fantasy */
        $location = app_Profile::getFieldValues($this->profile_id, array('country_id',
                    'zip',
                    'state_id',
                    'city_id',
                    'custom_location'
                ));
        $Layout->assign('location', $location);

        $select_location = app_Location::Countries();
        $Layout->assign('select_location', $select_location);

        foreach ($user_fields as $k => $field) {
            $Layout->assign($k, $field);
        }


        return parent::render($Layout);
    }

}
