<?php
class component_ProfileFantasy extends SK_Component
{
       public function __construct($param) {
        parent::__construct('profile_fantasy');
        $this->param = $param;
    }

    public function render(SK_Layout $Layout) {
        return parent::render($Layout);
    }

    /**
     * @see SK_Component::prepare()
     *
     * @param SK_Layout $Layout
     * @param SK_Frontend $Frontend
     */
    function prepare(SK_Layout $Layout, SK_Frontend $Frontend) {

        $Frontend->include_js_file(URL_STATIC . 'jquery.tokeninput.js');

        $Frontend->include_js_file(URL_STATIC . 'listfantasy.js');

        /* tag one search */
        $param = array();
        $tag = SK_HttpRequest::$GET['tag'];
        /* tag array search */
        $tag_list = SK_HttpRequest::$GET['search'];
        /*cat id*/
        $cat_id = SK_HttpRequest::$GET['cat_id'];
        
        if (strlen($tag) > 1) {
            $param['tag'] = trim($tag);
        } else if (strlen($tag_list) > 0) {
             $param['tag_list'] = trim($tag_list);
        }
        else if ($cat_id !=0){
            $param['cat_id'] = intval($cat_id);
        }


        $list = app_Fantasy::GetAllList(SK_HttpRequest::$GET['page'], null, $all, $param);
        $total = app_Fantasy::GetTotal($all, $param);

        foreach ($list as $k => $v) {
            $list[$k]['description'] = $list[$k]['description']; //substr($list[$k]['description'], 0, 150);
            $list[$k]['performer'] = app_Fantasy::GetTagPerformer($v['id_item']);
            $list[$k]['fantasy_tag'] = app_Fantasy::GetTagFantasy($v['id_item']);
            $list[$k]['fantasy_file'] = app_Fantasy::GetFantasyFile($v['id_item']);
            $list[$k]['my_fantasy'] = app_FantasyWorck::CheckMyFantasy($v['id_item']);
            $list[$k]['send'] = app_FantasyWorck::CheckPerformer($v['id_item']);
            $list[$k]['status'] = app_FantasyWorck::GetFantasyStatus($v['id_item']);
        }

        $Layout->assign('list_fantasy', $list);
        $Layout->assign('user', SK_HttpUser::profile_id());
        $Layout->assign('total', $total);
        $Layout->assign('paging', array(
            'total' => $total,
            'on_page' => SK_Config::Section('video')->Section('other_settings')->display_media_list_limit,
            'pages' => SK_Config::Section('site')->Section('additional')->Section('profile_list')->nav_per_page,
        ));
        
        
        $catArray = app_VideoList::getVideoCategories(true);
        $arr = array();
        foreach ($catArray as $k=>$item){
            $arr[$k]['id'] = $item;
            $arr[$k]['label'] = SK_Language::text('%video_categories.cat_'.$item);
        }
         $Layout->assign('catArray', $arr);
        return parent::render($Layout);
    }

}
