<?php

class component_NewsFeedListKuinkey extends SK_Component
{
	/**
	 * @var int
	 */
	private $params = array();

	public function __construct( array $params = null )
	{
                                $this->params = $params;

		parent::__construct('kuinkey_news_feed');
	}

	/**
	 * @see SK_Component::prepare()
	 *
	 * @param SK_Layout $Layout
	 * @param SK_Frontend $Frontend
	 */
	public function prepare( SK_Layout $Layout, SK_Frontend $Frontend )
	{

		return parent::prepare( $Layout, $Frontend );
	}

	/**
	 * @see SK_Component::render()
	 *
	 * @param SK_Layout $Layout
	 * @return unknown
	 */
	public function render( SK_Layout $Layout )
	{
                                $Layout->assign('tabs', 55);
		return parent::render($Layout);
	}



}

