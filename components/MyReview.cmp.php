<?php

/**
 * Created by Zend Studio for Eclipse
 * Project: Skadate 7
 * User: SD
 * Date: Jan 08, 2008
 * 
 */
class component_MyReview extends SK_Component {

    /**
     *
     * @var app_CommentService 
     */
    private $comment_service = array();
    private $comments_array;
    //private $features = array(FEATURE_BLOG, FEATURE_CLASSIFIEDS, FEATURE_EVENT, FEATURE_GROUP, FEATURE_MUSIC, FEATURE_PHOTO, FEATURE_PROFILE, FEATURE_VIDEO);
    private $features = array();
    private $mode;
    private $params;

    /**
     * Class constructor
     *
     */
    public function __construct($params = array()) {

        $this->params = $params;

        parent::__construct('view_review');

        if (app_Features::isAvailable(25)) {
            $this->features[] = FEATURE_PROFILE;
        }

        if (app_Features::isAvailable(4) && app_Features::isAvailable(57)) {
            $this->features[] = FEATURE_VIDEO;
        }

        $mode = @SK_HttpRequest::$GET['mode'];
        $this->mode = $mode ? $mode : 'all';

        if (!in_array($this->mode, $this->features) && $this->mode != 'all' && $this->mode != 'my') {
            SK_HttpRequest::showFalsePage();
        }

        if(isset($params['type']) && $params['type'] == 'profile'){
            $this->tpl_file = "profile_review.tpl";
        }

        ///  $this->tpl_file = "profile_review.tpl";
    }

    /**
     * @see SK_Component::render()
     *
     * @param SK_Layout $Layout
     */
    public function render(SK_Layout $Layout) {


        return parent::render($Layout);
    }

    /**
     * @see SK_Component::prepare()
     *
     * @param SK_Layout $Layout
     * @param SK_Frontend $Frontend
     */
    public function prepare(SK_Layout $Layout, SK_Frontend $Frontend) {
        /*
          $handler = new SK_ComponentFrontendHandler('CommentsOverview');
          $this->frontend_handler = $handler;

          $js_comm_array = array('items' => array());
          $profileIdList = array();

          foreach ($this->comments_array as $value) {
          $profileIdList[] = $value['author_id'];
          if ($value['delete']) {
          $js_comm_array['items'][] = array('feature' => $value['feature'], 'id' => $value['id'], 'entity_id' => $value['entity_id'], 'delete_link_id' => $value['delete_link_id']);
          }
          }

          app_Profile::getUsernamesForUsers($profileIdList);
          app_ProfilePhoto::getThumbUrlList($profileIdList);
          app_Profile::getOnlineStatusForUsers($profileIdList);

          $handler->construct($js_comm_array, $this->mode);
         * 
         */
        $this->params['profile_id'] = isset($this->params['profile_id'])?$this->params['profile_id']:  SK_HttpUser::profile_id();
        $list = app_ProfileReviews::GetReviews(SK_HttpRequest::$GET['page'], null,  $this->mode, $this->params['profile_id']);
        $lang_sec = SK_Language::section('profile_fields')->section('value');
        foreach ($list as $k => $v) {
               
            //получение информации о фантазии к оторой отзыв
               $video  = app_FantasyVideo::getProfileVideoByFantasyId($v['fantasy_id']);
            
            $list[$k]['video_thumb']= app_ProfileVideo::getVideoThumbnailSlider($video);
            $list[$k]['video_url']= app_ProfileVideo::getVideoURL($video['hash'], $video['extension']);
            $list[$k]['video_info'] = $video;
            $list[$k]['video_rate'] = app_FantasyVideo::Rate($video['video_id'], $list[$k]['profile_id']);
               
            ////////////////////////////////

            $list[$k]['old'] = round((time() - strtotime($list[$k]['birthdate'])) / (360 * 24 * 60 * 60));

            $count = app_ProfileList::GetCountStat($list[$k]['profile_id']);

            $list[$k]['video'] = $count['video'];

            $list[$k]['follow'] = $count['follow'];

            $profileIdList[] = $list[$k]['profile_id'];

            try {
                $list[$k]['sex_label'] = $lang_sec->text('sex_' . $list[$k]['sex']);
            } catch (SK_LanguageException $e) {
                $list[$k]['sex_label'] = "-";
            }
        }

        $profileFieldData = app_Profile::getFieldValuesForUsers($profileIdList, array('country', 'state', 'city', 'zip', 'custom_location'));
        foreach ($list as $k => $v) {
            $list[$k]['location'] = $profileFieldData[$list[$k]['profile_id']];

            if (!empty($list[$k]['location']['custom_location'])) {
                $list[$k]['location']['custom_location'] = trim(htmlspecialchars($list[$k]['location']['custom_location']));
            }
        }

        $Layout->assign('comments', $list);
        $Layout->assign('total', app_ProfileReviews::GetTotal());
        $Layout->assign('tabs', 44);
        $Layout->assign('mode',  $this->mode);
    }

}