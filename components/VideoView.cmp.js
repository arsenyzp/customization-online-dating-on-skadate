function component_VideoView(auto_id)
{
    this.DOMConstruct('VideoView', auto_id);
	
    var handler = this;
}

component_VideoView.prototype =
    new SK_ComponentHandler({
		
        construct: function(hash, profile_id) {
            var handler = this;
						
            this.$("#password_unlock form").submit(function(){
                handler.ajaxCall("ajaxUnlock", {
                    hash: hash, 
                    profile_id: profile_id, 
                    password: $(this.password).val()
                    });
                return false;
            });
        },
		
        refresh: function(){
            window.location.reload();
        }
    });


function ApproveVideoWorck(fantasy_id){
    
    var data = {
        action: 'approve', 
        fantasy_id: fantasy_id
    };
    $.ajax({
        url: 'view_works.php',
        data: data,
        type: 'POST',
        complete: function(data){
            data = JSON.parse(data.responseText);
            if(data.approve){
                SK_drawMessage('Successfully approved');
                //запрос на отзыв
                 var report_fl_box = new SK_FloatBox({
                    $title: 'Add review',
                    $contents: $('#invete').children(),
                    width: 350 
                    });
                
              //  setTimeout('document.location.reload();', 1000);
            }
            else  SK_drawError('Error Approve');
        }
    });
    
}

function BuyVideo(video_id){
    var data = {
        action: 'buy_video', 
        video_id: video_id
    };
    $.ajax({
        url: 'video_view.php',
        data: data,
        type: 'POST',
        complete: function(data){
            data = JSON.parse(data.responseText);
            if(data.buy){
                SK_drawMessage('Successfully Buy Video');
                setTimeout('document.location.reload();', 1000);
            }
            else  SK_drawError('You need to register');
        }
    });
}