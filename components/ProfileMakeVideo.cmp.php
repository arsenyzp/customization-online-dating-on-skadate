<?php

class component_ProfileMakeVideo extends SK_Component {

    public function __construct(array $params = null) {
        parent::__construct('make_video');
        $this->profile = $params['profile_id'];
    }

    public function render(SK_Layout $Layout) {

        $profile_video = app_FantasyVideo::getProfileWorc($this->profile);

        $Layout->assign('VideoTagEdit', new component_TagEdit(array('entity_id' => 1, 'feature' => 'video')));

        $Layout->assign('video', $profile_video['list']);
        $Layout->assign('video_count', $profile_video['total']);

        return parent::render($Layout);
    }

}