<?php

class component_AllUs extends component_ProfileList 
{
	
	public function __construct()
	{
		parent::__construct('new_members_list');
	}
	
	protected function profiles()
	{
                                if(isset($_GET['search']) || isset($_GET['tag'])){
                                    return app_ProfileList::AllMembersList(false, $_GET);
                                }
		return app_ProfileList::AllMembersList();
	}
	
	protected function is_permitted()
	{
		return true;
	}
	
}