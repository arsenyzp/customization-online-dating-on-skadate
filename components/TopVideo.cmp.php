<?php

class component_TopVideo extends SK_Component {

    protected $params;
    protected $profile_id;

    public function __construct(array $params = null) {
        parent::__construct('top_video');
        $this->params = $params;
        $this->profile_id = isset($params['profile_id']) ? $params['profile_id'] : null;
        $this->catigories = isset($params['catigories']) ? $params['catigories'] : null;
    }

    public function prepare(SK_Layout $Layout, SK_Frontend $Frontend) {

        //$is_authenticated = SK_HttpUser::is_authenticated();
        $video_a = app_ProfileVideo::GetTopVideoId($this->params['catigories'], $this->params['profile_id']);
        $video_id = $video_a['video_id'];
        $list_tag = app_Tags::GetVideoTagById($video_id);
        $video_views = app_ProfileVideo::GetViews($video_id);
        $video_rate = app_ProfileVideo::GetVideoRateCount($video_id);
        $video_comments_count = app_ProfileVideo::GetVideoCommentsCount($video_id);
        $video_like = app_Like::GetTotalLike($video_id);
        $video_hash = app_ProfileVideo::getVideoHash($video_id);
        $video_owner = app_ProfileVideo::getVideoOwner($video_hash);
        $video_info = app_ProfileVideo::getVideoInfo($video_owner, $video_hash);
        $video_info['video_url'] = app_ProfileVideo::getVideoURL($video_info['hash'], $video_info['extension']);
        $Layout->assign('video_id', $video_id);
        $Layout->assign('list_tag', $list_tag);
        $Layout->assign('video_views', $video_views);
        $Layout->assign('video_rate', $video_rate);
        $Layout->assign('video_comments_count', $video_comments_count);
        $Layout->assign('video_like', $video_like);
        $Layout->assign('video_info', $video_info);

        $Layout->assign('VideoRate', new component_Rate(array('entity_id' => $video_id, 'feature' => 'video')));
    }

}