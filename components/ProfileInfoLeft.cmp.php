<?php

class component_ProfileInfoLeft extends SK_Component {

    public function __construct(array $params = null) {
        parent::__construct('profile_info_left');
    }

    public function prepare(SK_Layout $Layout, SK_Frontend $Frontend) {
        $profile_id = SK_HttpUser::profile_id();
        if (!$profile_id) {
            return false;
        }

        $handler = new SK_ComponentFrontendHandler('MemberConsole');
        $handler->construct();
        $this->frontend_handler = $handler;

        return parent::prepare($Layout, $Frontend);
    }

    public function render(SK_Layout $Layout) {
        $profile_id = SK_HttpUser::profile_id();
        $lang_section = SK_Language::section('profile_fields')->section('value');
        $profile = array(
            'url' => SK_Navigation::href("view_profile", array('profile_id' => $profile_id)),
            'id' => $profile_id,
            'username' => app_Profile::username(),
            'hasThumb' => app_ProfilePhoto::hasThumbnail($profile_id),
            'photo_upload_url' => SK_Navigation::href("profile_photo_upload"),
            'mailbox_url' => SK_Navigation::href("mailbox"),
        );


        $Layout->assign('console_menu_opened', app_ProfilePreferences::get('hidden', 'member_console_menu_state'));
        $Layout->assign("profile", $profile);
        $profile_info = app_Profile::getFieldValues($profile_id);
        $Layout->assign('age', round((time() - strtotime($profile_info['birtdate'])) / (360 * 24 * 60 * 60)));
        $count = app_ProfileList::GetCountStat($profile_id);
        $Layout->assign('video', $count['video']);
        $Layout->assign('follow', $count['follow']);

        try {
            $profile_info['sex_label'] = $lang_section->text('sex_' . $profile_info['sex']);
        } catch (SK_LanguageException $e) {
            $profile_info['sex_label'] = "-";
        }
        $Layout->assign('profile_info', $profile_info);

        $profileIdList = array('id_creator'=>$profile_id);
        $profileFieldData = app_Profile::getFieldValuesForUsers($profileIdList, array('country', 'state', 'city', 'zip', 'custom_location'));
        $profile_info['location'] = $profileFieldData[$profile_id];

        if (!empty($profile_info['location']['custom_location'])) {
             $profile_info['location']['custom_location'] = trim(htmlspecialchars( $profile_info['location']['custom_location']));
        }
        
        $Layout->assign('profile_info', $profile_info);
        $Layout->assign('views', app_ProfileViewHistory::getViewerProfilesCount($profile_id,0, time()));
        $Layout->assign('total_rate', new component_RateTotal(array('entity_id' => $profile_id, 'feature' =>'profile')));
        return parent::render($Layout);
    }

    public static function ajax_DeleteThumb($params = null, SK_ComponentFrontendHandler $handler) {
        try {
            app_ProfilePhoto::deleteThumbnail();
        } catch (SK_ProfilePhotoException $e) {
            $handler->message(SK_Language::section("components.upload_photo.upload.message")->text("make_thumb_error"));
            $handler->changeThumb();
        }

        $handler->message(SK_Language::section("components.upload_photo.upload.message")->text("thumb_delete_success"));
        $handler->changeThumb(app_ProfilePhoto::getThumbUrl(SK_HttpUser::profile_id()));
        $handler->hideThumbDeleteBtn();
    }

    public static function ajax_saveMenuState($params = null, SK_ComponentFrontendHandler $handler) {
        try {
            app_ProfilePreferences::set("hidden", "member_console_menu_state", $params->opened);
        } catch (SK_ProfilePreferencesException $e) {
            
        }
    }

}
