<?php

class component_ThisIsMeSlider extends SK_Component {

       protected $params;
       protected $profile_id;

       public function __construct(array $params = null) {
              parent::__construct('this_is_me_slider');
              $this->params = $params;
              $this->profile_id = isset($params['profile_id']) ? $params['profile_id'] : null;
              $this->iter = isset($params['iter']) ? $params['iter'] : 0;
       }

       public function prepare(SK_Layout $Layout, SK_Frontend $Frontend) {
              $Frontend->include_js_file(URL_STATIC . 'this_is_me_slider.js');
              //$is_authenticated = SK_HttpUser::is_authenticated();
              $list = app_ThisMe::getMediaList($this->profile_id);

              $total = count($list);
              $pag = array();
              for ($i = (($this->iter > 8) ? ($this->iter - 6) : 2); $i < (($this->iter > 8) ? ($this->iter + 3) : 11); $i++) {
                     if ($i > $total)
                            continue;
                     $pag[$i] ['num'] = $i;
              }
              $Layout->assign('total', $pag);
              $Layout->assign('tot', $total);
              $Layout->assign('active', ($this->iter + 1));

              $Layout->assign('profile_id', $this->profile_id);
              $it = 0;
              foreach ($list as $k => $v) {
                     if ($it != $this->iter) {
                            $it++;
                            continue;
                     }
                     if (isset($v['photo_id'])) {
                            $list[$k]['photo_url'] = app_ProfilePhoto::getUrl($v['photo_id']);
                            $Layout->assign('photo_id', $v['photo_id']);
                            $Layout->assign('video_info', $list[$k]);
                     }
                     if (isset($v['video_id'])) {
                            $list[$k]['video_url'] = app_ProfileVideo::getVideoURL($v['hash'], $v['extension']);
                            $list[$k]['thumb_img'] = app_ProfileVideo::getVideoFrameURL($v['hash']);
                            //$list[$k]['video_page'] = app_ProfileVideo::getVideoViewURL($v['hash']);
                            $Layout->assign('video_id', $v['video_id']);
                            $Layout->assign('video_thumb', $list[$k]['thumb_img']);
                            $Layout->assign('video_info', $list[$k]);
                     }
                     $it++;
              }


              /*
                $video_a = app_ThisMe::GetVideoId($this->params['iter'], $this->params['profile_id']);
                $video_id = $video_a['video_id'];
                $video_hash = app_ProfileVideo::getVideoHash($video_id);
                $video_owner = app_ProfileVideo::getVideoOwner($video_hash);
                $video_info = app_ProfileVideo::getVideoInfo($video_owner, $video_hash);
                $video_info['video_url'] = app_ProfileVideo::getVideoURL($video_info['hash'], $video_info['extension']);
                $video_thumb = app_ProfileVideo::getVideoFrameURL($video_hash);

                $Layout->assign('video_id', $video_id);
                $Layout->assign('video_thumb', $video_thumb);
                $Layout->assign('video_info', $video_info);
               */
       }

}