<?php

class component_OfferDetails extends SK_Component {

       protected $params;

       public function __construct($params = array()) {
              $this->params = $params;
              if (isset($params['ajax'])) {
                     parent::__construct('offer_details_ajax');
              }
              else
                     parent::__construct('offer_details');
       }

       public function render(SK_Layout $Layout) {
              return parent::render($Layout);
       }

       /**
        * @see SK_Component::prepare()
        *
        * @param SK_Layout $Layout
        * @param SK_Frontend $Frontend
        */
       function prepare(SK_Layout $Layout, SK_Frontend $Frontend) {


              $Frontend->include_js_file(URL_STATIC . 'jquery-ui.js');
              $Frontend->include_js_file(URL_STATIC . 'offer_details.js');
              //$worck_id = intval(SK_HttpRequest::$GET['worck_id']);
              //$fantasy_id = intval(SK_HttpRequest::$GET['fantasy_id']);

              $worck_id = intval($_GET['worck_id']);
              $fantasy_id = intval($_GET['fantasy_id']);
              /*               * ********************************************************************* */
              if ($worck_id == 0) {//Создание нового контракта
                     $_SESSION['offerts_details'] = array('fantasy_id' => $fantasy_id, 'worck_id' => $worck_id, 'type' => 'new');
                     $this->tpl_file = "create_contract.tpl";
                     $fantasy = app_Fantasy::GetFantasy($fantasy_id);
                     $fantasy['data_ex'] = date("m/d/Y", $fantasy['data_ex']);
                     //информация о заказчике
                     $creator_info = app_FantasyWorck::GetCreatorInfo($fantasy_id);
                     $Layout->assign('creator_info', $creator_info[0]);
              } elseif ($fantasy_id == 0) {//обсуждение текущего контракта
                     $fantasy_id = app_FantasyWorck::GetFantasyID($worck_id);
                     if (!$fantasy_id)//если нет такого контракта
                            SK_HttpRequest::showFalsePage();
                     $_SESSION['offerts_details'] = array('fantasy_id' => $fantasy_id, 'worck_id' => $worck_id, 'type' => 'edit');
                     $Layout->assign('worck_messag', app_OfferDetails::GetMessagContract($worck_id));
                     $worck_info = app_FantasyWorck::GetWorck($worck_id);
                     $Layout->assign('lenght_video_per', $worck_info['lenght_video']);
                     $fantasy = app_Fantasy::GetFantasy($fantasy_id);
                     //преобразование даты
                     $worck_info['completion_date'] = date("m/d/Y", $worck_info['completion_date']);
                     $fantasy['data_ex'] = date("m/d/Y", $fantasy['data_ex']);
                     $Layout->assign('worck_info', $worck_info);
                     $Layout->assign('completion_date', $worck_info['completion_date']);
                     //если уже была отклонена заявка
                     if ($worck_info['status'] < 0)
                            SK_HttpRequest::showFalsePage();
                     //если уже принята или исполняеться запретить редактирование
                     if ($worck_info['status'] > 0)
                            $Layout->assign('form_enable', false);
                     else
                            $Layout->assign('form_enable', true);

                     $Layout->assign('performer_budget', $worck_info['budget']);

                     $Layout->assign('performer_price_video', $worck_info['price_video']);

                     //если уже пинята или исполняеться запретить редактирование
                     if ($worck_info['status'] > 0)
                            $form_enable = false;
                     else
                            $form_enable = true;

                     if (app_FantasyWorck::CheckMyFantasy($fantasy_id)) {//если просматривате зазчик
                            // инфо о исполнителе
                            $performer_info = app_FantasyWorck::GetWorck($worck_id);
                            $performer_name = app_Profile::username($performer_info['id_performer']);
                            $Layout->assign('performer_id', $performer_info['id_performer']);
                            $Layout->assign('performer_name', $performer_name);
                            //если уже была отклонена заявка
                            if ($worck_info['status'] < 0)
                                   SK_HttpRequest::showFalsePage();

                            //offer info
                            $Layout->assign('my_fantasy_list', app_Fantasy::GetListMyFantasy());

                            //если цена не была установлена ставим ту которую предложил исполнитель
                            //$fantasy['data_ex'] = date("m/d/Y", $fantasy['data_ex']);
                            $fantasy['budget'] = ($fantasy['budget'] == 0) ? $worck_info['budget'] : $fantasy['budget'];
                            //запрет повторного редактирования
                            if ($worck_info['status_update'] == 1)
                                   $form_enable = false;
                     }
                     else {//если просматривате исполнитель
                            //информация о заказчике $creator_info = app_FantasyWorck::GetCreatorInfo($fantasy_id);
                            $Layout->assign('creator_info', $creator_info[0]);
                            $this->tpl_file = "offert_performer.tpl";
                            //запрет повторного редактирования
                            if ($worck_info['status_update'] == 2 || $worck_info['status_update'] == 0)
                                   $form_enable = false;
                     }


                     $Layout->assign('form_enable', $form_enable);
              } else {
                     SK_HttpRequest::showFalsePage();
              }


              foreach ($fantasy as $k => $v) {
                     $Layout->assign($k, $v);
              }
              //fantasy info


              $Layout->assign('fantasy_id', $fantasy_id);
              $Layout->assign('worck_id', $worck_id);
              $Layout->assign('performers', app_Fantasy::GetTagPerformer($fantasy_id));
              $Layout->assign('fantasy_tag', app_Fantasy::GetTagFantasy($fantasy_id));
              $Layout->assign('user', SK_HttpUser::profile_id());
              $Layout->assign('my_fantasy', app_FantasyWorck::CheckMyFantasy($fantasy_id));
              $Layout->assign('send', app_FantasyWorck::CheckPerformer($fantasy_id));

              $files = app_Fantasy::GetFantasyFile($fantasy_id);
              $Layout->assign('files', $files);

              //calculate budget
              $Layout->assign('kuinkey_fee', $fantasy['budget'] * 0.1);
              $Layout->assign('paid_to_performer', $fantasy['budget'] - $fantasy['budget'] * 0.1);

              return parent::render($Layout);
       }

}