<?php

class component_ProfileInfoHeader extends SK_Component
{
    
    private $profile_id;

    public function __construct( array $params = null )
	{
		parent::__construct('profile_info_header');	
                
                 $this->profile_id = empty($params['profile_id']) ? SK_HttpUser::profile_id() : (int) $params['profile_id'];
        
                if (!$this->profile_id)
                {
                    $this->annul();
                }

	}

	public function prepare(SK_Layout $Layout, SK_Frontend $Frontend) {
        
            $handler = new SK_ComponentFrontendHandler('ProfileInfoHeader');
            $this->frontend_handler = $handler;
            $Layout->assign('us_status', app_Profile::getUserStatus($this->profile_id));
            		
	}
}
