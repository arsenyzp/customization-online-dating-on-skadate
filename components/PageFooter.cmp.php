<?php

class component_PageFooter extends SK_Component {

    public function __construct() {
        parent::__construct('page_footer');
    }

    public function render(SK_Layout $Layout) {
        $bottom = SK_Navigation::menu('bottom');

        $Layout->assign('bottom_menu', $bottom);

        $Layout->assign('site_name', SK_Config::section("site")->Section("official")->site_name);

        $Layout->assign('year', date("Y"));

        $is_authenticated = SK_HttpUser::is_authenticated();
        $Layout->assign('is_authenticated', $is_authenticated);
        $Layout->assign('index', $this->page);

        return parent::render($Layout);
    }
    
     function prepare(SK_Layout $Layout, SK_Frontend $Frontend) {

        $Frontend->include_js_file(URL_STATIC . 'newfantasy.js');
        $Frontend->include_js_file(URL_STATIC . 'select_plugin.js');
        $Frontend->include_js_file(URL_STATIC . 'jquery.tokeninput.js');
        $Frontend->include_js_file(URL_STATIC . 'jquery-ui.js');

        return parent::render($Layout);
    }

}
