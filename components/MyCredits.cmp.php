<?php

class component_MyCredits extends SK_Component {

       public function __construct(array $params = null) {
              if (!app_Features::isAvailable(44)) {
                     $this->annul();
                     return;
              }

              parent::__construct('my_credits');
       }

       public function render(SK_Layout $Layout) {
              $page = isset(SK_HttpRequest::$GET['page']) ? (int) SK_HttpRequest::$GET['page'] : 1;

              $profile_id = SK_HttpUser::profile_id();
              $Layout->assign('profile_id', SK_HttpUser::profile_id());
              $tabs = intval(SK_HttpRequest::$GET['tabs']);
              $Layout->assign('tabs', $tabs);
                     if($tabs == 0){
                            $log = app_UserPoints::getActionHistory($profile_id, $page);
                            foreach ($log['list'] as $k=>$v){
                                   if($v['opponent_id']>0){
                                          $username =  app_Profile::getFieldValues($v['opponent_id'], array('username'));
                                          $log['list'][$k]['username'] =$username['username'];
                                   }
                            }
                            $Layout->assign('log', $log['list']);
                            
                            $Layout->assign('paging', array(
                                'total' => $log['total'],
                                'on_page' => 20,
                                'pages' => 5,
                            ));
                     }
                     else if($tabs == 1){
                            $Layout->assign('credits_balance', app_UserPoints::getProfilePointsBalance($profile_id));
                     }
              
                 $Layout->assign('total_keys', app_KuinKeys::GetCountKeys($profile_id));    
              return parent::render($Layout);
       }

}
