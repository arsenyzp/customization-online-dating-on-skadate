<?php

class component_ProfileNotices extends SK_Component {

    public function __construct(array $params = null) {
        parent::__construct('profile_notices');
    }

    public function render(SK_Layout $Layout) {
        $profile_id = SK_HttpUser::profile_id();

        $friend_requests = app_FriendNetwork::countGotRequests($profile_id);
        $new_messages = app_MailBox::newMessages($profile_id);
        $request_count = app_FantasyWorck::CountRequest();
        $update_contract = app_FantasyWorck::UpdateContract();
        $update_request = app_FantasyWorck::UpdateRequest();
        $new_invate = app_FantasyWorck::NewInvate();
        $new_contract = app_FantasyWorck::NewContract();
        $fantasy_count = app_FantasyWorck::NewFantasy();
        if (app_Features::isAvailable(38)) {
            $group_invitations = app_Groups::getInvitations($profile_id);
            if ($group_invitations['count'] > 0) {
                $Layout->assign('invites_count', $group_invitations['count']);
                $Layout->assign('invites', $group_invitations['list']);
            }
        }

        if (!$friend_requests && !$new_messages && !$group_invitations['count'] && !$request_count && !$update_contract && !$update_request && !$new_invate && !$new_contract && !$fantasy_count) {
            return false;
        }

        $Layout->assign('friend_requests', $friend_requests);
        $Layout->assign('new_contract', $new_contract);
        $Layout->assign('new_messages', $new_messages);
        //performer request

        $Layout->assign('request_count', $request_count);
        $Layout->assign('update_request', $update_request);
        $Layout->assign('update_contract', $update_contract);
        $Layout->assign('new_invate', $new_invate);
       
        $Layout->assign('fantasy_count', $fantasy_count);


        return parent::render($Layout);
    }

}
