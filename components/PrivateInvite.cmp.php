<?php

class component_PrivateInvite extends SK_Component{
    
     public function __construct( array $params = null) {
        parent::__construct('invite_fantasy');
        
        $this->reporter_id = $params['reporter_id'];
        $this->entity_id = $params['entity_id'];
        $this->type = $params['type'];

        if ($this->reporter_id == $this->entity_id && $this->type == 'profile')
                $this->annul();
         if (!SK_HttpUser::is_authenticated())
                $this->annul();
        
        
    }
    
    /**
     * @see SK_Component::prepare()
     *
     * @param SK_Layout $Layout
     * @param SK_Frontend $Frontend
     */
   public function prepare( SK_Layout $Layout, SK_Frontend $Frontend )
	{
                                    $Frontend->include_js_file(URL_STATIC . 'jquery-ui.js');
		$handler = new SK_ComponentFrontendHandler('PrivateInvite');
		$this->frontend_handler = $handler;
		$report_title = "Select fantasy";
                                    $handler->construct($report_title);
                                    
                                    $Layout->assign('my_fantasy_list', app_Fantasy::GetListMyFantasy());
                                    
                                    $Layout->assign(array(
			'reporter_id'	=>	$this->reporter_id,
			'entity_id'		=>	$this->entity_id,
			'type'			=>	$this->type,
			'show_link'		=>  $this->show_link
		));
	}
	
	
	public function render( SK_Layout $Layout )
	{
		$Layout->assign(array(
			'reporter_id'	=>	$this->reporter_id,
			'entity_id'		=>	$this->entity_id,
			'type'			=>	$this->type,
			'show_link'		=>  $this->show_link
		));
		
		return parent::render($Layout);
	}
	
	public function handleForm( SK_Form $form )
	{
		$form->getField('reporter_id')->setValue($this->reporter_id);;
		$form->getField('entity_id')->setValue($this->entity_id);
		$form->getField('type')->setValue($this->type);
		
		//$form->frontend_handler->bind('success', 'function(){
		//	window.report_fl_box.close();
		//}');
	}
}