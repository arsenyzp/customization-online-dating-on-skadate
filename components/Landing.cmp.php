<?php

class component_Landing extends SK_Component {

    private $list_type;

    /**
     * Component VideoList constructor.
     *
     * @return component_VideoList
     */
    public function __construct(array $params = null) {
        parent::__construct('landing');

        $available_types = array('latest', 'toprated', 'most_viewed', 'discussed', 'tags', 'profile', 'categories', 'preferences');

        if (isset(SK_HttpRequest::$GET['profile_id'])) {
            $this->list_type = 'profile';
        } elseif (!isset($params['list_type']) || !in_array($params['list_type'], $available_types))
            $this->list_type = 'latest';
        else
            $this->list_type = $params['list_type'];
    }

    public function render(SK_Layout $Layout) {

        $tabs = intval(SK_HttpRequest::$GET['tabs']);

        $tag_limit = 50;

        if ($tabs == 1)
            $this->list_type = 'lenght';
        if ($tabs == 2)
            $this->list_type = 'toprated';
        if ($tabs == 3)
            $this->list_type = 'discussed';
        if ($tabs == 4)
            $this->list_type = 'like';
        if ($tabs == 5)
            $this->list_type = 'rand';


        if (SK_HttpRequest::$GET['tag']) {
            $video = app_VideoList::getVideoList('latest', SK_HttpRequest::$GET['page'], null, null,SK_HttpRequest::$GET['tag']);
        }
        else
            switch ($this->list_type) {
                case 'categories':
                    $cat_id = (int) SK_HttpRequest::$GET['cat_id'];
                    $video = app_VideoList::getVideoList('categories', SK_HttpRequest::$GET['page'], null, $cat_id);

                    $category = app_VideoList::getVideoCategoryById($cat_id);
                    if ($category) {
                        SK_Language::defineGlobal('category', SK_Language::text('%video_categories.cat_' . $category['category_id']));
                    } else {
                        SK_Language::defineGlobal('category', SK_Language::text('%components.video_categories.categories'));
                    }
                    break;

                case 'latest':
                    $video = app_VideoList::getVideoList('latest', SK_HttpRequest::$GET['page']);
                    break;
                case 'toprated':
                    $video = app_VideoList::getVideoListPreference('toprated', SK_HttpRequest::$GET['page']);
                    break;
                case 'preferences':
                    $video = app_VideoList::getVideoListPreference('preferences', SK_HttpRequest::$GET['page']);
                    break;

                case 'discussed':
                    $video = app_VideoList::getVideoList('discussed', SK_HttpRequest::$GET['page']);
                    break;

                case 'lenght':
                    $video = app_VideoList::getVideoListPreference('lenght', SK_HttpRequest::$GET['page']);
                    break;
                case 'like':
                    $video = app_VideoList::getVideoListPreference('like', SK_HttpRequest::$GET['page']);
                    break;
                case 'rand':
                    $video = app_VideoList::getVideoList('rand', SK_HttpRequest::$GET['page']);
                    break;
            }

        $Layout->assign('tabs', $tabs);
        $Layout->assign('list_type', $this->list_type);
        $Layout->assign('enable_categories', SK_Config::section('video')->Section('other_settings')->get('enable_categories'));
        $Layout->assign('total_video', app_VideoList::GetTotalVideo());
        SK_Language::defineGlobal('username', $username);

        if (isset($video)) {
            $Layout->assign('list', $video['list']);
            $Layout->assign('total', $video['total']);

            $Layout->assign('paging', array(
                'total' => $video['total'],
                'on_page' => SK_Config::Section('video')->Section('other_settings')->display_media_list_limit,
                'pages' => SK_Config::Section('site')->Section('additional')->Section('profile_list')->nav_per_page,
            ));
        }

        return parent::render($Layout);
    }

    function prepare(SK_Layout $Layout, SK_Frontend $Frontend) {
        $Frontend->include_js_file(URL_STATIC . 'jquery.tokeninput.js');
        return parent::render($Layout);
    }

}

