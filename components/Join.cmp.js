function component_Join()
{
	this.DOMConstruct('Join');
	
	var handler = this;
	
	this.references = [];
	
	this.delegates = {
		
	};
}

component_Join.prototype =
	new SK_ComponentHandler({
       
});

$(document).ready(function () {
    $('input:regex(id, ^form_\\d{1}-i_agree_with_tos)').attr('disabled', 'disabled');
    
    $("input:regex(id, ^form_\\d{1}-tag)").tokenInput(document.location.href, {
        theme: "facebook",
        queryParam: 'q_p'
    });
});

function popTerms(){
    var text_terms = $('#popTerms').text();
    SK_alert("Terms", text_terms, CheckTerms);
    $('.floatbox_body').css({
        'overflow-y' : 'scroll',
        'height' : '200px'
    });
    $(".floatbox_body").scroll( function() {
         if($(this)[0].scrollHeight - $(this).scrollTop() == $(this).outerHeight())
              $('input:regex(id, ^form_\\d{1}-i_agree_with_tos)').removeAttr('disabled');
              $('input:regex(id, ^form_\\d{1}-i_agree_with_tos)').attr('checked', 'checked');
            });
}

function CheckTerms(){
  //  $('#form_4-i_agree_with_tos').removeAttr('disabled');
   // $('#form_4-i_agree_with_tos').attr('checked', 'checked');
}
