<?php

class component_ProfileFantasiesList extends SK_Component {

       public function __construct($param) {
              parent::__construct('fantasy_list');
              $this->param = $param;
       }

       public function render(SK_Layout $Layout) {
              return parent::render($Layout);
       }

       /**
        * @see SK_Component::prepare()
        *
        * @param SK_Layout $Layout
        * @param SK_Frontend $Frontend
        */
       function prepare(SK_Layout $Layout, SK_Frontend $Frontend) {

              $Frontend->include_js_file(URL_STATIC . 'jquery.tokeninput.js');

              $Frontend->include_js_file(URL_STATIC . 'jquery-ui.js');
              $Frontend->include_js_file(URL_STATIC . 'offer_details.js');

              $Frontend->include_js_file(URL_STATIC . 'listfantasy.js');
              $Frontend->include_js_file(URL_STATIC . 'select_plugin.js');

              if (intval($tabs) < 0 OR intval($tabs) > 4)
                     $tabs = 0;
              $Layout->assign('tabs', intval($tabs));
              $this->tpl_file = "profile_fantasies.tpl";
              if (SK_HttpRequest::$GET['tag']) {
                     $tag = intval(SK_HttpRequest::$GET['tag']);
                     $param['tag'] = $tag;
              }

              $list = app_Fantasy::GetAllList(SK_HttpRequest::$GET['page'], null, false,  $this->param);
            
              //$list = app_Fantasy::GetAllList(SK_HttpRequest::$GET['page'], null, $all, $param);



              $total = app_Fantasy::GetTotal($all, $this->param);
              $lang_sec = SK_Language::section('profile_fields')->section('value');
              $profileIdList = array();
              foreach ($list as $k => $v) {
                     $list[$k]['description'] = $list[$k]['description']; //substr($list[$k]['description'], 0, 150);
                     $list[$k]['performer'] = app_Fantasy::GetTagPerformer($v['id_item']);
                     $list[$k]['fantasy_tag'] = app_Fantasy::GetTagFantasy($v['id_item']);
                     $list[$k]['fantasy_file'] = app_Fantasy::GetFantasyFile($v['id_item']);
                     $list[$k]['my_fantasy'] = app_FantasyWorck::CheckMyFantasy($v['id_item']);
                     $list[$k]['send'] = app_FantasyWorck::CheckPerformer($v['id_item']);
                     $list[$k]['status'] = app_FantasyWorck::GetFantasyStatus($v['id_item']);

                     $list[$k]['old'] = round((time() - strtotime($list[$k]['birthdate'])) / (360 * 24 * 60 * 60));

                     $count = app_ProfileList::GetCountStat($list[$k]['id_creator']);

                     $list[$k]['video'] = $count['video'];

                     $list[$k]['follow'] = $count['follow'];

                     $profileIdList[] = $list[$k]['id_creator'];

                     try {
                            $list[$k]['sex_label'] = $lang_sec->text('sex_' . $list[$k]['sex']);
                     } catch (SK_LanguageException $e) {
                            $list[$k]['sex_label'] = "-";
                     }
              }

              $profileFieldData = app_Profile::getFieldValuesForUsers($profileIdList, array('country', 'state', 'city', 'zip', 'custom_location'));
              foreach ($list as $k => $v) {
                     $list[$k]['location'] = $profileFieldData[$list[$k]['id_creator']];

                     if (!empty($list[$k]['location']['custom_location'])) {
                            $list[$k]['location']['custom_location'] = trim(htmlspecialchars($list[$k]['location']['custom_location']));
                     }
              }
              $Layout->assign('list_fantasy', $list);
              $Layout->assign('user', SK_HttpUser::profile_id());


              $Layout->assign('total', $total);

              $Layout->assign('paging', array(
                  'total' => $total,
                  'on_page' => SK_Config::Section('video')->Section('other_settings')->display_media_list_limit,
                  'pages' => SK_Config::Section('site')->Section('additional')->Section('profile_list')->nav_per_page,
              ));


              $catArray = app_VideoList::getVideoCategories(true);
              $arr = array();
              foreach ($catArray as $k => $item) {
                     $arr[$k]['id'] = $item;
                     $arr[$k]['label'] = SK_Language::text('%video_categories.cat_' . $item);
              }
              $Layout->assign('catArray', $arr);
              return parent::render($Layout);
       }

       /**
        * @see SK_Component::handleForm()
        *
        * @param SK_Form $form
        */
       public function handleForm($form) {
              
       }

}