<?php

class component_TopTag extends SK_Component
{

	public function __construct( array $params = null )
	{
		parent::__construct('top_tag');		

	}

	public function prepare(SK_Layout $Layout, SK_Frontend $Frontend) {

	    //$is_authenticated = SK_HttpUser::is_authenticated();
                        
                      $limit = 20;
                      
                      $list_tag = app_Tags::GetTopVideoTag($limit);
	    
	    $Layout->assign('list_tag', $list_tag);
		
	}
}