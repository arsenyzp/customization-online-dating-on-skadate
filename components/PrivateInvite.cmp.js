function component_PrivateInvite(auto_id)
{
    this.DOMConstruct('PrivateInvite', auto_id);
	
    var handler = this;
	
    this.delegates = {
		
    };
}

component_PrivateInvite.prototype =
    new SK_ComponentHandler({
	
        construct: function(f_title){
			
            var handler = this;
		
            this.title = f_title;
		               
            $('#button_invete').click(function() {
                var report_fl_box = new SK_FloatBox({
                    $title: handler.title, 
                    $contents: $('#invete').children(), 
                    width: 450
                });
            });
            
            var today = new Date();
            $('#datapicker2').val((today.getMonth()+1)+"/"+today.getDate()+"/"+today.getFullYear());
            $('#datapicker2').datepicker({
                format:'m/d/Y',
                date: $('#inputDate').val(),
                current: $('#inputDate').val(),
                starts: 1,
                position: 'r',
                onBeforeShow: function(){
                    $('#datapicker2').DatePickerSetDate($('#inputDate').val(), true);
                },
                onChange: function(formated, dates){
                    $('#datapicker2').val(formated);
                    $('#datapicker2').DatePickerHide();
                }
            });
            
        }
    });
    
    
    function UpdateFantasyInfo(){
        
    var id = $('#select_fantasy').val();
    var dat = {action: 'info', id_fantasy: id};
    $.ajax({
        url: 'fantasy_list.php',
        data: dat,
        type: 'POST',
        complete: function(data){
            data = JSON.parse(data.responseText);
            $('#budget_invite').val(data.budget);
            Calculate(data.budget);
            $('#video_length').text(data.price_video+' min');
        }
    });
}

function Calculate(sum){
    //var sum = $('#budget').val();
    $('#kuinkey_fee').text(sum*0.1);
    $('#paid_to_performer').text(sum - sum*0.1);
}

function NewFatasyInvite(id){
     $('.close_btn').click();
    $('#form_new_fantasy').show();
    $('body').append('<div class="this_me_background"></div>'); 
    
    $('input[name=fantasy_private]').attr('checked', 'checked');
    $('input[name=performer_id]').val(id);
}