function component_ProfileInfoHeader(auto_id)
{
	this.DOMConstruct('ProfileInfoHeader', auto_id);
	
	var handler = this;
	
	this.delegates = {
		toggle_profile_status: function() {
			handler.ajaxCall('toggleProfileInfoHeader', {curr_status: handler.profile_status});
			return false;
		}
	};
}

component_ProfileInfoHeader.prototype =
	new SK_ComponentHandler({
			
	});
        
        $(document).ready(function(){
             $("#profile_status").focusout(function(){
                 var data = $("#profile_status").val();
                 data = {'status': data};
                 $.post('/member/ajax_user_update.php', data, function(){
                     SK_drawMessage("Status update", 'message', 1000);
                 });
               });
               
            $('#up_media').click(function(){
                $('#video_upload_form').show();
                $('body').append('<div class="this_me_background"></div>');
            });
            
              $('#up_media_close').click(function(){
                   $('#video_upload_form').hide();
                  $('.this_me_background').remove();
              });
        });