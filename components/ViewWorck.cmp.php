<?php

class component_ViewWorck extends SK_Component {

       protected $profile_id;

       public function __construct(array $params = null) {
              if (isset($params['profile_id']))
                     $this->tpl_file = "profile_video.tpl";
              $this->profile_id = isset($params['profile_id']) ? $params['profile_id'] : SK_HttpUser::profile_id();
              parent::__construct('view_works');
       }

       public function render(SK_Layout $Layout) {

              $tabs = intval(SK_HttpRequest::$GET['tabs']) < 1 ? 6 : intval(SK_HttpRequest::$GET['tabs']);
              
              $tag = null;
               if (SK_HttpRequest::$GET['tag']) {
                    $tag = intval(SK_HttpRequest::$GET['tag']);
              }
              
              switch ($tabs) {
                     case 0:
                            $profile_video = app_FantasyWorck::getProfileUploadedVideo($this->profile_id);
                            break;
                     case 1:
                            $profile_video = app_ProfileVideo::getProfileUploadedVideo($this->profile_id);
                            break;
                     case 2:
                            $profile_video = app_FantasyVideo::getProfileUploadedVideo($this->profile_id);
                            break;
                     case 3:
                            $profile_video = app_FantasyVideo::getProfileFantasyVideo($this->profile_id);
                            break;
                     case 4:
                            $profile_video = app_FantasyVideo::getProfileBuyVideo($this->profile_id);
                            break;
                     case 5:
                            $profile_video = app_FantasyVideo::getProfileWorc($this->profile_id, $tag);
                            break;
                     case 6:
                            $profile_video = app_FantasyVideo::getProfileLikeVideo($this->profile_id, $tag);
                            break;
              }

              $Layout->assign('tabs', $tabs);
              $Layout->assign('profile_id', $this->profile_id );
              $Layout->assign('VideoTagEdit', new component_TagEdit(array('entity_id' => 1, 'feature' => 'video')));
              
              foreach ($profile_video['list'] as $k=>$v){
                     $tag_list = array();
                     $profile_video['list'][$k]['tag_list'] = app_Tags::GetVideoTagById($v['video_id']);
              }
              
              $Layout->assign('video', $profile_video['list']);
              $Layout->assign('video_count', $profile_video['total']);

              $Layout->assign('paging', array(
                  'total' => $profile_video['total'],
                  'on_page' => SK_Config::Section('video')->Section('other_settings')->display_media_list_limit,
                  'pages' => SK_Config::Section('site')->Section('additional')->Section('profile_list')->nav_per_page,
              ));

              return parent::render($Layout);
       }

       function prepare(SK_Layout $Layout, SK_Frontend $Frontend) {
              $Frontend->include_js_file(URL_STATIC . 'jquery.tokeninput.js');
              return parent::render($Layout);
       }

}