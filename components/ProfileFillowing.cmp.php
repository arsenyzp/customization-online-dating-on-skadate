<?php

class component_ProfileFillowing extends component_ProfileList 
{
       protected $profile_id;
       public function __construct($param)
	{
                                        $this->profile_id=intval($param['profile_id']);
		parent::__construct('profile_follow');
	}
	
	protected function profiles()
	{
		return app_Bookmark::Following($this->profile_id);
	}
	
	protected function is_permitted()
	{
		if (!app_Features::isAvailable(18)) {
			$this->no_permission_msg = SK_Language::section('membership')->text("feature_inactive");
			return false;	
		}
		return true;
	}
	
}