<?php

class component_ProfileReviews extends SK_Component {

    public function __construct() {
        parent::__construct('profile_reviews');
    }

    public function render(SK_Layout $Layout) {
        return parent::render($Layout);
    }

    /**
     * @see SK_Component::prepare()
     *
     * @param SK_Layout $Layout
     * @param SK_Frontend $Frontend
     */
    function prepare(SK_Layout $Layout, SK_Frontend $Frontend) {

        $tabs = SK_HttpRequest::$GET['tabs'];
        $Layout->assign('tabs', intval($tabs));
        
        

        $list = app_ProfileReviews::GetReviews(SK_HttpRequest::$GET['page'], null,  intval($tabs));
        $total = app_ProfileReviews::GetTotal( intval($tabs));


        $Layout->assign('list_reviews', $list);
        $Layout->assign('user', SK_HttpUser::profile_id());


        $Layout->assign('total', $total);

        $Layout->assign('paging', array(
            'total' => $total,
            'on_page' => SK_Config::Section('video')->Section('other_settings')->display_media_list_limit,
            'pages' => SK_Config::Section('site')->Section('additional')->Section('profile_list')->nav_per_page,
        ));

        return parent::render($Layout);
    }

}
