<?php

class component_Fantasy extends SK_Component {

    public function __construct() {
        parent::__construct('fantasy');
    }

    public function render(SK_Layout $Layout) {
        return parent::render($Layout);
    }

    /**
     * @see SK_Component::prepare()
     *
     * @param SK_Layout $Layout
     * @param SK_Frontend $Frontend
     */
    function prepare(SK_Layout $Layout, SK_Frontend $Frontend) {

        
        
        //если на старницу создания прешли с профиля перформера, для создания ему персональной фантазии
        $performer_id = SK_HttpRequest::$GET['performer'];
        $_SESSION['performer_id'] = $performer_id;
        //установить что приватная, и создать контракт
        $private =  $performer_id !=0 ? true : false;
        $Layout->assign('private', $private);
        $edit = intval(SK_HttpRequest::$GET['edit']);
        if ($edit > 0) {//edit mode
            $Frontend->include_js_file(URL_STATIC . 'editfantasy.js');
            $fantasy = app_Fantasy::GetFantasy($edit);
            $fantasy['data_ex'] = date("m/d/Y",$fantasy['data_ex']);
            if ($fantasy['id_creator'] != SK_HttpUser::profile_id()) {//if no creater
                return parent::render($Layout);
            }
            $this->tpl_file = "edit.tpl";
            foreach ($fantasy as $k => $v) {
                $Layout->assign($k, $v);
            }

            $tags = app_Fantasy::GetTagFantasy($edit);
            $tags_res = array();
            foreach ($tags as $k => $v) {
                $tags_res[$k]['id'] = $v['id_tag'];
                $tags_res[$k]['name'] = $v['tag'];
            }
            $Layout->assign('tags', json_encode($tags_res));

            $performers_list = app_Fantasy::GetTagPerformer($edit);
            $result_performer = array();
            foreach ($performers_list as $k => $v) {
                $t = array();
                foreach ($v['list_tag'] as $tag) {
                    $t[] = array('id' => $tag['id'], 'name' => $tag['tag']);
                }
                $result_performer[] = json_encode($t);
            }
            $Layout->assign('performers', $result_performer);

            $files = app_Fantasy::GetFantasyFile($edit);
            $Layout->assign('files', $files);

            return parent::render($Layout);
        } else {
         //   $Frontend->include_js_file(URL_STATIC . 'newfantasy.js');
            return parent::render($Layout);
        }
    }

    /**
     * @see SK_Component::handleForm()
     *
     * @param SK_Form $form
     */
    public function handleForm($form) {
        
    }

}