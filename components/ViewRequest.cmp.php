<?php

class component_ViewRequest  extends SK_Component {

    public function __construct() {
        parent::__construct('view_request');
    }

    public function render(SK_Layout $Layout) {
        return parent::render($Layout);
    }

    /**
     * @see SK_Component::prepare()
     *
     * @param SK_Layout $Layout
     * @param SK_Frontend $Frontend
     */
    function prepare(SK_Layout $Layout, SK_Frontend $Frontend) {
      
        $tabs = SK_HttpRequest::$GET['tabs'];
        if(intval($tabs)<3 OR intval($tabs)>4)$tabs = 3;
        $Layout->assign('tabs', intval($tabs));
        
        $Frontend->include_js_file( URL_STATIC.'view_request.js');
        $Frontend->include_js_file(URL_STATIC . 'jquery-ui.js');
     
        
        $profile_id = SK_HttpUser::profile_id();
        if($tabs == 3){
            $list = app_FantasyWorck::GetAllRequest(SK_HttpRequest::$GET['page'], null);
            $total = app_FantasyWorck::GetTotalRequest();
        }
        if($tabs == 4){
            $list = app_FantasyWorck::GetAllRequest(SK_HttpRequest::$GET['page'], null, null, 2);
             $total = app_FantasyWorck::GetTotalRequest(null, 2);
        }
       
        //$total = 16;
          $lang_sec = SK_Language::section('profile_fields')->section('value');
        foreach ($list as $k=>$v){
            $list[$k]['description'] = $list[$k]['description'];
            $list[$k]['performer'] = app_Fantasy::GetTagPerformer($v['id']);
            $list[$k]['fantasy_tag'] = app_Fantasy::GetTagFantasy($v['id']);
            $list[$k]['fantasy_file'] = app_Fantasy::GetFantasyFile($v['id']);
            
           //$list[$k]['SendMessage'] = new component_SendMessage(array( 'sender_id' => $profile_id , 'recipient_id' =>  $list[$k]['id_performer'] , 'type' => 'new') );
            
            $list[$k]['old'] = round((time() - strtotime($list[$k]['birthdate'])) / (360 * 24 * 60 * 60));

            $count = app_ProfileList::GetCountStat($list[$k]['id_creator']);

            $list[$k]['video'] = $count['video'];

            $list[$k]['follow'] = $count['follow'];

            $profileIdList[] = $list[$k]['id_creator'];

            try {
                $list[$k]['sex_label'] = $lang_sec->text('sex_' . $list[$k]['sex']);
            } catch (SK_LanguageException $e) {
                $list[$k]['sex_label'] = "-";
            }
            
            $list[$k]['video_key'] =app_Fantasy::GetVideoHash($list[$k]['id_worck']);
            //$Layout->assign( 'SendMessage', new component_SendMessage(array( 'sender_id' => $this->viewer_id, 'recipient_id' => $this->profile_id, 'type' => 'new') ) );
            
            
        }
        
        $Layout->assign('list_performer', $list);
        $Layout->assign('user', SK_HttpUser::profile_id());
        

        $Layout->assign('total', $total);

        $Layout->assign('paging',array(
                'total'=> $total,
                'on_page'=> SK_Config::Section('video')->Section('other_settings')->display_media_list_limit,
                'pages'=> SK_Config::Section('site')->Section('additional')->Section('profile_list')->nav_per_page,
        ));

        return parent::render($Layout);
    }

    /**
     * @see SK_Component::handleForm()
     *
     * @param SK_Form $form
     */
    public function handleForm($form) {
  
    }

}