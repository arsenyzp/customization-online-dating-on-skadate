<?php

class component_FantasyView extends SK_Component {

    protected $params;


    public function __construct($params=  array()) {
        $this->params = $params;
        if(isset($params['ajax'])){
            parent::__construct('fantasy_view_ajax');
        }
        else
            parent::__construct('fantasy_view');
    }

    public function render(SK_Layout $Layout) {
        return parent::render($Layout);
    }

    /**
     * @see SK_Component::prepare()
     *
     * @param SK_Layout $Layout
     * @param SK_Frontend $Frontend
     */
    function prepare(SK_Layout $Layout, SK_Frontend $Frontend) {
        
            //$edit = intval(SK_HttpRequest::$GET['edit']);
            $edit = intval($_GET['edit']);
            $fantasy = app_Fantasy::GetFantasy($edit);
            $fantasy['data_ex'] = date("m/d/Y",$fantasy['data_ex']);
            
            foreach ($fantasy as $k => $v) {
                $Layout->assign($k, $v);
            }

            $Layout->assign('performers',  app_Fantasy::GetTagPerformer($edit));
            $Layout->assign('fantasy_tag',  app_Fantasy::GetTagFantasy($edit));
            $Layout->assign('user', SK_HttpUser::profile_id());
            $Layout->assign('my_fantasy',  app_FantasyWorck::CheckMyFantasy($edit));
            $Layout->assign('send', app_FantasyWorck::CheckPerformer($edit));
            $creator_info = app_FantasyWorck::GetCreatorInfo($edit);
            $creator_info = $creator_info[0];
            /****************INFO******************/
            $lang_section = SK_Language::section('profile_fields')->section('value');
            $creator_info['old'] = round((time() - strtotime($creator_info['birthdate'])) / (360 * 24 * 60 * 60));
            
             try {
                $creator_info['sex_label'] = $lang_section->text('sex_' . $creator_info['sex']);
            } catch (SK_LanguageException $e) {
                $creator_info['sex_label'] = "-";
            }
            $profileIdList = array();
            $profileIdList[] = $creator_info['profile_id'];
            $profileFieldData = app_Profile::getFieldValuesForUsers($profileIdList, array('country', 'state', 'city', 'zip', 'custom_location'));
            $creator_info['location'] = $profileFieldData[$creator_info['profile_id']];
            if (!empty( $creator_info['location']['custom_location'])) {
                 $creator_info['location']['custom_location'] = trim(htmlspecialchars( $creator_info['location']['custom_location']));
            }
            /****************************************/
            
            $Layout->assign('creator_info', $creator_info);

            $files = app_Fantasy::GetFantasyFile($edit);
            $Layout->assign('files', $files);

            return parent::render($Layout);
    }

}