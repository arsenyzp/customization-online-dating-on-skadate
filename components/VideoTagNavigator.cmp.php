<?php

/**
 * Created by Zend Studio for Eclipse
 * Project: Skadate 7
 * User: SD
 * Date: Oct 25, 2008
 *
 */

final class component_VideoTagNavigator extends SK_Component
{
	/**
	 * @var string
	 */
	private $feature;

	/**
	 * @var app_TagService
	 */
	private $tag_service;

	/**
	 * @var string
	 */
	private $url;

	/**
	 * @var integer
	 */
	private $tag_limit;

	/**
	 * @var integer
	 */
	private $entity_id;

	/**
	 * Class
	 *
	 * @param string $feature ( blog | event | photo | video | profile )
	 * @param string $url
	 * @param integer $tag_limit
	 */
	public function __construct( $feature, $url, $entity_id, $tag_limit = null )
	{
		parent::__construct( 'video_tag_navigator' );

		$this->feature = $feature;
		$this->url = $url;
		$this->tag_limit = $tag_limit;
		$this->entity_id = $entity_id;
	}


	/**
	 * @see SK_Component::render()
	 *
	 * @param SK_Layout $Layout
	 */
	public function render ( SK_Layout $Layout ) 
	{
		$tags = app_Tags::GetVideoTagById( $this->entity_id );

		if( empty( $tags ) )
		{
            $Layout->assign( 'no_tags', "No tags" );
			return parent::render( $Layout );
		}

		foreach ( $tags as $key => $value )
		{
			$tags[$key]['url'] = $this->url. ( strstr( $this->url, '?' ) ? '&' : '?' ).'tag='.urlencode($value['tag_id']);
		}

		$Layout->assign( 'tags', $tags );

		return parent::render( $Layout );
	}
}