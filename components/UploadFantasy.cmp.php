<?php

class component_UploadFantasy extends SK_Component {

    public function __construct() {
        parent::__construct('upload_fantasy');
    }

    public function render(SK_Layout $Layout) {
        return parent::render($Layout);
    }

    /**
     * @see SK_Component::prepare()
     *
     * @param SK_Layout $Layout
     * @param SK_Frontend $Frontend
     */
    function prepare(SK_Layout $Layout, SK_Frontend $Frontend) {

        $fantasy_id = SK_HttpRequest::$GET['id_fantasy'];
        $Layout->assign('fantasy_id', intval($fantasy_id));
        $Frontend->include_js_file(URL_STATIC . 'upload_fantasy.js');

        $fantasy = app_Fantasy::GetFantasy($fantasy_id);
        $fantasy['data_ex'] = date("m/d/Y", $fantasy['data_ex']);

        foreach ($fantasy as $k => $v) {
            $Layout->assign($k, $v);
        }

        $Layout->assign('fantasy_id', $fantasy_id);
        $Layout->assign('profile_id', SK_HttpUser::profile_id());
        $Layout->assign('performers', app_Fantasy::GetTagPerformer($fantasy_id));
        $Layout->assign('fantasy_tag', app_Fantasy::GetTagFantasy($fantasy_id));
        $Layout->assign('user', SK_HttpUser::profile_id());
        $Layout->assign('my_fantasy', app_FantasyWorck::CheckMyFantasy($fantasy_id));
        $Layout->assign('send', app_FantasyWorck::CheckPerformer($fantasy_id));

        $files = app_Fantasy::GetFantasyFile($fantasy_id);
        $Layout->assign('files', $files);

        return parent::render($Layout);
    }

    /**
     * @see SK_Component::handleForm()
     *
     * @param SK_Form $form
     */
    public function handleForm($form) {
        
    }

}