<?php

class component_SentApplications extends SK_Component {

    public function __construct() {
        parent::__construct('sent_applications');
    }

    public function render(SK_Layout $Layout) {
        return parent::render($Layout);
    }

    /**
     * @see SK_Component::prepare()
     *
     * @param SK_Layout $Layout
     * @param SK_Frontend $Frontend
     */
    function prepare(SK_Layout $Layout, SK_Frontend $Frontend) {
        //$handler = new SK_ComponentFrontendHandler('EventEdit');
        //  $Frontend->include_js_file( URL_STATIC.'jquery1.8.js');
        //  $Frontend->include_js_file( URL_STATIC.'jquery.tokeninput.js');
        //  $Frontend->include_js_file( URL_STATIC.'datepicker.js');
        
        $tabs = SK_HttpRequest::$GET['tabs'];
        $Layout->assign('tabs', intval($tabs));
        $Frontend->include_js_file(URL_STATIC . 'listfantasy.js');

        // $Frontend->include_js_file( URL_COMPONENTS.'Fantasy.cmp.js');
        //$this->frontend_handler = $handler;        

        $list = app_FantasyWorck::GetAllWorks(SK_HttpRequest::$GET['page'], null,  intval($tabs));
        $total = app_FantasyWorck::GetTotal( intval($tabs));
        //$total = 16;
        foreach ($list as $k => $v) {
            $list[$k]['description'] = substr($list[$k]['description'], 0, 150);
            $list[$k]['performer'] = app_Fantasy::GetTagPerformer($v['id']);
            $list[$k]['fantasy_tag'] = app_Fantasy::GetTagFantasy($v['id']);
            $list[$k]['fantasy_file'] = app_Fantasy::GetFantasyFile($v['id']);
         /*   switch ($list[$k]['worck_status']) {
                case 0:
                    $list[$k]['worck_status'] = "expects";
                    break;
                case 1:
                    $list[$k]['worck_status'] = "accepted";
                    break;
                case -1:
                    $list[$k]['worck_status'] = "rejected";
                    break;
            }
          * 
          */
            $list[$k]['fantasy_file'] = app_Fantasy::GetFantasyFile($v['id']);
        }

        $Layout->assign('list_fantasy', $list);
        $Layout->assign('user', SK_HttpUser::profile_id());


        $Layout->assign('total', $total);

        $Layout->assign('paging', array(
            'total' => $total,
            'on_page' => SK_Config::Section('video')->Section('other_settings')->display_media_list_limit,
            'pages' => SK_Config::Section('site')->Section('additional')->Section('profile_list')->nav_per_page,
        ));

        return parent::render($Layout);
    }

    /**
     * @see SK_Component::handleForm()
     *
     * @param SK_Form $form
     */
    public function handleForm($form) {
        
    }

}