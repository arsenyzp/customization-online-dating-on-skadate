<?php

function smarty_function_paging(array $params, SK_Layout $Layout) {
    
    $total = (int) $params['total'];
    $in_page = (int) $params['on_page'];
    $pages_count = (int) $params['pages'] ? (int) $params['pages'] : 5;
    $var = isset($params['var']) ? (string) $params['var'] : 'page';

    $url = empty($params['url']) ? null : $params['url'];

    $request_uri = sk_make_url($url);

    if (isset($params["exclude"])) {
        $excludes = explode(',', $params["exclude"]);

        if (count($excludes)) {
            $_exl = array();
            foreach ($excludes as $item) {
                $_exl[$item] = null;
            }
            $excludes = array_combine($excludes, $_exl);

            $request_uri = sk_make_url($url, $excludes);
        }
    }

    if (!$total || !$in_page || !$pages_count)
        return '';

    if (($total_pages = ceil($total / $in_page)) <= 1)
        return '';
    //ширина
    $width = (int)(1+$total/$in_page)*30+60;

    $current = @SK_HttpRequest::$GET[$var];
    $current = intval($current) ? $current : 1;

    $offset = ceil($pages_count / 2) - 1;
    $offset_inc = ($total_pages - $offset) - $current;
    $offset+= ($offset_inc <= 0) ? abs($offset_inc) + (($pages_count % 2) ? 0 : 1) : 0;

    $page = ($current - $offset) > 1 ? ($current - $offset) : 1;

    $paging = '';

    for ($counter = 1; $counter <= $pages_count && $page <= $total_pages; $counter++) {
        $active = ($page == $current) ? 'active' : '';
        $url = sk_make_url($request_uri, array($var => $page));
        $paging.= "<div  class='span1'><a class='paging_num {$active}' href='{$url}'>{$page}</a></div>";
        $page++;
    }


    /*
      switch ($current)
      {
      case 1:
      $paging.="<div  class='paging_next span1'><a href='".sk_make_url($request_uri,array( $var=>$current+1))."'><span>".SK_Language::section('navigation.paging')->text('next_page')."</span></a></div>";
      $paging.="<div  class='paging_last span1'><a href='".sk_make_url($request_uri,array( $var=>$total_pages))."'><span>".SK_Language::section('navigation.paging')->text('last_page')."</span></a></div>";
      break;

      case $total_pages:
      $paging= "<div  class='paging_prev span1'><a href='".sk_make_url($request_uri,array( $var=>$current-1))."'><span>".SK_Language::section('navigation.paging')->text('prev_page')."</span></a></div>".$paging;
      $paging= "<div  class='paging_first span1'><a  href='".sk_make_url($request_uri,array( $var=>1))."'><span>".SK_Language::section('navigation.paging')->text('first_page')."</span></a></div>".$paging;
      break;

      default:
      $paging= "<div  class='paging_prev span1'><a href='".sk_make_url($request_uri,array( $var=>$current-1))."'><span>".SK_Language::section('navigation.paging')->text('prev_page')."</span></a></div>".$paging;
      $paging= "<div  class='paging_first span1'><a href='".sk_make_url($request_uri,array( $var=>1))."'><span>".SK_Language::section('navigation.paging')->text('first_page')."</span></a></div>".$paging;

      $paging.="<div  class='paging_next span1'><a href='".sk_make_url($request_uri,array( $var=>$current+1))."'><span>".SK_Language::section('navigation.paging')->text('next_page')."</span></a></div>";
      $paging.="<div  class='paging_last span1'><a href='".sk_make_url($request_uri,array( $var=>$total_pages))."'><span>".SK_Language::section('navigation.paging')->text('last_page')."</span></a></div>";
      break;
      } */

    switch ($current) {
        case 1:
           // $paging.="<div  class='paging_next span1'><a href='" . sk_make_url($request_uri, array($var => $current + 1)) . "'><span>Older</span></a></div>";
            $paging.="<div  class='span1 paging_last'><a href='" . sk_make_url($request_uri, array($var => $total_pages)) . "'><span>Older</span></a></div>";
            break;

        case $total_pages:
            $paging = "<div  class='span1 paging_prev'><a href='" . sk_make_url($request_uri, array($var => $current - 1)) . "'><span>Prev</span></a></div>" . $paging;
           // $paging = "<div  class='paging_first span1'><a  href='" . sk_make_url($request_uri, array($var => 1)) . "'><span>" . SK_Language::section('navigation.paging')->text('first_page') . "</span></a></div>" . $paging;
            break;

        default:
            $paging = "<div  class='span1 paging_prev'><a href='" . sk_make_url($request_uri, array($var => $current - 1)) . "'><span><</span></a></div>" . $paging;
            $paging = "<div  class='span1 paging_first'><a href='" . sk_make_url($request_uri, array($var => 1)) . "'><span>Prev</span></a></div>" . $paging;

            $paging.="<div  class='span1 paging_next'><a href='" . sk_make_url($request_uri, array($var => $current + 1)) . "'><span>></span></a></div>";
            $paging.="<div  class='span1 paging_last'><a href='" . sk_make_url($request_uri, array($var => $total_pages)) . "'><span>Older</span></a></div>";
            $width+=120;
            break;
    }

    $out = '<div class="pagin_footer"><div class="paging_f row" style="width: '.$width.'px">';
    //$out.= '<span class="paging_label">'.SK_Language::section('navigation.paging')->text('pages').': </span>';
    $out.=$paging . '</div></div><br clear="all">';

    return $out;
}

