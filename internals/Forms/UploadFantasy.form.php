<?php

class form_UploadFantasy extends SK_Form {

    public function __construct() {
        parent::__construct('fantasy_upload');
    }

    public function setup() {

        $fantasy_id = new fieldType_hidden('fantasy_id');
        parent::registerField($fantasy_id);

        $profile_id = new fieldType_hidden('profile_id');
        $profile_id->setValue(SK_HttpUser::profile_id());
        parent::registerField($profile_id);

        $text = new fieldType_textarea('description');
        $text->maxlength = 5000;
        parent::registerField($text);

        $file = new field_profile_video('fantasy');
        parent::registerField($file);

        parent::registerAction('formAction_FantasyUpload');
    }

}

class formAction_FantasyUpload extends SK_FormAction {

    public function __construct() {
        parent::__construct('fantasy_upload');
    }

    public function setup(SK_Form $form) {
        //$this->required_fields = array('title', 'tags');

        parent::setup($form);
    }

    public function process(array $post_data, SK_FormResponse $response, SK_Form $from) {
        //var_dump($post_data);
        $performer_id = SK_HttpUser::profile_id();
        $fantasy_id = intval($post_data['fantasy_id']);
        if (!$performer_id) {
            $response->addError("You need to register");
            return false;
        }

        if ($post_data['profile_id'] != SK_HttpUser::profile_id()) {
            $response->addError("You need to register");
            return false;
        }

        if (app_FantasyWorck::CheckPerformer($fantasy_id)) {
            $response->addError("You are not a performer");
            return false;
        }

        if ($post_data['fantasy'] == null) {
            $response->addError("You need to download the video");
            return false;
        }

        $tmp_file = new SK_TemporaryFile($post_data['fantasy']);

        $file_hash = md5(time());
        $file_ext = $tmp_file->getExtension();

        $destination = app_ProfileVideo::getVideoDir($file_hash, $file_ext);

        $fantasy = app_Fantasy::GetFantasy($fantasy_id);

        $title = $fantasy['title'];
        $desc = $fantasy['description'];
        /* пока заносим теги из фантазии в текстовом виде */
       // $sql = "SELECT id_tag FROM ".TBL_FANTASY_LIST_TAGS." WHERE id_fantasy = ".$fantasy_id;
        $list_tag = app_Fantasy::GetTagFantasy($fantasy_id);
        //$list_tag = MySQL::fetchArray($sql);
        $array_tag = array();
        foreach ($list_tag as $tag_item) {
            $array_tag[] = $tag_item['id_tag'];        
        }
        $tag = implode(',', $array_tag);
        $creator_id = $fantasy['id_creator'];
        $category_id = (int) $fantasy['category_id'];
        // $password = !empty($post_data['password']) ? htmlspecialchars($post_data['password']) : null;

        $error_ns = SK_Language::section('forms.video_upload.error_msg');
        $message_ns = SK_Language::section('forms.video_upload.msg');
        
        $profile_id = SK_HttpUser::profile_id();
        
        $video_mode = SK_Config::section('video')->get('media_mode');
        $video_id = 0;
        switch ($video_mode) {
            case 'windows_media':
                try {
                    $video_id = app_ProfileVideo::addVideo($creator_id, $title, $desc, $post_data['privacy_status'], $file_hash, $file_ext, true, $category_id, $password, null, $fantasy_id);

                    switch ($video_id) {
                        case -1:
                        case -2:
                        case -3:
                            $response->addError($error_ns->text('video_add_error'));
                            break;
                        case -4:
                            $response->addError(SK_Language::section('components.video_upload')->text('video_limit_exceeded'));
                            break;
                        default:
                            $tmp_file->move($destination);
                            $service = new SK_Service('upload_media', $profile_id);
                            $service->checkPermissions();
                            $service->trackServiceUse();

                            $userAction = new SK_UserAction('media_upload', $profile_id);
                            $userAction->item = (int) $video_id;
                            $userAction->unique = $video_id;

                            $userAction->status = (SK_Config::section('site')->Section('automode')->get('set_active_video_on_upload') == true) ? 'active' : 'approval';
                            app_UserActivities::trace_action($userAction);

                            if (app_Features::isAvailable(app_Newsfeed::FEATURE_ID)) {
                                $newsfeedDataParams = array(
                                    'params' => array(
                                        'feature' => FEATURE_VIDEO,
                                        'entityType' => 'fantasy_complete',
                                        'entityId' => $video_id,
                                        'userId' => $profile_id,
                                        'status' => (SK_Config::section('site')->Section('automode')->get('set_active_video_on_upload') == true) ? 'active' : 'approval'
                                    )
                                );
                                app_Newsfeed::newInstance()->action($newsfeedDataParams);
                            }

                            if (isset($tag))
                               // app_TagService::stAddEntityTags($video_id, $tag, 'video');
                                   app_Tags::SetVideoTag ($video_id, $tag);

                            $response->addMessage($message_ns->text('video_added'));
                            //$response->exec('window.location.reload();');
                            break;
                    }
                } catch (SK_TemporaryFileException $e) {
                    $response->addError($e->getMessage());
                }
                break;

            case 'flash_video':
                $video_id = app_ProfileVideo::addVideo($creator_id, $title, $desc, $post_data['privacy_status'], $file_hash, $file_ext, false, $category_id . $password, null, $fantasy_id);

                switch ($video_id) {
                    case -1:
                    case -2:
                    case -3:
                        $response->addError($error_ns->text('video_add_error'));
                        break;
                    case -4:
                        $response->addError(SK_Language::section('components.video_upload')->text('video_limit_exceeded'));
                        break;
                    default:
                        app_ProfileVideo::sheduleVideoForConvert($video_id, $tmp_file, $file_hash);

                        $service = new SK_Service('upload_media', $profile_id);
                        $service->checkPermissions();
                        $service->trackServiceUse();

                        $userAction = new SK_UserAction('media_upload', $profile_id);
                        $userAction->item = (int) $video_id;
                        $userAction->unique = $video_id;

                        $userAction->status = (SK_Config::section('site')->Section('automode')->get('set_active_video_on_upload') == true) ? 'active' : 'approval';

                        app_UserActivities::trace_action($userAction);

                        if (app_Features::isAvailable(app_Newsfeed::FEATURE_ID)) {
                            $newsfeedDataParams = array(
                                'params' => array(
                                    'feature' => FEATURE_VIDEO,
                                    'entityType' => 'fantasy_complete',
                                    'entityId' => $video_id,
                                    'userId' => $profile_id,
                                    'status' => (SK_Config::section('site')->Section('automode')->get('set_active_video_on_upload') == true) ? 'active' : 'approval'
                                )
                            );
                            app_Newsfeed::newInstance()->action($newsfeedDataParams);
                        }

                        if (isset($tag))
                            // app_TagService::stAddEntityTags($video_id, $tag, 'video');
                                   app_Tags::SetVideoTag ($video_id, $tag);

                        $response->addMessage($message_ns->text('video_added_for_convert'));
                        // $response->exec('window.location.reload();');
                        break;
                }
                break;
        }

        //Update tbl list worck
        $id = app_FantasyWorck::GetWorckID($fantasy_id, $performer_id);
        app_FantasyWorck::UpdateStatusWorck(2, $id, $video_id);
        app_FantasyWorck::UpdateCommentWorck($post_data['description'], $fantasy_id);
        //добавление сообщения
        if (strlen($post_data['description']) > 1) {
            app_MailBox::sendMessage($performer_id, $creator_id, $post_data['description'], "Made fantasy");
            $id_messag = MySQL::insert_id();

            $sql = "INSERT INTO " . TBL_FANTASY_MESSAG . " ( `worck_id`, `message_id`)
                    VALUES('$id', '$id_messag')";
            if (!MySQL::query($sql))
                return false;
        }
        //уведомления на почту
        app_MailNotice:: CompletedFantasy($performer_id, $creator_id);
        //notice
        $video_hash = app_ProfileVideo::getVideoHash($video_id);
        $desc_notice = "Your fantasy $title has been uploaded!<a href=\"/member/video_view.php?videokey=$video_hash\"> (Click to go to video)</a>";
        app_Notice::AddNotice(NOTICE_COMPLETED_YOUR_FANTASY, $desc_notice, $creator_id, $performer_id);
        $response->addMessage("Successfully sent");
        $location = SK_Navigation::href('sent_aplication');
        $response->exec("setTimeout(\"location.href='" . $location . "'\", 1000)");		
    }

}

