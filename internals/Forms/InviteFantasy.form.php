<?php

class form_InviteFantasy extends SK_Form {

    public function __construct() {
        parent::__construct('invite_fantasy');
    }

    public function setup() {
        $select_fantasy = new fieldType_textarea('fantasy_id');
        parent::registerField($select_fantasy);

        $reporter_id = new fieldType_hidden('reporter_id');
        parent::registerField($reporter_id);
        
        $messag = new fieldType_textarea('messag');
        $messag->maxlength = 5000;
         parent::registerField($messag);

        $entity_id = new fieldType_hidden('entity_id');
        parent::registerField($entity_id);

        $type = new fieldType_hidden('type');
        parent::registerField($type);
        
        $completion_date = new fieldType_text('completion_date');
        parent::registerField($completion_date);

        $select_fantasy = new field_integer('select_fantasy');
        parent::registerField($select_fantasy);

        $budget = new field_integer('budget');
        parent::registerField($budget);

        parent::registerAction('formFantasyInvite');
    }

}

class formFantasyInvite extends SK_FormAction {

    public function __construct() {
        parent::__construct('add');
    }

    public function setup(SK_Form $form) {
        $this->required_fields = array('reporter_id', 'entity_id', 'type');

        parent::setup($form);
    }

    public function process(array $post_data, SK_FormResponse $response, SK_Form $form) {
        if (app_FantasyWorck::CreateInvite($post_data)) {
            $response->addMessage("Fantasia invite successfully");
             $response->exec("setTimeout(\"location.reload()\", 1000)");
        } else {
            $response->addError("Fantasia invite error");
        }
    }

}

