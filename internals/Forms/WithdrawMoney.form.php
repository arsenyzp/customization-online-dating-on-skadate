<?php

class form_WithdrawMoney extends SK_Form 
{
	
	public function __construct()
	{
		parent::__construct('withdraw_money');
	}

	public function setup()
	{
		$profile_id = new fieldType_hidden('profile_id');
		parent::registerField($profile_id);
		
		$amount = new field_integer('amount');
		parent::registerField($amount);
		
		parent::registerAction('formWithdrawMoneyAction');
	}

}

class formWithdrawMoneyAction extends SK_FormAction 
{
	public function __construct()
	{
		parent::__construct('withdraw_money');
	}
	
	public function setup( SK_Form $form )
	{
		$this->required_fields = array('profile_id', 'amount');
		
		parent::setup($form);
	}
	
	public function process( array $post_data, SK_FormResponse $response, SK_Form $form)
	{
                                    if($post_data['profile_id'] != SK_HttpUser::profile_id()){
                                        $response->addError("Error profile");
                                    }
            
                                    $keys_result = app_KuinKeys::WithdrawKeys($post_data['amount'], $post_data['profile_id']);
		
		switch ( $keys_result )
		{
			case 1:
				//successfully
				$response->addMessage("Successfully");
                                                                        $response->exec("setTimeout(\"location.reload()\", 1000)");
				break;
			case -2:
				//wrong parameters
				$response->addError("Not enough money in the account");
				break;
			case -3:
				//content already reported by member				
				$response->addError("Error profile");
		}
	}
}

