<?php

class form_OrderDetails extends SK_Form {

    public function __construct() {
        parent::__construct('order_datails');
    }

    public function setup() {

        $fantasy_id = new field_integer('fantasy_id');
        parent::registerField($fantasy_id);

        $worck_id = new field_integer('worck_id');
        parent::registerField($worck_id);

        $completion_date = new fieldType_text('completion_date');
        parent::registerField($completion_date);

        $offer_type = new fieldType_set('offer_type');
        parent::registerField($offer_type);

        $select_fantasy = new field_integer('select_fantasy');
        parent::registerField($select_fantasy);

        $budget = new field_integer('budget');
        $budget->setValue(111);
        parent::registerField($budget);
        
        $price_video = new field_integer('price_video');
        parent::registerField($price_video);
        
        $lenght_video = new fieldType_text('lenght_video');
        parent::registerField($lenght_video);

        $messag = new fieldType_textarea('messag');
        $messag->maxlength = 5000;
        parent::registerField($messag);

        parent::registerAction('formAction_OrderDetailsEdit');
    }

}

class formAction_OrderDetailsEdit extends SK_FormAction {

    public function __construct() {
        parent::__construct('order_datails');
    }

    public function setup(SK_Form $form) {
        //$this->required_fields = array('title', 'tags');

        parent::setup($form);
    }

    public function process(array $post_data, SK_FormResponse $response, SK_Form $from) {
//        var_dump($post_data);
        /* check register */
        $profile_id = SK_HttpUser::profile_id();
        if (!$profile_id) {
            $response->addError("You need to register");
            return false;
        }


        if ($_SESSION['offerts_details']['fantasy_id'] != $post_data['fantasy_id']) {
            $response->addError("Error update contract");
            return false;
        }


        if (app_Fantasy::CheckOwn($post_data['fantasy_id'], $profile_id)) {
            //редактирует заказчик
            if (app_OfferDetails::UpdateCreator($post_data)) {
                $response->addMessage("Request update successfully");
                $response->exec("setTimeout(\"location.href='" . SK_Navigation::href('view_request') . "'\", 1000)");
            } else {
                $response->addError("Error update Request");
            }
        }
        else{
            //редактирует исполнитель
             if (app_OfferDetails::UpdatePerformer($post_data)) {
             $response->addMessage("Request update successfully");
              $response->exec("setTimeout(\"location.href='/member/sent_applications.php'\", 1000)");
             } else {
                $response->addError("Error update Request");
            }
        }
	
    }

}