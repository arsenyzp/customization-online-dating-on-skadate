<?php

class form_Notifications extends SK_Form {

    public function __construct() {
        parent::__construct('notifications');
    }
    /*
     *  
    Email Notice
        invited_to_fantasize
        checkbox
        contract_updated
        checkbox
        received_keys
        checkbox
        new_follower
        checkbox
        fantasy_completed
        checkbox
        received_new_comment_performance
        checkbox
        you_rated
        checkbox
        added_favorites_video
        checkbox
        posted_fantasy
        checkbox

     */

    public function setup() {

        $real_name = new field_group_type('invited_to_fantasize');
        parent::registerField($real_name);

        $real_name = new field_group_type('contract_updated');
        parent::registerField($real_name);
        
        $real_name = new field_group_type('received_keys');
        parent::registerField($real_name);
        
        $real_name = new field_group_type('new_follower');
        parent::registerField($real_name);
        
        $real_name = new field_group_type('fantasy_completed');
        parent::registerField($real_name);
                
        $you_rated = new field_group_type('you_rated');
        parent::registerField($you_rated);
        
        $posted_fantasy = new field_group_type('posted_fantasy');
        parent::registerField($posted_fantasy);
        
        $new_message = new field_group_type('new_message');
        parent::registerField($new_message);

        $pass = new fieldType_text('pass');
        parent::registerField($pass);

        parent::registerAction('form_Notifications_submit');
    }

}

class form_Notifications_submit extends SK_FormAction {

    public function __construct() {
        parent::__construct('submit');
    }

    public function process(array $data, SK_FormResponse $response, SK_Form $form) {
        ///$response->debug($data);
        $profile_id = SK_HttpUser::profile_id();
        
        if(app_Passwords::hashPassword($data['pass']) !=  app_Profile::getFieldValues($profile_id, 'password')){
            $response->addError("Incorrect password");
            return;
        }
        
        foreach ($data as $field_name => $field_value) {
            if($field_name != 'pass')
                app_Profile::setFieldValueExtend($profile_id, $field_name, $field_value);
        }
        $response->addMessage("Profile Update");
    }

}