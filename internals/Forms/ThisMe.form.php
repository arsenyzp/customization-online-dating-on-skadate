<?php

class form_ThisMe extends SK_Form {

    public function __construct() {
        parent::__construct('video_upload');
    }

    public function setup() {
        $profile_id = new fieldType_hidden('profile_id');
        parent::registerField($profile_id);

        $title = new fieldType_text('title');
        parent::registerField($title);

        $description = new fieldType_textarea('description');
        $description->maxlength = 1500;
        parent::registerField($description);

        $video_file = new field_img_video('profile_video');
        parent::registerField($video_file);

        parent::registerAction('formVideoUpload_Upload');
    }

}

class formVideoUpload_Upload extends SK_FormAction {

    public function __construct() {
        parent::__construct('upload');
    }

    public function setup(SK_Form $form) {
        $this->required_fields = array('profile_id', 'title', 'privacy_status', 'profile_video');

        parent::setup($form);
    }

    public function process(array $post_data, SK_FormResponse $response, SK_Form $form) {
        if ($post_data['profile_id'] != SK_HttpUser::profile_id())
            return false;

        $tmp_file = new SK_TemporaryFile($post_data['profile_video']);

        $file_hash = md5(time());
        $file_ext = $tmp_file->getExtension();

        $destination = app_ProfileVideo::getVideoDir($file_hash, $file_ext);

        $title = trim($post_data['title']);
        $desc = trim($post_data['description']);
        $profile_id = $post_data['profile_id'];
        $error_ns = SK_Language::section('forms.video_upload.error_msg');
        $message_ns = SK_Language::section('forms.video_upload.msg');

        $video_mode = SK_Config::section('video')->get('media_mode');

        ////проверяем что было загруженно (фото или видео)
        $extension_video = array('mp4', 'avi', 'wmv', '3gp', 'flv', 'mkv', 'mpeg', 'mpg');
        $extension_image = array('jpg', 'jpeg', 'gif', 'png');
        if (in_array($file_ext, $extension_video)){
            switch ($video_mode) {
                case 'windows_media':
                    try {
                        $video_id = app_ProfileVideo::addVideo($profile_id, $title, $desc, $post_data['privacy_status'], $file_hash, $file_ext, true, $category_id, $password);

                        switch ($video_id) {
                            case -1:
                            case -2:
                            case -3:
                                $response->addError($error_ns->text('video_add_error'));
                                break;
                            case -4:
                                $response->addError(SK_Language::section('components.video_upload')->text('video_limit_exceeded'));
                                break;
                            default:
                                $tmp_file->move($destination);
                                $service = new SK_Service('upload_media', $profile_id);
                                $service->checkPermissions();
                                $service->trackServiceUse();

                                $userAction = new SK_UserAction('media_upload', $profile_id);
                                $userAction->item = (int) $video_id;
                                $userAction->unique = $video_id;

                                $userAction->status = (SK_Config::section('site')->Section('automode')->get('set_active_video_on_upload') == true) ? 'active' : 'approval';
                                app_UserActivities::trace_action($userAction);

                                if (app_Features::isAvailable(app_Newsfeed::FEATURE_ID)) {
                                    $newsfeedDataParams = array(
                                        'params' => array(
                                            'feature' => FEATURE_VIDEO,
                                            'entityType' => 'media_upload',
                                            'entityId' => $video_id,
                                            'userId' => $profile_id,
                                            'status' => (SK_Config::section('site')->Section('automode')->get('set_active_video_on_upload') == true) ? 'active' : 'approval'
                                        )
                                    );
                                    app_Newsfeed::newInstance()->action($newsfeedDataParams);
                                }

                                if (isset($tag))
                                // app_TagService::stAddEntityTags($video_id, $tag, 'video');
                                    app_Tags::SetVideoTag($video_id, $tag);

                                $response->addMessage($message_ns->text('video_added'));
                                $response->exec('window.location.reload();');
                                break;
                        }
                    } catch (SK_TemporaryFileException $e) {
                        $response->addError($e->getMessage());
                    }
                    break;

                case 'flash_video':
                    $video_id = app_ProfileVideo::addVideo($profile_id, $title, $desc, $post_data['privacy_status'], $file_hash, $file_ext, false, $category_id . $password);

                    switch ($video_id) {
                        case -1:
                        case -2:
                        case -3:
                            $response->addError($error_ns->text('video_add_error'));
                            break;
                        case -4:
                            $response->addError(SK_Language::section('components.video_upload')->text('video_limit_exceeded'));
                            break;
                        default:
                            app_ProfileVideo::sheduleVideoForConvert($video_id, $tmp_file, $file_hash);

                            $service = new SK_Service('upload_media', $profile_id);
                            $service->checkPermissions();
                            $service->trackServiceUse();

                            $userAction = new SK_UserAction('media_upload', $profile_id);
                            $userAction->item = (int) $video_id;
                            $userAction->unique = $video_id;

                            $userAction->status = (SK_Config::section('site')->Section('automode')->get('set_active_video_on_upload') == true) ? 'active' : 'approval';

                            app_UserActivities::trace_action($userAction);

                            if (app_Features::isAvailable(app_Newsfeed::FEATURE_ID)) {
                                $newsfeedDataParams = array(
                                    'params' => array(
                                        'feature' => FEATURE_VIDEO,
                                        'entityType' => 'media_upload',
                                        'entityId' => $video_id,
                                        'userId' => $profile_id,
                                        'status' => (SK_Config::section('site')->Section('automode')->get('set_active_video_on_upload') == true) ? 'active' : 'approval'
                                    )
                                );
                                app_Newsfeed::newInstance()->action($newsfeedDataParams);
                            }

                            $response->addMessage($message_ns->text('video_added_for_convert'));
                            $response->exec('window.location.href = window.location.href;');
                            break;
                    }
                    break;
            } 
        }
        else if (in_array($file_ext, $extension_image)) {
            $slot = app_ProfilePhoto::GetFreeSlot($profile_id);
            $param = array('name'=>$post_data['title'], 'description'=>$post_data['description']);
            try{
                  $phot_id =  app_ProfilePhoto::upload($tmp_file->getUniqid(), $slot, null, null, $param);
            }
              catch (SK_ProfilePhotoException $e){
                     $response->addError($e->getMessag());
                     return false;
              }
              
              
              if ( app_Features::isAvailable(app_Newsfeed::FEATURE_ID) )
            {
                $newsfeedDataParams = array(
                    'params' => array(
                        'feature' => FEATURE_PHOTO,
                        'entityType' => 'photo_upload',
                        'entityId' => $phot_id,
                        'userId' => $profile_id,
                        'status' => (SK_Config::section('site')->Section('automode')->get('set_active_photo_on_upload') == true) ? 'active' : 'approval'
                    )
                );
                app_Newsfeed::newInstance()->action($newsfeedDataParams);
            }
              
            $response->addMessage("Photo Upload");
            $response->exec('window.location.href = window.location.href;');
        }
        return array('video_mode' => $video_mode);
    }

}