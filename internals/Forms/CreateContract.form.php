<?php

class form_CreateContract extends SK_Form {

    public function __construct() {
        parent::__construct('create_contract');
    }

    public function setup() {

        $fantasy_id = new fieldType_hidden('fantasy_id');
        parent::registerField($fantasy_id);

        $worck_id = new fieldType_hidden('worck_id');
        parent::registerField($worck_id);

        $budget = new fieldType_text('budget');
        parent::registerField($budget);
        
        $price_video = new field_integer('price_video');
        parent::registerField($price_video);
        
        $lenght_video = new fieldType_text('lenght_video');
        parent::registerField($lenght_video);

        $completion_date = new fieldType_text('completion_date');
        parent::registerField($completion_date);

        $text = new fieldType_textarea('messag');
        $text->maxlength = 5000;
        parent::registerField($text);

        parent::registerAction('formAction_CreateContract');
    }

}

class formAction_CreateContract extends SK_FormAction {

    public function __construct() {
        parent::__construct('create_contract');
    }

    public function setup(SK_Form $form) {
        $this->required_fields = array('title', 'tags');

        parent::setup($form);
    }

    public function process(array $post_data, SK_FormResponse $response, SK_Form $from) {
//        var_dump($post_data);
        /* check register */
        $profile_id = SK_HttpUser::profile_id();
        if (!$profile_id) {
            $response->addError("You need to register");
            return false;
        }
        /* проверяем если пользователь создатель фантазии то не сохраняем */
        if (app_Fantasy::CheckOwn($post_data['fantasy_id'], $profile_id)) {
            $response->addError("Сan not fulfill their fantasies");
            return false;
        }
        /*
        if ($_SESSION['offerts_details']['fantasy_id'] != $post_data['fantasy_id']) {
            $response->addError("Error create contract");
            return false;
        }*/

        if (app_FantasyWorck::CheckMyFantasy($post_data['fantasy_id'])) {
            
        } else {
            if (app_FantasyWorck::CreateContract($post_data)) {
                $response->addMessage("Create contract successfully");
                $response->exec("setTimeout(\"location.href='" . SK_Navigation::href('sent_aplication') . "?edit=" . $post_data['id'] . "'\", 1000)");
            } else {
                $response->addError("Error create contract");
            }
        }
    }

}