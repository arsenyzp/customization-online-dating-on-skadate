<?php

class form_EditProfileKuinkey extends SK_Form {

    public function __construct() {
        parent::__construct('edit_profile_kuinkey');
    }

    public function setup() {

        $general_description = new fieldType_textarea('general_description');
        $general_description->maxlength = 5000;
        parent::registerField($general_description);

        $real_name = new fieldType_text('real_name');
        parent::registerField($real_name);

        $last_name = new fieldType_text('last_name');
        parent::registerField($last_name);

        $username = new fieldType_text('username');
        parent::registerField($username);

        $email = new field_email('email');
        parent::registerField($email);

        $age = new fieldType_text('age');
        parent::registerField($age);

        $sex = new field_group_type('sex');
        parent::registerField($sex);

        $сountry_id = new field_group_type('location');
        parent::registerField($сountry_id);

        $interested_in = new field_group_type('interested_in');
        parent::registerField($interested_in);

        $match_sex = new field_group_type('match_sex');
        parent::registerField($match_sex);

        $tag = new fieldType_text('tag');
        parent::registerField($tag);

        $pass = new fieldType_text('pass');
        parent::registerField($pass);

        parent::registerAction('form_EditProfileKuinkey_submit');
    }

}

class form_EditProfileKuinkey_submit extends SK_FormAction {

    public function __construct() {
        parent::__construct('submit');
    }

    public function process(array $data, SK_FormResponse $response, SK_Form $form) {
        ///$response->debug($data);
        $profile_id = SK_HttpUser::profile_id();
        
        if(app_Passwords::hashPassword($data['pass']) !=  app_Profile::getFieldValues($profile_id, 'password')){
            $response->addError("Incorrect password");
            return;
        }
        
        foreach ($data as $field_name => $field_value) {
            if (in_array($field_name, array('username', 'email', 'match_sex', 'sex', 'general_description'))){
                
                app_Profile::setFieldValue($profile_id, $field_name, $field_value);
                
                }
           else if (in_array($field_name, array('real_name', 'last_name', 'age', 'interested_in', 'tag'))){
      
                app_Profile::setFieldValueExtend($profile_id, $field_name, $field_value);
        }
           else if ($field_name == 'location'){
                app_Profile::setFieldValueLocation($profile_id, 'country_id', $field_value['сountry_id']);
                
                }
        }
        //newsfeed
            $newsfeedAction = app_Newsfeed::newInstance()->getAction('profile_edit', $profile_id, $profile_id);
            if ( !empty($newsfeedAction) )
            {
                //app_Newsfeed::newInstance()->removeActionById($newsfeedAction->getId());
            }

            $newsfeedDataParams = array(
                'params' => array(
                    'feature' => FEATURE_NEWSFEED,
                    'entityType' => 'profile_edit',
                    'entityId' => $profile_id,
                    'userId' => $profile_id,
                    'status' => 'active',
                    'replace' => true
                )
            );
            app_Newsfeed::newInstance()->action($newsfeedDataParams);
        
        
        $response->addMessage("Profile Update");
    }

}