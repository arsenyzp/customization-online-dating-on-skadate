<?php

class form_ThisIsMeEdit extends SK_Form {

       public function __construct() {
              parent::__construct('this_is_me_edit');
       }

       public function setup() {
              $profile_id = new fieldType_hidden('profile_id');
              parent::registerField($profile_id);

              $password = new fieldType_text('password');
              parent::registerField($password);

              $title = new field_group_type('title');
              parent::registerField($title);

              $description = new field_group_type('description');
              parent::registerField($description);

              $id_media = new field_group_type('id_media');
              parent::registerField($id_media);

              $type = new field_group_type('type');
              parent::registerField($type);

              parent::registerAction('formThisIsMeEdit');
       }

}

class formThisIsMeEdit extends SK_FormAction {

       public function __construct() {
              parent::__construct('upload');
       }

       public function setup(SK_Form $form) {
              $this->required_fields = array('profile_id', 'title', 'type', 'id_media');

              parent::setup($form);
       }

       public function process(array $post_data, SK_FormResponse $response, SK_Form $form) {
              if (intval($post_data['profile_id']) != SK_HttpUser::profile_id())
                     return false;
              if (app_Passwords::hashPassword($post_data['password']) != app_Profile::getFieldValues(SK_HttpUser::profile_id(), 'password')) {
                     $response->addError("Incorrect password");
                     return;
              }
              foreach ($post_data['id_media'] as $k => $v) {
                     switch ($post_data['type'][$k]) {
                            case 'photo':
                                   app_ThisMe::SavePhoto($post_data['id_media'][$k], $post_data['title'][$k], $post_data['description'][$k]);
                                   break;
                            case 'video':
                                   app_ThisMe::SaveVideo($post_data['id_media'][$k], $post_data['title'][$k], $post_data['description'][$k]);
                                   break;
                     }
              }


              $response->addMessage("This is me media update");
              $response->exec('window.location.href = window.location.href;');

              return array('video_mode' => $video_mode);
       }

}