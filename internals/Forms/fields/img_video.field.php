<?php

class field_img_video extends fieldType_file
{
    
    public $extension;
    
    public $extension_video = array('mp4', 'avi', 'wmv',  '3gp', 'flv', 'mkv', 'mpeg', 'mpg');
    
    public $extension_image = array('jpg', 'jpeg', 'gif', 'png');

    public function detected_extension(){
        $this->extension = $this->getExtension();
        if(in_array($this->getExtension(), $this->extension_image)){
            return 'image';
        }
         if(in_array($this->getExtension(), $this->extension_video)){
             return 'video';
         }
    }

        public function setup(SK_Form $form)
    {
        //$this->multifile = true;
        $this->multifile = false;
        $this->max_files_num = 50;
        $this->max_file_size = 100 * 1024 * 1024;
        
       /* $this->default_allowed_extensions = array (
            'txt', 'doc', 'docx', 'sql', 'csv', 'doc', 'docx', 
            'jpg', 'jpeg', 'png', 'gif', 'bmp', 'psd', 'ai', 'rtf', 
            'avi', 'wmv', 'mp3', '3gp', 'flv', 'mkv', 'mpeg', 'mpg', 'swf',
            'zip', 'gz', '.tgz', 'gzip', '7z', 'bzip2', 'rar'
        );*/
         $this->default_allowed_extensions = array_merge($this->extension_image, $this->extension_video);
        
        parent::setup($form);
    }
    
    public function preview( SK_TemporaryFile $tmp_file )
    {
        $src = $tmp_file->getURL();
        $label = substr($tmp_file->getFileName(), 0, 18);
        
        $size = round($tmp_file->getSize() / 1024, 2);
        /* ($size KB)*/
        $output = <<<EOT
<div class="af_attachment_item">
    <span class="af_attachment_label">
        $label
    </span>
    <a class="delete_file_btn lbutton" href="javascript://"></a>
</div>
EOT;
        return $output;
    }
}