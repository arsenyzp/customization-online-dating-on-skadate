<?php

class form_FantasyEdit extends SK_Form {

    public function __construct() {
        parent::__construct('fantasy_edit');
    }

    public function setup() {
        
        $id = new fieldType_hidden('id');
        parent::registerField($id);
       
        $title = new fieldType_text('title');
        parent::registerField($title);
        
        $tags = new field_group_type('tags');
        parent::registerField($tags);
        
         if ( SK_Config::section('video')->Section('other_settings')->get('enable_categories') )
        {
            $categories = new fieldType_select('category');
            $categories->setType('select');
            $categories->label_prefix = 'cat';
            $catArray = app_VideoList::getVideoCategories(true);
            $categories->setValues($catArray);
            parent::registerField($categories);
        }

        $text = new fieldType_textarea('description');
        $text->maxlength = 5000;
        parent::registerField($text);
        
        $perfomers = new field_group_type('perfomers');
        parent::registerField($perfomers);

        $budget = new field_integer('budget');
        parent::registerField($budget);
        
        $price_video = new field_integer('price_video');
        parent::registerField($price_video);
        
        $length_video = new fieldType_text('length_video');
        parent::registerField($length_video);
        
        $buggest_null = new fieldType_checkbox('buggest_null');
        parent::registerField($buggest_null);
        
        $files = new field_group_type('files');
        parent::registerField($files);
        
        $newfile = new field_attachment('newfile');
        parent::registerField($newfile);
        
        $data_fantasy_ex = new fieldType_text('data_fantasy_ex');
        parent::registerField($data_fantasy_ex);
        
        $fantasy_private = new fieldType_checkbox('fantasy_private');
        parent::registerField($fantasy_private);
        
        
        parent::registerAction('formAction_FantasyEdit');
    }

}

class formAction_FantasyEdit extends SK_FormAction {

    public function __construct() {
        parent::__construct('fantasy_edit');
    }

    public function setup(SK_Form $form) {
        $this->required_fields = array('title', 'tags');

        parent::setup($form);
    }

    public function process(array $post_data, SK_FormResponse $response, SK_Form $from) {
//        var_dump($post_data);
        /*check register*/
        $profile_id = SK_HttpUser::profile_id();
        if(!$profile_id){
            $response->addError("You need to register");
            return false;
            }
        /*check own*/
        if(!app_Fantasy::CheckOwn($post_data['id'], $profile_id)){
            $response->addError("Do not have access");
            return false;
        }    
            
        if(app_Fantasy::UpdateFantasy($post_data)){
               $response->addMessage("Fantasia update successfully"); 
               $response->exec("setTimeout(\"location.href='".SK_Navigation::href('fantasy')."?edit=".$post_data['id']."'\", 1000)"); 
        }
        else{
               $response->addError("Error update fantasy");
        }
        
        //$response->addError( SK_Language::section('forms.send_message.error_msg')->text('send_yourself') );
        //$response->addMessage( SK_Language::section('forms.send_message.msg')->text('msg_sent')); 
	//$location = SK_Navigation::href('fantasy');
	//		$response->redirect($location);		
        
    }

}