<?php

class form_Reviews extends SK_Form 
{
	
	public function __construct()
	{
		parent::__construct('reviews');
	}

	public function setup()
	{
		$reason = new fieldType_textarea('text_review');
		parent::registerField($reason);
		
		$reporter_id = new field_integer('reporter_id');
		parent::registerField($reporter_id);
		
		$entity_id = new field_integer('entity_id');
		parent::registerField($entity_id);
                
                               $fantasy_id = new field_integer('fantasy_id');
		parent::registerField($fantasy_id);
		
                               $type = new fieldType_text('type');
		parent::registerField($type);
                
		parent::registerAction('formReview_Add');
	}

}

class formReview_Add extends SK_FormAction 
{
	public function __construct()
	{
		parent::__construct('reviews');
	}
	
	public function setup( SK_Form $form )
	{
		$this->required_fields = array('reporter_id', 'entity_id', 'type', 'reason');
		
		parent::setup($form);
	}
	
	public function process( array $post_data, SK_FormResponse $response, SK_Form $form)
	{
                        if($post_data['reporter_id'] != SK_HttpUser::profile_id()){
                             $response->addError("Error reviews fantasy");
                             return false;
                        }
                            
                        app_ProfileReviews::AddReviews($post_data);
                        $response->addMessage("Review posted"); 
                        $response->exec("setTimeout(\"location.reload()\", 1000)"); 
	}
}

