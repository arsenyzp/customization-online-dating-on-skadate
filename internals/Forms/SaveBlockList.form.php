<?php

class form_SaveBlockList extends SK_Form {

    public function __construct() {
        parent::__construct('save_block_list');
    }

    public function setup() {
       
        $list_block_add = new fieldType_text('array_block_add');
        parent::registerField($list_block_add);
        
        $list_block_delete = new fieldType_text('array_block_delete');
        parent::registerField($list_block_delete);
        
        $pass = new fieldType_text('pass');
        parent::registerField($pass);
        
        parent::registerAction('formAction_SaveBlockList');
    }

}

class formAction_SaveBlockList extends SK_FormAction {

    public function __construct() {
        parent::__construct('save_block_list');
    }

    public function setup(SK_Form $form) {
        $this->required_fields = array('list_block_add', 'list_block_delete', 'pass');
        parent::setup($form);
    }

    public function process(array $post_data, SK_FormResponse $response, SK_Form $from) {

        $profile_id = SK_HttpUser::profile_id();
        if(!$profile_id){
            $response->addError("You need to Sign in");
            return false;
            }
         //проверяем пароль
         if(!app_Profile::CheckPassword($post_data['pass'])){
              $response->addError("Incorrect password");
               return false;
         }  
         //добавляем новые заблокированные ид
            $block = explode(',', $post_data['array_block_add']);
            foreach ($block as $id){
                if($id>0)
                    app_Bookmark::BlockProfile ($profile_id, $id);
            }
        //удаляем разблокированные
            $unblock = explode(',', $post_data['array_block_delete']);
            foreach ($unblock as $id){
                if($id>0)
                    app_Bookmark::UnblockProfile($profile_id, $id);
            }
            
        if(true){
               $response->addMessage("Save block List"); 
               $response->exec("setTimeout(\"location.href='/member/block_list.php'\", 1000)"); 
        }
        else{
               $response->addError("Error creating fantasy");
        }
    }
}

