<?php

class form_Fantasy extends SK_Form {

    public function __construct() {
        parent::__construct('fantasy');
    }

    public function setup() {

        $perfomer_id = new fieldType_hidden('performer_id');
        $perfomer_id->setValue(0);
        parent::registerField($perfomer_id);

        $title = new fieldType_text('title');
        parent::registerField($title);

        $tags = new field_group_type('tags');
        parent::registerField($tags);

        if (SK_Config::section('video')->Section('other_settings')->get('enable_categories')) {
            $categories = new fieldType_select('category');
            $categories->setType('select');
            $categories->label_prefix = 'cat';
            $catArray = app_VideoList::getVideoCategories(true);
            $categories->setValues($catArray);
            parent::registerField($categories);
        }

        $text = new fieldType_textarea('description');
        $text->maxlength = 5000;
        parent::registerField($text);

        $perfomers = new field_group_type('perfomers');
        parent::registerField($perfomers);

        $budget = new field_integer('budget');
        parent::registerField($budget);

        $price_video = new field_integer('price_video');
        parent::registerField($price_video);

        $lenght_video = new field_integer('length_video');
        parent::registerField($lenght_video);

        $buggest_null = new fieldType_checkbox('buggest_null');
        parent::registerField($buggest_null);

        $file = new field_attachment('file');
        parent::registerField($file);

        $data_fantasy_ex = new fieldType_text('data_fantasy_ex');
        parent::registerField($data_fantasy_ex);

        $completion_date = new fieldType_text('completion_date');
        parent::registerField($completion_date);

        $fantasy_private = new fieldType_checkbox('fantasy_private');
        parent::registerField($fantasy_private);


        parent::registerAction('formAction_Fantasy');
    }

}

class formAction_Fantasy extends SK_FormAction {

    public function __construct() {
        parent::__construct('fantasy');
    }

    public function setup(SK_Form $form) {
        $this->required_fields = array('title', 'tags');

        parent::setup($form);
    }

    public function process(array $post_data, SK_FormResponse $response, SK_Form $from) {

        $profile_id = SK_HttpUser::profile_id();
        if (!$profile_id) {
            $response->addError("You need to register");
            return false;
        }

        $fantasy_id = app_Fantasy::Create($profile_id, $post_data);
        if ($fantasy_id) {
            $response->addMessage("Fantasia created successfully");
            $response->exec("setTimeout(\"location.href='" . SK_Navigation::href('fantasy_list') . "'\", 1000)");
        } else {
            $response->addError("Error creating fantasy");
        }
        if ($post_data['performer_id'] > 0 && $post_data['performer_id'] != $profile_id) {
            //создаём контракт
            $data_invite = array(
                                             'fantasy_id'=>$fantasy_id,
                                             'budget' =>$post_data['budget'],
                                             'completion_date'=>  date("d-m-Y", time()),
                                             'entity_id'=>$post_data['performer_id']
                                            );
            app_FantasyWorck::CreateInvite($data_invite);
        }
        if (!$post_data['fantasy_private']) {
            $newsfeedDataParams = array(
                'params' => array(
                    'feature' => FEATURE_NEWSFEED,
                    'entityType' => 'forum_add_topic',
                    'entityId' => $fantasy_id,
                    'userId' => SK_HttpUser::profile_id(),
                )
            );
            app_Newsfeed::newInstance()->action($newsfeedDataParams);
        }
    }

}