<?php

class app_KuinKeys {

    /**
     * баланс пользователя
     * @param int $profile_id
     * @return array
     */
    public static function GetCountKeys($profile_id) {
        return app_UserPoints::getProfilePointsBalance($profile_id);
    }

    /**
     * пополнение баланса пользоватля
     * @param int $count
     * @param int $profile_id
     * @return boolean
     */
    public static function AddKeys($count, $profile_id, $operation = null) {
        if($operation == null)$operation = 'add';
       //app_UserPoints::logAction($profile_id, $operation, $count, null, '+');
       //уведомление на почту
       app_MailNotice::ReceivedKeys($count, $profile_id);
        return app_UserPoints::updateProfilePointsBalance($profile_id, $count, 'inc');
    }

    /**
     * снятие денег с баланса пользователя
     * @param int $count
     * @param int $profile_id
     * @return boolean
     */
    public static function PaymentKeys($count, $profile_id, $operation = null) {
        if($operation == null)$operation = 'pay';
        //app_UserPoints::logAction($profile_id, $operation, $count, null, '-');
        return app_UserPoints::updateProfilePointsBalance($profile_id, $count, 'dec');
    }

    public static function Log($profile_id, $count, $operation = null) {
        if ($operation == null) {
            $operation = 'pay';
        }
        app_UserPoints::logAction($profile_id, $operation, $count);
    }

    /**
     * оплата выполненной фантазии с учётом комисси
     * @param int $count
     * @param int $sender_id
     * @param int $recipient_id
     * @param int $params
     * @return boolean
     */
    public static function Transfer($amount, $sender_id, $recipient_id, $params = array()) {
        $action = 'payment made ​​fantasy';
        self::PaymentKeys($amount, $sender_id, $action);
         $amount = $amount - ($amount*SK_Config::section('finance_key')->get('fee'))/100;//вычет комисии
        self::AddKeys($amount, $recipient_id, $action);
        //записываем тому у кого сняли
         app_UserPoints::logAction($profile_id, $operation, $count, null, '-', $recipient_id);
         //записываем тому кому начислили
         app_UserPoints::logAction($recipient_id, $operation, null, '+', $profile_id);
        return true;
    }

    /**
     * проверяет достаточно ли средств на счету для совершения оплаты
     * @param int $count
     * @param int $profile_id
     * @return boolean
     */
    public static function CheckBalans($count, $profile_id) {
        if (app_UserPoints::getProfilePointsBalance($profile_id) - $count > 0)
            return true;
        else
            return false;
    }
    
    /**
     * список операция в админке
     * @param type $limit
     * @param type $_sf
     * @param type $_so
     * @return type
     */
    public static function GetAllListAdmin($limit, $_sf = 'log_id', $_so = 'ASC') {
        $order = ($_sf != '' && $_so != '') ? "ORDER BY $_sf $_so" : '';
        $sql = "SELECT *  FROM `" . TBL_USER_POINT_ACTION_LOG . "` 
                        LEFT JOIN `" . TBL_PROFILE . "` 
                            ON `" . TBL_USER_POINT_ACTION_LOG . "`.`profile_id` = `" . TBL_PROFILE . "`.`profile_id`  $order $limit";
        return MySQL::fetchArray($sql);
    }

    /**
     * колличество записей в логе
     * @return type
     */
    public static function GetTotalAdmin() {
        $sql = "SELECT COUNT(`" . TBL_USER_POINT_ACTION_LOG . "`.`log_id`) FROM `" . TBL_USER_POINT_ACTION_LOG . "` 
                        LEFT JOIN `" . TBL_PROFILE . "`
                            ON `" . TBL_USER_POINT_ACTION_LOG . "`.`profile_id` = `" . TBL_PROFILE . "`.`profile_id`";
        return MySQL::fetchField($sql);
    }
    
    /**
     * список запросов на выплату
     * @param type $limit
     * @param type $_sf
     * @param type $_so
     * @return type
     */
    public static function GetRequestsPayment($limit, $_sf = 'id', $_so = 'ASC') {
        $order = ($_sf != '' && $_so != '') ? "ORDER BY $_sf $_so" : '';
        $sql = "SELECT *  FROM `" . TBL_CREDITS_PAYMENTS . "` 
                        LEFT JOIN `" . TBL_PROFILE . "` 
                            ON `" . TBL_CREDITS_PAYMENTS . "`.`profile_id` = `" . TBL_PROFILE . "`.`profile_id`  $order $limit";
        return MySQL::fetchArray($sql);
    }

    /**
     * колличество запросов на выплату
     * @return type
     */
    public static function GetRequestsPaymentCount() {
        $sql = "SELECT COUNT(`" . TBL_CREDITS_PAYMENTS . "`.`id`) FROM `" . TBL_CREDITS_PAYMENTS . "` 
                        LEFT JOIN `" . TBL_PROFILE . "`
                            ON `" . TBL_CREDITS_PAYMENTS . "`.`profile_id` = `" . TBL_PROFILE . "`.`profile_id`";
        return MySQL::fetchField($sql);
    }

    /**
     * подача заявки на выплату
     * @param type $amount
     * @param type $profile_id
     * @return int
     */
    public static function WithdrawKeys($amount, $profile_id) {
        if (!$profile_id) {
            return -3;
        }
        if(self::PaymentKeys($amount, $profile_id, 'WithdrawKeys')){
              $sql = MySQL::placeholder("INSERT INTO " . TBL_CREDITS_PAYMENTS . "(`profile_id`, `amount`, `date_request`) VALUES(?,?,?)", $profile_id, $amount, time());
              if(MySQL::query($sql)){
                  return 1;
              }
        }
    else 
        return -2;
    }
    
    /**
     * отмечает как выплаченный
     * @param type $id
     * @return boolean
     */
    public static function PayAdmin($id){
        
        $sql = "UPDATE ".TBL_CREDITS_PAYMENTS." SET status_operation = 'paid' WHERE id = $id";
         if(!MySQL::query($sql))
             return false;
         return true;
    }

}
