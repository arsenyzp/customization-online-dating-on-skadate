<?php

class app_FantasyWorck {
    /*
     * status
     * отклонили -1
     * ожидает решения 0
     * принят 1
     * выполнен ожидает проверки 2
     * выполнение принято 3
     * выполнение отклонено 4
     * отменил исполнитель 5
     */

    /*
     * status_update
     * 0 создано исполнителем
     * 1 ответ заказчика
     * 2 ответ исполнителя
     * 3 инвайт от заказчика
     */
    
    public static function GetStatusName($id){
        $name = array(
            1=>'accepted',
            2=>'Executed pending verification',
            3=>'Implementation of adopted',
            4=>'Performance declined',
            5=>'Canceled performer',
            '-1'=>'declined',
            0=>'Waiting for a decision',
        );
        return $name[$id];
    }

    /**
     * sending a request to run a fantasy
     */
    public static function SendRequest($id_fantasy) {
        $profile_id = SK_HttpUser::profile_id();
        if (!$profile_id || !intval($id_fantasy) > 0)
            return false;

        if (!self::CheckPerformer($id_fantasy))
            return false;

        $sql = "INSERT INTO " . TBL_FANTASY_LIST_WORCK . " ( `id_fantasy`, `id_performer`, `time_start`)
           VALUES('$id_fantasy', '$profile_id', '" . time() . "')";
        if (!MySQL::query($sql))
            return false;
        //notice
        $creator_info = app_Fantasy::GetFantasy($id_fantasy);
        $desc = "Your fantasy ". $creator_info['titel'] ." has a new contract request! <a href=\"/member/view_request.php?tabs=3\">(click to go to Fantasy Contract)</a>";
        app_Notice::AddNotice(NOTICE_PERFORMER_REQUEST, $desc, $creator_info['id_creator'], $profile_id);
        return true;
    }

    /**
     * cancellation request to run a fantasy
     */
    public static function DeleteReqest($id_fantasy) {
        $profile_id = SK_HttpUser::profile_id();
        if (!$profile_id || !intval($id_fantasy) > 0)
            return false;
        if (self::CheckPerformer($id_fantasy))
            return false;

        if (!self::CheckStatus($id_fantasy))
            return false;

        $sql = "DELETE FROM " . TBL_FANTASY_LIST_WORCK . " WHERE id_fantasy = $id_fantasy AND id_performer = $profile_id";
        if (!MySQL::query($sql))
            return false;

        return true;
    }

    public static function CreateContract($post_data) {
        $profile_id = SK_HttpUser::profile_id();
        $id_fantasy = intval($post_data['fantasy_id']);
        if (!$profile_id || !intval($id_fantasy) > 0)
            return false;
        if (self::CheckMyFantasy($id_fantasy))
            return false;
        if (!self::CheckStatus($id_fantasy))
            return false;
       $arr_data = explode('/', $post_data['completion_date']);
       $post_data['completion_date'] = strtotime($arr_data[1]."-".$arr_data[0]."-".$arr_data[2]) ;
        //добавление в список работ  
        $sql = "INSERT INTO " . TBL_FANTASY_LIST_WORCK . " ( `id_fantasy`, `id_performer`, `time_start`, `budget`, `price_video`,`completion_date`,`lenght_video`)
                VALUES('$id_fantasy', '$profile_id', '" . time() . "', '" . intval($post_data['budget']) . "', " . intval($post_data['price_video']) . ", " . $post_data['completion_date']. ", ".intval($post_data['lenght_video']).")";
      
        if (!MySQL::query($sql))
            return false;
        $id_contract = MySQL::insert_id();
        //notification
        $recipient = self::GetCreatorInfo($id_fantasy);
        $desc = "Your fantasy ". $recipient[0]['title'] ." has a new contract request! <a href=\"/member/view_request.php?tabs=3\">(click to go to Fantasy Contract)</a>";
        app_Notice::AddNotice(NOTICE_PERFORMER_REQUEST, $desc, $recipient[0]['profile_id'], $profile_id);
        //добавление сообщения
        if (strlen($post_data['messag']) > 1) {
           
            app_MailBox::sendMessage($profile_id, $recipient[0]['profile_id'], $post_data['messag'], $recipient[0]['title']);
            $id_messag = MySQL::insert_id();

            $sql = "INSERT INTO " . TBL_FANTASY_MESSAG . " ( `worck_id`, `message_id`)
                    VALUES('$id_contract', '$id_messag')";
            if (!MySQL::query($sql))
                return false;
        }
        return true;
    }

    /**
     * создание приглашения для исполнителя
     * @param array $post_data
     * @return boolean
     */
    public static function CreateInvite($post_data) {
        $profile_id = SK_HttpUser::profile_id();
        $id_fantasy = intval($post_data['fantasy_id']);
        if (!$profile_id || !intval($id_fantasy) > 0)
            return false;
        if (!self::CheckMyFantasy($id_fantasy))
            return false;
        //получение информации о фантазии
        $fantasy_info = app_Fantasy::GetFantasy($id_fantasy);
        //добавление в список работ
        $sql = "INSERT INTO " . TBL_FANTASY_LIST_WORCK . " ( `id_fantasy`, `id_performer`, `time_start`, `budget`, `price_video`, `status_update`, `completion_date`)
                VALUES('$id_fantasy', '" . intval($post_data['entity_id']) . "', '" . time() . "', '" . intval($post_data['budget']) . "', " . intval($fantasy_info[0]['price_video']) . ", 3, " . strtotime($post_data['completion_date']) . ")";
        if (!MySQL::query($sql))
            return false;
        $id_contract = MySQL::insert_id();
        //notice
        $text="invited to perform fantasy ".$fantasy_info[0]['title']." <a href=\"/member/sent_applications.php\">(click to go to Fantasy Contract)</a>";
        app_Notice::AddNotice(NOTICE_INVITED_YOU_TO_FANTASIZE, $text, $post_data['entity_id'], $profile_id);
        //добавление сообщения
        if (strlen($post_data['messag']) > 1) {
            app_MailBox::sendMessage($profile_id, $post_data['entity_id'], $post_data['messag'], $fantasy_info[0]['title']);
            $id_messag = MySQL::insert_id();
            $sql = "INSERT INTO " . TBL_FANTASY_MESSAG . " ( `worck_id`, `message_id`)
                    VALUES('$id_contract', '$id_messag')";
            if (!MySQL::query($sql))
                return false;
        }
        //отправка уведомления на почту
        app_MailNotice::InvitedToFantasize(SK_HttpUser::profile_id(), $post_data['entity_id'], $id_contract);
        return true;
    }

    /**
     * check whether the user is executing this fantasy
     * @param type $id_fantasy
     * @return boolean
     */
    public static function CheckPerformer($id_fantasy) {
        $profile_id = SK_HttpUser::profile_id();
        if (!$profile_id)
            return false;
        $sql = "SELECT COUNT(id) FROM " . TBL_FANTASY_LIST_WORCK . " WHERE id_fantasy = $id_fantasy AND id_performer = $profile_id";
        if (MySQL::fetchField($sql) > 0)
            return false;
        else
            return true;
    }

    /**
     * 
     * @param type $id_fantasy
     * @return boolean
     */
    public static function CheckStatus($id_fantasy) {
        $profile_id = SK_HttpUser::profile_id();
        $sql = "SELECT status FROM " . TBL_FANTASY_LIST_WORCK . " WHERE id_fantasy = $id_fantasy AND id_performer = $profile_id";
        if (MySQL::fetchField($sql) > 0)
            return false;
        else
            return true;
    }

    /**
     * list of all the fantasies
     * @param type $page_number
     * @param type $page_limit
     * @param int $type status worck
     * @return type
     */
    public static function GetAllWorks($page_number, $page_limit = null, $type = 0) {
        $profile_id = SK_HttpUser::profile_id();

        $where = "";
        switch ($type) {
            case 0:
                break;
            case 1:
                $where = " AND " . TBL_FANTASY_LIST_WORCK . ".status = 3";
                break;
            case 2:
                $where = " AND " . TBL_FANTASY_LIST_WORCK . ".status = 1";
                break;
            case 3:
                $where = " AND " . TBL_FANTASY_LIST_WORCK . ".status = -1";
                break;
            case 4:
                $where = " AND " . TBL_FANTASY_LIST_WORCK . ".status = 0 AND (status_update = 1 OR status_update = 3)";
                break;
        }

        $page_limit = $page_limit ? $page_limit : SK_Config::Section('video')->Section('other_settings')->display_media_list_limit;
        $page_number = $page_number ? $page_number : 1;
        $order_by = 'time_start';
        $order_type = 'DESC';

        $sql = SK_MySQL::placeholder("SELECT *, " . TBL_FANTASY_LIST_WORCK . ".status as worck_status, " . TBL_FANTASY_LIST_WORCK . ".id as worck_id 
            FROM " . TBL_FANTASY_LIST_WORCK . " RIGHT JOIN " . TBL_FANTASY . " ON 
           " . TBL_FANTASY_LIST_WORCK . ".id_fantasy = " . TBL_FANTASY . ".id  WHERE id_performer = $profile_id  $where
               ORDER BY $order_by $order_type LIMIT ?, ?", ($page_number - 1) * $page_limit, $page_limit);
        // echo $sql;
        return MySQL::fetchArray($sql);
    }

    /**
     * 
     * @param type $type
     * @return type
     */
    public static function GetTotal($type = 0) {
        $profile_id = SK_HttpUser::profile_id();

        $where = "";
        switch ($type) {
            case 0:
                break;
            case 1:
                $where = " AND " . TBL_FANTASY_LIST_WORCK . ".status = 3";
                break;
            case 2:
                $where = " AND " . TBL_FANTASY_LIST_WORCK . ".status = 1";
                break;
            case 3:
                $where = " AND " . TBL_FANTASY_LIST_WORCK . ".status = -1";
                break;
            case 4:
                $where = " AND " . TBL_FANTASY_LIST_WORCK . ".status = 0 AND (status_update = 1 OR status_update = 3)";
                break;
        }

        $sql = SK_MySQL::placeholder("SELECT COUNT(time_start) FROM " . TBL_FANTASY_LIST_WORCK . " RIGHT JOIN " . TBL_FANTASY . " ON 
           " . TBL_FANTASY_LIST_WORCK . ".id_fantasy = " . TBL_FANTASY . ".id  WHERE id_performer = $profile_id $where");

        return MySQL::fetchField($sql);
    }
    
    
     /**
     * список всех контрактов
     * @param type $page_number
     * @param type $page_limit
     * @param int $type status worck
     * @return type
     */
    public static function GetAllContract($limit, $_sf = 'id', $_so = 'ASC', $type = 0) {
        if($_sf == 'id'){
            $_sf = TBL_FANTASY_LIST_WORCK . ".status";
        }
        $where = "";
        switch ($type) {
            case 0:
                break;
            case 1:
                $where = " WHERE " . TBL_FANTASY_LIST_WORCK . ".status = 3";
                break;
            case 2:
                $where = " WHERE " . TBL_FANTASY_LIST_WORCK . ".status = 1";
                break;
            case 3:
                $where = " WHERE " . TBL_FANTASY_LIST_WORCK . ".status = -1";
                break;
            case 4:
                $where = " WHERE " . TBL_FANTASY_LIST_WORCK . ".status = 0 AND (status_update = 1 OR status_update = 3)";
                break;
        }
        
        $order = ($_sf != '' && $_so != '') ? "ORDER BY $_sf $_so" : '';
        $sql = SK_MySQL::placeholder("SELECT *, " . TBL_FANTASY_LIST_WORCK . ".status as worck_status, " . TBL_FANTASY_LIST_WORCK . ".id as worck_id 
            FROM " . TBL_FANTASY_LIST_WORCK . " RIGHT JOIN " . TBL_FANTASY . " ON 
           " . TBL_FANTASY_LIST_WORCK . ".id_fantasy = " . TBL_FANTASY . ".id   $where
          $order $limit");
        // echo $sql;
        return MySQL::fetchArray($sql);
    }

    /**
     * 
     * @param type $type
     * @return type
     */
    public static function GetTotalContract($type = 0) {
        $where = "";
        switch ($type) {
            case 0:
                break;
            case 1:
                $where = " WHERE " . TBL_FANTASY_LIST_WORCK . ".status = 3";
                break;
            case 2:
                $where = " WHERE " . TBL_FANTASY_LIST_WORCK . ".status = 1";
                break;
            case 3:
                $where = " WHERE " . TBL_FANTASY_LIST_WORCK . ".status = -1";
                break;
            case 4:
                $where = " WHERE " . TBL_FANTASY_LIST_WORCK . ".status = 0 AND (status_update = 1 OR status_update = 3)";
                break;
        }

        $sql = SK_MySQL::placeholder("SELECT COUNT(time_start) FROM " . TBL_FANTASY_LIST_WORCK . " RIGHT JOIN " . TBL_FANTASY . " ON 
           " . TBL_FANTASY_LIST_WORCK . ".id_fantasy = " . TBL_FANTASY . ".id  $where");

        return MySQL::fetchField($sql);
    }

    /**
     * check whether the user is the owner of this fantasy
     * @param type $id_fantasy
     * @return boolean
     */
    public static function CheckMyFantasy($id_fantasy) {
        $profile_id = SK_HttpUser::profile_id();
        if (!$profile_id)
            return false;
        $sql = "SELECT COUNT(id) FROM " . TBL_FANTASY . " WHERE id_creator = $profile_id AND id = $id_fantasy";
        if (MySQL::fetchField($sql) > 0)
            return true;
        else
            return false;
    }

    /**
     * number of unanswered questions in the fulfillment of my fantasy
     * @return boolean
     */
    public static function CountRequest() {
        $profile_id = SK_HttpUser::profile_id();
        if (!$profile_id)
            return false;
        $sql = "SELECT COUNT(id_performer) FROM " . TBL_FANTASY_LIST_WORCK . " RIGHT JOIN " . TBL_FANTASY . " 
               ON " . TBL_FANTASY_LIST_WORCK . ".id_fantasy = " . TBL_FANTASY . ".id 
                   WHERE id_creator = $profile_id AND " . TBL_FANTASY_LIST_WORCK . ".status = 0 AND status_update = 0";
        return MySQL::fetchField($sql);
    }
/**
 * колличество обновлённых контрактов
 * @return int
 */
    public static function UpdateContract() {
        $profile_id = SK_HttpUser::profile_id();
        if (!$profile_id)
            return false;
        $sql = "SELECT COUNT(id_performer) FROM " . TBL_FANTASY_LIST_WORCK . " RIGHT JOIN " . TBL_FANTASY . " 
               ON " . TBL_FANTASY_LIST_WORCK . ".id_fantasy = " . TBL_FANTASY . ".id 
                   WHERE id_performer = $profile_id AND " . TBL_FANTASY_LIST_WORCK . ".status = 0 AND status_update = 1";
        return MySQL::fetchField($sql);
    }

    /**
     * колличество обновлённых запросов
     * @return boolean
     */
    public static function UpdateRequest() {
        $profile_id = SK_HttpUser::profile_id();
        if (!$profile_id)
            return false;
        $sql = "SELECT COUNT(id_performer) FROM " . TBL_FANTASY_LIST_WORCK . " RIGHT JOIN " . TBL_FANTASY . " 
               ON " . TBL_FANTASY_LIST_WORCK . ".id_fantasy = " . TBL_FANTASY . ".id 
                   WHERE id_creator = $profile_id AND " . TBL_FANTASY_LIST_WORCK . ".status = 0 AND status_update = 2";
        return MySQL::fetchField($sql);
    }

    public static function NewInvate() {
        $profile_id = SK_HttpUser::profile_id();
        if (!$profile_id)
            return false;
        $sql = "SELECT COUNT(id_performer) FROM " . TBL_FANTASY_LIST_WORCK . " 
                   WHERE id_performer = $profile_id AND status = 0 AND status_update = 3";
        return MySQL::fetchField($sql);
    }

    public static function NewContract() {
        $profile_id = SK_HttpUser::profile_id();
        if (!$profile_id)
            return false;
        $sql = "SELECT COUNT(id_performer) FROM " . TBL_FANTASY_LIST_WORCK . "
                   WHERE  " . TBL_FANTASY_LIST_WORCK . ".status = 1 AND id_performer = $profile_id ";
        return MySQL::fetchField($sql);
    }

    /**
     * list of unanswered questions on the performance of my fantasies
     * @param int $page_number
     * @param int $page_limit
     * @param int $status
     * @param int $update
     * @return array
     */
    public static function GetAllRequest($page_number, $page_limit = null, $status = 0, $update = 0) {
        $where = "";
        if ($update != 0) {
            //$where = " AND status_update = ".  intval($update)." ";
            $where = " AND (status_update = 0 OR status_update = 2) AND " . TBL_FANTASY_LIST_WORCK . ".status = 0 ";
        }
        $profile_id = SK_HttpUser::profile_id();
        if (!$profile_id)
            return array();
        $page_limit = $page_limit ? $page_limit : SK_Config::Section('video')->Section('other_settings')->display_media_list_limit;
        $page_number = $page_number ? $page_number : 1;
        $order_by = 'time_start';
        $order_type = 'DESC';

        $sql = SK_MySQL::placeholder("SELECT *, " . TBL_FANTASY_LIST_WORCK . ".id as id_worck, " . TBL_FANTASY_LIST_WORCK . ".status as worck_status FROM " . TBL_PROFILE . " LEFT JOIN (" . TBL_FANTASY_LIST_WORCK . " RIGHT JOIN " . TBL_FANTASY . " ON 
           " . TBL_FANTASY_LIST_WORCK . ".id_fantasy = " . TBL_FANTASY . ".id) 
               ON " . TBL_PROFILE . ".profile_id =  " . TBL_FANTASY_LIST_WORCK . ".id_performer
               WHERE " . TBL_FANTASY . ".id_creator = $profile_id  $where
               ORDER BY $order_by $order_type LIMIT ?, ?", ($page_number - 1) * $page_limit, $page_limit);
        //AND " . TBL_FANTASY_LIST_WORCK . ".`status` = 0
        return MySQL::fetchArray($sql);
    }

    /**
     * Number of unanswered questions in the fulfillment of my dreams
     * @return array
     */
    public static function GetTotalRequest($status = 0, $update = 0) {
        $where = "";
        if ($update != 0) {
            //$where = " AND status_update = ".  intval($update)." ";
            $where = " AND (status_update = 0 OR status_update = 2) AND " . TBL_FANTASY_LIST_WORCK . ".status = 0 ";
        }
        $profile_id = SK_HttpUser::profile_id();
        $sql = "SELECT COUNT(id_performer) FROM " . TBL_FANTASY_LIST_WORCK . " RIGHT JOIN " . TBL_FANTASY . " 
               ON " . TBL_FANTASY_LIST_WORCK . ".id_fantasy = " . TBL_FANTASY . ".id   
                   WHERE id_creator = $profile_id $where";
        //AND " . TBL_FANTASY_LIST_WORCK . ".`status` = 0
        return MySQL::fetchField($sql);
    }

    /**
     * 
     * @param type $id
     */
    public static function GetRequest($id) {
        
    }

    /**
     * approval of the performer
     * @param type $id
     * @return boolean
     */
    public static function Accepted($id) {
        $profile_id = SK_HttpUser::profile_id();
        $sql = "SELECT id_creator, title FROM " . TBL_FANTASY . " RIGHT JOIN " . TBL_FANTASY_LIST_WORCK . " ON " . TBL_FANTASY_LIST_WORCK . ".id_fantasy = " . TBL_FANTASY . ".id WHERE " . TBL_FANTASY_LIST_WORCK . ".id = $id";
        $info = MySQL::fetchRow($sql);
        if ($info['id_creator'] != $profile_id)
            return false;
        $sql = "UPDATE " . TBL_FANTASY_LIST_WORCK . " SET status = 1 WHERE id = $id";
        if (!MySQL::query($sql))
            return FALSE;
        else {
            //снимаем со счёта заказчика ту сумму которая необходима для оплаты выполнения
            //если суммы недостаточно ошибка
            
            //записываем в список уведомлений
            $notice_description="Your fantasy ".$info['title'] ." request  has been accepted! <a href='/member/sent_applications.php'>View contract</a>";
            $sql = "SELECT id_performer FROM " . TBL_FANTASY_LIST_WORCK . "  WHERE id = $id";
            app_Notice::AddNotice(NOTICE_ACCEPTED_YOUR_REQUEST, $notice_description, (int)MySQL::fetchField($sql) , $profile_id);
            
        }
        return TRUE;
    }

    /**
     * Declined performer
     * @param type $id
     * @return boolean
     */
    public static function Decline($id) {
        $profile_id = SK_HttpUser::profile_id();
        $sql = "SELECT id_creator, title FROM " . TBL_FANTASY . " RIGHT JOIN " . TBL_FANTASY_LIST_WORCK . " ON " . TBL_FANTASY_LIST_WORCK . ".id_fantasy = " . TBL_FANTASY . ".id WHERE " . TBL_FANTASY_LIST_WORCK . ".id = $id";
        $info= MySQL::fetchRow($sql);
        if ($info['id_creator'] != $profile_id)
            return false;
        $sql = "UPDATE " . TBL_FANTASY_LIST_WORCK . " SET status = -1 WHERE id = $id";
        if (!MySQL::query($sql))
            return FALSE;
        else{
             //записываем в список уведомлений
              $notice_description="Your fantasy ".$info['title'] ." request  has been declined! <a href='/member/sent_applications.php'>View contract</a>";
            $sql = "SELECT id_performer " . TBL_FANTASY_LIST_WORCK . "  WHERE id = $id";
                app_Notice::AddNotice(NOTICE_DECLINED_YOUR_REQUEST, $notice_description, (int)MySQL::fetchField($sql) , $profile_id);
            return TRUE;
            }
    }

    /**
     * Cancel performer
     * @param type $id
     * @return boolean
     */
    public static function CancelRequest($id) {
        $profile_id = SK_HttpUser::profile_id();
        $sql = "SELECT id_performer FROM  " . TBL_FANTASY_LIST_WORCK . " WHERE " . TBL_FANTASY_LIST_WORCK . ".id = $id";
        if (MySQL::fetchField($sql) != $profile_id)
            return false;
        $sql = "UPDATE " . TBL_FANTASY_LIST_WORCK . " SET status = 5 WHERE id = $id";
        if (!MySQL::query($sql))
            return false;
        else
            return true;
    }
    
    public static function CancelRequestAdmin($video_id){
        //присваеваем видео исполнителю
        $fantasy_id = app_ProfileVideo::getVideoFantasyId($video_id);
        $performer_id = app_Fantasy::GetPerformer($fantasy_id);
        if(!$fantasy_id || !$performer_id)
            return false;
        app_ProfileVideo::ChangeOwner($video_id, $performer_id);
        //обновляем статус
         $sql = "UPDATE " . TBL_FANTASY_LIST_WORCK . " SET status = 5 
                           WHERE id_fantasy =  $fantasy_id 
                               AND  id_performer = $performer_id 
                                   AND status = 2";
        if (!MySQL::query($sql))
            return false;
        return true;
    }

    /**
     * status update fantasy worck
     * @param type $status
     * @param type $id_worck
     * @return boolean
     */
    public static function UpdateStatusWorck($status, $id_worck, $video_id) {
        $sql = "UPDATE " . TBL_FANTASY_LIST_WORCK . " SET status =$status, video_id = $video_id WHERE id = $id_worck";
        return MySQL::query($sql);
    }

    public static function UpdateCommentWorck($comment, $id_worck) {
        $sql = "UPDATE " . TBL_FANTASY_LIST_WORCK . " SET comment = '$comment'  WHERE id = $id_worck";
        return MySQL::query($sql);
    }

    /**
     * Get id
     * @param type $id_fantasy
     * @param type $id_performer
     * @return int
     */
    public static function GetWorckID($id_fantasy, $id_performer) {
        $sql = "SELECT id FROM " . TBL_FANTASY_LIST_WORCK . " WHERE id_fantasy = $id_fantasy AND id_performer = $id_performer LIMIT 0, 1";
        return MySQL::fetchField($sql);
    }

    /**
     * Get array
     * @param type $id_fantasy
     * @param type $id_performer
     * @return int
     */
    public static function GetWorck($worck_id) {
        $worck_id = intval($worck_id);
        if ($worck_id <= 0)
            return array();
        $sql = "SELECT * FROM " . TBL_FANTASY_LIST_WORCK . " WHERE id = $worck_id LIMIT 0, 1";
        return MySQL::fetchRow($sql);
    }

    /**
     * Get id
     * @param type $id_fantasy
     * @param type $id_performer
     * @return int
     */
    public static function GetFantasyID($worck_id) {
        $sql = "SELECT id_fantasy FROM " . TBL_FANTASY_LIST_WORCK . " WHERE id = $worck_id LIMIT 0, 1";
        return MySQL::fetchField($sql);
    }

    public static function GetWorckStatus($creator_id = 0, $performer_id) {
        if ($creator_id == 0 && $performer_id == 0)
            return false;
        if ($creator_id != 0) {
            $sql = "SELECT " . TBL_FANTASY_LIST_WORCK . ".status FROM " . TBL_FANTASY_LIST_WORCK . " RIGHT JOIN " . TBL_FANTASY . " 
                ON " . TBL_FANTASY_LIST_WORCK . ".id_fantasy = " . TBL_FANTASY . ".id WHERE " . TBL_FANTASY . ".id_creator = $creator_id ";
        } elseif ($performer_id != 0) {
            
        }
    }

    /**
     * the number of new completed fantasies
     */
    public static function NewFantasy() {
        $profile_id = SK_HttpUser::profile_id();
        if (!$profile_id)
            return false;
        $sql = "SELECT COUNT(id_performer) FROM " . TBL_FANTASY_LIST_WORCK . " RIGHT JOIN " . TBL_FANTASY . " 
               ON " . TBL_FANTASY_LIST_WORCK . ".id_fantasy = " . TBL_FANTASY . ".id 
                   WHERE id_creator = $profile_id AND " . TBL_FANTASY_LIST_WORCK . ".status = 2";
        return MySQL::fetchField($sql);
    }

    /**
     * Get current status (Worck)
     * отклонили -1
     * ожидает решения 0
     * принят 1
     * выполнен ожидает проверки 2
     * выполнение принято 3
     * выполнение отклонено 4
     * @param int $id
     * @return int
     */
    public static function GetFantasyStatus($id, $profile_id = 0) {

        if ($profile_id == 0) {
            $profile_id = SK_HttpUser::profile_id();
            if(!$profile_id)return false;
            $sql = "SELECT status FROM `" . TBL_FANTASY_LIST_WORCK . "` WHERE id_fantasy = $id AND id_performer = $profile_id";
            return MySQL::fetchField($sql);
        } else {
            $sql = "SELECT `" . TBL_FANTASY_LIST_WORCK . "`.status FROM `" . TBL_FANTASY_LIST_WORCK . "` LEFT JOIN `" . TBL_FANTASY . "` 
                            ON `" . TBL_FANTASY_LIST_WORCK . "`.id_fantasy = `" . TBL_FANTASY . "`.id  WHERE id_creator = $profile_id 
                                AND `" . TBL_FANTASY . "`.id = $id";
            return MySQL::fetchField($sql);
        }
    }

    /**
     * approval of the fantasy
     * @param type $fantasy_id
     * @return boolean
     */
    public static function ApproveWorck($fantasy_id) {
        $profile_id = SK_HttpUser::profile_id();
        $sql = "SELECT * FROM " . TBL_FANTASY_LIST_WORCK . " WHERE id_fantasy = $fantasy_id AND status = 2";
        $worck = MySQL::fetchRow($sql);
        $worck_id = $worck['id'];
        if (intval($worck_id) == 0)
            return false;
        //устанавливаем цену за просмотр согласовваную при оформление контракта
        $sql ="UPDATE ".TBL_PROFILE_VIDEO." SET view_price = ".intval($worck['price_video'])." WHERE fantasy_id = $fantasy_id";
            MySQL::query($sql);
        //обновляем статус контракта
        $sql = "UPDATE " . TBL_FANTASY_LIST_WORCK . " SET status = '3' WHERE id = $worck_id";
        MySQL::query($sql);
        //оплата выполнения
        //записываем в список уведомлений
        app_Notice::AddNotice(NOTICE_APPROVE_WORCK, '', $worck['id_performer'] , $profile_id);
                
        if (app_KuinKeys::Transfer( $worck['budget'] , $profile_id, $worck['id_performer']))
            return MySQL::query($sql);
        else
            return false;
    }

    /**
     * Returns list of new uploaded fantasy video
     *
     * @param integer $profile_id
     * @return array('list'=>array(),'total'=>integer)
     */
    public static function getProfileUploadedVideo($profile_id) {
        $profile_id = intval($profile_id);

        if (!$profile_id)
            return array();

        $query = SK_MySQL::placeholder("SELECT `video`.*,
			(SELECT COUNT(`video_id`) FROM `" . TBL_PROFILE_VIDEO_VIEW . "`
                            WHERE `video_id`=`video`.`video_id`) AS `view_count`,
			(SELECT COUNT(`id`) FROM `" . TBL_VIDEO_COMMENT . "` WHERE `entity_id`=`video`.`video_id`) AS `comment_count`,
			(SELECT ROUND(AVG(`score`),2) FROM `" . TBL_VIDEO_RATE . "` WHERE `entity_id`=`video`.`video_id`) AS `rate_score`,
			(SELECT COUNT(`id`) FROM `" . TBL_VIDEO_RATE . "` WHERE `entity_id`=`video`.`video_id`) AS `rate_count`
			FROM `" . TBL_PROFILE_VIDEO . "` AS `video`
                             LEFT JOIN `" . TBL_FANTASY_LIST_WORCK . "` ON 
                                `video`.fantasy_id =  `" . TBL_FANTASY_LIST_WORCK . "`.id_fantasy
			WHERE `" . TBL_FANTASY_LIST_WORCK . "`.status=2 AND `video`.`profile_id`=? 
			ORDER BY `video`.`upload_stamp` DESC", $profile_id);
        $res = SK_MySQL::query($query);

        $ret = array();
        while ($item = $res->fetch_assoc()) {
            $item['video_page'] = app_ProfileVideo::getVideoViewURL($item['hash']);
            $item['thumb_img'] = app_ProfileVideo::getVideoThumbnail($item);
            $ret[] = $item;
        }

        $return['list'] = $ret;

        $query = SK_MySQL::placeholder("SELECT COUNT( `video_id` ) FROM `" . TBL_PROFILE_VIDEO . "`
			WHERE `profile_id`=?", $profile_id);
        $return['total'] = SK_MySQL::query($query)->fetch_cell();

        return $return;
    }

    /**
     * 
     * @param int $fantasy_id
     * @return array
     */
    public static function GetCreatorInfo($fantasy_id) {
        $sql = "SELECT * FROM " . TBL_FANTASY . " 
                        LEFT JOIN " . TBL_PROFILE . " 
                            ON " . TBL_FANTASY . ".id_creator = " . TBL_PROFILE . ".profile_id 
                                WHERE " . TBL_FANTASY . ".id = $fantasy_id";
        return MySQL::fetchArray($sql);
    }
    
    /**
     * выбор ид исполнителя
     * @param type $fantasy_id
     * @param type $status
     * @return type
     */
    public static function GetIDPerformer($fantasy_id, $status=2){
        $sql = "SELECT id_performer FROM ".TBL_FANTASY_LIST_WORCK." WHERE  id_fantasy = $fantasy_id AND status = $status";
        return MySQL::fetchField($sql);
    }

}
