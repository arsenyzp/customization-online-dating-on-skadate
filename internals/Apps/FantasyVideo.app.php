<?php

class app_FantasyVideo {

       /**
        * загруженные пользователем
        * @param int $profile_id
        * @return array
        */
       public static function getProfileUploadedVideo($profile_id) {
              $profile_id = intval($profile_id);

              if (!$profile_id)
                     return array();

              $query = SK_MySQL::placeholder("SELECT `video`.*,
			(SELECT COUNT(`video_id`) FROM `" . TBL_PROFILE_VIDEO_VIEW . "`
                            WHERE `video_id`=`video`.`video_id`) AS `view_count`,
			(SELECT COUNT(`id`) FROM `" . TBL_VIDEO_COMMENT . "` WHERE `entity_id`=`video`.`video_id`) AS `comment_count`,
			(SELECT ROUND(AVG(`score`),2) FROM `" . TBL_VIDEO_RATE . "` WHERE `entity_id`=`video`.`video_id`) AS `rate_score`,
			(SELECT COUNT(`id`) FROM `" . TBL_VIDEO_RATE . "` WHERE `entity_id`=`video`.`video_id`) AS `rate_count`
			FROM `" . TBL_PROFILE_VIDEO . "` AS `video`
			WHERE `video`.`fantasy_id` = 0 AND `video`.`profile_id`=? 
			ORDER BY `video`.`upload_stamp` DESC", $profile_id);
              $res = SK_MySQL::query($query);

              $ret = array();
              while ($item = $res->fetch_assoc()) {
                     $item['video_page'] = app_ProfileVideo::getVideoViewURL($item['hash']);
                     $item['thumb_img'] = app_ProfileVideo::getVideoThumbnail($item);
                     $ret[] = $item;
              }

              $return['list'] = $ret;

              $query = SK_MySQL::placeholder("SELECT COUNT( `video_id` ) FROM `" . TBL_PROFILE_VIDEO . "`
			WHERE fantasy_id=0 AND `profile_id`=?", $profile_id);
              $return['total'] = SK_MySQL::query($query)->fetch_cell();

              return $return;
       }

       /**
        * все загруженные видео
        * @param int $profile_id
        * @return array
        */
       public static function getProfileFantasyVideo($profile_id) {
              $profile_id = intval($profile_id);

              if (!$profile_id)
                     return array();

              $query = SK_MySQL::placeholder("SELECT `video`.*,
			(SELECT COUNT(`video_id`) FROM `" . TBL_PROFILE_VIDEO_VIEW . "`
                            WHERE `video_id`=`video`.`video_id`) AS `view_count`,
			(SELECT COUNT(`id`) FROM `" . TBL_VIDEO_COMMENT . "` WHERE `entity_id`=`video`.`video_id`) AS `comment_count`,
			(SELECT ROUND(AVG(`score`),2) FROM `" . TBL_VIDEO_RATE . "` WHERE `entity_id`=`video`.`video_id`) AS `rate_score`,
			(SELECT COUNT(`id`) FROM `" . TBL_VIDEO_RATE . "` WHERE `entity_id`=`video`.`video_id`) AS `rate_count`
			FROM `" . TBL_PROFILE_VIDEO . "` AS `video`
			WHERE `video`.`fantasy_id` <> 0 AND `video`.`profile_id`=? 
			ORDER BY `video`.`upload_stamp` DESC", $profile_id);
              $res = SK_MySQL::query($query);

              $ret = array();
              while ($item = $res->fetch_assoc()) {
                     $item['video_page'] = app_ProfileVideo::getVideoViewURL($item['hash']);
                     $item['thumb_img'] = app_ProfileVideo::getVideoThumbnail($item);
                     $ret[] = $item;
              }

              $return['list'] = $ret;

              $query = SK_MySQL::placeholder("SELECT COUNT( `video_id` ) FROM `" . TBL_PROFILE_VIDEO . "`
			WHERE fantasy_id <> 0 AND `profile_id`=?", $profile_id);
              $return['total'] = SK_MySQL::query($query)->fetch_cell();

              return $return;
       }

       /**
        * купленные видео
        * @param int $profile_id
        * @return array
        */
       public static function getProfileBuyVideo($profile_id) {
              $profile_id = intval($profile_id);

              if (!$profile_id)
                     return array();

              $query = SK_MySQL::placeholder("SELECT `video`.*,
			(SELECT COUNT(`video_id`) FROM `" . TBL_PROFILE_VIDEO_VIEW . "`
                            WHERE `video_id`=`video`.`video_id`) AS `view_count`,
			(SELECT COUNT(`id`) FROM `" . TBL_VIDEO_COMMENT . "` WHERE `entity_id`=`video`.`video_id`) AS `comment_count`,
			(SELECT ROUND(AVG(`score`),2) FROM `" . TBL_VIDEO_RATE . "` WHERE `entity_id`=`video`.`video_id`) AS `rate_score`,
			(SELECT COUNT(`id`) FROM `" . TBL_VIDEO_RATE . "` WHERE `entity_id`=`video`.`video_id`) AS `rate_count`
			FROM `" . TBL_PROFILE_VIDEO . "` AS `video`    RIGHT JOIN `" . TBL_VIDEO_PURCHASED . "` ON 
                                `video`.video_id =  `" . TBL_VIDEO_PURCHASED . "`.video_id
			WHERE `" . TBL_VIDEO_PURCHASED . "`.`profile_id`=? 
			ORDER BY `video`.`upload_stamp` DESC", $profile_id);
              $res = SK_MySQL::query($query);

              $ret = array();
              while ($item = $res->fetch_assoc()) {
                     $item['video_page'] = app_ProfileVideo::getVideoViewURL($item['hash']);
                     $item['thumb_img'] = app_ProfileVideo::getVideoThumbnail($item);
                     $ret[] = $item;
              }

              $return['list'] = $ret;

              $query = SK_MySQL::placeholder("SELECT COUNT( `video`.`video_id` ) 
            FROM `" . TBL_PROFILE_VIDEO . "` AS `video`  
                RIGHT JOIN `" . TBL_VIDEO_PURCHASED . "` ON 
                                `video`.video_id =  `" . TBL_VIDEO_PURCHASED . "`.video_id
			WHERE `" . TBL_VIDEO_PURCHASED . "`.`profile_id`=?", $profile_id);
              $return['total'] = SK_MySQL::query($query)->fetch_cell();

              return $return;
       }

       /**
        * выполненные видео
        * @param int $profile_id
        * @return array
        */
       public static function getProfileWorc($profile_id, $tag_id=null) {
              $profile_id = intval($profile_id);
              $where ="";
              if($tag_id){
                     $where = " AND tag_id = ".$tag_id;
              }
              
              if (!$profile_id)
                     return array();

              $query = SK_MySQL::placeholder("SELECT `video`.*,
			(SELECT COUNT(`video_id`) FROM `" . TBL_PROFILE_VIDEO_VIEW . "`
                            WHERE `video_id`=`video`.`video_id`) AS `view_count`,
			(SELECT COUNT(`id`) FROM `" . TBL_VIDEO_COMMENT . "` WHERE `entity_id`=`video`.`video_id`) AS `comment_count`,
			(SELECT ROUND(AVG(`score`),2) FROM `" . TBL_VIDEO_RATE . "` WHERE `entity_id`=`video`.`video_id`) AS `rate_score`,
			(SELECT COUNT(`id`) FROM `" . TBL_VIDEO_RATE . "` WHERE `entity_id`=`video`.`video_id`) AS `rate_count`
			FROM `" . TBL_PROFILE_VIDEO . "` AS `video`    RIGHT JOIN `" . TBL_FANTASY . "` ON 
                                                      `video`.fantasy_id =  `" . TBL_FANTASY . "`.id RIGHT JOIN " . TBL_FANTASY_LIST_WORCK . " ON
                                                       `" . TBL_FANTASY . "`.`id` = " . TBL_FANTASY_LIST_WORCK . ".`id_fantasy`
                                                                LEFT JOIN ".TBL_VIDEO_TAG." AS `tag` ON `video`.`video_id`=`tag`.`entity_id`
			WHERE `" . TBL_FANTASY_LIST_WORCK . "`.`id_performer`=? AND
                                                     `" . TBL_FANTASY_LIST_WORCK . "`.`status`=3 $where
			ORDER BY `video`.`upload_stamp` DESC", $profile_id);
              $res = SK_MySQL::query($query);

              $ret = array();
              while ($item = $res->fetch_assoc()) {
                     $item['video_page'] = app_ProfileVideo::getVideoViewURL($item['hash']);
                     $item['thumb_img'] = app_ProfileVideo::getVideoThumbnail($item);
                     $ret[] = $item;
              }

              $return['list'] = $ret;

              $query = SK_MySQL::placeholder("SELECT COUNT( `video`.`video_id` ) 
            FROM `" . TBL_PROFILE_VIDEO . "` AS `video`  
                RIGHT JOIN `" . TBL_VIDEO_PURCHASED . "` ON 
                                `video`.video_id =  `" . TBL_VIDEO_PURCHASED . "`.video_id
                                          LEFT JOIN ".TBL_VIDEO_TAG." AS `tag` ON `video`.`video_id`=`tag`.`entity_id`
			WHERE `" . TBL_VIDEO_PURCHASED . "`.`profile_id`=? $where", $profile_id);
              $return['total'] = SK_MySQL::query($query)->fetch_cell();

              return $return;
       }

       public static function getProfileLikeVideo($profile_id, $tag_id=null) {
              $profile_id = intval($profile_id);
              $where ="";
              if($tag_id){
                     $where = " AND tag_id = ".$tag_id;
              }
              if (!$profile_id)
                     return array();

              $query = SK_MySQL::placeholder("SELECT `video`.*,
			(SELECT COUNT(`video_id`) FROM `" . TBL_PROFILE_VIDEO_VIEW . "`
                                                               WHERE `video_id`=`video`.`video_id`) AS `view_count`,
                                                            (SELECT COUNT(`video_id`) FROM `" . TBL_VIDEO_LIKE . "`
                                                               WHERE `video_id`=`video`.`video_id`) AS `like_count`,
			(SELECT COUNT(`id`) FROM `" . TBL_VIDEO_COMMENT . "` WHERE `entity_id`=`video`.`video_id`) AS `comment_count`,
			(SELECT ROUND(AVG(`score`),2) FROM `" . TBL_VIDEO_RATE . "` WHERE `entity_id`=`video`.`video_id`) AS `rate_score`,
			(SELECT COUNT(`id`) FROM `" . TBL_VIDEO_RATE . "` WHERE `entity_id`=`video`.`video_id`) AS `rate_count`
			FROM `" . TBL_PROFILE_VIDEO . "` AS `video`    LEFT JOIN `" . TBL_VIDEO_LIKE . "` as `like` ON 
                                                               `video`.video_id =  `like`.video_id 
                                                               LEFT JOIN ".TBL_VIDEO_TAG." AS `tag` ON `video`.`video_id`=`tag`.`entity_id`
                                                                      WHERE `like`.`profile_id`=? $where
                                                               GROUP BY `video`.`video_id`
			ORDER BY `video`.`upload_stamp` DESC ", $profile_id);
              $res = SK_MySQL::query($query);

              $ret = array();
              while ($item = $res->fetch_assoc()) {
                     $item['video_page'] = app_ProfileVideo::getVideoViewURL($item['hash']);
                     $item['thumb_img'] = app_ProfileVideo::getVideoThumbnail($item);
                     $ret[] = $item;
              }

              $return['list'] = $ret;

              $query = SK_MySQL::placeholder("SELECT COUNT( `video`.`video_id` ) 
            FROM `" . TBL_PROFILE_VIDEO . "` AS `video`  
                RIGHT JOIN `" . TBL_VIDEO_LIKE . "`  as `like` ON 
                                `video`.video_id =  `like`.video_id
                                 LEFT JOIN ".TBL_VIDEO_TAG." AS `tag` ON `video`.`video_id`=`tag`.`entity_id`
			WHERE `like`.`profile_id`=?  $where", $profile_id);
              $return['total'] = SK_MySQL::query($query)->fetch_cell();

              return $return;
       }
       
       public static function getProfileVideoByFantasyId($fantasy_id){
              $sql = "SELECT * FROM ".TBL_PROFILE_VIDEO." WHERE `fantasy_id` = ".  intval($fantasy_id);
              return MySQL::fetchRow($sql);
       }
       
       public static function Rate($video_id, $profile_id){
              $sql = MySQL::placeholder("SELECT `score` FROM ".TBL_VIDEO_RATE." WHERE `entity_id`=? AND `profile_id`=?", $video_id, $profile_id);
              return MySQL::fetchField($sql);
       }

}