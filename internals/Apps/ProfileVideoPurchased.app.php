<?php

class app_ProfileVideoPurchased {

    public static function BuyVideo($video_id) {
        $profile_id = SK_HttpUser::profile_id();
        if(!$profile_id)
             return false;
        $sql = "SELECT COUNT(id) FROM " . TBL_VIDEO_PURCHASED . " WHERE video_id = $video_id AND profile_id = $profile_id";
        if (MySQL::fetchField($sql) > 0) {
            return false;
        }
        
        //перечисление денег
        $sql = "SELECT * FROM " . TBL_PROFILE_VIDEO . " WHERE video_id = $video_id";
        $video_info = MySQL::fetchRow($sql);
        //расчёт оплат
       // $KuinkeyFee =SK_Config::section('finance_key')->get('kuinkey_fee_buy_video')*($video_info['view_price']/100);
        $PerformerFee = SK_Config::section('finance_key')->get('performer_fee_buy_video')*($video_info['view_price']/100);
        $OwnerFee =SK_Config::section('finance_key')->get('owner_fee_buy_video')*($video_info['view_price']/100);
        
        //снимаем с баланса покупателя
        if(!app_KuinKeys::PaymentKeys($video_info['view_price'], $profile_id, 'buy video'))
            return false;
        //записываем в лог
        app_UserPoints::logAction($profile_id, 'buy video', $video_info['view_price'], null, '-', $video_info['view_price']);
        //начисляем создателю видео (фантазии)
        app_KuinKeys::AddKeys($OwnerFee, $video_info['profile_id'], 'Buy created video');
         //записываем в лог
        app_UserPoints::logAction( $video_info['profile_id'], 'Buy created video',  $OwnerFee, null,'-',$profile_id);
        //начисляем исполнителю
        $performer = app_Fantasy::GetPerformer($video_info['fantasy_id']);
         //записываем в лог
        app_UserPoints::logAction($performer, 'Buy created video',  $PerformerFee, null,'-',$profile_id);
        if($performer)
        app_KuinKeys::AddKeys($PerformerFee, $performer , 'Buy created video');
        
        $sql = "INSERT INTO  " . TBL_VIDEO_PURCHASED . "(`profile_id`, `video_id`) VALUES($profile_id, $video_id)";
        if (!MySQL::query($sql))
            return false;
        //notice
        $video_owner_id = app_ProfileVideo::getVideoOwnerById($video_id);
        $desc='';
        app_Notice::AddNotice(NOTICE_PURCHASED_YOUR_VIDEO, $descl, $video_owner_id, $profile_id);
        return true;
    }

    /**
     * Проверяет являеться ли пользователь владельцем видео 
     * или купил ли он это видео 
     * @param int $video_id
     * @return boolean
     */
    public static function CheckPurchased($video_id) {
        $hash = app_ProfileVideo::getVideoHash($video_id);
        $profile_id = SK_HttpUser::profile_id();
         if(!$profile_id)
             return false;
        if ($profile_id && (app_ProfileVideo::getVideoOwner($hash) == $profile_id OR app_ProfileVideo::getVideoPerformer($hash) == $profile_id)) {//если владелец или производитель
            return true;
        }
        //проверям купил ли пользователь видео
        $sql = "SELECT COUNT(id) FROM " . TBL_VIDEO_PURCHASED . " WHERE video_id = $video_id AND profile_id = $profile_id";
        if (MySQL::fetchField($sql) > 0) {
            return true;
        }
        //если не куплено и он не владелец этого видео
        return false;
    }

}