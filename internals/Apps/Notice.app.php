<?php
/*
 * не закончил так как нет списка, доделать
 */
class app_Notice{
    
    /**
     * колличество новых уведомлений для аккаунта
     * @return type
     */
    
    public static function GetTotal(){
        $profile_id = SK_HttpUser::profile_id();
        $sql = "SELECT COUNT(notice_id) FROM " . TBL_PROFILE_NOTICE ." WHERE profile_id = $profile_id AND notice_status = 1";
        return MySQL::fetchField($sql);
    }
    
    /**
     * Добавление нового уведомление для аккаунта
     * @param int $notice_type тип уведомления
     * @param string $notice_description описание уведомления
     * @param int $profile_id для кого уведомление
     * @param int $initiator_id от кого уведомление
     */

    public static function AddNotice($notice_type, $notice_description, $profile_id, $initiator_id){
        $sql = "INSERT INTO ".TBL_PROFILE_NOTICE."(`profile_id`, `notice_type`, `notice_description`, `date_added`, `initiator_id`) VALUES(?, ?, '?', ?, ?)";
        $sql = MySQL::placeholder($sql, $profile_id, $notice_type, $notice_description, time(), $initiator_id);
        MySQL::query($sql); 
    }
    
    public static function DeleteNotice($id){
        $profile_id = SK_HttpUser::profile_id();
        if(!$profile_id){
            return false;
        }
         $sql = SK_MySQL::placeholder("DELETE FROM " . TBL_PROFILE_NOTICE . " WHERE  profile_id = ? AND notice_id = ?", $profile_id , $id);
          return MySQL::query($sql);
    }
    
    public static function GetNoticeList($page_number, $page_limit = null, $type = 0){
        $profile_id = SK_HttpUser::profile_id();
        $page_limit = $page_limit ? $page_limit : SK_Config::Section('video')->Section('other_settings')->display_media_list_limit;
        $page_number = $page_number ? $page_number : 1;
        $order_by = 'date_added ';
        $order_type = 'DESC';
        $sql="UPDATE " . TBL_PROFILE_NOTICE . " SET notice_status = 0
                WHERE " . TBL_PROFILE_NOTICE . ".profile_id = $profile_id ";
        MySQL::query($sql);
        
        $sql = SK_MySQL::placeholder("SELECT * FROM " . TBL_PROFILE_NOTICE . " LEFT JOIN ".TBL_PROFILE." 
            ON " . TBL_PROFILE_NOTICE . ".initiator_id = ".TBL_PROFILE.".profile_id
                WHERE " . TBL_PROFILE_NOTICE . ".profile_id = $profile_id 
               ORDER BY $order_by $order_type LIMIT ?, ?", ($page_number - 1) * $page_limit, $page_limit);
          return MySQL::fetchArray($sql);
    }
}