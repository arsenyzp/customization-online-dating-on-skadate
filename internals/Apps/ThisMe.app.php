<?php

class app_ThisMe {

    public static function getMediaList($profile_id, $page_number=0, $page_limit = null) {
        $page_number = intval($page_number);
        $page_limit = intval($page_limit);
        $page_limit = $page_limit ? $page_limit : SK_Config::Section('video')->Section('other_settings')->display_media_list_limit;
        $page_number = $page_number ? $page_number : 1;
        $query = SK_MySQL::placeholder("SELECT  `photo`.* FROM  ".TBL_PROFILE_PHOTO." as `photo` WHERE `profile_id` =  ? AND `number` > 0
            GROUP BY  `photo`.`photo_id`
            ORDER BY `photo`.`photo_id` DESC", $profile_id);

        $res_photo = MySQL::fetchArray($query);
        
        $query = SK_MySQL::placeholder("SELECT  `video`.*, `video`.`upload_stamp` as `added_stamp`  FROM  ".TBL_PROFILE_VIDEO." as `video` WHERE `profile_id` =  ?
            GROUP BY  `video`.`video_id`
            ORDER BY `video`.`video_id` DESC", $profile_id);

        $res_video = MySQL::fetchArray($query);
        
        $res = array_merge($res_photo, $res_video);
        
        function myCmp($a, $b) {
            if ($a['added_stamp'] === $b['added_stamp']) return 0;
            return $a['added_stamp'] < $b['added_stamp'] ? 1 : -1;
        }

        uasort($res, 'myCmp');
        
        
        /*
        $return['list'] = $list;

        $query = SK_MySQL::placeholder("SELECT COUNT( `video_id` ) FROM `" . TBL_PROFILE_VIDEO . "` AS `video`
			INNER JOIN `" . TBL_PROFILE . "` AS `profile` USING (`profile_id`)
            LEFT JOIN `" . TBL_VIDEO_CATEGORY . "` AS `c` ON (`video`.`category_id`=`c`.`category_id`)
			WHERE " . self::sqlActiveCond('video') . " AND " . app_Profile::SqlActiveString('profile') . $category_cond . " ");

        $return['total'] = SK_MySQL::query($query)->fetch_cell();

        $return['page'] = $page_number;
        */
        return $res;
    }
    
      public static function GetVideoId($iter = 0, $profile_id = null){
             
         if($profile_id){
                $limit = intval($iter).", ".(intval($iter)+1);
           $sql = "SELECT ".TBL_PROFILE_VIDEO.".video_id, ".TBL_PROFILE_VIDEO.".hash, COUNT( ".TBL_PROFILE_VIDEO_VIEW. ".video_id) as count 
                FROM ".TBL_PROFILE_VIDEO." 
                    LEFT JOIN ".TBL_PROFILE_VIDEO_VIEW. " 
                        ON  ".TBL_PROFILE_VIDEO.".video_id = ".TBL_PROFILE_VIDEO_VIEW. ".video_id
                            WHERE  ".TBL_PROFILE_VIDEO.".profile_id = $profile_id AND fantasy_id = 0
                                GROUP BY ".TBL_PROFILE_VIDEO.".video_id 
                                    ORDER BY count DESC 
                                        LIMIT ".$limit;
         }
        return MySQL::fetchRow($sql);
    }
    
    public static function SavePhoto($photo_id, $title, $description){
           $profile_id = SK_HttpUser::profile_id();
           if(!$profile_id)
                  return false;
           $sql = MySQL::placeholder("UPDATE ". TBL_PROFILE_PHOTO ." SET `name` = '?', `description` = '?' WHERE `photo_id`=? AND `profile_id`=?", $title, $description, $photo_id, $profile_id);
           return MySQL::query($sql);
    }
    
    public static function SaveVideo($video_id, $title, $description){
           $profile_id = SK_HttpUser::profile_id();
           if(!$profile_id)
                  return false;
            $sql = MySQL::placeholder("UPDATE ". TBL_PROFILE_VIDEO ." SET `title` = '?', `description` = '?' WHERE `video_id`=? AND `profile_id`=?", $title, $description, $video_id, $profile_id);
           return MySQL::query($sql);
    }

}
