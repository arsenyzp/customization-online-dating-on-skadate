<?php

class app_Fantasy {

    /**
     * 
     * @return array
     */
    public static function GetAllList($page_number, $page_limit = null, $all = true, $param = array()) {
        $profile_id = ($param['profile_id'] >0)?$param['profile_id']:SK_HttpUser::profile_id();
        $where = "";
        if (isset($param['tag'])) {
            //из-за изменения поиска чтобы не менять дальше получаем тесктовое значение тега по переданному ид
            $param['tag'] = app_Tags::GetFantasyTagById($param['tag']);
            $where = " tag LIKE '" . $param['tag'] . "%' ";
        }


        $page_limit = $page_limit ? $page_limit : SK_Config::Section('video')->Section('other_settings')->display_media_list_limit;
        $page_number = $page_number ? $page_number : 1;
        $order_by = TBL_FANTASY . '.id';
        $order_type = 'DESC';
        
          if(isset($param['order_by'])){
             $order_by = TBL_FANTASY .".". $param['order_by'];
         }
        
        
        if ($profile_id && !$all) {//мои фантазии
            if (isset($param['tag'])) {
                $where = " AND " . TBL_FANTASY_TAGS . ".tag LIKE '" . $param['tag'] . "' ";
            }
            if (isset($param['tag_list'])) {
                $where = " AND " . TBL_FANTASY_TAGS . ".id IN(" . $param['tag_list'] . ") ";
            }
             if (isset($param['cat_id'])) {
                $where = " AND " . TBL_FANTASY . ".category_id = " . $param['cat_id'] ;
            }
            $sql = SK_MySQL::placeholder("SELECT *, " . TBL_FANTASY . ".id as id_item FROM " . TBL_FANTASY . " 
                LEFT JOIN (" . TBL_FANTASY_LIST_TAGS . " 
                    LEFT JOIN " . TBL_FANTASY_TAGS . " 
                        ON " . TBL_FANTASY_LIST_TAGS . ".id_tag = " . TBL_FANTASY_TAGS . ".id  )  ON " . TBL_FANTASY . ".id =  " . TBL_FANTASY_LIST_TAGS . ".id_fantasy
                                    WHERE id_creator = $profile_id  $where GROUP BY " . TBL_FANTASY . ".id  ORDER BY $order_by $order_type LIMIT ?, ?", ($page_number - 1) * $page_limit, $page_limit);
        } else {//все  списком
             $where = " WHERE " . TBL_FANTASY . ".private = 0";
            if (isset($param['tag'])) {
                $where = " WHERE " . TBL_FANTASY_TAGS . ".tag LIKE '" . $param['tag'] . "'  AND private = 0";
            }
            if (isset($param['tag_list'])) {
                $where = " WHERE " . TBL_FANTASY_TAGS . ".id IN(" . $param['tag_list'] . ")   AND private = 0";
            }
              if (isset($param['cat_id'])) {
                $where = " WHERE " . TBL_FANTASY . ".category_id = " . $param['cat_id'] ;
            }
            
            $sql = SK_MySQL::placeholder("SELECT *, " . TBL_FANTASY . ".id as id_item 
                FROM " . TBL_FANTASY . " 
                LEFT JOIN (" . TBL_FANTASY_LIST_TAGS . " 
                    LEFT JOIN " . TBL_FANTASY_TAGS . " 
                        ON " . TBL_FANTASY_LIST_TAGS . ".id_tag = " . TBL_FANTASY_TAGS . ".id  ) ON " . TBL_FANTASY . ".id =  " . TBL_FANTASY_LIST_TAGS . ".id_fantasy
                            RIGHT JOIN " . TBL_PROFILE . " ON(" . TBL_FANTASY . ".id_creator = ".TBL_PROFILE.".profile_id)
                            $where
                            GROUP BY " . TBL_FANTASY . ".id                            
                                ORDER BY $order_by $order_type 
                                    LIMIT ?, ?", ($page_number - 1) * $page_limit, $page_limit);
        }

        return MySQL::fetchArray($sql);
    }
    
    
     public static function GetAllPreferenceList($page_number, $page_limit = null, $all = true, $param = array()) {

        $where = "";
     
        //если пользователь не залогинен то выдаём просто список без учёта предпочтений
        $profile_id = ($param['profile_id'] >0)?$param['profile_id']:SK_HttpUser::profile_id();
        if(!$profile_id)
            return self::GetAllList ($page_number, $page_limit = null, $all = true, $param);
        
           if (isset($param['tag'])) {
            //из-за изменения поиска чтобы не менять дальше получаем тесктовое значение тега по переданному ид
            $param['tag'] = app_Tags::GetFantasyTagById($param['tag']);
            $where = " tag LIKE '" . $param['tag'] . "%' ";
        }
        
        $list_profile_tag = MySQL::fetchField("SELECT `" . TBL_PROFILE_EXTEND . "`.`tag` FROM `" . TBL_PROFILE_EXTEND . "` WHERE `" . TBL_PROFILE_EXTEND . "`.`profile_id` = $profile_id");


        $page_limit = $page_limit ? $page_limit : SK_Config::Section('video')->Section('other_settings')->display_media_list_limit;
        $page_number = $page_number ? $page_number : 1;
        $order_by = 'preference_tag';
        $order_type = 'DESC';
        $profile_id = SK_HttpUser::profile_id();
        if ($profile_id && !$all) {//мои фантазии
             $order_by =TBL_FANTASY .  '.id';
            if (isset($param['tag'])) {
                $where = " AND " . TBL_FANTASY_TAGS . ".tag LIKE '" . $param['tag'] . "' ";
            }
            if (isset($param['tag_list'])) {
                $where = " AND " . TBL_FANTASY_TAGS . ".id IN(" . $param['tag_list'] . ") ";
            }
             if (isset($param['cat_id'])) {
                $where = " AND " . TBL_FANTASY . ".category_id = " . $param['cat_id'] ;
            }
            $sql = SK_MySQL::placeholder("SELECT *, " . TBL_FANTASY . ".id as id_item FROM " . TBL_FANTASY . " 
                LEFT JOIN (" . TBL_FANTASY_LIST_TAGS . " 
                    LEFT JOIN " . TBL_FANTASY_TAGS . " 
                        ON " . TBL_FANTASY_LIST_TAGS . ".id_tag = " . TBL_FANTASY_TAGS . ".id  )  ON " . TBL_FANTASY . ".id =  " . TBL_FANTASY_LIST_TAGS . ".id_fantasy
                                    WHERE id_creator = $profile_id  $where GROUP BY " . TBL_FANTASY . ".id  ORDER BY $order_by $order_type LIMIT ?, ?", ($page_number - 1) * $page_limit, $page_limit);
        } else {//все  списком
             $where = " WHERE " . TBL_FANTASY . ".private = 0";
            if (isset($param['tag'])) {
                $where = " WHERE " . TBL_FANTASY_TAGS . ".tag LIKE '" . $param['tag'] . "'  AND private = 0";
            }
            if (isset($param['tag_list'])) {
                $where = " WHERE " . TBL_FANTASY_TAGS . ".id IN(" . $param['tag_list'] . ")   AND private = 0";
            }
              if (isset($param['cat_id'])) {
                $where = " WHERE " . TBL_FANTASY . ".category_id = " . $param['cat_id'] ;
            }
            
            $sql = SK_MySQL::placeholder("SELECT *, " . TBL_FANTASY . ".id as id_item,
            
    (SELECT COUNT(`".TBL_FANTASY_LIST_TAGS."`.`id`) FROM `".TBL_FANTASY_LIST_TAGS."` WHERE 
     ".( (strlen($list_profile_tag)>2)? "`".TBL_FANTASY_LIST_TAGS."`.`id_tag`   IN ($list_profile_tag )  AND  ":"") ."
            `".TBL_FANTASY."`.`id` = `".TBL_FANTASY_LIST_TAGS."`.`id_fantasy`) AS `preference_tag`

            FROM " . TBL_FANTASY . " 
                LEFT JOIN (" . TBL_FANTASY_LIST_TAGS . " 
                    LEFT JOIN " . TBL_FANTASY_TAGS . " 
                        ON " . TBL_FANTASY_LIST_TAGS . ".id_tag = " . TBL_FANTASY_TAGS . ".id  ) ON " . TBL_FANTASY . ".id =  " . TBL_FANTASY_LIST_TAGS . ".id_fantasy
                            LEFT JOIN ". TBL_PROFILE  ." ON ". TBL_PROFILE  .".profile_id = " . TBL_FANTASY . " .id_creator
                            $where
                            GROUP BY " . TBL_FANTASY . ".id                            
                                ORDER BY $order_by $order_type 
                                    LIMIT ?, ?", ($page_number - 1) * $page_limit, $page_limit);
        }

        return MySQL::fetchArray($sql);
    }

    public static function GetTotal($all = true, $param = array()) {
       $profile_id = ($param['profile_id'] >0)?$param['profile_id']:SK_HttpUser::profile_id();
        $where = "";
        if ($profile_id && !$all) {
            if (isset($param['tag'])) {
                $where = " AND " . TBL_FANTASY_TAGS . ".tag LIKE '" . $param['tag'] . "'";
            }
            if (isset($param['tag_list'])) {
                $where = " AND " . TBL_FANTASY_TAGS . ".id IN(" . $param['tag_list'] . ") ";
            }
              if (isset($param['cat_id'])) {
                $where = " AND " . TBL_FANTASY . ".category_id = " . $param['cat_id'] ;
            }
            $sql = "SELECT COUNT( " . TBL_FANTASY . ".id) FROM " . TBL_FANTASY . " 
                LEFT JOIN (" . TBL_FANTASY_LIST_TAGS . " 
                    LEFT JOIN " . TBL_FANTASY_TAGS . " 
                        ON " . TBL_FANTASY_LIST_TAGS . ".id_tag = " . TBL_FANTASY_TAGS . ".id  )  ON " . TBL_FANTASY . ".id =  " . TBL_FANTASY_LIST_TAGS . ".id_fantasy $where
                            WHERE id_creator = $profile_id  
                                GROUP BY " . TBL_FANTASY . ".id  ";
        } else {
            if (isset($param['tag'])) {
                $where = " WHERE " . TBL_FANTASY_TAGS . ".tag LIKE '" . $param['tag'] . "' AND private = 0";
            }
            if (isset($param['tag_list'])) {
                $where = " WHERE " . TBL_FANTASY_TAGS . ".id IN(" . $param['tag_list'] . ") AND private = 0";
            }
              if (isset($param['cat_id'])) {
                $where = " WHERE " . TBL_FANTASY . ".category_id = " . $param['cat_id'] ."AND private = 0";
            }
            $sql = "SELECT COUNT( " . TBL_FANTASY . ".id) FROM " . TBL_FANTASY . " 
                LEFT JOIN (" . TBL_FANTASY_LIST_TAGS . " 
                    LEFT JOIN " . TBL_FANTASY_TAGS . " 
                        ON " . TBL_FANTASY_LIST_TAGS . ".id_tag = " . TBL_FANTASY_TAGS . ".id  ) ON " . TBL_FANTASY . ".id =  " . TBL_FANTASY_LIST_TAGS . ".id_fantasy $where
                            GROUP BY " . TBL_FANTASY . ".id ";
        }
        return MySQL::fetchField($sql);
    }

    /**
     * 
     */
    public static function GetListMyFantasy($profile_id = 0, $params = array()) {
        $profile_id = $profile_id <= 0 ? SK_HttpUser::profile_id() : $profile_id;
        if(!$profile_id)
                        return array();
        $sql = "SELECT * FROM " . TBL_FANTASY . " WHERE id_creator = $profile_id";
        return MySQL::fetchArray($sql);
    }

    /**
     * 
     */
    public static function GetAllListAdmin($limit, $_sf = 'id', $_so = 'ASC') {
        $order = ($_sf != '' && $_so != '') ? "ORDER BY $_sf $_so" : '';
        $sql = "SELECT * FROM " . TBL_FANTASY . " $order $limit";
        return MySQL::fetchArray($sql);
    }

    /**
     * 
     * @param type $id_fantasy
     * @return array tags
     *      
     */
    public static function GetTagFantasy($id) {
        $sql = "SELECT * FROM " . TBL_FANTASY_TAGS . " LEFT JOIN " . TBL_FANTASY_LIST_TAGS . " 
            ON " . TBL_FANTASY_TAGS . ".id = " . TBL_FANTASY_LIST_TAGS . ".id_tag  WHERE id_fantasy = $id";
        return MySQL::fetchArray($sql);
    }

    /**
     * 
     * @param type $id_fantasy
     * 
     * @return array performer (array(tags))
     */
    public static function GetTagPerformer($id) {
        $sql = "SELECT * FROM " . TBL_FANTASY_PERFORMER_LIST . " WHERE id_fantasy = $id";
        $result = MySQL::fetchArray($sql);
        foreach ($result as $k => $v) {
            $sql = "SELECT " . TBL_PERFORMER_TAGS . ".id, tag FROM " . TBL_PERFORMER_TAGS . " LEFT JOIN " . TBL_PERFORMER_LIST_TAGS . " 
                ON " . TBL_PERFORMER_TAGS . ".id = " . TBL_PERFORMER_LIST_TAGS . ".id_tag WHERE id_performer = " . $v['id_performer'];
            $result[$k]['list_tag'] = MySQL::fetchArray($sql);
        }
        return $result;
    }

    /**
     * 
     * @param type $id_fantasy
     * @return array 
     */
    public static function GetFantasyFile($id) {
        $sql = "SELECT * FROM `" . TBL_FANTASY_FILES . "` WHERE id_fantasy = $id";
        return MySQL::fetchArray($sql);
    }

    /**
     * 
     * @param type $id_fantasy
     * 
     * @return boolean result
     */
    public static function Delete($id) {
        //удаление файлов прикреплённых к фантазии
        $sql = "SELECT name FROM " . TBL_FANTASY_FILES . " WHERE id_fantasy = $id";
        $list_file = MySQL::fetchArray($sql);
        foreach ($list_file as $file) {
            @unlink(DIR_USERFILES_FANTASY . "/" . $file);
        }
        $sql = "DELETE FROM " . TBL_FANTASY_FILES . " WHERE id_fantasy = $id";
        MySQL::query($sql);

        $sql = "SELECT name FROM " . TBL_FANTASY_FILES . " WHERE id_fantasy = $id";
        $list_file = MySQL::fetchArray($sql);
        foreach ($list_file as $file) {
            @unlink(DIR_USERFILES_FANTASY . "/" . $file);
        }

        //удаление информации о тегах исполнителей
        $sql = "DELETE FROM " . TBL_PERFORMER_LIST_TAGS . " WHERE id_performer IN (SELECT id FROM " . TBL_FANTASY_PERFORMER_LIST . " 
            WHERE id_fantasy = $id) ";
        MySQL::query($sql);
        $sql = "DELETE FROM " . TBL_FANTASY_PERFORMER_LIST . " WHERE id_fantasy = $id";
        MySQL::query($sql);

        //удвляем список тегов
        $sql = "DELETE FROM " . TBL_FANTASY_LIST_TAGS . " WHERE id_fantasy = $id";
        MySQL::query($sql);

        //удаляем заявки на работу
        $sql = "DELETE FROM " . TBL_FANTASY_LIST_WORCK . " WHERE id_fantasy = $id";
        MySQL::query($sql);

        //удаляем фантазию
        $sql = "DELETE FROM " . TBL_FANTASY . " WHERE id = $id";
        MySQL::query($sql);
    }

    /*
     * $tag_list_fantasy array(id_tag)
     * $array_performer = array(
     *      array(
     *           id_tag_performer,
     *           id_tag_performer,
     *           id_tag_performer
     *      ),
     *       array(
     *           id_tag_performer
     *      ),
     *       array(
     *           id_tag_performer
     *      ),
     * );
     */
//category
    public static function Create($id_creator, $data) {
        $profile_id = SK_HttpUser::profile_id();
        if (!$profile_id || intval($id_creator) != $profile_id)
            return false;

        $title = trim($data['title']);
        $description = trim($data['description']);
        $array_performer = $data['perfomers'];
        $tag_list_fantasy = explode(',', trim($data['tags']));
        $budget = trim($data['budget']);
        $price_video = intval($data['price_video']);
        $lenght_video = intval($data['length_video']);
        $buggest_null = trim($data['buggest_null']);
        $file_array = $data['file'];
        $data_fantasy_ex = trim($data['data_fantasy_ex']);
        $ptivate = trim($data['fantasy_private']);
        $category = intval($data['category']);
        //если фантазия приватная снимаем необходимую сумму со счёта
        if($ptivate>0){
           if(!app_KuinKeys::PaymentKeys(SK_Config::section('finance_key')->get('private_fantasy'), $profile_id))
               return false;
        }

        $sql = SK_MySQL::placeholder("INSERT INTO `" . TBL_FANTASY . "`(`title`, `description`, `id_creator`, `type`, `private`, `date_create`, `budget`, `data_ex`, `price_video`, `length_video`,`category_id`) 
            VALUES ('?','?', ?,'?',?,?,?,?,?,?,?)", $title, $description, $id_creator, 'new', $ptivate, time(), $budget, strtotime($data_fantasy_ex), $price_video, $lenght_video,$category);
        if (!SK_MySQL::query($sql))
            return false;
        $id_new_fantasy = SK_MySQL::insert_id();
        /* заносим теги фантазии */
        $sql = "INSERT INTO `" . TBL_FANTASY_LIST_TAGS . "`(`id_fantasy`, `id_tag`) VALUES";
        $array_insert = array();
        foreach ($tag_list_fantasy as $tag_id) {
            $array_insert[] = "('$id_new_fantasy', '" . intval($tag_id) . "')";
        }
        $sql .= implode(',', $array_insert) . ";";
        if (!SK_MySQL::query($sql))
            return false;
        /* заносим описание исполнителей */
        foreach ($array_performer as $performer) {
            $sql = "INSERT INTO " . TBL_FANTASY_PERFORMER_LIST . "(`id_fantasy`) VALUES('$id_new_fantasy')";
            if (!SK_MySQL::query($sql))
                return false;
            $id_new_performer = SK_MySQL::insert_id();
            $tags = explode(',', $performer);
            foreach ($tags as $tag_id) {
                $sql = "INSERT INTO " . TBL_PERFORMER_LIST_TAGS . "(`id_performer`, `id_tag`) VALUES('$id_new_performer', '$tag_id')";
                SK_MySQL::query($sql);
            }
        }
        /* заносим файлы */
        foreach ($file_array as $file) {
            if ($file != "")
                self::SaveFile($file, $id_new_fantasy);
        }
      
        //если уже определён исполнитель
        if(intval($_SESSION['performer_id'])>0){
            $data_invite = array('fantasy_id'=>$id_new_fantasy);
            $data_invite['entity_id'] = intval($_SESSION['performer_id']);
            unset($_SESSION['performer_id']);
            $data_invite['budget'] = $budget;
            $data_invite['price_video'] = $price_video;
            $data_invite['completion_date'] = $data['completion_date'];
            app_FantasyWorck::CreateInvite($data_invite);
        }
        
        //уведомление на почту
        $list_following = app_Bookmark::Following($profile_id);
        if(count($following['profiles'])>0)
        foreach ( $list_following as $following)
            app_MailNotice::PostedNewFantasy($profile_id, $following['profiles']['profile_id']);
        
        return $id_new_fantasy;
    }

    public static function SaveFile($hash, $id_fantasy) {
        $id_creator = SK_HttpUser::profile_id();
        $file = new SK_TemporaryFile($hash);
        $sourse_patch = $file->getPath();
        $source_file = $file->getFileName();
        $dest_patch = DIR_USERFILES . "upload_fantasy/" . $source_file;
        @copy($sourse_patch, $dest_patch);
        $sql = "INSERT INTO `" . TBL_FANTASY_FILES . "`(`id_fantasy`, `name`, `id_creator`) VALUES ('$id_fantasy', '" . trim($source_file) . "', '$id_creator')";
        if (!SK_MySQL::query($sql))
            return false;
    }

    public static function Search() {
        
    }

    /**
     * 
     * @param int $id
     * @return array
     */
    public static function GetFantasy($id) {
        $sql = "SELECT * FROM " . TBL_FANTASY . " WHERE id= $id";
        return MySQL::fetchRow($sql);
    }

    public static function GetPerformer($fantasy_id) {
        $sql = "SELECT id_performer FROM ".TBL_FANTASY_LIST_WORCK ." 
                        WHERE id_fantasy = $fantasy_id 
                            AND (status = 3 OR status = 2)";
        $id = MySQL::fetchField($sql);
        $id = intval($id)>0 ? intval($id) : false;
         return $id;
    }
    
    public static function GetTags($id) {
        
    }

    /**
     * проверяет являеться ли пользователь создателем фантазии
     * @param type $id_fantasy
     * @param type $id_profile
     */
    public static function CheckOwn($id_fantasy, $id_profile) {
        $sql = "SELECT id FROM " . TBL_FANTASY . " WHERE id= " . intval($id_fantasy) . " AND id_creator = " . intval($id_profile);
        if (mysql_num_rows(mysql_query($sql)) == 0)
            return false;
        else
            return true;
    }

    public static function UpdateFantasy($data) {
        $profile_id = SK_HttpUser::profile_id();
        if (!$profile_id)
            return false;

        $id_fantasy = intval($data['id']);
        $title = trim($data['title']);
        $description = trim($data['description']);
        $array_performer = $data['perfomers'];
        $tag_list_fantasy = explode(',', trim($data['tags']));
        $budget = trim($data['budget']);
        $price_video = intval($data['price_video']);
        $lenght_video = floatval($data['length_video']);
        $buggest_null = trim($data['buggest_null']);
        $files_array = $data['files'];
        $newfile = $data['newfile'];
        $data_fantasy_ex = strtotime($data['data_fantasy_ex']);
        $data_fantasy_ex =  $data_fantasy_ex ==0 ?time(): $data_fantasy_ex;
        $ptivate = trim($data['fantasy_private']);
         $category = intval($data['category']);

        $sql =  MySQL::placeholder("UPDATE " . TBL_FANTASY . " SET `title`= '$title', 
            `description` = '?', 
            `budget` = '$budget', 
            `private` = '$ptivate',
            `data_ex` = $data_fantasy_ex,
            `price_video` =  $price_video,
             `length_video` =  $lenght_video,
               `category_id` = $category
             WHERE id= " . $id_fantasy . " AND id_creator = ?", $description,  $profile_id);
        if (!SK_MySQL::query($sql))
            return false;

        $sql = "DELETE FROM `" . TBL_FANTASY_LIST_TAGS . "` WHERE `id_fantasy` = $id_fantasy";
        if (!SK_MySQL::query($sql))
            return false;
        $sql = "INSERT INTO `" . TBL_FANTASY_LIST_TAGS . "`(`id_fantasy`, `id_tag`) VALUES";
        $array_insert = array();
        foreach ($tag_list_fantasy as $tag_id) {
            $array_insert[] = "('$id_fantasy', '" . intval($tag_id) . "')";
        }
        $sql .= implode(',', $array_insert) . ";";
        if (!SK_MySQL::query($sql))
            return false;

        /* update performer */
        $arr_id = self::GetTagPerformer($id_fantasy);
        $arr_delete = array();
        foreach ($arr_id as $k => $v) {
            $arr_delete[] = $v['id_performer'];
        }
        if (count($arr_delete) > 0) {
            $sql = "DELETE FROM `" . TBL_PERFORMER_LIST_TAGS . "` WHERE `id_performer` IN (" . implode(',', $arr_delete) . ")";
            if (!SK_MySQL::query($sql))
                return false;
        }
        $sql = "DELETE FROM `" . TBL_FANTASY_PERFORMER_LIST . "` WHERE `id_fantasy` = $id_fantasy";
        if (!SK_MySQL::query($sql))
            return false;
        foreach ($array_performer as $performer) {
            $sql = "INSERT INTO " . TBL_FANTASY_PERFORMER_LIST . "(`id_fantasy`) VALUES('$id_fantasy')";
            if (!SK_MySQL::query($sql))
                return false;
            $id_new_performer = SK_MySQL::insert_id();
            $tags = explode(',', $performer);
            foreach ($tags as $tag_id) {
                $sql = "INSERT INTO " . TBL_PERFORMER_LIST_TAGS . "(`id_performer`, `id_tag`) VALUES('$id_new_performer', '$tag_id')";
                SK_MySQL::query($sql);
            }
        }

        /* update file */
        $in = "";
        if (count($files_array) > 0) {
            $in = "NOT IN (" . implode(',', $files_array) . ")";
        }

        $sql = "DELETE FROM " . TBL_FANTASY_FILES . " WHERE id $in AND id_fantasy = $id_fantasy";
        if (!SK_MySQL::query($sql))
            return false;
        foreach ($newfile as $file) {
            if ($file != "")
                self::SaveFile($file, $id_fantasy);
        }
        return true;
    }
    
    public static function GetVideoHash($work_id){
           $work_id = intval($work_id);
           $status = 2;
           if(!$work_id){
                  return array();
           }
           
           $sql="SELECT  `video`.`hash`, `video`.`is_converted` FROM ".TBL_PROFILE_VIDEO." as `video` 
                            LEFT JOIN ".TBL_FANTASY_LIST_WORCK." as `work` 
                                   ON `video`.`fantasy_id` = `work`.`id_fantasy`
                                          WHERE `work`.`id` = $work_id AND `work`.`status` = 2";
           $res = MySQL::fetchRow($sql);
           $key = $res['is_converted'] == 'yes' ?$res['hash'] : false;
           return $key;
    }

}
