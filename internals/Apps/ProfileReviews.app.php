<?php

/*
 * review_type
 * 0 - отзыв заказчика
 * 1- отзыв исполнителя
 */

class app_ProfileReviews {

    /**
     * добавлние нового отзыва 
     */
    public static function AddReviews($post_data) {
        //уведомление на почту
        app_MailNotice::NewCommentPerformance($post_data['entity_id']);
        //запись в уведомления
        $notice_description = "Add a review about you. <a href=\"/member/view_review.php?mode=my\">View</a>";
        app_Notice::AddNotice(NOTICE_REVIEW_WORCK, $notice_description, $post_data['reporter_id'], $post_data['entity_id']);
        $sql = "INSERT INTO " . TBL_PROFILE_REVIEWS . "(`profile_id`, `sender_id`, `review`, `date_send`, `review_type`, `fantasy_id`) 
            VALUES(?, ?, '?', ?, ?, ?)";
        $sql = MySQL::placeholder($sql, $post_data['entity_id'], $post_data['reporter_id'], $post_data['text_review'], time(), $post_data['type'], $post_data['fantasy_id']);
        return MySQL::query($sql);
    }

    public static function GetTotal($profile_id=null) {
        $profile_id = ($profile_id) ? $profile_id : SK_HttpUser::profile_id();
        $sql = MySQL::placeholder("SELECT COUNT(reviews_id) FROM " . TBL_PROFILE_REVIEWS . " WHERE `profile_id`=?", $profile_id);
        return MySQL::fetchField($sql);
    }

    public static function GetReviews($page_number, $page_limit = null, $type = 'my', $profile_id=null) {
        $profile_id = ($profile_id!=null) ? $profile_id : SK_HttpUser::profile_id();
        $page_limit = $page_limit ? $page_limit : SK_Config::Section('video')->Section('other_settings')->display_media_list_limit;
        $page_number = $page_number ? $page_number : 1;
        $order_by = 'date_send ';
        $order_type = 'DESC';
        $where = " WHERE " . TBL_PROFILE_REVIEWS . ".profile_id = $profile_id";
        $join = TBL_PROFILE_REVIEWS . ".sender_id = " . TBL_PROFILE . ".profile_id ";
        if($type == 'all'){
             $where = " WHERE " . TBL_PROFILE_REVIEWS . ".sender_id = $profile_id";
             $join = TBL_PROFILE_REVIEWS . ".profile_id = " . TBL_PROFILE . ".profile_id ";
        }
        
        $sql = SK_MySQL::placeholder("SELECT * FROM " . TBL_PROFILE_REVIEWS . " 
                                                            LEFT JOIN " . TBL_PROFILE . " ON
                                                                $join
                                                                     $where
                                                                        ORDER BY $order_by $order_type 
                                                                            LIMIT ?, ?", ($page_number - 1) * $page_limit, $page_limit);
        return MySQL::fetchArray($sql);
    }

    public static function DeleteReviews() {
        
    }

    /**
     * проверяет был ли уже оставлен отзыв для этой работы
     * @param int $profile_id
     * @param int $entity_id
     * @param int $fantasy_id
     * @return boolean
     */
    public static function CheckReviews($profile_id, $fantasy_id) {
        $sql = "SELECT COUNT(reviews_id) FROM " . TBL_PROFILE_REVIEWS . " 
            WHERE sender_id = $profile_id 
                    AND fantasy_id = $fantasy_id";
        //profile_id = $entity_id   AND 
        if (MySQL::fetchField($sql) > 0)
            return false;
        else
            return true;
    }

}
