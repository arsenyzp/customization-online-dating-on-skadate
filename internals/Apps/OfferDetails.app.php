<?php

class app_OfferDetails {

    /**
     * 
     * @param int $worck_id
     * @return boolean
     */
    public static function GetMessagContract($worck_id) {
        if (intval($worck_id) <= 0)
            return array();    
        $profile_id = SK_HttpUser::profile_id();
        //отмечаем свои полученные сообщения как прочитанные
        $sql = SK_MySQL::placeholder("UPDATE `" . TBL_MAILBOX_CONVERSATION . "`
			LEFT JOIN (`" . TBL_MAILBOX_MESSAGE . "` RIGHT JOIN `" . TBL_FANTASY_MESSAG . "`
                            ON `" . TBL_MAILBOX_MESSAGE . "`.message_id = `" . TBL_FANTASY_MESSAG . "`.message_id) 
                                ON `" . TBL_MAILBOX_CONVERSATION . "`.conversation_id = `" . TBL_MAILBOX_MESSAGE . "`.conversation_id
                                    SET `ska_mailbox_conversation`.`bm_read` = 3
                            WHERE `" . TBL_FANTASY_MESSAG . "`.worck_id = $worck_id
                                AND " . TBL_MAILBOX_MESSAGE . ".recipient_id = $profile_id");
        MySQL::query($sql);
        //выбираем письма
        $sql = SK_MySQL::placeholder("SELECT * FROM `" . TBL_MAILBOX_CONVERSATION . "`
			LEFT JOIN (`" . TBL_MAILBOX_MESSAGE . "` RIGHT JOIN `" . TBL_FANTASY_MESSAG . "`
                            ON `" . TBL_MAILBOX_MESSAGE . "`.message_id = `" . TBL_FANTASY_MESSAG . "`.message_id) 
                                ON `" . TBL_MAILBOX_CONVERSATION . "`.conversation_id = `" . TBL_MAILBOX_MESSAGE . "`.conversation_id
                            WHERE `" . TBL_FANTASY_MESSAG . "`.worck_id = $worck_id");
        return MySQL::fetchArray($sql);
    }

    /*
     * status_update
     * 0 новый котракт
     * 1 обновил заказчик
     * 2 обновил исполнитель
     */

    /**
     * 
     * @param array $post_data
     */
    public static function UpdateCreator($post_data) {
        //проверяем кто создатель
        $profile_id = SK_HttpUser::profile_id();
        /*
          $sql = "SELECT id_creator FROM " . TBL_FANTASY . "
          RIGHT JOIN " . TBL_FANTASY_LIST_WORCK . "
          ON " . TBL_FANTASY_LIST_WORCK . ".id_fantasy = " . TBL_FANTASY . ".id
          WHERE " . TBL_FANTASY_LIST_WORCK . ".id = ".intval($post_data['worck_id'])."
          AND status_update = 0";
          if (MySQL::fetchField($sql) != $profile_id)
          return false;
         */
        if (!app_FantasyWorck::CheckMyFantasy($post_data['fantasy_id']))
            return false;
        //обновляем контракт
        $arr_data = explode('/', $post_data['completion_date']);
        $post_data['completion_date'] = strtotime($arr_data[1]."-".$arr_data[0]."-".$arr_data[2]) ;
        $sql = "UPDATE " . TBL_FANTASY_LIST_WORCK . " 
                    SET status_update = 1, 
                    budget = " . intval($post_data['budget']) . ",
                    completion_date = " . $post_data['completion_date'] . ",
                    price_video = " . intval($post_data['price_video']) . ",
                    lenght_video = ".  intval($post_data['lenght_video'])."
                    WHERE id =" . intval($post_data['worck_id']);
        if (!MySQL::query($sql))
            return FALSE;
        //отправляем сообщение если есть
        $creator = app_FantasyWorck::GetCreatorInfo(intval($post_data['fantasy_id']));
        $worck = app_FantasyWorck::GetWorck(intval($post_data['worck_id']));
        //уведомления
        $notice_description=" Your fantasy request has been edited! <a href='/member/sent_applications.php'>Click to view</a>";
        app_Notice::AddNotice(NOTICE_UPDATE_YOUR_CONTRACT, $notice_description, $worck['id_performer'], $profile_id);
        
        if (strlen($post_data['messag']) > 1) {
            app_MailBox::sendMessage($profile_id, $worck['id_performer'], $post_data['messag'], $creator[0]['title']);
            $id_messag = MySQL::insert_id();
            $sql = "INSERT INTO " . TBL_FANTASY_MESSAG . " ( `worck_id`, `message_id`)
                        VALUES('" . $post_data['worck_id'] . "', '$id_messag')";
            if (!MySQL::query($sql))
                return false;
        }
        //уведомление на почту
        app_MailNotice::ContractUpdate($worck['id_performer'],  intval($post_data['worck_id']));
        return TRUE;
    }

    public static function UpdatePerformer($post_data) {
        $profile_id = SK_HttpUser::profile_id();
        if (app_FantasyWorck::CheckMyFantasy($post_data['fantasy_id']))
            return false;
        //обновляем контракт
       $arr_data = explode('/', $post_data['completion_date']);
       $post_data['completion_date'] = strtotime($arr_data[1]."-".$arr_data[0]."-".$arr_data[2]) ;
        $sql = "UPDATE " . TBL_FANTASY_LIST_WORCK . " 
                    SET status_update = 2, 
                    budget = " . intval($post_data['budget']) . ",
                    completion_date = " . $post_data['completion_date'] . ",
                    price_video = " . intval($post_data['price_video']) . ",
                    lenght_video = ".  intval($post_data['lenght_video'])."
                    WHERE id =" . intval($post_data['worck_id']);
        if (!MySQL::query($sql))
            return FALSE;
        //отправляем сообщение если есть
        $creator = app_FantasyWorck::GetCreatorInfo(intval($post_data['fantasy_id']));
        //$worck = app_FantasyWorck::GetWorck(intval($post_data['worck_id']));
         //уведомления
        $notice_description="Your performer has requested a change in the contract. <a href='/member/view_request.php?tabs=3'>Click here to view</a>";
        app_Notice::AddNotice(NOTICE_UPDATE_YOUR_FANTASY, $notice_description, $creator[0]['profile_id'], $profile_id);
        if (strlen($post_data['messag']) > 1) {
            app_MailBox::sendMessage($profile_id, $creator[0]['profile_id'], $post_data['messag'], $creator[0]['title']);
            $id_messag = MySQL::insert_id();
            $sql = "INSERT INTO " . TBL_FANTASY_MESSAG . " ( `worck_id`, `message_id`)
                        VALUES('" . $post_data['worck_id'] . "', '$id_messag')";
            if (!MySQL::query($sql))
                return false;
        }
         //уведомление на почту
        app_MailNotice::ContractUpdate($creator[0]['profile_id'],  intval($post_data['worck_id']));
        return TRUE;
    }

}