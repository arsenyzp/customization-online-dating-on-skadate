<?php

class app_Like{
    
    public static function SetLike($video_id){
        $profile_id = SK_HttpUser::profile_id();
        if(!$profile_id)
            return false;
        $sql = "SELECT COUNT(id) FROM ".TBL_VIDEO_LIKE." WHERE video_id = $video_id AND profile_id = $profile_id";
        if((int)MySQL::fetchField($sql)>0)
            return false;
        $sql =  MySQL::placeholder("INSERT INTO ".TBL_VIDEO_LIKE."(`video_id`, `profile_id`, `timeStamp`) VALUES(?,?,?) ", $video_id, $profile_id, time());
        $video_owner_id = app_ProfileVideo::getVideoOwnerById($video_id);
        $notice_description = "";
        app_Notice::AddNotice(NOTICE_LIKE_YOUR_VIDEO, $notice_description, $video_owner_id, $profile_id);
        return MySQL::query($sql);
    }
    
    public static function GetTotalLike($video_id){
        if(!$video_id)
            return 0;
        $sql = "SELECT COUNT(id) FROM ".TBL_VIDEO_LIKE." WHERE video_id = $video_id";
        return intval(MySQL::fetchField($sql));
    }
    
}