<?php

class app_Tags {

    public static function GetAllTags($limit, $_sf = 'id', $_so = 'ASC') {
        $order = ($_sf != '' && $_so != '') ? "ORDER BY $_sf $_so" : '';
        $sql = "SELECT * FROM " . TBL_FANTASY_TAGS . " $order $limit";
        return MySQL::fetchArray($sql);
    }

    public static function GetAllTagsCount() {
        $sql = "SELECT COUNT(id) FROM " . TBL_FANTASY_TAGS;
        return MySQL::fetchField($sql);
    }
    
    public static function GetAllTagsFantasy($limit){
          $sql = "SELECT * FROM " . TBL_FANTASY_TAGS . " LIMIT 0,  $limit";
        return MySQL::fetchArray($sql);
    }

    public static function InsertTag($tag) {
        $sql = SK_MySQL::placeholder("SELECT COUNT(id) FROM  " . TBL_FANTASY_TAGS . " WHERE tag = '$tag'");
        if (MySQL::fetchField($sql) > 0)
            return false;
        $sql = SK_MySQL::placeholder("INSERT INTO ` " . TBL_FANTASY_TAGS . "`(`tag`) VALUES ('$tag')");
        return SK_MySQL::query($sql);
    }

    public static function DeleteTag($id) {
        $sql = SK_MySQL::placeholder("DELETE FROM  " . TBL_FANTASY_TAGS . " WHERE id = " . intval($id));
        return SK_MySQL::query($sql);
    }

    public static function UpdateTag($id, $value) {
        $sql = SK_MySQL::placeholder("UPDATE  " . TBL_FANTASY_TAGS . " SET tag = '$value'  WHERE id = " . intval($id));
        return SK_MySQL::query($sql);
    }

    /*     * ************************ */

    public static function PerformerGetAllTags($limit, $_sf = 'id', $_so = 'ASC') {
        $order = ($_sf != '' && $_so != '') ? "ORDER BY $_sf $_so" : '';
        $sql = "SELECT * FROM " . TBL_PERFORMER_TAGS . " $order $limit";
        return MySQL::fetchArray($sql);
    }

    public static function PerformerGetAllTagsCount() {
        $sql = "SELECT COUNT(id) FROM " . TBL_PERFORMER_TAGS;
        return MySQL::fetchField($sql);
    }

    public static function PerformerInsertTag($tag) {
        $sql = SK_MySQL::placeholder("SELECT COUNT(id) FROM " . TBL_PERFORMER_TAGS . " WHERE tag = '$tag'");
        if (MySQL::fetchField($sql) > 0)
            return false;
        $sql = SK_MySQL::placeholder("INSERT INTO `" . TBL_PERFORMER_TAGS . "`(`tag`) VALUES ('$tag')");
        return SK_MySQL::query($sql);
    }

    public static function PerformerDeleteTag($id) {
        $sql = SK_MySQL::placeholder("DELETE FROM " . TBL_PERFORMER_TAGS . " WHERE id = " . intval($id));
        return SK_MySQL::query($sql);
    }

    public static function PerformerUpdateTag($id, $value) {
        $sql = SK_MySQL::placeholder("UPDATE " . TBL_PERFORMER_TAGS . " SET tag = '$value'  WHERE id = " . intval($id));
        return SK_MySQL::query($sql);
    }

    public static function GetTagsSearch($q) {
        $sql = SK_MySQL::placeholder("SELECT id, tag as name FROM " . TBL_FANTASY_TAGS . " WHERE tag LIKE '$q%'  LIMIT 0, 20");
        $t = MySQL::fetchArray($sql);
        return $t;
    }

    public static function PerformerGetTagsSearch($q) {
        $sql = SK_MySQL::placeholder("SELECT id, tag as name FROM " . TBL_PERFORMER_TAGS . " WHERE tag LIKE '$q%' LIMIT 0, 20");
        $t = MySQL::fetchArray($sql);
        return $t;
    }

    public static function GetPerformerTagById($tag_id) {
        $sql = SK_MySQL::placeholder("SELECT tag FROM " . TBL_PERFORMER_TAGS . " WHERE id =?", $tag_id);
        return MySQL::fetchField($sql);
    }
    
    public static function GetFantasyTagById($tag_id) {
        $sql = SK_MySQL::placeholder("SELECT tag FROM " . TBL_FANTASY_TAGS . " WHERE id =?", $tag_id);
        return MySQL::fetchField($sql);
    }

    /**
     * добавляет список тегов к видео
     * @param int $video_id
     * @param string $tag 1,2,3,5
     * @return boolean
     */
    public static function SetVideoTag($video_id, $tag) {
        $array_tag = explode(',', $tag);
        foreach ($array_tag as $id_tag) {
            $sql = MySQL::placeholder("INSERT INTO " . TBL_VIDEO_TAG . "(`tag_id`, `entity_id`) VALUES(?,?)", $id_tag, $video_id);
            MySQL::query($sql);
        }
        return true;
    }

    public static function GetVideoTagById($video_id) {
        $sql = MySQL::placeholder("SELECT * FROM  " . TBL_VIDEO_TAG . " 
                                                        RIGHT JOIN ".TBL_FANTASY_TAGS." 
                                                            ON  " . TBL_VIDEO_TAG . ".tag_id = ".TBL_FANTASY_TAGS.".id  WHERE entity_id = ?", $video_id);
        return MySQL::fetchArray($sql);
    }
    
    public static function GetVideoIdByTag($tag_id){
        $sql = MySQL::placeholder("SELECT entity_id FROM " . TBL_VIDEO_TAG . " WHERE tag_id = ?", $tag_id);
        $res =  MySQL::fetchArray($sql);
        $t = array();
        foreach ($res as $v){
            $t[] = $v['entity_id'];
        }
        return $t;
    }
    
    public static function GetTopVideoTag($limit){
        $sql = "SELECT ". TBL_FANTASY_TAGS .".tag as tag_name,  ". TBL_FANTASY_TAGS .".id as id_tag, COUNT(".TBL_VIDEO_TAG.".tag_id) as count 
                        FROM ". TBL_FANTASY_TAGS ." 
                            LEFT JOIN ".TBL_VIDEO_TAG." 
                                ON  ". TBL_FANTASY_TAGS .".id = ".TBL_VIDEO_TAG.".tag_id 
                                    GROUP BY tag_name
                                        ORDER BY count DESC LIMIT 0, $limit";
        return MySQL::fetchArray($sql);
    }

}