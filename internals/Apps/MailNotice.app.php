<?php

class app_MailNotice {
    
    /**
     * новый кто следит за мной
     */
    public static function Newfollower($id_follower, $profile_id){
        $profileInfo = app_Profile::getFieldValues($id_follower, array('username', 'email', 'real_name') );
        $message = "You have a new follower User {$profileInfo['username']}";
         self::Mail('new_follower', $messag, $profile_id);
    }
    
    /**
     * вы получили приглашение
     */
     public static function InvitedToFantasize($creator_id, $performer_id, $contract_id){
        $profileInfo = app_Profile::getFieldValues($creator_id, array('username', 'email', 'real_name') ); 
        $message = "You have been invited to fantasize by User {$profileInfo['username']}. (<a href='".SITE_URL."/member/offer_details.php?worck_id={$contract_id}'>Click to view contract</a>)";
         self::Mail('invited_to_fantasize', $messag, $creator_id);
    }
    
    /**
     * ваш контракт обновлён
     */
     public static function ContractUpdate($profile_id, $contract_id){
        $profileInfo = app_Profile::getFieldValues($profile_id, array('username', 'email', 'real_name') ); 
        $message = "Your contract has been updated by User {$profileInfo['username']}. (<a href='".SITE_URL."/member/offer_details.php?worck_id={$contract_id}'>Click to view contract</a>)";
         self::Mail('invited_to_fantasize', $messag, $profile_id);
    }
    
    /**
     * вы получили ключи 
     */
     public static function ReceivedKeys($keys, $profile_id){
         $message = "You received a payment of {$keys} keys";
         self::Mail('received_keys', $messag, $profile_id);
    }
    
    /**
     * вы получили завершнонную фантазию Your fantasy has been completed and uploaded by USER X. Enjoy!
     */
    public static function CompletedFantasy($performer_id, $creator_id){
        $profileInfo = app_Profile::getFieldValues($performer_id, array('username', 'email', 'real_name') ); 
        $message = "Your fantasy has been completed and uploaded by USER {$profileInfo['username']}. Enjoy!";
         self::Mail('fantasy_completed', $messag, $creator_id);
    }
    
    /**
     * вы получили новый комметарий (отзыв)
     */
    public static function NewCommentPerformance($profile_id){
        $message = "You received a new comment on your performance (<a href='".SITE_URL."'>show comment</a>)";
         self::Mail('received_new_comment_performance', $messag, $profile_id);
    }
    
    /**
     * ваше видео получило новую оценку
     */
    public static function NewRatedVideo($profile_rated_id, $creator_id, $performer_id){
        $profileInfo = app_Profile::getFieldValues($profile_rated_id, array('username', 'email', 'real_name') ); 
        $message = "Youor video have been rated by User {$profileInfo['username']}";
        self::Mail('you_rated', $messag, $creator_id);
        if($performer_id)
        self::Mail('you_rated', $messag, $performer_id);
    }
    
        /**
     * ваш профиль получил новую оценку
     */
    public static function NewRatedProfile($profile_rated_id, $creator_id, $performer_id){
        $profileInfo = app_Profile::getFieldValues($profile_rated_id, array('username', 'email', 'real_name') ); 
        $message = "You have been rated by User {$profileInfo['username']}";
        self::Mail('you_rated', $messag, $creator_id);
        if($performer_id)
        self::Mail('you_rated', $messag, $performer_id);
    }
    
    /**
     * видео добавлено в избранное
     */
    public static function AddedVideoToFavorites($user, $title, $profile_id){
        $profileInfo = app_Profile::getFieldValues($user, array('username', 'email', 'real_name') ); 
        $message = "User {$profileInfo['username']} added Video {$title} to their favorites";
         self::Mail('added_favorites_video', $messag, $profile_id);
    } 
    
    /**
     * пользователь опубликовал новую фантазию
     */
    public static function  PostedNewFantasy($user_posted, $profile_id){
        $message = "User X just posted a new fantasy! (following users)";
        self::Mail('posted_fantasy', $messag, $profile_id);
    }
    
    public static function Mail($type, $messag, $profile_id){
        $array_val = app_Profile::getFieldValues(SK_HttpUser::profile_id());
        //если пользователь выключил уведомления на почту
        if(!$array_val[$type])
            return false;
        
         $profileInfo = app_Profile::getFieldValues($user, array('username', 'email', 'real_name') ); 
        
        switch($type){
            case 'invited_to_fantasize':
                break;
            case 'contract_updated':
                break;
            case 'received_keys':
                break;
            case 'new_follower':
                break;
            case 'fantasy_completed':
                break;
            case 'received_new_comment_performance':
                break;
            case 'you_rated':
                break;
            case 'added_favorites_video':
                break;
            case 'posted_fantasy':
                break;
            
            $msg = app_Mail::createMessage(app_Mail::NOTIFICATION)
				->setRecipientEmail(SK_Config::section('site')->Section('official')->get('site_email_billing'))
				->setSubject(SK_Config::section('site')->Section('official')->get( 'site_name' ))
                                                                        ->setRecipientEmail($profileInfo['email'])
				->setContent($message);
            app_Mail::send($msg);
        }
        
    }
    
}
