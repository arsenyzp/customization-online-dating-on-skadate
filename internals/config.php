<?php

// Site Roots
define('DIR_SITE_ROOT', '/var/www/kuinkey.ls/');
define('SITE_URL', 'http://192.168.20.20/');

define('SK_PASSWORD_SALT', '5153f30e1781f');

// Development mode
define('DEV_MODE', false);
define('DEV_FORCE_COMPILE', false);
define('DEV_PROFILER', false);

// MySQL Connection
define('DB_HOST', 'localhost');
define('DB_USER', 'skadate_up');
define('DB_PASS', 'skadate_up');
define('DB_NAME', 'skadate_up');
define('DB_TBL_PREFIX', 'ska_');
