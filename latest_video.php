<?php
require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'internals'.DIRECTORY_SEPARATOR.'Header.inc.php';

if (! app_Features::isAvailable(4))
	SK_HttpRequest::showFalsePage();
	
$Layout = SK_Layout::getInstance();
$list_type = 'landing';
if(isset($_GET['tag']))
    $list_type = 'tags';

$httpdoc = new component_VideoList(array('list_type'=>$list_type));

$ajax_mode = (SK_HttpRequest::isXMLHttpRequest() == 1);

if (!$ajax_mode) {
    $Layout->display($httpdoc);
} else {
    $result = array();

    if (isset($_GET['q'])) {
        $result = app_Tags::GetTagsSearch($_GET['q']);
    }
    if (isset($_POST['action'])){
           $result['like']=app_Like::SetLike(intval($_POST['id']));
    }
      echo(json_encode($result));
}
