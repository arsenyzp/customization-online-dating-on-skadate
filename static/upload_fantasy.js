function Show(id){
    $('#desc_'+id).css('height', '100%');
    $('#show_'+id).css('display', 'none');
    $('#hide_'+id).css('display', 'block');
    
}

function Hide(id){
    $('#desc_'+id).css('height', '5em');
    $('#show_'+id).css('display', 'block');
    $('#hide_'+id).css('display', 'none');
}