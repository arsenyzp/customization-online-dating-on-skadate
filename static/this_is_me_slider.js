//this_is_me_slider.js
$(document).ready(function(){
       $('.pagin_item').on('click', function(){
              var id = $(this).attr('id');
              var profile_id = $('#profile_id').val();
              AjaxSlider((id-1), profile_id);
       });
});

function AjaxSlider(iter, profile_id){
       $.ajax({
              url: '/member/profile.php',
              type: 'POST',
              success: function(dat){
                     $('#slider_div').html(dat);
                      
                     $('.pagin_item').on('click', function(){
                            var id = $(this).attr('id');
                            var profile_id = $('#profile_id').val();
                            AjaxSlider((id-1), profile_id);
                     });
              },
              data: {
                     'iter': iter, 
                     'profile_id': profile_id
              }
       });
}