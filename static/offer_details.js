$(document).ready(function () {

    var today = new Date();
    if($('#datapicker').val()=='')
    $('#datapicker').val((today.getMonth()+1)+"/"+today.getDate()+"/"+today.getFullYear());
    $('#datapicker').datepicker({
        format:'m/d/Y',
        date: $('#datapicker').val(),
        current: $('#datapicker').val(),
        starts: 1,
        position: 'r',
        onBeforeShow: function(){
            $('#datapicker').DatePickerSetDate($('#datapicker').val(), true);
        },
        onChange: function(formated, dates){
            $('#datapicker').val(formated);
            $('#datapicker').DatePickerHide();
        }
    });
    
    
     $('#budget').keyup(function (){
        var num = $('#budget').val();
        if (parseInt(num) != num) {
            $('#budget').val(num.replace(new RegExp('[^\\d]|[\\s]', 'g'), ''));
        }
    });


});

function SendDecline(id){
      var dat = {action: 'senddecline', id_worck: id};
    $.ajax({
        url: 'view_request.php',
        data: dat,
        type: 'POST',
        complete: function(data){
          data = JSON.parse(data.responseText);
            $('#button_confirm_'+id).parent().prepend('<span class="declined">Declined</span>');
            $('#button_refuse_'+id).remove();
            $('#button_confirm_'+id).remove();
        if(data.result){
            SK_drawMessage('Successfully');
            setTimeout("document.location.href='view_request.php'", 1000);
        }
        else  SK_drawError('!!!!');
        }
    });
}

function SendAccept(id){
      var dat = {action: 'sendaccepted', id_worck: id};
    $.ajax({
        url: 'view_request.php',
        data: dat,
        type: 'POST',
        complete: function(data){
          data = JSON.parse(data.responseText);
        if(data.result){
            SK_drawMessage('Successfully');
            $('#button_refuse_'+id).parent().prepend('<span class="accept">Accepted</span>');
            $('#button_refuse_'+id).remove();
            $('#button_confirm_'+id).remove();
            setTimeout('document.location="fantasy_list.php"', 1000);
        }
        else  SK_drawError('!!!!');
        }
    });
}

function CanelRequest(id){
      var dat = {action: 'canel_reqest', id_worck: id};
    $.ajax({
        url: 'view_request.php',
        data: dat,
        type: 'POST',
        complete: function(data){
          data = JSON.parse(data.responseText);
        if(data.result){
            SK_drawMessage('Successfully');
            setTimeout('document.location="fantasy_list.php"', 1000);
        }
        else  SK_drawError('!!!!');
        }
    });
}