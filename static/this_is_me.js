$(document).ready(function(){
    $('.close_pop').click(function(){
        //alert($(this).parent().attr('class')+': '+this.id);
        var dat = {'type': $(this).parent().attr('class'), 'id': this.id, 'action': 'delete_media'};
        $.ajax({
            url: '/member/ajax_user_update.php',
            type: 'POST',
            data: dat
        });
        $(this).parent().remove();
        SK_drawMessage('Media deleted');
    });
});