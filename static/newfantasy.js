$(document).ready(function () {
//проверка на числовое значение суммы
       $('#budget').keyup(function (){
              var num = $('#budget').val();
              if (parseInt(num) != num) {
                     $('#budget').val(num.replace(new RegExp('[^\\d]|[\\s]', 'g'), ''));
              }
       });
   //договорная сумма, скрытие окошка суммы
       $('#buggest_null').bind('click', function(){
              $('#budget').toggle(600);
              $('#budget_negotiable').toggle(600);
              $('#budget').val(0);
       });
    
//датапиккер
       var today = new Date();
       $('#inputDate').val(today.getDate()+"/"+(today.getMonth()+1)+"/"+today.getFullYear());
       $('#inputDate').datepicker({
              format:'m/d/Y',
              date: $('#inputDate').val(),
              current: $('#inputDate').val(),
              starts: 1,
              position: 'r',
              onBeforeShow: function(){
                     $('#inputDate').DatePickerSetDate($('#inputDate').val(), true);
              },
              onChange: function(formated, dates){
                     $('#inputDate').val(formated);
                     $('#inputDate').DatePickerHide();
              }
       });
    
       $('#completion_date').val(today.getDate()+"/"+(today.getMonth()+1)+"/"+today.getFullYear());
       $('#completion_date').datepicker({
              format:'m/d/Y',
              date: $('#completion_date').val(),
              current: $('#completion_date').val(),
              starts: 1,
              position: 'r',
              onBeforeShow: function(){
                     $('#completion_date').DatePickerSetDate($('#completion_date').val(), true);
              },
              onChange: function(formated, dates){
                     $('#completion_date').val(formated);
                     $('#completion_date').DatePickerHide();
              }
       });
    //показ попапа формы новой фантоазии
       $('#button_new_fantasy').click(function(){
              $('#form_new_fantasy').show();
              $('body').append('<div class="this_me_background"></div>');
       }); 
       
       $('.new_fantasy_form').click(function(){
              $('#form_new_fantasy').show();
              $('body').append('<div class="this_me_background"></div>');
       }); 
     //закрытие попапа  
       $('#up_fantasy_add_close').click(function(){
              $('#form_new_fantasy').hide();
              $('.this_me_background').remove();
       });
    //теги
       $("#demo-input-facebook-theme").tokenInput('/member/myfantasy.php', {
              theme: "facebook"
       });  
       
       $("#performer_tag").tokenInput('/member/myfantasy.php', {
              theme: "facebook",
              queryParam: 'q_p'
       });
     //добавление нового поля исполнителя  
       $('.file_list_add').on('click', function(){
              $('#file_list').append('<tr><td class="td_label"><label for="upload" >Upload:</label></td><td><input type="file"  name="file[]" id="file_fantasy" /><input type="button" value="+" class="file_list_add"  /><input type="button" value="-" onclick="$(this).parents(\'tr:first\').remove();" /></td></tr>');
       });
       //плагин для стилизации селектов
       function PlaginSelectBox(){
              $('select').customSelect();
       }
       
       $('.performer_plus').on('click', function(){
            var id = getRandomInt(1,9999);
            $(this).parent().parent().parent().append(' <div class="row popup_form">\
                     <div class="performer_list span1"> Performer '+(count_perfirmer++)+': </div> \
                     <div class="performer_list_tag span3" style="width: 285px;">\
                            <input type="text"  name="perfomers[]" id="'+id+'" />\
                            <img src="/layout/img/close.png"  onclick="$(this).parents().parents(\'div:first\').remove();count_perfirmer--;" class="performer_plus" />\
                     </div>\
              </div>');  
            
             $("#"+id).tokenInput(document.location.href, {
                     theme: "facebook",
                     queryParam: 'q_p'
              });
       });
       
       window.setTimeout(PlaginSelectBox, 1000);
        
});



var count_perfirmer = 2;


function getRandomInt(min, max)
{
       return Math.floor(Math.random() * (max - min + 1)) + min;
}

/*
function CheckInt(inp) {
        if (parseInt(inp.value) != inp.value) {
                inp.value = inp.value.replace(new RegExp('[^\\d]|[\\s]', 'g'), '');
        }
}
*/