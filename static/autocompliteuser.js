$(document).ready(function () {
    $( "#search_user" ).autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: "/member/ajax_user_update.php",
                data: {
                    term: request.term
                    },
                dataType: "json",
                success: function( data ) {
                    response( $.map( data.names, function( item ) {
                        return {
                            label: item.username,
                            value: item.username,
                            test: item.profile_id
                        }
                    }));
                }
            });
    
        },
        select: function( event, ui ) {
            $( "#search_user" ).val(ui.item.label);
            $( "#search_user_value" ).val(ui.item.test);
        }
    });
});