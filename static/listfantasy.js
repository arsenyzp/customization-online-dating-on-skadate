function SendRequest(id){

    var report_fl_box = new SK_FloatBox({
        $title: 'CREATE CONTRACT', 
        $contents: $('#form_create_contract').children(), 
        width: 400
    });
    var today = new Date();
    $('#datapicker').val((today.getMonth()+1)+"/"+today.getDate()+"/"+today.getFullYear());
    $('#datapicker').datepicker({
        format:'m/d/Y',
        date: $('#inputDate').val(),
        current: $('#inputDate').val(),
        starts: 1,
        position: 'r',
        onBeforeShow: function(){
            $('#datapicker').DatePickerSetDate($('#inputDate').val(), true);
        },
        onChange: function(formated, dates){
            $('#datapicker').val(formated);
            $('#datapicker').DatePickerHide();
        }
    });
            
    $('#budget').keyup(function (){
        var num = $('#budget').val();
        if (parseInt(num) != num) {
            $('#budget').val(num.replace(new RegExp('[^\\d]|[\\s]', 'g'), ''));
        }
    });
        $('#budget_contract').keyup(function (){
        var num = $('#budget_contract').val();
        if (parseInt(num) != num) {
            $('#budget_contract').val(num.replace(new RegExp('[^\\d]|[\\s]', 'g'), ''));
        }
    });
    /**получаем информацию по контракту**/

    var dat = {
        action: 'info', 
        id_fantasy: id
    };
    $.ajax({
        url: 'fantasy_list.php',
        data: dat,
        type: 'POST',
        complete: function(data){
            data = JSON.parse(data.responseText);
            $('#fantasy_id').val(id);
            $('#worck_id').val(0);
            $('#lenght_video').val(data.length_video);
            $('#budget_contract').val(data.budget);
            $('#price_video').val(data.price_video);
            $('#performer_r').val(Number(data.price_video*(0.5)).toFixed(2));
            $('#user_r').val(Number(data.price_video*(0.2)).toFixed(2));
            $('#kuinkey_r').val(Number(data.price_video*(0.2)).toFixed(2));
        }
    });
     
    
    /***/
        
    $('#price_video').keyup(function (){
        var num = $('#price_video').val();
        if (parseInt(num) != num) {
            $('#price_video').val(num.replace(new RegExp('[^\\d]|[\\s]', 'g'), ''));
        }
        $('#performer_r').val(num*(0.5));
        $('#user_r').val(num*(0.2));
        $('#kuinkey_r').val(num*(0.2));
    });
        
    $('#lenght_video').keyup(function (){
        var num = $('#lenght_video').val();
        if (parseInt(num) != num) {
            $('#lenght_video').val(num.replace(new RegExp('[^\\d]|[\\s]', 'g'), ''));
        }
    });
    
    
        
        
/*
    $.get('/member/ajax_contract_details.php', {
        'fantasy_id': id
    },HtmlAddPopUpContract);
    */
}
/*
function SendDelete(id){
    var dat = {
        action: 'delete', 
        id_fantasy: id
    };
    $.ajax({
        url: document.location.href,
        data: dat,
        type: 'POST',
        complete: function(data){
            data = JSON.parse(data.responseText);
            $('#button_'+id).val('Send');
            $('#button_'+id).removeAttr('onclick');
            $('#button_'+id).unbind();
            $('#button_'+id).click(function(){
                SendRequest(id);
            });
            if(data.sendworck) SK_drawMessage('Successfully Delete');
            else  SK_drawError('!!!!');
        }
    });
}
*/
function Show(id){
    $('#desc_'+id).css('height', '100%');
    $('#show_'+id).css('display', 'none');
    $('#hide_'+id).css('display', 'block');
    
}

function Hide(id){
    $('#desc_'+id).css('height', '5em');
    $('#show_'+id).css('display', 'block');
    $('#hide_'+id).css('display', 'none');
}

function ViewMoreFantasy(id){
    $.get('/member/ajax_view_fantasy.php', {
        'edit': id
    },HtmlAddPopUp);
}

function HtmlAddPopUp(data){
    var $title = "FANTASY DETAILS";
    var $contents = data;
    var fl_box = new SK_FloatBox({
        $title: $title,
        $contents: $contents,
        $controls: ''
    });
}

$(document).ready(function() {
    $("#search_fantasy").tokenInput(document.location.href, {
        theme: "facebook",
        queryParam: 'q',
        onAdd: function(){
                $('#filter_list').submit();
            }
    });
});

function HtmlAddPopUpContract(data){
    var $title = "Contract Details";
    var $contents = data;
    var fl_box = new SK_FloatBox({
        $title: $title,
        $contents: $contents,
        $controls: ''
    });
    
    var today = new Date();
    if($('#datapicker').val()=='')
        $('#datapicker').val(today.getMonth()+"/"+today.getDay()+"/"+today.getFullYear());

    $('#datapicker').datepicker({
        format:'m/d/Y',
        date: $('#datapicker').val(),
        current: $('#datapicker').val(),
        starts: 1,
        position: 'r',
        onBeforeShow: function(){
            $('#datapicker').DatePickerSetDate($('#datapicker').val(), true);
        },
        onChange: function(formated, dates){
            $('#datapicker').val(formated);
            $('#datapicker').DatePickerHide();
        }
    });
    
    $('#budget').keyup(function (){
        var num = $('#budget').val();
        if (parseInt(num) != num) {
            $('#budget').val(num.replace(new RegExp('[^\\d]|[\\s]', 'g'), ''));
        }
    });
        
    $('#price_video').keyup(function (){
        var num = $('#price_video').val();
        if (parseInt(num) != num) {
            $('#price_video').val(num.replace(new RegExp('[^\\d]|[\\s]', 'g'), ''));
        }
        $('#performer_r').val(num*(0.5));
        $('#user_r').val(num*(0.2));
        $('#kuinkey_r').val(num*(0.2));
    });
        
    $('#lenght_video').keyup(function (){
        var num = $('#lenght_video').val();
        if (parseInt(num) != num) {
            $('#lenght_video').val(num.replace(new RegExp('[^\\d]|[\\s]', 'g'), ''));
        }
    });
    
    $('#performer_r').val(num*(0.5));
    $('#user_r').val(num*(0.2));
    $('#kuinkey_r').val(num*(0.2));

}

function SubmitPopUpForm(){
    var dat=$('#create_contract').serialize();
    dat = JSON.stringify(dat);
    dat = escape(dat);
    var post_dat = {
        'action': 'create_contract', 
        'form': 'create_contract', 
        'data': dat
    };

    $.ajax({
        url: '/form_processor.php',
        data: post_dat,
        type: 'POST',
        complete: function(data){
            data = JSON.parse(data.responseText);
            if(data.sendworck) SK_drawMessage('Successfully Delete');
            else  SK_drawError('!!!!');
        }
    });
}