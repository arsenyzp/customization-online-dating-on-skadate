//отмена запроса
function CancelRequest(id){
       var dat = {
              action: 'canel_reqest', 
              id_worck: id
       };
       $.ajax({
              url: '/member/ajax_user_update.php',
              data: dat,
              type: 'POST',
              complete: function(data){
                     data = JSON.parse(data.responseText);
                     if(data.result){
                            SK_drawMessage('Request has been successfully canceled');
                            setTimeout('document.location="sent_applications.php"', 1000);
                     }
                     else  SK_drawError('Request error');
              }
       });
}

//редактирование запроса
function EditRequest(id){
       var report_fl_box = new SK_FloatBox({
              $title: 'UPDATE CONTRACT', 
              $contents: $('#form_edit_contract').children(), 
              width: 400
       });

       $('#budget').keyup(function (){
              var num = $('#budget').val();
              if (parseInt(num) != num) {
                     $('#budget').val(num.replace(new RegExp('[^\\d]|[\\s]', 'g'), ''));
              }
       });
       $('#budget_contract').keyup(function (){
              var num = $('#budget_contract').val();
              if (parseInt(num) != num) {
                     $('#budget_contract').val(num.replace(new RegExp('[^\\d]|[\\s]', 'g'), ''));
              }
       });
       /**получаем информацию по контракту**/
       var dat = {
              action: 'info_work', 
              id_work: id
       };
       $.ajax({
              url: 'fantasy_list.php',
              data: dat,
              type: 'POST',
              complete: function(data){
                     data = JSON.parse(data.responseText);
                     $('#fantasy_id').val(data.id_fantasy);
                     $('#worck_id').val(id);
                     $('#lenght_video').val(data.length_video);
                     $('#budget_contract').val(data.budget);
                     $('#price_video').val(data.price_video);
                     $('#performer_r').val(Number(data.price_video*(0.5)).toFixed(2));
                     $('#user_r').val(Number(data.price_video*(0.2)).toFixed(2));
                     $('#kuinkey_r').val(Number(data.price_video*(0.2)).toFixed(2));
                      var today = new Date(parseInt(data.completion_date)*1000);
                     $('#datapicker').val((today.getMonth()+1)+"/"+today.getDate()+"/"+today.getFullYear());
              }
       });
       
         $('#datapicker').datepicker({
              format:'m/d/Y',
              date: $('#inputDate').val(),
              current: $('#inputDate').val(),
              starts: 1,
              position: 'r',
              onBeforeShow: function(){
                     $('#datapicker').DatePickerSetDate($('#inputDate').val(), true);
              },
              onChange: function(formated, dates){
                     $('#datapicker').val(formated);
                     $('#datapicker').DatePickerHide();
              }
       });
     
       //подсчёт коммисий
       $('#price_video').keyup(function (){
              var num = $('#price_video').val();
              if (parseInt(num) != num) {
                     $('#price_video').val(num.replace(new RegExp('[^\\d]|[\\s]', 'g'), ''));
              }
              $('#performer_r').val(num*(0.5));
              $('#user_r').val(num*(0.2));
              $('#kuinkey_r').val(num*(0.2));
       });
        
       $('#lenght_video').keyup(function (){
              var num = $('#lenght_video').val();
              if (parseInt(num) != num) {
                     $('#lenght_video').val(num.replace(new RegExp('[^\\d]|[\\s]', 'g'), ''));
              }
       });
}

//просмотр полной версии фантазии
function ViewMoreFantasy(id){
       $.get('/member/ajax_view_fantasy.php', {
              'edit': id
       },HtmlAddPopUp);
}

function HtmlAddPopUp(data){
       var $title = "Fantasy Details";
       var $contents = data;
       var fl_box = new SK_FloatBox({
              $title: $title,
              $contents: $contents,
              $controls: ''
       });
}

function SentResult(id){
       ///document.location = "/member/upload_fantasy.php?id_fantasy="+id;
       ////форма загрузки выполненной фантазии
    $('#fantasy_id_up').val(id);
    $('#up_fantasy').show();
    $('body').append('<div class="this_me_background"></div>');

   //закрытие попапа  
   $('#up_fantasy_add_close').click(function(){
         $('#up_fantasy').hide();
         $('.this_me_background').remove();
   });
     $('#up_fantasy_cancel').click(function(){
         $('#up_fantasy').hide();
         $('.this_me_background').remove();
   });
}