<?php

require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'internals'.DIRECTORY_SEPARATOR.'Header.inc.php';

$form_name = isset($_GET['SK-Field-Owner-Form']) ? $_GET['SK-Field-Owner-Form'] : $_POST['SK-Field-Owner-Form'];
$field_name = isset($_GET['SK-Field-Name']) ? $_GET['SK-Field-Name'] : $_POST['SK-Field-Name'];

if ( $_GET['submitted'] == 1 && (! isset($_FILES) || (count($_FILES) == 0)) )
{
    $FATAL_ERROR = SK_Language::text('%forms._errors.max_filesize_exceeded'); // post_max_size exceeded;
}

if ( !$form_name || !preg_match('~^\w+$~i', $form_name) ) {
	$FATAL_ERROR = 'undefined form name';
}
else {
	$include_path = DIR_FORMS_C . $form_name . '.form.php';

	if ( !file_exists($include_path) || ( dirname($include_path) . DIRECTORY_SEPARATOR ) !== DIR_FORMS_C ) {
		$FATAL_ERROR = 'unrecognized form "'.$form_name.'"';
	}
	else {
		require $include_path;

		try {
			$field = $form->getField($field_name);
		}
		catch ( SK_FormException $e ) {
			$FATAL_ERROR = $e->getMessage();
		}
	}
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xml:lang="en" lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<script type="text/javascript" src="<?php echo URL_STATIC; ?>jquery.js"></script>
<script type="text/javascript">
// <!--
<?php

if ( isset($FATAL_ERROR) ) {
	echo "window.sk_upload_error = ".json_encode($FATAL_ERROR).";\n"; // TODO: user language messages
}
elseif ( isset($_FILES[$field_name]) )
{
	try {
		$tmp_file = SK_TemporaryFile::catchFile($_FILES[$field_name], $field); 
		$preview_html = $field->preview($tmp_file);

		echo "window.sk_userfile_uniqid = '".$tmp_file->getUniqid()."';\n",
			 "window.sk_userfile_preview = ".json_encode($preview_html).";\n";
	}
	catch (Exception $e) {
		echo "window.sk_upload_error = ".json_encode($e->getMessage()).";\n"; // TODO: user language messages
	}
}

?>

$(function() {
	window.$upload_form = $('#file_upload_form');
	window.$userfile = $('#userfile_input');      
});

/******/
(function($) {
    $.fn.jInputFile = function(options) {
		
		return this.each(function() {
		
			$(this).val('');
			$(this).wrap('<div></div>');
			$(this).parent().css('height', '16px');
                        
                        $(this).parent().css('width', '140px');
                        $(this).parent().css('float', 'right');
                        
			$(this).after('<div class="jInputFile-fakeButton"></div><div class="jInputFile-blocker"></div><div class="jInputFile-activeBrowseButton jInputFile-fakeButton"></div><div class="jInputFile-fileName"></div>');
			$(this).addClass('jInputFile-customFile');
		
			$(this).hover(
				function () {
					$(this).parent().children('.jInputFile-activeBrowseButton').css('display', 'block');
				},
				function () {
					$(this).parent().children('.jInputFile-activeBrowseButton').css('display', 'none');
				}
			);
		
			$(this).change(function(){
                                                                
                                                                $('#loadavatar').submit();
                                                                $('.profile_thumb_wrapper').hide();
                                                                $('#hiddenframe').show();
                                                                
                                                                var file = $(this).val();
           
				//Находим название файла и его расширение
				var reWin = /.*\\(.*)/;
				var fileName = file.replace(reWin, '$1');
				var reUnix = /.*\/(.*)/;
				fileName = fileName.replace(reUnix, '$1');
				var regExExt =/.*\.(.*)/;
				var ext = fileName.replace(regExExt, '$1');
			
				//Показываем значок и имя файла
				var pos;
				if (ext){
					switch (ext.toLowerCase()) {
						case 'doc': pos = '0'; break;
						case 'bmp': pos = '16'; break;                       
						case 'jpg': pos = '32'; break;
						case 'jpeg': pos = '32'; break;
						case 'png': pos = '48'; break;
						case 'gif': pos = '64'; break;
						case 'psd': pos = '80'; break;
						case 'mp3': pos = '96'; break;
						case 'wav': pos = '96'; break;
						case 'ogg': pos = '96'; break;
						case 'avi': pos = '112'; break;
						case 'wmv': pos = '112'; break;
						case 'flv': pos = '112'; break;
						case 'pdf': pos = '128'; break;
						case 'exe': pos = '144'; break;
						case 'txt': pos = '160'; break;
						default: pos = '176'; break
					};
					$(this).parent().children('.jInputFile-fileName').html(fileName).css({'background-position':('0px -'+pos+'px'),'background-repeat':'no-repeat', 'display':'block'});
				};
                                
                                
			});	
		});
	}
})(jQuery);


// -->
</script>
<style>
    
    /*input file*/
.jInputFile-input{
	position: absolute;
}

.jInputFile-customFile{
	position: absolute;
	width: 219px;
                margin-left: -140px;
                cursor: default;
                height: 21px;
                z-index: 2;
	font: 13px Verdana;			/*!!!!!!!!!!!!!!*/
	filter: alpha(opacity: 0);
    opacity: 0;
}

.jInputFile-fakeButton{
    position: absolute;
    z-index: 1;
    width: 85px;
    height: 21px;
    background: url("/layout/img/button.png") no-repeat left top;
    float: left;
}
       
.jInputFile-blocker{
    position: absolute;
    z-index: 3;
    width: 150px;
    height: 21px;
    background: url("/layout/img/transparent.gif");
    margin-left: -155px;
}

.jInputFile-fileName{
    display: none;
    font-size: 9px;
    height: 15px;
    line-height: 15px;
    margin-left: 72px;
    margin-top: 0;
    padding-left: 19px;
    padding-top: 1px;
    position: absolute;
    background:url("/layout/img/icons.png");
}

.jInputFile-activeBrowseButton{
    background: url("/layout/img/button_active.png") no-repeat left top;
    display: none;
}
.profile_edit_fields{
    max-width:325px;
    text-align: left;
}
.profile_field_row{
    margin-top:10px;
    margin-bottom:10px;
}
.form_block_user {
    color: #30A5DA;
    margin-top: 10px;
    text-align: left;
}
.profile_edit_radio .span4 input{
    margin-top: -1px;
}
.select_left_div{
    background: url("/layout/img/strelka.png") no-repeat left top;
    height: 30px;
    width:30px;
    
}
    
</style>
</head>

<body style="margin: 0px; padding: 0px">

<form id="file_upload_form" action="<?php echo SITE_URL . 'file_uploader.php?SK-Field-Owner-Form='.$form_name.'&SK-Field-Name='.$field_name.'&submitted=1'; ?>" method="post" enctype="multipart/form-data">
	<input type="hidden" name="SK-Field-Owner-Form" value="<?php echo $form->getName(); ?>" />
	<input type="hidden" name="SK-Field-Name" value="<?php echo $field->getName(); ?>" />
	<input type="file" name="<?php echo $field->getName(); ?>" id="userfile_input" size="14" style="font-size: 11px" />
</form>
    <script>
    $(document).ready(function(){
    /*оформление*/
    $('input:file').jInputFile();
    });
    </script>
</body>
</html>
