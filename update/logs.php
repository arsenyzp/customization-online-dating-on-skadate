<?php

require_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'internals'.DIRECTORY_SEPARATOR.'Header.inc.php';

require_once 'inc/Pack.class.php';

$update = trim($_GET['update']);
$update_dir = $update ? LOG_DIR . $update . DIRECTORY_SEPARATOR : LOG_DIR;

$log = new Update_LogReader($update_dir);

$unit = trim($_GET['unit']);
$title = '';
switch ($unit) {
	case Update_LogReader::LOG_LANG:
		$items = $log->readLangLog();
		$title = 'Language log';
		break;
	case Update_LogReader::LOG_DOC:
		$items = $log->readDocLog();
		$title = 'Documents log';
		break;
	default:
		$items = $log->readDbLog();
		$title = 'Database log';
		$unit = Update_LogReader::LOG_DB;
		break;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xml:lang="en" lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<title><?php echo $title ?></title>
<link rel="stylesheet" type="text/css" href="../admin/css/admin.css" />
<style>
#content {
	width: 96%;
	margin: 0 auto;
}

.item {
	margin: 2px;
	padding: 5px;
}

.success {
	border: 1px solid green;
	background-color: #DFFFE0;
}

.faild {
	border: 1px solid red;
	background-color: #FFDFDF;
}

.log_content {
	margin-top: 5px;
	width: 100%;
	height: 700px;
	overflow:scroll;
}

</style>

<script type="text/javascript" src="../static/jquery.js"></script>
<script type="text/javascript">
	var $ = jQuery.noConflict();
	$(function () {
		$('#show_errors_btn').click(function(){
			if (this.checked) {
				$('.log_content .faild').show();
			} else {
				$('.log_content .faild').hide();
			}
		});

		$('#show_success_btn').click(function(){
			if (this.checked) {
				$('.log_content .success').show();
			} else {
				$('.log_content .success').hide();
			}
		});
	});
	</script>
</head>
<body>
	<div id="content">
		<div class="type_switcher">
			<select onchange="window.location.href='<?php echo $_SERVER['PHP_SELF'] . "?update=$update"?>&unit=' + this.value">
				<option <?php echo ($unit == Update_LogReader::LOG_DB) ? 'selected="selected"' : '' ?> value="<?php echo Update_LogReader::LOG_DB ?>">Datatbase log</option>
				<option <?php echo ($unit == Update_LogReader::LOG_LANG) ? 'selected="selected"' : '' ?> value="<?php echo Update_LogReader::LOG_LANG ?>">Language log</option>
				<option <?php echo ($unit == Update_LogReader::LOG_DOC) ? 'selected="selected"' : '' ?> value="<?php echo Update_LogReader::LOG_DOC ?>">Document log</option>
			</select>
		</div>
		<div class="log_content">
			<?php foreach ($items as $item) {?>
			
				<div class="item <?php echo $item['result']?>">
					<?php echo $item['content']?>
				</div>
				
			<?php }?>
	</div>
	Show errors <input type="checkbox" id="show_errors_btn" checked="checked" /> &nbsp;&nbsp;Show successes <input type="checkbox" id="show_success_btn" checked="checked" /> 
</body>
</html>
