<?php 
require_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'internals'.DIRECTORY_SEPARATOR.'Header.inc.php';

$result = SK_MySQL::query("SELECT * FROM `" . TBL_CONFIG);

while ($item = $result->fetch_object()) {
	
	switch ($item->presentation) {
		case 'varchar':
		case 'textarea':
		case 'select':
		case 'text':
		case 'hidden':
		case 'pallete':
			$value = (string)trim($item->value, '"');
			break;
								
		case 'checkbox':
			$value = (bool)$item->value;
			break;
		
		case 'integer':	
		case 'number':
			$value = intval($item->value);
			break;
		case 'float':
			$value = floatval($item->value);
			break;
	}
			
	$query = SK_MySQL::placeholder( 'UPDATE `' . TBL_CONFIG . '` SET `value`="?" 
		WHERE `config_id`=?', json_encode($value), $item->config_id );
	SK_MySQL::query($query);
}

echo 'Config data converted.';
