<?php

abstract class Update_Master
{
	protected $step = 1;
	
	public $header_js = "";
	
	public $onready_js = "";
	
	protected $session;
	
	private $control_buttons = array();
	
	protected $step_count = 5;
	
	private $errors = array();
	
	private $messages = array();
	
	/**
	 * @return int
	 */
	public function getStep_count() {
		return $this->step_count;
	}
	
	/**
	 * @param int $step_count
	 */
	public function setStep_count($step_count) {
		$this->step_count = $step_count;
	}

	public function __construct()
	{
		$this->session = & $_SESSION["%update_session%"];
		$this->step = & $this->session["step"];
		$this->step = (int)@$this->step ? $this->step : 1;
		$this->control_buttons = array(
			'next' => true,
			'back' => false
		);
		
		$this->errors = & $this->session["errors"];
		$this->errors = count($this->errors) ? $this->errors : array();
		
		foreach ($this->errors as $key => $error) {
			$this->printError($error);
			unset($this->errors[$key]);
		}
		
		$this->messages = & $this->session["messages"];
		$this->messages = count($this->messages) ? $this->messages : array();
		
		foreach ($this->messages as $key => $msg) {
			$this->printMessage($msg);
			unset($this->messages[$key]);
		}
		
		$this->stepControlPrepare();
		
		if ($_POST) {
			$this->step = (int)@$_POST["master_step"];
			
			if (!isset($_POST['back'])) {
				$data = $this->stepOnHandle($_POST);
				if (!count($this->errors)) {
					$result = $this->stepHandler($data);
				}
			}
			if( $result !== false ) {
				$this->stepHandleComplete();
			}
		}
	}
	
	public abstract function title();

	
	public abstract function legend();
	
	public function step() {
		return (int) $this->step;
	}
	
	public abstract function render();
	
	
	public function next_control() {
		return $this->control_buttons['next'] ? '<input type="submit" name="next" value="Next &raquo;" />' 
			: '<input type="submit" value="Finish" name="next" />';
	}
	
	public function back_control() {
		return $this->control_buttons['back'] ? '<input type="submit" value="&laquo; Back" name="back" />' 
			: ' ';
	}
		
	protected function saveVar($name, $value = null) {
		$this->session["vars"][$name] = $value;
	}
	
	protected function clearSession() {
		$this->session = array();
	}
	
	protected function getVar($name) {
		return $this->session["vars"][$name];
	}
	
	protected function stepControlPrepare() {
		$this->control_buttons['back'] = ($this->step > 1);
		$this->control_buttons['next'] = !($this->step >= $this->step_count);
	}
	
	protected function stepHandleComplete() {
		if (count($this->errors)) {
			return;
		}
			
		if (isset($_POST["next"])) {
			$this->step = $this->step < $this->step_count ? $this->step + 1 : $this->step;
		} elseif (isset($_POST["back"])) {
			$this->step = $this->step > 1 ? $this->step - 1: $this->step;
		}
				
		$this->stepControlPrepare();
		
		foreach ($this->messages as $key => $msg) {
			$this->printMessage($msg);
			unset($this->messages[$key]);
		}
	}
	
	protected function stepOnHandle($data) {
		foreach ($data as $key => $item) {
			$this->saveVar($key, $item);
		}
		return $data;
	}
	
	protected function stepHandler($data) {
		return array();
	}
	
	public function printError($error_msg) {
		$out = '
			<div style="
			margin:10px 2px;
			border:1px inset #efefef;
			padding:10px;
			background: #efefef;
			color: #CF0000;
			font-family: \'Courier New\';
			font-size: 12px;
			text-align: center;
			">'.str_replace('  ', ' &nbsp;', nl2br(
				$error_msg
		)).'</div>';
		
		echo $out;
	}
	
	public function printMessage($msg) {
		$out = '
			<div style="
			margin:10px 2px;
			border:1px inset #efefef;
			padding:10px;
			background: #efefef;
			color: #0000E0;
			font-family: \'Courier New\';
			font-size: 12px;
			text-align: center;
			">'.str_replace('  ', ' &nbsp;', nl2br(
				$msg
		)).'</div>';
		
		echo $out;
	}

	public function error($error_msg) {
		$this->errors[] = $error_msg;
	}
	
	public function message($msg) {
		$this->messages[] = $msg;
	}
	
	public function display()
	{
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xml:lang="en" lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<title><?php echo $this->title() ?></title>
<link rel="stylesheet" type="text/css" href="../admin/css/admin.css" />
<style>

</style>
<script type="text/javascript" src="../static/jquery.js"></script>
<script type="text/javascript">
	<?php echo $Step->header_js ?>
	var $ = jQuery.noConflict();
	$(function () {
		<?php echo $Step->onready_js ?>
	});
	</script>
</head>
<body>
<form method="post" enctype="multipart/form-data">
<input type="hidden" name="master_step" value="<?php echo $this->step(); ?>" />
<table class="skadate7_install" style="margin: 20px auto">
	<thead>
		<tr>
			<td class="corner_left_top"></td>
			<td colspan="2" class="top_side"><?php echo $this->legend(); ?></td>
			<td class="corner_right_top"></td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td colspan="4" class="center_side"><?php echo $this->render(); ?></td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<td class="corner_left_bottom"></td>
			<td class="bottom_side" style="text-align: left">
					<?php echo $this->back_control(); ?>
				</td>
			<td class="bottom_side" style="text-align: right">
					<?php echo $this->next_control(); ?>
				</td>
			<td class="corner_right_bottom"></td>
		</tr>
	</tfoot>
</table>
</form>
</body>
</html>

<?php 
	}
}
