<?php
// including debug functions
require_once DIR_INTERNALS.'Debug.func.php';

define("LOG_DIR", 'log' . DIRECTORY_SEPARATOR);

class Update_LanguageDump
{
	
}


class Update_LanguageDumpSection
{
	private $name;
	
	private $keys = array();
	
	private $child_sections = array();
	
	/**
	 * Information about Tree
	 *
	 * @var Update_DumpInfo
	 */
	public $tree_info;
	
	/**
	 * Parent Section
	 *
	 * @var Update_LanguageDumpSection
	 */
	private $parent_section;
	
	public function __construct($section_name, $parent_section = null) {
		$this->name = $section_name;
		$this->parent_section = $parent_section;
	}
	
	public function name() {
		return $this->name;
	}

	public function hasChildren() {
		return (bool)count($this->child_sections);
	}
	
	/**
	 * Returns parent section
	 *
	 * @return Update_LanguageDumpSection
	 */
	public function parentSection() {
		return $this->parent_section;
	}
	
	public function path() {
		if (!isset($this->parent_section)) {
			return "";
		}
		$parent_path = $this->parent_section->path();
		return ($parent_path ? $parent_path . '.' : '') . $this->name();
	}
	
	public function keyExists($key) {
		return array_key_exists($key, $this->keys);
	}
	
	public function keyValueExists($key, $lang_id = null) {
		if (!$this->keyExists($key)) {
			return false;
		}
		if (isset($lang_id)) {
			$key = $this->key($key);
			return isset($key[$lang_id]);
		}
		
		foreach ($this->key($key) as $value) {
			if ($value) {
				return true;
			}
		}
		return false;
	}
	
	public function setKey($key, array $value) {
		$this->keys[$key] = $value;
	}
	
	public function setKeyValue($key, $value, $lang_id) {
		$this->keys[$key][$lang_id] = $value;
	}
	
	public function setSection(Update_LanguageDumpSection $section) {
		$this->child_sections[$section->name] = $section;
		return $this;
	}
	
	public function key($key, $language = null) {
		$value = $this->keys[$key];
		if (is_array($language)) {
			
			$value = array();
			foreach ($language as $key_lang_id => $value_lang_id) {
				$value[$key_lang_id] = $this->keys[$key][$value_lang_id];
			}
		} elseif (is_int($language)) {
			return $this->keys[$key][$language];
		}
		
		
		return $value;
	}
	
	/**
	 * Returns Section
	 *
	 * @param string $section_path
	 * @return Update_LanguageDumpSection
	 */
	public function section($section_path) {
		$section_cramp = explode('.', $section_path);
	
		$item = array_shift($section_cramp);
		$item = $item == $this->name ? 	array_shift($section_cramp) : $item;
		if (isset($this->child_sections[$item])) {
			if (!count($section_cramp)) {
				return $this->child_sections[$item];
			}
			return $this->child_sections[$item]->section(implode('.', $section_cramp));
		}
		return null;
	}
	
	/**
	 * Creates Section and returns it
	 *
	 * @param string $section_name
	 * @return Update_LanguageDumpSection
	 */
	public function createSection($section_name) {
		if (array_key_exists($section_name, $this->child_sections)) {
			return $this->child_sections[$section_name];
		}
		return $this->child_sections[$section_name] = new self($section_name, $this);
	}
	
	/**
	 * Returns Section List Array
	 *
	 * @return array
	 */
	public function sectionList() {
		return $this->child_sections;
	}
	
	/**
	 * Returns Keys List Array
	 *
	 * @return array
	 */
	public function keyList($language = null) {
		if (!isset($language)) {
			return $this->keys;
		}
		$out = array();
		foreach ($this->keys as $key=>$val) {
			$out[$key] = $this->key($key, $language);
		}
		return $out;
	}
	
	public function toArray( $child_ection = null ) {
		$out = array();
		
		if (!isset($child_ection)) {
			$child_ection = $this;
		} elseif (is_string($child_ection)) {
			$child_ection = $this->section($child_ection);
		}
		
		$out['keys'] = $child_ection->keyList();
		$out['sections'] = array();
		foreach ($child_ection->sectionList() as $section_name =>$section_item) {
			$out['sections'][$section_name] = $section_item->toArray();
		}
		return $out;
	}
	
}

class Update_DumpInfo
{
	public $revision;
	public $languages = array();
	
	public function addLanguage($abrev, $label, $id) {
		$this->languages[$id] = array(
			'id'=>$id,
			'abrev'=>$abrev,
			'label'=>$label
		);
	}
	
	public function setPackageVersion($vers) {
		$this->revision = $vers;
	}
}

class Update_DBControler
{
	/**
	 * Downloads section tree from db and returns it
	 *
	 * @return Update_LanguageDumpSection
	 */
	public static function loadTree( $languages = null, $package_version = null) {
		$tree = self::load($languages);
		$tree->tree_info = new Update_DumpInfo();
		
		$db_langs = SK_LanguageEdit::getLanguages();
		$_languages = $db_langs;
		
		if (isset($languages)) {
			$_languages = array();
			foreach ($languages as $lang_item) {
				$lang_id = isset($lang_item['alias']) ? $lang_item['alias'] : $lang_item['lang_id'];
				$_languages[$lang_id] = $db_langs[$lang_item['lang_id']];
			}
		}
		
		foreach ($_languages as $lang_id => $lang) {
			$tree->tree_info->addLanguage($lang->abbrev, $lang->label, $lang_id);
		}
		
		if (isset($package_version)) {
			$tree->tree_info->setPackageVersion($package_version);
		} elseif(defined("PACKAGE_VERSION")) {
			$tree->tree_info->setPackageVersion(PACKAGE_VERSION);
		} else {
			$tree->tree_info->setPackageVersion("");
		}
		
		return $tree;
	}
	
	
	private static function load( $languages = null, Update_LanguageDumpSection $section = null )
	{
		$section_id = 0;
		if (!isset($section)) {
			$section = new Update_LanguageDumpSection('root');
		} else {
			$section_id = self::path2id($section->path());
		}
		
		foreach (self::key_list($section_id) as $key => $key_obj ) {
			
			$value = $key_obj->values;
			if ($languages) {
				$value = array();
				foreach ($languages as $lang_alias) {
					$lang_alias['alias'] = isset($lang_alias['alias']) ? $lang_alias['alias'] : $lang_alias['lang_id'];
					$value[$lang_alias['alias']] = $key_obj->values[$lang_alias['lang_id']];
				}
			}
			$section->setKey($key, $value);
		}
		
		foreach (self::section_list($section_id) as $section_name=> $section_item) {
			$section_object = $section->createSection($section_name);
			self::load($languages, $section_object);
		}
		
		return $section;
	}
	
	private function section_list($section = 0) {
				
		if (!is_integer($section)) {
			$section = self::path2id($section);
		}
		$query = SK_MySQL::placeholder("
			SELECT * FROM `" . TBL_LANG_SECTION . "` WHERE `parent_section_id`=?
		", $section);
		$result = SK_MySQL::query($query);
		
		$out = array();
		while ($section_item = $result->fetch_object()) {
			$out[$section_item->section] = $section_item;
		}
		return $out;
	}
	
	private function key_list($section) {
		if (!is_integer($section)) {
			$section = self::path2id($section);
		}
		
		$compiled_val_ph = SK_MySQL::compile_placeholder("
			SELECT `lang_id`, `value` FROM `" . TBL_LANG_VALUE . "` WHERE `lang_key_id`=?
		");
		$query = SK_MySQL::placeholder("
			SELECT * FROM `" . TBL_LANG_KEY . "` WHERE `lang_section_id`=?
		", $section);
		$out = array();
		$result = SK_MySQL::query($query);
		while ($key_item = $result->fetch_object()) {
			$out[$key_item->key] = $key_item;
			$values_result = SK_MySQL::query(SK_MySQL::placeholder($compiled_val_ph, $key_item->lang_key_id));
			
			$out[$key_item->key]->values = array();
			
			while ($value = $values_result->fetch_object()) {
				$out[$key_item->key]->values[$value->lang_id] = $value->value;
			}
		}
		
		return $out;
	}
	
	private static function path2id($section_path, $parent_section_id = 0) {
		if (!($section_path = trim($section_path))) {
			return 0;
		}
		static $compiled_query;
		
		if (!isset($compiled_query)) {
			$compiled_query = SK_MySQL::compile_placeholder("
				SELECT `lang_section_id` FROM " . TBL_LANG_SECTION . " WHERE `section`='?' AND `parent_section_id`=?
			");
		}
		
		$section_stack = explode('.', $section_path);
		$section_name = array_shift($section_stack);
		
		$section_id = SK_MySQL::query(SK_MySQL::placeholder($compiled_query, $section_name, $parent_section_id))->fetch_cell();

		if ($section_id === false) {
			throw new UpdateLanguageException("Can not detect section id", 3);
		}
		
		if (count($section_stack)) {
			return (int)self::path2id(implode('.', $section_stack), $section_id);
		}
		return (int)$section_id;
	}
	
	public static function saveTree(Update_LanguageDumpSection $tree) {
		
		$log = Update_Log::instance(Update_Log::LOG_TYPE_LANG);
		
		try {
			$section_id = self::path2id($tree->path());
		} catch (UpdateLanguageException $e) {
			try {
				$parent_path = $tree->parentSection()->path();
				$section_id = self::addSection(self::path2id($parent_path), $tree->name());
				$sec_name = $tree->name();
				$msg = "Operation : Add Lang section <b>`$parent_path.$sec_name`</b>;";
				
				$log->add($msg, 'add', 'success');
				
			} catch (UpdateLanguageException $e) {
				$sec_name = $tree->name();
				$msg = "Operation : Add Lang section <b>`$parent_path.$sec_name`</b>;";
				$log->add($msg, 'add', 'faild');
			}
		}
				
		foreach ($tree->keyList() as $key => $value) {
			try {
				foreach ($value as $lang_id => $value_item) {
					self::setKey($section_id, $key, $value_item, $lang_id);
					$path = $tree->path();
					$msg = "Operation : Set Key <b>`$path -> $key`</b> (lang_id = $lang_id);";
					$log->add($msg, 'set', 'success');
				}
			}catch (UpdateLanguageException $e) {
				$path = $tree->path();
				$msg = "Operation : Set Key <b>`$path -> $key`</b> (lang_id = $lang_id);";
				$log->add($msg, 'set', 'faild');
			}
		}
		
		foreach ($tree->sectionList() as $section) {
			self::saveTree($section);
		}
	}
	
	private static function addSection($parent_name, $name) {
		try {
			$result = SK_LanguageEdit::createSection($parent_name, $name, '');//TODO description
		}catch (SK_LanguageEditException $e ) {
			throw new UpdateLanguageException($e->getMessage());
		}
		return $result->lang_section_id;
	}
	
	private static function setKey($section_id, $key, $value, $lang_id)
	{
		
		if (!($section_id = (int)$section_id)) {
			throw new UpdateLanguageException("wrong section id", 2);
		}
		
		if (!($key = trim($key))) {
			throw new UpdateLanguageException("wrong key", 2);
		}
		
		if (!($lang_id = (int)$lang_id)) {
			throw new UpdateLanguageException("wrong lang id", 2);
		}
		
		$key_id = SK_MySQL::query(SK_MySQL::placeholder("
			SELECT `lang_key_id` FROM `" . TBL_LANG_KEY . "` WHERE `lang_section_id`=? AND `key`='?'
		", $section_id, $key))->fetch_cell();
		
		if (!$key_id) {
			SK_MySQL::query(SK_MySQL::placeholder("
				INSERT INTO `" . TBL_LANG_KEY . "` (`lang_section_id`, `key`) VALUES(?, '?')
			", $section_id, $key));
			$key_id = SK_MySQL::insert_id();
			
			$existing_value = false;
		} else {
			$existing_value = SK_MySQL::query(SK_MySQL::placeholder("
				SELECT `value` FROM `" . TBL_LANG_VALUE . "` WHERE `lang_id`=? AND `lang_key_id`=?
			", $lang_id, $key_id))->fetch_cell();
		}
		
		if ( $existing_value !== false ) {
			if ($existing_value == $value) {
				return false;
			}
			
			SK_MySQL::query(SK_MySQL::placeholder("
				UPDATE `" . TBL_LANG_VALUE . "` SET `value`='?' WHERE `lang_id`=? AND `lang_key_id`=?
			", $value, $lang_id, $key_id));
			
		} else {
			SK_MySQL::query(SK_MySQL::placeholder("
				INSERT INTO `" . TBL_LANG_VALUE . "`(`value`, `lang_key_id`, `lang_id`) VALUES('?', ?, ?)
			", $value, $key_id, $lang_id));
		}
		$result = SK_MySQL::affected_rows();
		return (bool) $result;
		
	}
	
	
	public static function query($query) {
		$log = Update_Log::instance(Update_Log::LOG_TYPE_DB);
		
		try {
			SK_MySQL::query($query);
			$msg = "Query :<br> $query";
			$log->add($msg, 'query', 'success');
		} catch (SK_MySQL_Exception $e) {
			$msg = "Query :<br> <b>$query</b><br /><br />" . $e->getMessage();
			$log->add($msg, 'query', 'faild');
		}
	}
}

class Update_XMLControler
{
	/**
	 * Xml Document
	 *
	 * @var DOMDocument
	 */
	private static $xml_document;
	
	/**
	 * Root element of Xml document
	 *
	 * @var DOMElement
	 */
	private static $root_node;
	
	public static function saveTree($file_name, Update_LanguageDumpSection $section) {
		self::$xml_document = new DOMDocument('1.0');
		self::$root_node = self::$xml_document->createElement('language_tree');
		
		if (isset($section->tree_info)) {
			self::saveInfo($section->tree_info);
		}
		
		self::$xml_document->appendChild(self::$root_node);
		self::createXML($section, self::$root_node);
		return self::$xml_document->save($file_name);
	}
	
	private static function saveInfo(Update_DumpInfo $info) {
		 $root = self::$xml_document->createElement('tree_info');
		 self::$root_node->appendChild($root);
		 
		 $package_version = self::$xml_document->createElement('package_version');
		 $root->appendChild($package_version);
		 $package_version->setAttribute('version', $info->revision);
		 
		 $languages = self::$xml_document->createElement('languages');
		 $root->appendChild($languages);
		 
		 foreach ($info->languages as $lang) {
		 	$lang_node = self::$xml_document->createElement('lang');
		 	$lang_node->setAttribute('id', $lang['id']);
		 	$lang_node->setAttribute('abrev', $lang['abrev']);
		 	$lang_node->setAttribute('label', $lang['label']);
		 	$languages->appendChild($lang_node);
		 }
	}
	

	private function createXML(Update_LanguageDumpSection $section, DOMElement $root_section)
	{
		$section_node = self::$xml_document->createElement('section');
		$root_section->appendChild($section_node);
		$section_node->setAttribute('name', $section->name());
		
		foreach ($section->keyList() as $key_name => $key_value) 
		{
			$key_node = self::$xml_document->createElement('key');
			$key_node->setAttribute('name', $key_name);
			
			foreach ($key_value as $lang_id => $lang_value) 
			{
				$value_node = self::$xml_document->createElement('value');
				$value_node->setAttribute('lang_id', $lang_id);
				$cdata = self::$xml_document->createTextNode($lang_value);
				$value_node->appendChild($cdata);
				
				$key_node->appendChild($value_node);
			}
			
			$section_node->appendChild($key_node);
		}
		
		foreach ($section->sectionList() as $section_item) {
			self::createXML($section_item, $section_node);
		}
	}
	
	/**
	 * Xpath Object
	 *
	 * @var DOMXpath
	 */
	private static $xpath;
	
	public static function loadTree($file_name, array $languages = null) {
		self::$xml_document = new DOMDocument('1.0');
		self::$xml_document->preserveWhiteSpace = false;
		self::$xml_document->load($file_name);
		self::$xpath = new DOMXPath(self::$xml_document);
		self::$root_node = self::$xpath->query("section[@name='root']")->item(0);
		
		$tree = self::load($languages);
		$tree->tree_info = self::loadInfo();
		return $tree;
	}
	
	
	/**
	 * Returns Information about Tree
	 *
	 * @return Update_DumpInfo
	 */
	private static function loadInfo() {
		$root_node = self::$xpath->query('tree_info')->item(0);
		$out = new Update_DumpInfo();
		$out->setPackageVersion(self::$xpath->query('package_version', $root_node)->item(0)->getAttribute("version"));
		
		$languages_node = self::$xpath->query('languages', $root_node)->item(0);
		foreach (self::$xpath->query('lang', $languages_node) as $item) {
			$out->addLanguage($item->getAttribute('abrev'), $item->getAttribute('label'), $item->getAttribute('id'));
		}
		return $out;
	}
	
	private static function key_list(DOMElement $section) {
		$out = array();
		
		foreach (self::$xpath->query('key', $section) as $key) {
			$key_item = & $out[$key->getAttribute("name")];
			$key_item->values = array();
			foreach (self::$xpath->query('value', $key) as $value) {
				$key_item->values[$value->getAttribute("lang_id")] = $value->textContent;
			}
		}
		return $out;
	}
	
	private static function load( $languages = null, Update_LanguageDumpSection $section = null )
	{
		$section_node = self::$root_node;
		
		if (!isset($section)) {
			$section = new Update_LanguageDumpSection('root');
		} else {
			$section_node = self::sectionNode($section->path());
		}
		
		
		foreach (self::key_list($section_node) as $key => $key_obj ) {
			$value = $key_obj->values;
			if ($languages) {
				$value = array();
				foreach ($languages as $lang_alias) {
					$lang_alias['alias'] = isset($lang_alias['alias']) ? $lang_alias['alias'] : $lang_alias['lang_id'];
					$value[$lang_alias['alias']] = $key_obj->values[$lang_alias['lang_id']];
				}
			}
			
			$section->setKey($key, $value);
		}
		
		foreach (self::$xpath->query('section', $section_node)  as $section_item) {
			$section_object = $section->createSection($section_item->getAttribute("name"));
			self::load($languages, $section_object);
		}
		
		return $section;
	}
	
	private static function sectionNode($section_path, DOMElement $parent_node = null) {
		
		$section_stack = explode('.', $section_path);
		$section_name = array_shift($section_stack);
		if (!isset($parent_node)) {
			$parent_node = self::$root_node;
		}
		
		if ($section_name) {
			$section = self::$xpath->query("section[@name='$section_name']", $parent_node)->item(0);
		}
		
		if (count($section_stack)) {
			return self::sectionNode(implode('.', $section_stack), $section);
		}
		return $section;
	}
}

class Update_SectionComparer
{
	/**
	 * new lang tree
	 *
	 * @var Update_LanguageDumpSection
	 */
	private $new_tree;
	
	/**
	 * Old lang tree
	 *
	 * @var Update_LanguageDumpSection
	 */
	private $old_tree;
	
	/**
	 * Temp lang tree
	 *
	 * @var Update_LanguageDumpSection
	 */
	private $tmp_tree;
	
	/**
	 * Current lang tree
	 *
	 * @var Update_LanguageDumpSection
	 */
	private $current_tree;
	
	/**
	 * Log
	 *
	 * @var Update_Log
	 */
	private $log;
	
	/**
	 * Constructor of object
	 *
	 * @param Update_LanguageDumpSection $new_tree
	 * @param Update_LanguageDumpSection $old_tree
	 * @param Update_LanguageDumpSection $current_tree
	 */
	public function __construct($new_tree, $old_tree, $current_tree) {
		$this->new_tree = $new_tree;
		$this->old_tree = $old_tree;
		$this->current_tree = $current_tree;
		$this->tmp_tree = new Update_LanguageDumpSection('root');
	}
	
	/**
	 * Compares trees and returns result of comparison
	 *
	 * @return Update_LanguageDumpSection
	 */
	public function compare() {
		$this->compareSection();
		return $this->tmp_tree;
	}
	
	private function compareSection($parrent_section = null) {
		
		if (!isset($parrent_section)) {
			$section_new = $this->new_tree;
			$section_old = $this->old_tree;
			$section_cur = $this->current_tree;
		} else {
			$path = $parrent_section->path();
			$section_new = $this->new_tree->section($path);
			$section_old = $this->old_tree->section($path);
			$section_cur = $this->current_tree->section($path);
		}
		
		foreach ($section_new->keyList() as $key => $_value)
		{
			foreach ($_value as $lang_id => $value)
			{
				if (!$section_old || !$section_old->keyValueExists($key, $lang_id)) {
					
					if(!$section_cur || !$section_cur->keyValueExists($key, $lang_id)) {
						$this->saveKeyToTmp($section_new->path(), $key, array($lang_id=>$value));
					}
					continue;
				} 			
				
				if ($value != $section_old->key($key, $lang_id)) 
				{
					if ( $section_cur->key($key, $lang_id) != $section_old->key($key, $lang_id) ) {
						continue; 
					}
					$this->saveKeyToTmp($section_new->path(), $key, array($lang_id=>$value));
				}
			}
		}
		
		foreach ($section_new->sectionList() as $section_item) {
			$this->compareSection($section_item);
		}
	}
	
	private function saveKeyToTmp($section_path, $key, $value) {
		$section_stack = explode('.', $section_path);
		$section = $this->tmp_tree->createSection(array_shift($section_stack));
		foreach ($section_stack as $stack_item) {
			$section = $section->createSection($stack_item);
		}
		
		foreach ($value as $lang_id=>$val) {
			$section->setKeyValue($key, $val, $lang_id);
		}
	}
	
}

class Update_Log
{
	const LOG_TYPE_LANG = 'lang';
	const LOG_TYPE_DB = 'db';
	const LOG_TYPE_DOC = 'doc';
	
	private $log_type;
	
	private static $instances = array();
	
	private $entryes = array();	
	
	private function __construct($log_type) {
		$this->log_type = $log_type;
	}
	
	public static function saveAll($update = null) {
		
		if (isset($update)) {
			$update = $update . DIRECTORY_SEPARATOR;
		} else {
			$update = "";
		}
		
		if (file_exists(LOG_DIR  . $update)) {
			@mkdir($update);
		}
		
		foreach (self::$instances as $key => $inst) {
			self::save(LOG_DIR  . $update . $key . '_log.xml' ,$key);
		}
	}
	
	/**
	 * Returns Instance of Log Type Object
	 *
	 * @param string $log_type
	 * @return Update_Log
	 */
	public static function instance($log_type) {
		if ( isset(self::$instances[$log_type]) ) {
			return self::$instances[$log_type];
		}
		self::$instances[$log_type] = new self($log_type);
		return self::$instances[$log_type];
	}
	
	/**
	 * Loads Log
	 *
	 * @param string $file_name
	 * @return Update_Log
	 */
	public static function load($file_name) {
		if (!file_exists($file_name)) {
			return false;
		}
		$dom = new DOMDocument('1.0');
		$dom->preserveWhiteSpace = false;
		$dom->load($file_name);
		$xpath = new DOMXPath($file_name);
		$root_node = $xpath->query("log")->item(0);
		$log_type = $root_node->getAttribute('type');
		
		self::$instances[$log_type] = new self($log_type);
		$log = self::$instances[$log_type];
		
		$entryes = $xpath->query('entry', $root_node);
		foreach ($entryes as $item) {
			$log->add($item->textContent, $item->getAttribute("action"), $item->getAttribute("result"));
		}
		
		return $log;
	}
	
	public static function save($file_name, $log_type) {
		if (!is_writable( LOG_DIR )) {
			return false;
		}
		
		if (!file_exists(dirname($file_name))) {
			mkdir(dirname($file_name));
		}
		
		$dom = new DOMDocument('1.0');
		$root_node = $dom->createElement('log_document');
		$dom->appendChild($root_node);
		$log_node = $dom->createElement('log');
		$log_node->setAttribute('type', $log_type);
		$root_node->appendChild($log_node);
		$log = self::$instances[$log_type];
		foreach ($log->entryes as $item) {
			$entry_node = $dom->createElement('entry');
			$log_node->appendChild($entry_node);
			$data = new DOMText($item['data']);
			$entry_node->appendChild($data);
			$entry_node->setAttribute('action', $item['action']);
			$entry_node->setAttribute('result', $item['result']);
		}
		
		return $dom->save($file_name);
	}
	
	public function add($entry, $action, $result) {
		$this->entryes[] = array(
				'data'	=> $entry,
				'action'	=> $action,
				'result'	=> $result
			);
	}
	
	public function getList() {
		return $this->entryes;
	}
}

class Update_LogReader
{
	const LOG_DB = 'db';
	const LOG_DOC = 'doc';
	const LOG_LANG = 'lang';
	
	private $dir;
	
	/**
	 * XML Document
	 *
	 * @var DOMDocument
	 */
	private $xml_document;
	
	/**
	 * Xpath Object
	 *
	 * @var DOMXPath
	 */
	private $xpath;
	
	/**
	 * Root Element
	 *
	 * @var DOMElement
	 */
	private $root_node;
	
	public function __construct($dir = LOG_DIR) {
		$this->dir = $dir;
	}
	
	private function openLog($file) {
		if (!file_exists($file)) {
			return false;
		}
		
		$this->xml_document = new DOMDocument('1.0');
		$this->xml_document->preserveWhiteSpace = false;
		$this->xml_document->load($file);
		$this->xpath = new DOMXPath($this->xml_document);
	}
	
	private function readLog($type) {
		if (!isset($this->xpath)) {
			return array();
		}
		$root_node = $this->xpath->query("log[@type='$type']")->item(0);
		$nodes = $this->xpath->query('entry', $root_node);
		$out = array();
		foreach ($nodes as $node) {
			 $item = & $out[];
			 $item['action'] = $node->getAttribute('action');
			 $item['result'] = $node->getAttribute('result');
			 $item['content'] = $node->textContent;
		}
		return $out;
	}
	
	public function readDbLog() {
		$this->openLog($this->dir . 'db_log.xml');
		
		return $this->readLog(self::LOG_DB); 
	}
	
	public function readDocLog() {
		$this->openLog($this->dir . 'doc_log.xml');
		return $this->readLog(self::LOG_DOC); 
	}
	
	public function readLangLog() {
		$this->openLog($this->dir . 'lang_log.xml');
		return $this->readLog(self::LOG_LANG);
	}
}

class UpdateLanguageException extends Exception {}