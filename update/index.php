<?php
require_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'internals'.DIRECTORY_SEPARATOR.'Header.inc.php';
require_once 'inc/Pack.class.php';
require_once 'inc/step.class.php';

define("UPDATE_DIR", 'updates' . DIRECTORY_SEPARATOR);

class Update_PrepareMaster extends Update_Master
{
	const LANG_TREE_NEW = 'new_tree.xml';
	const LANG_TREE_OLD = 'old_tree.xml';
	
	public function __construct() {
		$this->setStep_count(5);
		parent::__construct();
	}
	
	
	/**
	 * @see Update_Master::back_control()
	 *
	 * @return unknown
	 */
	public function back_control() {
		if ($this->step == $this->step_count) {
			return "";
		}
		return parent::back_control();
	}
	
	/**
	 * @see Update_Master::next_control()
	 *
	 * @return unknown
	 */
	public function next_control() {
		return parent::next_control();
	}
	
	/**
	 * @see Update_Step::legend()
	 *
	 */
	public function legend() {
		
	switch ($this->step())
		{
			case 1:
				return "Update selection";
			case 2:
				return "Update configuration";		
			case 3:
				return "Language matching";
			case 4:
				return "Permissions setup";
			case 5:
				return "Update completed!";
		}
	}
	
	/**
	 * @see Update_Step::render()
	 *
	 */
	public function render() {
		switch ($this->step())
		{
			case 1:
				return $this->selectUpdateRender();
			case 2:
				return $this->selectUpdateTypeRender();		
			case 3:
				return $this->selectLangForUpdateRender();
			case 4:
				return $this->lastPageRender();
			case 5:
				$update = $this->getVar('update');
				return "<br />&nbsp;&nbsp;Update completed. (<a href='logs.php?update=$update' target=_blank>log</a>)<br/>&nbsp;&nbsp;Press `<b>Finish</b>` to regenerate cache!";
		}
	}
	
	private function selectLangRender() {
		 return <<<EOT
	<table>
		<tr>
			<td>
				Languages
			</td>
			<td>
				<input type="checkbox" name="update_items[]" value="lang">
			</td>
		</tr>
		</table>
EOT;
	}
	
	private function selectUpdateTypeRender() {
		
		$update_items = $this->getVar('update_items') ? $this->getVar('update_items') : array();
		$lang_checked = in_array("lang", $update_items) ? 'checked="checked"' : '';
		$doc_checked = in_array("doc", $update_items) ? 'checked="checked"' : '';
		$db_checked = in_array("db", $update_items) ? 'checked="checked"' : ''; 
		
		 return <<<EOT
	<table>
		<tr>
			<td>
				Languages
			</td>
			<td>
				<input type="checkbox" name="update_items[]" value="lang" $lang_checked>
			</td>
		</tr>
		<tr>
			<td>
				Document Structure
			</td>
			<td>
				<input type="checkbox" name="update_items[]" value="doc" $doc_checked>
			</td>
		</tr>
		<tr>
			<td>
				Database
			</td>
			<td>
				<input type="checkbox" name="update_items[]" value="db" $db_checked>
			</td>
		</tr>
		</table>
EOT;
	}
	
	private function selectUpdateRender() {
	
		$updates = '<select name="update">';
		$dir = dir(UPDATE_DIR);
		$has_updates = false;
		while ( false !== ($update = $dir->read()) ) {
			if ($update !='.' && $update != '..' && is_dir(UPDATE_DIR . $update)) {
				$has_updates = true;
				$updates .= '<option value="' . $update . '">' . $update . '</option>';
			} 
		}
		$updates .= '</select>';
		if (!@$this->getVar("update_type")) {
			$auto = 'checked="checked"';
		} else {
			$auto = $this->getVar("update_type")=='auto' ? 'checked="checked"' : '';
			$custom = $this->getVar("update_type")!='auto' ? 'checked="checked"' : '';
		}
		
		
		if (!$has_updates) {
			$updates = '<div style="border: 1px solid red; padding: 4px">
			There are no updates in directory `<b>updates</b>`.<br />Please dounload it from CA.
			</div>';
		}
		
		return <<<EOT
	<table>
		<tr>
			<td>
				Update
			</td>
			<td>
				$updates
			</td>
		</tr>
	</table>
	<br />
	<br />
	<table width="100%">
		<tr>
			<td class="thead_td" style="padding-left: 18px">Update type</td>
		</tr>
		<tr>
			<td width="50%">Standard update</td>
			<td><input type="radio" name="update_type" value="auto" $auto></td>
		</tr>
		<tr>
			<td>Advanced update</td>
			<td><input type="radio" name="update_type" value="custom" $custom></td>
		</tr>
	</table>
EOT;
	}
	
	private function lastPageRender() {
	    
	    $warnings = $this->getWarnings();
	    $warnings[] = 'Set permissions <b>`777`</b> to folder <b>`update/log`</b> <br /> <br /> Press `next` to update the Site!'; 
	    
	    $msgString = '';
	    foreach ( $warnings as $w )
	    {
	        $msgString .='<tr><td colspan="2">' . $w . '</td></tr>';
	    }
	    
		return "<table>$msgString</table>";
	}
	
	protected function stepHandleComplete() {
		parent::stepHandleComplete();
		if ($this->step == 2) {
			$skip = $this->getVar("update_type") == 'auto';
			if ($skip) {
				if ($_POST["next"]) {
					$this->step = $this->step + 1;
				} elseif ($_POST["back"]) {
					$this->step = $this->step - 1;
				}
			}
		}
		
		if ($this->step == 3) {
			$units = $this->getVar("update_items") ? $this->getVar("update_items") : array();
			$skip = ($this->getVar("update_type") == 'custom' && !in_array('lang', $units));
			if ($skip) {
				if ($_POST["next"]) {
					$this->step = $this->step + 1;
				} elseif ($_POST["back"]) {
					$this->step = $this->step - 1;
				}
			}
		}
		
		SK_HttpRequest::redirect($_SERVER['PHP_SELF']);
	}
	
	private $tries = array();
	private function readLangTreeInfo($update, $tree) {
		if (isset($this->tries[$update][$tree])) {
			return  $this->tries[$update][$tree]->tree_info;
		}
		
		$update_dir = UPDATE_DIR . $update . DIRECTORY_SEPARATOR;
		$this->tries[$update][$tree] = Update_XMLControler::loadTree($update_dir . $tree);
		return  $this->tries[$update][$tree]->tree_info;
	}
	
	private function selectLangForUpdateRender() {
	
		$exist_value = $this->getVar("lang_aliases");
		
		$tree_info = $this->readLangTreeInfo($this->getVar('update'), self::LANG_TREE_NEW); 

		$langs = "";
		foreach ($tree_info->languages as $key => $language) {
		
			$langs .= "<tr><td>";
			
			$rev_langs = '<select class="rev_langs" name="langs['.$key.'][lang_id]">
				<option value="">Select Language</option>';
			foreach ($tree_info->languages as $language) {
				$selected = $exist_value[$key]["lang_id"] == $language['id'] ? 'selected="selected"' : '';				
				$rev_langs .= ' to <option value="' . $language['id'] . '" '.$selected.'>' . $language['label'] . '</option>';
			}
			$rev_langs .= '</select>';
			
			
			
			$db_langs = ' -> <select class="db_langs" name="langs['.$key.'][alias]"><option value="">Select Language</option>';
			$_db_langs = SK_LanguageEdit::getLanguages();
			foreach ($_db_langs as $language) {
				$selected = $exist_value[$key]["alias"] == $language->lang_id ? 'selected="selected"' : '';
				$db_langs .= '<option value="' . $language->lang_id . '" '.$selected.'>' . $language->label . '</option>';
			}
			
			if (count($_db_langs) < count($tree_info->languages)) {
				$selected = $exist_value[$key]["alias"] == 'new' ? 'selected="selected"' : '';
				$db_langs .= '<option value="new" '.$selected.'>New</option>';
			} 
			
			$db_langs .= '</select>';
			
			$langs .= $rev_langs;
			$langs .= '</td><td>';
			$langs .= $db_langs;
			
			$langs .= "</td></tr>";
		}

		return <<<EOT
	<table>
		<tr>
			<td align="center">Revision Language</td>
			<td align="center">Site Language</td>
		<tr>
		$langs
	</table>
	<br />
	<p align="center">
		"Revision Language" will overwrite "Site Language"!
	</p>
EOT;
	}
	
	/**
	 * @see Update_Master::stepOnHandle()
	 *
	 * @param unknown_type $data
	 */
	protected function stepOnHandle($data) {
		switch ($this->step())
		{
			case 1:
				if (!@$data['update']) {
					$this->error("Update not selected!");
				}
				break;
			case 2:
				if (!count($data['update_items'])) {
					$this->error("Please select what should be updated!");
				}
				break;	

			case 3: 
				
				$aliases = array();
				$lang_ids = array();
				
				foreach ($data['langs'] as $item) {
																		
					if (($item['alias'] && !$item['lang_id']) || (!$item['alias'] && $item['lang_id'])) {
						$this->error("Languages matched incorrectly!");
						break;
					}
					
					if(!$item['alias'] && !$item['lang_id']) {
						continue;
					}
					
					if ($item['alias'] != 'new') {
						if (in_array($item['alias'], $aliases) || in_array($item['lang_id'], $lang_ids)) {
							$this->error("Languages matched incorrectly!");
							break;
						}
					}
					
					$aliases[] = $item['alias'];
					$lang_ids[] = $item['lang_id'];
				}
				
				break;
			
			case 4:
				
				break;
			case 5:
				
				break;
		}
		return $data;
	}

	
	/**
	 * @see Update_Step::stepHandler()
	 *
	 * @param array $data
	 * @return array
	 */
	protected function stepHandler($data) {
		switch ($this->step())
		{
			case 1:
				$this->saveVar("update_type", $data['update_type']);
				$this->saveVar("update", $data['update']);
				break;
			case 2:
				$this->saveVar("update_items", $data['update_items']);
				break;
			case 3:
				$this->saveVar("lang_aliases", $data['langs']);
				break;
				
			case 4:
				return $this->update();
			case 5:
				@mkdir(DIR_INTERNAL_C . 'lang');
				SK_LanguageEdit::generateCache();
				SK_Layout::clear_all_compiled();
				$this->session = array();
				SK_HttpRequest::redirect(SITE_URL);
				break;
		}
	}
	
	/**
	 * @see Update_Step::title()
	 *
	 */
	public function title() {
		return "Site Update";
	}
	

	private function runFunctions()
	{
		$update = $this->getVar('update');
		$fnc_file = UPDATE_DIR . $update . DIRECTORY_SEPARATOR . 'functions.php';
		if (file_exists($fnc_file))
		{
			require_once $fnc_file;
		}
	}
	
    private function getWarnings()
    {
        $update = $this->getVar('update');
        $file = UPDATE_DIR . $update . DIRECTORY_SEPARATOR . 'warnings.php';
        @include $file;

        return empty($updateMessageList) ? array() : $updateMessageList; 
    }
	
	private function update()
	{
		$update_items = $this->getVar('update_items');
		$update_items = $update_items ? $update_items : array('lang', 'doc', 'db'); 
		foreach ($update_items as $item) {
			switch ($item) {
				case 'lang':
					$this->updateLang();
					break;
				case 'doc':
					$this->updateDoc();
					break;
				case 'db':
					$this->updateDB();
					break;
			}
		}
		
		$this->runFunctions();
		
		$update = $this->getVar('update');
		Update_Log::saveAll($update);
	}
	
	private function updateLang()
	{
		$lang_alias = array();
		$update = $this->getVar('update');
		
		$tree_info = $this->readLangTreeInfo($update, self::LANG_TREE_NEW); 
		
		foreach ($this->getVar('lang_aliases') as $key => $item) {
			if ($item['alias'] == 'new') {
				$language = $tree_info->languages[$item['lang_id']];
				$query = SK_MySQL::placeholder("INSERT INTO `".TBL_LANG."`(`abbrev`,`label`) VALUES('?', '?')",	$language['abrev'] , $language['label'] );
				SK_MySQL::query($query);
				if( !($item['alias'] = SK_MySQL::insert_id()) ) {
					continue;
				}
			}
			
			if (isset($item["lang_id"]) && intval($item["lang_id"])) {
				$lang_alias[$key]["lang_id"] = intval($item["lang_id"]);
				if (isset($item["alias"]) && $item["alias"]) {
					$lang_alias[$key]["alias"] = $item["alias"];
				}
			}
		}
		
		$new_tree = Update_XMLControler::loadTree( UPDATE_DIR . $update . DIRECTORY_SEPARATOR . 'new_tree.xml', $lang_alias);
		$old_tree = Update_XMLControler::loadTree( UPDATE_DIR . $update . DIRECTORY_SEPARATOR . 'old_tree.xml', $lang_alias);
			
		$db_langs = array();
		foreach ($lang_alias as $key => $item) {
			$db_langs[$key]['lang_id'] = $item['alias'];
		}
		
		$current_tree = Update_DBControler::loadTree($db_langs);
			
		$comparer = new Update_SectionComparer($new_tree, $old_tree, $current_tree);
		
		$result = $comparer->compare();
		Update_DBControler::saveTree($result);
		
		@mkdir(DIR_INTERNAL_C . 'lang');
	}
	
	private function updateDB()
	{
		$dom = new DOMDocument('1.0');
		$dom->preserveWhiteSpace = false;
		$update = $this->getVar('update');
		
		$file_name = UPDATE_DIR . $update . DIRECTORY_SEPARATOR . 'queries.xml';
		
		$dom->load($file_name);
		$xpath = new DOMXPath($dom);
		
		$queryes= $xpath->query("query");
		
		$_queryes = array();	
		foreach ($queryes as $item) {
			$query = str_replace('%DB_PREFIX%', DB_TBL_PREFIX, $item->textContent);
			$index = $item->getAttribute('order');
			$_queryes[$index] = $query;
		}
		
		foreach ($_queryes as $item) {
			Update_DBControler::query($item);
		}
		
	}

	private function updateDoc()
	{
		$dom = new DOMDocument('1.0');
		$dom->preserveWhiteSpace = false;
		$update = $this->getVar('update');
		
		$file_name = UPDATE_DIR . $update . DIRECTORY_SEPARATOR . 'documents.xml';
		
		$dom->load($file_name);
		$xpath = new DOMXPath($dom);
		
		$docs= $xpath->query("document");
		foreach ($docs as $item) {
			$doc_key = $item->getAttribute('key');
			$parent_doc_key = $xpath->query("parent_document_key", $item)->item(0)->textContent;
			$url = $xpath->query("url", $item)->item(0)->textContent;
			$access_guest = $xpath->query("access_guest", $item)->item(0)->textContent;
			$access_member = $xpath->query("access_member", $item)->item(0)->textContent;
			$status = $xpath->query("status", $item)->item(0)->textContent;
			$order = $xpath->query("order", $item)->item(0)->textContent;
			$custom = $xpath->query("custom", $item)->item(0)->textContent;
			
			
			
			$exists = SK_MySQL::query("SELECT COUNT(*) FROM `" . TBL_DOCUMENT . "` WHERE `document_key`='$doc_key'")->fetch_cell();
			if (!$exists)
			{
				$this->createDocument($doc_key, $parent_doc_key, $url, $access_guest, $access_member, $status, $custom);
			}
		}
	}
	
	private function createDocument($doc_key, $parent_doc_key, $url, $access_guest, $access_member, $status, $custom)
	{
		$log = Update_Log::instance(Update_Log::LOG_TYPE_DOC);
		
		// get max document order
		$query = "SELECT MAX( `order` ) FROM `".TBL_DOCUMENT."` 
			WHERE `parent_document_key`='$parent_doc_key'";	
		
		$max_order = SK_MySQL::query($query)->fetch_cell() + 1;
		
		$query = "INSERT INTO `".TBL_DOCUMENT."`
			( `document_key`, `parent_document_key`, `url`, `access_guest`, `access_member`, `status`, `order`,`custom`) 
			VALUES( '$doc_key', '$parent_doc_key', '$url', '$access_guest', '$access_member', '$status', '$max_order', $custom )";

		try {	
			SK_MySQL::query($query);
			$msg = "Document Create: <br>";
			$msg .= "<b>$parent_doc_key.$doc_key</b>";
			$log->add($msg, 'add_doc', 'success');
			
		} catch (SK_MySQL_Exception $e) {
			$msg = "Document Create: <br>";
			$msg .= "<b>$parent_doc_key.$doc_key</b>";
			$log->add($msg, 'add_doc', 'faild');
		}
		
		for ($i=1; $i <=4; $i++) {
			try {
				SK_MySQL::query("
					INSERT INTO `" . TBL_LINK_ADS_PAGE_POSITION . "` VALUES
					(NULL, '$doc_key', $i);
				");
			} catch (SK_MySQL_Exception $e) {
					$msg = "Ads position (<b>$i</b>) creation faild<br>";
					$msg .= "<b>$parent_doc_key.$doc_key</b>";
					$log->add($msg, 'add_doc', 'faild');
			}
		}
	}
	
}

$step = new Update_PrepareMaster();
$step->display();
