<?php

require_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'internals' . DIRECTORY_SEPARATOR . 'Header.inc.php';
/*
  if(!app_Features::isAvailable(6))
  SK_HttpRequest::showFalsePage();
 */
$Layout = SK_Layout::getInstance();

$httpdoc = new component_Fantasy_List('all');

$ajax_mode = (SK_HttpRequest::isXMLHttpRequest() == 1);

if (!$ajax_mode) {
    $Layout->display($httpdoc);
} else {
    $result = array();

    if (isset($_GET['q'])) {
        $result = app_Tags::GetTagsSearch($_GET['q']);
    } elseif (isset($_GET['q_p'])) {
        $result = app_Tags::PerformerGetTagsSearch($_GET['q_p']);
    } elseif (isset($_GET['request'])) {
        
    } elseif (isset($_POST['action'])) {
        $result['sendworck'] = false;
        switch (trim($_POST['action'])) {
            case 'send':
                if (app_FantasyWorck::SendRequest(intval($_POST['id_fantasy']))) {
                    $result['sendworck'] = true;
                }
                break;
            case 'delete':
                if (app_FantasyWorck::DeleteReqest(intval($_POST['id_fantasy']))) {
                    $result['sendworck'] = true;
                }
                break;
            case 'info'://информация по фантазии
                if ($result = app_Fantasy::GetFantasy(intval($_POST['id_fantasy']))) {
                    $result['sendworck'] = true;
                }
                break;
            case 'info_work'://информация по контракту
                if ($fantasy_id = app_FantasyWorck::GetFantasyID($_POST['id_work'])) {
                    $_SESSION['offerts_details']['fantasy_id'] = $fantasy_id;
                    $result = app_Fantasy::GetFantasy($fantasy_id);
                    $work_info = app_FantasyWorck::GetWorck($_POST['id_work']);
                    $result = array_merge($result, $work_info);
                    $result['sendworck'] = true;
                }
                break;
        }
    }

    echo(json_encode($result));
}