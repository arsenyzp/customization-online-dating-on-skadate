<?php

require_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'internals' . DIRECTORY_SEPARATOR . 'Header.inc.php';

$Layout = SK_Layout::getInstance();

$ajax_mode = (SK_HttpRequest::isXMLHttpRequest() == 1);

if (!$ajax_mode) {
    $httpdoc = new component_VideoView;
    $Layout->display($httpdoc);
} else {
    $result = array('buy' => false);
    if (app_ProfileVideoPurchased::BuyVideo(intval($_POST['video_id']))) {
        $result['buy'] = true;
    }
    echo(json_encode($result));
}
