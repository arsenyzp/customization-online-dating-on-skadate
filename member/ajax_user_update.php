<?php

require_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'internals' . DIRECTORY_SEPARATOR . 'Header.inc.php';


//сохранение временной картинки при редактировании профиля
if (isset($_FILES['file'])) {
       $profile_id = SK_HttpUser::profile_id();
       // $album_id = app_PhotoAlbums::getAlbums($profile_id)
       // ;
       try {
              $field = new field_profile_photo('file');
              $tmp_file = SK_TemporaryFile::catchFile($_FILES['file'], $field, false);
              $preview_html = $field->preview($tmp_file);
              $preview_html = str_replace('<div style="float: left; margin: 2px; text-align: center">', '', $preview_html);
              $preview_html = str_replace('<br />
	<a class="delete_file_btn" href="#">[delete]</a>
</div>', '', $preview_html);
              $_SESSION['new_foto'] = $tmp_file->getUniqid();
              echo $preview_html;
       } catch (Exception $e) {
              echo "window.sk_upload_error = " . json_encode($e->getMessage()) . ";\n"; // TODO: user language messages
       }
       exit();
}

/**
 * сохранение изменений в профиле пользователя из форм на разных страницах
 */
$ajax_mode = (SK_HttpRequest::isXMLHttpRequest() == 1);
//переделать 
if (!$ajax_mode) {
       SK_HttpRequest::showFalsePage();
} else {
       $result = array();
       $profile_id = SK_HttpUser::profile_id();
       if (isset($_POST['status'])) {//обновление статуса
              app_Profile::updateUserStatus($profile_id, $_POST['status']);
       } else if (isset($_GET['term'])) {//список пользователей для автокомплитера
              $result['names'] = app_Profile::SearchUsername($_GET['term'], 10);
              header('Cache-Control: no-cache, must-revalidate');
              header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
              header('Content-type: application/json');
       } else if (isset($_POST['action'])) {
              switch ($_POST['action']) {
                     case 'delete_profile_foto'://удалние аватарки
                            app_ProfilePhoto::deleteThumbnail(intval($_GET['profile_id']));
                            break;
                     case 'update_profile_foto'://сохранение аватарки
                            if (isset($_SESSION['new_foto']))
                                   $photo_id = app_ProfilePhoto::upload($_SESSION['new_foto'], 1);
                            app_ProfilePhoto::createThumbnail($photo_id);
                            break;
                     case 'delete_media'://удалние из this_is_me
                            if (isset($_POST['type']) && $_POST['type'] == 'photo') {
                                   app_ProfilePhoto::delete(intval($_POST['id']));
                            }
                            if (isset($_POST['type']) && $_POST['type'] == 'video') {
                                   app_ProfileVideo::deleteVideo(intval($_POST['id']));
                            }
                            break;
                     case 'canel_reqest'://отменазапроса на выполение фантазии
                            $result['result'] = app_FantasyWorck::CancelRequest(intval($_POST['id_worck']));
                            break;
              }
       }

       echo(json_encode($result));
}