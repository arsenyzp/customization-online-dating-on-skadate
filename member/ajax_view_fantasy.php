<?php
/**
 * получение формы с полным описанием фантазии
 */
require_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'internals' . DIRECTORY_SEPARATOR . 'Header.inc.php';

$Layout = SK_Layout::getInstance();

$httpdoc = new component_FantasyView(array('ajax'=>true));

$Layout->displayAjax($httpdoc);