<?php
require_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'internals'.DIRECTORY_SEPARATOR.'Header.inc.php';

$Layout = SK_Layout::getInstance();

$httpdoc = new component_Notice();

$ajax_mode = (SK_HttpRequest::isXMLHttpRequest() == 1);

if (!$ajax_mode) {
    $Layout->display($httpdoc);
} else {
    $result = array();
        if (isset($_POST['id'])) {
            $result = app_Notice::DeleteNotice($_POST['id']);
        }
    echo(json_encode($result));
}
