<?php

require_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'internals' . DIRECTORY_SEPARATOR . 'Header.inc.php';
/*
  if(!app_Features::isAvailable(6))
  SK_HttpRequest::showFalsePage();
 */
$Layout = SK_Layout::getInstance();

$httpdoc = new component_ViewWorck();

$ajax_mode = (SK_HttpRequest::isXMLHttpRequest() == 1);

if (!$ajax_mode) {
    $Layout->display($httpdoc);
} else {
    $result = array();

    if (isset($_GET['q'])) {
        $result = app_Tags::GetTagsSearch($_GET['q']);
    } elseif (isset($_GET['q_p'])) {
        $result = app_Tags::PerformerGetTagsSearch($_GET['q_p']);
    } elseif (isset($_GET['request'])) {
        
    } elseif (isset($_POST['action'])) {
        switch (trim($_POST['action'])) {
            case 'send':
                if (app_FantasyWorck::SendRequest(intval($_POST['id_fantasy']))) {
                    $result['sendworck'] = true;
                }
                else $result['sendworck'] = false;
                break;
            case 'delete':
                if (app_FantasyWorck::DeleteReqest(intval($_POST['id_fantasy']))) {
                    $result['sendworck'] = true;
                }
                else $result['sendworck'] = false;
                break;
            case 'approve':
                if (app_FantasyWorck::CheckMyFantasy(intval($_POST['fantasy_id']))) {
                    $result['approve'] = app_FantasyWorck::ApproveWorck(intval($_POST['fantasy_id']));
                }
                break;
        }
    }


    echo(json_encode($result));
}