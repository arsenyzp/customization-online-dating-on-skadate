<?php

require_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'internals' . DIRECTORY_SEPARATOR . 'Header.inc.php';

$Layout = SK_Layout::getInstance();

$httpdoc = new component_ViewRequest();

$ajax_mode = (SK_HttpRequest::isXMLHttpRequest() == 1);

if (!$ajax_mode) {
    $Layout->display($httpdoc);
} else {
    $result = array();
    if (isset($_GET['q'])) {
        $result = app_Tags::GetTagsSearch($_GET['q']);
    } elseif (isset($_GET['q_p'])) {
        $result = app_Tags::PerformerGetTagsSearch($_GET['q_p']);
    }
    elseif(isset($_GET['request'])){
        
    }
    elseif(isset($_POST['action'])){
        if(isset($_POST['id_worck']) && intval($_POST['id_worck'])>0)
        switch (trim($_POST['action'])){
            case 'sendaccepted':
               $result['result'] = app_FantasyWorck::Accepted(intval($_POST['id_worck']));
                break;
            case 'senddecline':
               $result['result'] = app_FantasyWorck::Decline(intval($_POST['id_worck']));
                break;
            case 'canel_reqest':
               $result['result'] = app_FantasyWorck::CancelRequest(intval($_POST['id_worck']));
                break;
        }
    }

    echo(json_encode($result));
}