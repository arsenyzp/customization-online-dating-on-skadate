<?php

require_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'internals' . DIRECTORY_SEPARATOR . 'Header.inc.php';

$Layout = SK_Layout::getInstance();

$ajax_mode = (SK_HttpRequest::isXMLHttpRequest() == 1);

if (!$ajax_mode) {
       $httpdoc = new component_ProfileView();
       $Layout->display($httpdoc);
} else {
       $params = array(
           'iter' => intval($_POST['iter']),
           'profile_id' => intval($_POST['profile_id'])
           );
       $httpdoc = new component_ThisIsMeSlider($params);
       $Layout->displayAjax($httpdoc);
}