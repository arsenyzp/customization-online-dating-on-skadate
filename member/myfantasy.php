<?php

require_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'internals' . DIRECTORY_SEPARATOR . 'Header.inc.php';
/*
  if(!app_Features::isAvailable(6))
  SK_HttpRequest::showFalsePage();
 */
$Layout = SK_Layout::getInstance();

$httpdoc = new component_Fantasy_List('my');

$ajax_mode = (SK_HttpRequest::isXMLHttpRequest() == 1);

if (!$ajax_mode) {
    $Layout->display($httpdoc);
} else {
    $result = array();
    if (isset($_GET['q'])) {
        $result = app_Tags::GetTagsSearch($_GET['q']);
    } elseif (isset($_GET['q_p'])) {
        $result = app_Tags::PerformerGetTagsSearch($_GET['q_p']);
    }
    elseif(isset($_GET['request'])){
        
    }

    echo(json_encode($result));
}