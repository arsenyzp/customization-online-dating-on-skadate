{* component Invate Friend (PrivateInvite.cmp.php)*}
{container stylesheet="invite_fantasy.style" class="report"}
{if $reporter_id}
	
{if $show_link}
 <a id="button_invete" href="javascript://">Invite to Fantasy</a> 
{else}
<input id="button_invete" type="button" value="Invite to Fantasy" /> 
{/if}
	<div id="invete" style="display: none">
		{form InviteFantasy reporter_id=$reporter_id entity_id=$entity_id type=$type}
                <div class="rows_form">
                        <div class="left_cols_form">Is this offer related to an existing fantasy?</div>
                        <div class="right_cols_form">
                            <div>
                                <input type="radio" name="offer_type" checked="checked" /><b>Yes</b> -I pick which one
                            </div>
                            <div>
                                <input type="radio" name="offer_type" onclick="NewFatasyInvite({$entity_id})" /><b>No</b> - Automatically create a new private fantasy for this offer
                            </div>
                        </div>
                        <br clear="all" />
                    </div>
                
                
                <div class="padding10">
                    <div class="row popup_form">
                        <div style="margin-left: 0; width: 60px;" class="performer_list span1">Fantasy: </div>
                        <div class="span3">
                            <select name="fantasy_id" id="select_fantasy" onchange="UpdateFantasyInfo();">
                                 <option value="0">-- Select Fantasy --</option>
                                {foreach from=$my_fantasy_list item=option}
                                    <option value="{$option.id}">{$option.title}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                </div>
                <div class="padding10">
                    <div class="row popup_form">
                        <div style="margin-left: 0;" class="invite_label span2">Price offer: </div>
                        <div class="span3"><input type="text" value="" name="budget" id="budget_invite" /></div>
                    </div>
                    <div class="row popup_form">
                        <div style="margin-left: 0;" class="invite_label span3">Kuinkey.com fee: </div>
                        <div class="span1" id="kuinkey_fee"></div>
                    </div>
                    <div class="row popup_form">
                        <div style="margin-left: 0;" class="invite_label span3">Paid to Performer: </div>
                        <div class="span1"  id="paid_to_performer"></div>
                    </div>
                    <div class="row popup_form">
                        <div style="margin-left: 0;" class="invite_label span3">Lenght video: </div>
                        <div class="span1" id="video_length"></div>
                    </div>
                    <div class="row popup_form">
                        <div style="margin-left: 0;" class="invite_label span2">Completion Date: </div>
                        <div class="span3">
                            <input type="text" name="completion_date" id="datapicker2" value="{$worck_info.completion_date}"  />
                        </div>
                    </div>
                    <div class="row popup_form">
                        <div style="margin-left: 0;" class="invite_label span2">Message to Performer: </div>
                        <div class="span2">
                          
                        </div>
                    </div>
                    <div class="row popup_form">
                        <textarea name="messag"></textarea>
                    </div>
                    <div class="row popup_form">
                        <input type="submit" value="Invite" />
                        <input type="button" value="Cancel"  />
                    </div>
                </div>
    {/form}
  </div>
                
                
                       
                
{/container}
{/if}
