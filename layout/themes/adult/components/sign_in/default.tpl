{* component Sign In Form *}

{container stylesheet="sign_in.style"}

<div class="sign_in_cont" {if $hidden}style="display: none"{/if}>

            <div class="fb_content" id="sing_in_form_m">
            {block}

                            {form SignIn}
                                    <div class="field">
                                            <div>{label for='login'}</div>
                                            <div class="input_name">{input name='login'}</div>
                                    </div>
                                    <div class="field">
                                            <div >
                                                                                            <div style="float:left; margin-right: 5px;">Password</div>  
                                                                                            <a href="#" onclick="disp(); return false; " style="color: #333333">Forgot it</a>
                                                                                            </div>
                                            <div class="input_pass">{input name='password'}</div>
                                    </div>
                                                                            <div class="field">
                                            <div >{input name='remember_me' label=%.forms.sign_in.fields.remember_me.label}</div>
                                    </div>
                                                                            <div class="field">
                                            <div style="width:50px; margin-top: 10px;">{button action='process'}</div>
                                            <div class="input_pass"></div>
                                    </div>
                            {/form}	
            {/block}

            </div>
                
                <div id="forgot_form_m" style="display: none;">
                    <div class="block_body">
                                {form ForgotPassword}
                                        <div class="field">
                                                <div>{label for='email'}</div>
                                                <div class="input_name">{input name='email'}</div>
                                        </div>
                                        <div class="field">
                                                <div style="width:50px; margin-top: 10px;">{button action='send'}</div>
                                                <div class="input_pass"></div>
                                        </div>
                                                <div style="height: 20px;"></div>
                                {/form}
                      </div>
	</div>
</div>  
                            {*не работает скрытие формы*}    
                                {literal}
                                    <script>
                                    function disp(){
                                        $('#sing_in_form_m').hide();
                                         $('#forgot_form_m').show();
                                    }
                                    </script>
                                {/literal}
{/container}
