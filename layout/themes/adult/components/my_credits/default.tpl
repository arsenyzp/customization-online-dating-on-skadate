
{canvas}
	{container stylesheet="my_credits.style"}

	

	{component ProfileInfoLeft}

<div class="content_100">

    {component ProfileInfoHeader}

    <div class="content span7">	
        <div class="content_text">
            <!--Меню пользователя-->
            <div class="content_menu">
                <div class="content_menu_item {if $tabs eq(55)}activ_menu{/if}"><a href="/member/newsfeed.php">NEWS</a></div>
                <div class="content_menu_item  {if $tabs eq(1)}activ_menu{/if}"><a href="/member/myfantasy.php">MY FANTASIES</a></div>
                <div class="content_menu_item {if $tabs eq(11)} activ_menu{/if}"><a href="/member/view_works.php">MY LIBRARY</a></div>
                <div class="content_menu_item {if $tabs eq(77)} activ_menu{/if}"><a href="/member/sent_applications.php">MY JOBS</a></div>
                <div class="content_menu_item {if $tabs eq(44)}activ_menu{/if}"><a href="/member/view_review.php?mode=my">MY REVIEWS</a></div>
                <div class="content_menu_item {if $tabs eq(3)} activ_menu{/if}"><a href="/member/following.php?tabs=3">FOLLOWING</a></div>
                <div class="content_menu_item {if $tabs eq(22)} activ_menu{/if}"><a href="/member/bookmarks.php?tabs=22">FOLLOWERS</a></div>
                <div class="content_menu_item activ_menu" style="float: right; background-color: #412756; margin-right: 10px;"><a href="/member/edit_profile.php">MY ACCOUNT</a></div>
            </div>
            <div style="height: 10px"></div>
            <div class="content_menu">
                                    <div class="content_menu_item"><a href="/member/edit_profile.php">SETTINGS</a></div>
                                    <div class="content_menu_item activ_menu"><a href="/member/points_purchase.php">MANAGE KEYS</a></div>
                                    <div class="content_menu_item"><a href="/member/block_list.php">BLOCK LIST</a></div>
                                    <div class="content_menu_item"><a href="/member/payment_selection.php">KUINKEY+</a></div>
                                </div>

            <div class="content_tab">
                   
                     <div class="line"></div>
                <div class="profile_menu row">
                    <div class="span1">
                        <a {if $tabs eq(0)}class="active"{/if} href="/member/my_credits.php">Summary</a>
                    </div>
                    <div class="span1">
                        <a  href="/member/points_purchase.php">Add Keys</a>
                    </div>
                    <div class="span2">
                        <a {if $tabs eq(1)}class="active"{/if} href="/member/my_credits.php?tabs=1">Withdraw keys</a>
                    </div>
                </div>
                <div class="line"></div>
                
                <div class="row">
                       <div class="span1 blue_keys"></div>
                       <div class="span kuinkey_title key_t">{$total_keys}</div>
                </div>
                
                
                            {if $tabs eq(0)}
                                   <div>
                                          {foreach from=$log item='e'}
                                          <div class="{cycle values=tr_1,tr_2} row">
                                                   <div class="{if $e.sign == '+'}plus{else}minus{/if} span">&nbsp;</div>
                                                   <div class="credits span kuinkey_title">{$e.amount} keys</div>
                                                   <div class="span">
                                              {if $e.service}
                                                  {capture assign='service'}{text %.membership.services.`$e.action`}{/capture}
                                                  {text %credits_taken service=$service}
                                              {elseif $e.action == 'given_by_admin'}
                                                  {text %credits_given_by_admin}
                                              {else}
                                                  {capture assign='action'}{text %.user_points.actions.`$e.action`}{/capture}
                                                  {text %credits_given action=$action}
                                              {/if}
                                              {if $e.opponent_id >0 }
                                                     <a class="kuinkey_title" href="/member/profile.php?profile_id={$e.opponent_id}">{$e.username}</a>
                                              {/if}
                                             </div>
                                             <div class="small left span">{$e.log_timestamp|spec_date}</div>
                                                </div>   
                                                <div class="line"></div>
                                          {/foreach}
                                          </div>
                                      {paging total=$paging.total on_page=$paging.on_page pages=$paging.pages}
                            {else if $tabs eq(1)}
                                          {text %.components.profile_status.credits_left}:
                                          <span class="bold">{if $credits_balance == 0}0{else}{$credits_balance}{/if}</span>
                                          <span class="small">(<a href="{document_url doc_key='points_purchase'}">{text %.user_points.buy_more}</a>)</span>
                                           {form WithdrawMoney}
                                           <div>
                                               <label for="amount">Amount: </label>
                                               <input type="hidden" name="profile_id" value="{$profile_id}" />
                                               <input type="text" name="amount" value="{if $credits_balance == 0}0{else}{$credits_balance | string_format: '%d'}{/if}" />
                                               <input type="submit" value="withdraw money" />
                                           </div>
                                          {/form}
                                          <br /><br />
                            {/if}
                     </div>
              </div>
       </div>
</div>
 
	{/container}
{/canvas}