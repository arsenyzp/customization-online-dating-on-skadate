{* component Rate *}

{container}
{if $feature == 'profile'}
    <div class="span2">
        <div class="inactive_rate_list_black">
            <div class="active_rate_list_black" style="width:{$rate_info.widthp}px;"></div>
         </div>
    </div>
 {else}
<div class="row">
    <div class="span2">
<span style="float:left;" class="kuinkey_title">Video rating</span>
    </div>
    <div class="span2">
        <div class="inactive_rate_list">
            <div class="active_rate_list" style="width:{$rate_info.widthp}px;"></div>
         </div>
    </div>
</div>
{/if}
{/container}
