{* Profile Comments Overview template *}

{canvas}

{container stylesheet='view_review.style'}


{component ProfileInfoLeft}

<div class="content_100">

                        {component ProfileInfoHeader}
        
                <div class="content span7">
                    <div class="content_text">

                         <!--Меню пользователя-->
                                 <div class="content_menu">
                                    <div class="content_menu_item {if $tabs eq(55)}activ_menu{/if}"><a href="/member/newsfeed.php">NEWS</a></div>
                                    <div class="content_menu_item  {if $tabs eq(1)}activ_menu{/if}"><a href="/member/myfantasy.php">MY FANTASIES</a></div>
                                    <div class="content_menu_item {if $tabs eq(0)} activ_menu{/if}"><a href="/member/view_works.php">MY LIBRARY</a></div>
                                    <div class="content_menu_item {if $tabs eq(77)} activ_menu{/if}"><a href="/member/sent_applications.php">MY JOBS</a></div>
                                    <div class="content_menu_item {if $tabs eq(44)}activ_menu{/if}"><a href="/member/view_review.php?mode=my">MY REVIEWS</a></div>
                                    <div class="content_menu_item {if $tabs eq(3)} activ_menu{/if}"><a href="/member/following.php?tabs=3">FOLLOWING</a></div>
                                    <div class="content_menu_item {if $tabs eq(22)} activ_menu{/if}"><a href="/member/bookmarks.php?tabs=22">FOLLOWERS</a></div>
                                    <div class="content_menu_item" style="float: right; background-color: #412756; margin-right: 10px;"><a href="/member/edit_profile.php">MY ACCOUNT</a></div>
                                </div>
                                    <div style="height: 10px;"></div>
                                <div class="content_menu">
                                    <div style="max-width: 140px;" class="content_menu_item {if $mode =='all'}activ_menu{/if}"><a href="/member/view_review.php?mode=all">REVIEWS BY ME</a></div>
                                    <div class="content_menu_item {if $mode =='my'}activ_menu{/if}"><a href="/member/view_review.php?mode=my">REVIEWS OF ME</a></div>
                                </div>

                        <div class="content_search">
                                    <div class="input-append">
                                        <input class="span2" id="searc_input" size="16" type="text" placeholder="search">
                                        <span class="add-on"><img src="/layout/themes/adult/img/searchi.png"></span>
                                    </div>
                                </div>
                                <div class="content_tab" style="text-align: left;">
                                    <div class="count_p">{$total} Reviews</div>
                                </div>
                                <div class="content_tab">

{if $comments}
    <div {id="comment_list_cont"}>
        {foreach from=$comments item='reviews_item'}
           
               <div class="content_tab">
                    <div class="line"></div>
                </div>
                
<div class="row">
    <div class="span4" style="width: 230px;">
                <div class="row">
                        <div style="width:72px; margin-left: 0px;" class="span1">
                                {profile_thumb profile_id=$reviews_item.profile_id redirect_params=$fantasy.url_params size=72}
                        </div>
                        <div style="margin-left: 15px; width: 140px;" class="span3">
                                <div class="list_profile_name">{$reviews_item.username}</div>
                                <div class="list_profile_location">
                                        {if $reviews_item.location.city}
                                            {$reviews_item.location.city},
                                        {elseif !empty($reviews_item.location.custom_location)}
                                             {$reviews_item.location.custom_location},
                                        {/if}

                                        {if $reviews_item.location.state}
                                            {$reviews_item.location.state},
                                        {/if}

                                        {if $reviews_item.location.country}
                                            {$reviews_item.location.country}.
                                        {/if}
                                </div>
                                <div class="list_profile_old">{$reviews_item.sex_label}, {$reviews_item.old} yers old</div>
                                <div class="list_profile_follower">
                                    <div class="row">
                                        <div class="span1" style="width: 30px;">
                                            {component ProfileReferences profile_id=$reviews_item.profile_id mode='follow'}
                                        </div>
                                         <div class="span2" style="width: 100px;">
                                             <div class="row">
                                                <div style="border-right: solid 1px #2da5da" class="num span1">{$reviews_item.video}</div>
                                                <div class="num span1">{$reviews_item.follow}</div>
                                        </div>
                                        <div style="line-height: 10px;" class="row">
                                                <div style="border-right: solid 1px #2da5da" class="span1">videos</div>
                                                <div class="span1">followers</div>
                                        </div>
                                         </div>
                                    </div>   
                                </div>
                        </div>
                </div>
        </div>
        <div class="span4">
            <div class="fantasy_list">
                     {rate rate=$reviews_item.video_rate feature='video' small='small'}
           </div>  
            
             <div class="description_fantasy left">
                  
                    {$reviews_item.review}
             </div>
            
                     
              <div class="informer_fantasy">
                  

                </div>
        </div>
</div>
                
            
       {*    
            <li class="item comment_list" id="{$comment_item.feature}_{$comment_item.id}">
                <div class="list_thumb">
                    {profile_thumb profile_id=$comment_item.author_id size='40'}
                </div>
                <div class="list_block">
                    <div class="list_info">
                        <div class="comment_stat">
                            <div class="item_title">
                                {if $comment_item.username}
                                    <a href="{$comment_item.profile_url}">{$comment_item.username}</a>&nbsp;{text %.comment.commented}
                                   {if $comment_item.entity_url}
                                        <a href="{$comment_item.entity_url}">{$comment_item.entity_title}</a>
                                   {else}
                                       {$comment_item.entity_title}
                                   {/if}
                                {else}
                                    <span>{text %.label.deleted_member}</span>
                                {/if}
                             </div>
                            {$comment_item.create_time_stamp|spec_date}
                        </div>
                        <div class="del_link">
                            {if $comment_item.delete}<a href="javascript://" {id="`$comment_item.delete_link_id`"}>{text %.comment.comment_manage_delete}</a>{/if}
                        </div>
                    </div>
                    <div class="list_content">
                        {$comment_item.text|out_format:'comment'|censor:'comment'|smile}
                    </div>
                </div>
            </li>*}
        {/foreach}
     </div>
{else}
    <div class="no_content">{text %.comment.no_items}</div>
{/if}
</div>
</div>
</div>
</div>
{/container}

{/canvas}
