{* Profile Comments Overview template *}

{canvas}

{container stylesheet='view_review.style'}

                                <div class="content_tab">

{if $comments}
      {foreach from=$comments item='reviews_item'}
           

                    <div class="line"></div>

                
<div class="row">
    <div class="span4" style="width: 230px;">
                <div class="row">
                        <div style="width:72px; margin-left: 0px;" class="span1">
                                {profile_thumb profile_id=$reviews_item.profile_id redirect_params=$fantasy.url_params size=72}
                        </div>
                        <div style="margin-left: 15px; width: 140px;" class="span3">
                                <div class="list_profile_name">{$reviews_item.username}</div>
                                <div class="list_profile_location">
                                        {if $reviews_item.location.city}
                                            {$reviews_item.location.city},
                                        {elseif !empty($reviews_item.location.custom_location)}
                                             {$reviews_item.location.custom_location},
                                        {/if}

                                        {if $reviews_item.location.state}
                                            {$reviews_item.location.state},
                                        {/if}

                                        {if $reviews_item.location.country}
                                            {$reviews_item.location.country}.
                                        {/if}
                                </div>
                                <div class="list_profile_old">{$reviews_item.sex_label}, {$reviews_item.old} yers old</div>
                                <div class="list_profile_follower">
                                    <div class="row">
                                        <div class="span1" style="width: 30px;">
                                            {component ProfileReferences profile_id=$reviews_item.profile_id mode='follow'}
                                        </div>
                                         <div class="span2" style="width: 100px;">
                                             <div class="row">
                                                <div style="border-right: solid 1px #2da5da" class="num span1">{$reviews_item.video}</div>
                                                <div class="num span1">{$reviews_item.follow}</div>
                                        </div>
                                        <div style="line-height: 10px;" class="row">
                                                <div style="border-right: solid 1px #2da5da" class="span1">videos</div>
                                                <div class="span1">followers</div>
                                        </div>
                                         </div>
                                    </div>   
                                </div>
                        </div>
                </div>
        </div>
     <div class="span2">

              <div class="thumb video_thumb">
                     <a href="/member/video_view.php?videokey={$reviews_item.video_info.hash}">
                     {if $reviews_item.video_thumb eq 'default'}
                     <div class="video_def_thumb"></div>
                     {elseif $reviews_item.video_thumb eq 'friends_only'}
                     <div class="video_friends_thumb"></div>
                     {elseif $reviews_item.video_thumb == 'password_protected'}
                     <div class="video_password_thumb"></div>	
                     {else}
                     <img id="default_{$reviews_item.video_info.hash}" src="{$reviews_item.video_thumb}" class="video_thumb" align="left" />
                     {/if}
                     </a>
              </div>

     </div>
        <div class="span4">
               <div class="row-fluid">
                      <div class="list_profile_name left span2" style="margin-top: 5px; width: 205px;">
                             <a href="/member/video_view.php?videokey={$reviews_item.video_info.hash}">  {$reviews_item.video_info.title}</a>
                        {*<span style="border-left: solid 1px;margin-left: 5px;"></span>*}
                    </div>  
                    <div class="span1">
                     {rate rate=$reviews_item.video_rate feature='video' small='small'}
                    </div>
               </div>
             <div class="description_fantasy left">
                    {$reviews_item.review}
             </div>
            
                     
              <div class="informer_fantasy">
                  

                </div>
        </div>
</div>
        {/foreach}
{/if}
</div>

{/container}

{/canvas}
