{* component MailboxConversationsList *}
{canvas}

{container text_ns='components.mailbox_conversations_list' stylesheet="mailbox_conversations_list.style"}

<div class="content_100">
                    <div class="content_text">
                        
                         <!--чёрная верхняя линия-->
                        <div class="content_head row-fluid">
                                <div class="span5" style=" text-align: left;">
                                      <div id="envelope"></div>
    <span class="menu_text" style="font-size:13px;">MESSAGES</span>
                                </div>
                                <div class="span1">
   
                                </div>
                                <div class="span5">
   
                                </div>
                                <div style="clear:both;"></div>
                        </div>

                        <div class="content_search">
                                    <div class="input-append">
                                        <form id="search_messag" method="GET">
                                            <input class="span2" id="searc_input" size="16" name="sm" type="text" placeholder="search">
                                            <span class="add-on" id="button_messag_search"><img src="/layout/themes/adult/img/searchi.png"></span>
                                        </form>
                                    </div>
                          
                            <div style="float: right; margin-right: 25px; margin-top: 22px;">
                                <input type="button" value="New message" onclick="NewMessagForm();" />
                            </div>
                              <div id="prof_info_line" style="float: right; margin-right: 25px; margin-top: 22px;">
                                
                            </div>
                                </div>
                                <div class="content_tab" style="text-align: left;">
                                    <div class="count_p">Inbox ({$total})</div>
                                </div>
                                
                                <div id="form_send_new" style="display: none;">
                                       
                                          
                                  <div class="padding10"> 
                                      <div style="height: 10px"></div>
                                            <div class="popup_form">
                                                    {*label %.forms.send_message_page.fields.recipient for='recipient'*}<br />
                                                   Recipient: <input type="text" id="search_user" />
                                             </div>
                                                {form SendMessagePage}
                                             <div class="popup_form" style="display: none;">
                                                    {label %.forms.send_message.fields.subject for='subject'}<br />
                                                    {input name="subject"}
                                                    
                                                      {input name="recipient"}
                                             </div>
                                             <div class="popup_form">
                                                    {label %.forms.send_message.fields.text for='text'}<br />
                                                    {text_formatter for='text' entity="mailbox" controls="bold,italic,underline,link,emoticon,image"}{input name="text"}
                                             </div>
                                            <div class="popup_form">
                                                {button action='send'}
                                                <br clear="all" />
                                            </div>               
                                                  {/form}
                                   </div> 
                                        
                                </div>
                                
                                <div class="content_tab">
                                    <div class="line" style="margin: 0;"></div>
                                    <div class="row">
                                        <div class="converstetion_list span5" style="margin: 0;">
                                       
	{block}
	{if $conv_arr}
	{form ManageMailbox mark_for=$profile_id}	
                
		{foreach from=$conv_arr item='conv'}
		{if ($conv.mails_count < 1 && $folder=='sentbox')}
		{else}
                    <div class="mess_item {if $conv.bm_read eq  '2' or $conv.bm_read eq  '0' or $conv.bm_read eq  '1'}no_read{/if}">
                    <div class="mess_line line"></div>
                    <div class="messege_link row" id="{$conv.conversation_id}">
                        <div class="span1" style="margin:0; width: 75px;">
                            {profile_thumb profile_id=$conv.sender_id size=72 }
                        </div>
                        <div class="span4" style="width: 240px; margin: 0;">
                            <div class="row">
                                <div class="list_profile_name span1">
                                        {if $conv.username && $conv.username != 'username'}<a href="{if $folder eq 'sentbox'}{document_url doc_key='profile' profile_id=$conv.recipient_id}{else}{document_url doc_key='profile' profile_id=$conv.sender_id}{/if}">{$conv.username}</a>{else}{text %.label.deleted_member}{/if}
                                </div>
                                 <div class="date_create span2">{$conv.last_message_ts|date_format}</div>
                            </div>
                            <div class="row profile_info_line">
                                <div class="list_profile_old span1">{$conv.age} yrs old</div>
                                <div class="list_profile_sex span1">{$conv.sex_label}</div>
                                <div class="list_profile_location span2">
                                    {*if $conv.location.city}
                                            {$conv.location.city},
                                        {elseif !empty($conv.location.custom_location)}
                                             {$conv.location.custom_location},
                                        {/if*}

                                        {*if $conv.location.state}
                                            {$conv.location.state},
                                        {/if*}

                                        {if $conv.location.country}
                                            {$conv.location.country}.
                                        {/if}
                                </div>
                                       
                            </div>
                            <div class="message_list_text row">
                                <div class="mess span3">
                                    {if $conv.is_system eq 'yes'}
                                    {$conv.text|smile|truncate:150|censor:"mailbox"}
                                    {elseif $check_rdbl and $conv.is_readable eq 'no' and $conv.sender_id != $profile_id}
                                            {$perm_msg}
                                    {elseif isset($perm_msg) and !$check_rdbl and $conv.sender_id != $profile_id}
                                            {$perm_msg}
                                    {else}
                                            {$conv.text|out_format:"mailbox"|truncate:150|smile|censor:"mailbox"}
                                    {/if}
                                </div>
                                <div class="span1"></div>
                                {if $conv.is_system eq 'no'}
				{component MailboxControls profile_id=$conv.sender_id conversation_id=$conv.conversation_id folder=$folder}
			{else}
				{component MailboxControls profile_id=$conv.sender_id conversation_id=$conv.conversation_id folder='sentbox'}
			{/if}
                           </div>
                        </div>
                    </div>
                    </div>
                                                {/if}
                                                {/foreach}
	{/form}
	{else}
		{if $folder eq 'inbox'}
			<div class="no_content">{text %no_incoming_msg}</div>
		{elseif $folder eq 'sentbox'}
			<div class="no_content">{text %no_sent_msg}</div>
		{/if}
	{/if}
	{/block}
        
                            </div>
                                        <div class="fram span4">
                                            <iframe src="" class="frame_converstetion" name="frame_conv" id="frame_conv" scrolling="no" seamless></iframe>
                                        </div>
                                    </div>
        
	{*paging total=$paging.total on_page=$paging.on_page pages=$paging.pages*}
        
        
        </div>
</div>
</div>
        
{/container}
{/canvas}
