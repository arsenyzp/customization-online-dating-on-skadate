{* component Comment Add Form *}

{container}
	
	{capture name='tag_navigator'}
		Tag
	{/capture}
	
	{block title=$smarty.capture.tag_navigator}
		{if $no_tags}<div class="no_content">No tag</div>
        {else}
		{foreach from=$tags item='tag'}
	    	<a href="{$tag.url}" style="font-size:{$tag.size}px;line-height:{$tag.line_height}px;">{$tag.tag}</a>&nbsp;&nbsp;
	    {/foreach}
        {/if}
	{/block}
	
{/container}
