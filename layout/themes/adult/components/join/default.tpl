{* Component Join*}

{canvas}


<div class="content_100">
    
    <!--чёрная верхняя линия-->
    <div class="content_head row-fluid">
            <div class="span5">

    <span class="menu_text" style="font-size:13px">CREATE YOUR KUINKEY ACCOUNT</span>
            </div>
            <div class="span2">
    <span class="menu_text" style="font-size:13px">

    </span>
    </div>
            <div class="span4">

            </div>
            <div style="clear:both;"></div>
    </div>
    
    <div class="content_profile_info">
{container stylesheet="join.style" } 
{form Join}

		
			{foreach from=$fields item=section key=sect_id}
				<table class="form_join">
					{*<thead>
						<tr>
							<th class="{cycle values='th1,th2,th3,th4'}" colspan="2">{text %.profile_fields.section.`$sect_id`}</th>
						</tr>
					</thead>*}
                                           <tbody>
                                                     {foreach from=$section item=field}
                                                           
                                                               <tr>
                                                                         {if $field.presentation =='location'}
                                                                                   <td colspan="2">
                                                                                             <div style="display: none">
                                                                                                       {label for=$field.name}
                                                                                             </div>
                                                                                             {input name=$field.name labelsection=profile_fields.label_join}
                                                                                   </td>
                                                                         {else}
                                                                                   <td>
                                                                                           {if $field.name != 'headline'}
                                                                                                  {label %.profile_fields.label_join.`$field.id` for=$field.name}{if $field.required}<span class="required_star">*</span>
                                                                                           {/if}
                                                                                      {/if}
                                                                                   </td>
                                                                                   <td class="value">{input name=$field.name labelsection=profile_fields.value}</td>
                                                                         {/if}

                                                               </tr>
                                                               {if $field.confirm}
                                                                         <tr>
                                                                                   <td>
                                                                                      {label %.profile_fields.confirm.`$field.id` for="re_`$field.name`"}{if $field.required}<span class="required_star">*</span>{/if}
                                                                                   </td>
                                                                                   <td class="value">{input name="re_`$field.name`" labelsection=profile_fields.value}</td>								
                                                                         </tr>
                                                               {/if}


                                                     {/foreach}
                                           </tbody>
                                 </table>
					
			{/foreach}
			{if $has_captcha}
				<br clear="all">
				<table class="form_join">
					<tbody>
					<tr><td colspan="2" class="captcha">{input name=captcha}</td></tr>
					</tbody>
				</table>
				<br clear="all">	
			{/if}
                                                                <table class="form_join">
                                                                        <tbody>
                                                                        <tr><td colspan="2" class="captcha">{button action=$form->active_action label=%.forms._actions.join}</td></tr>
                                                                        </tbody>
                                                                </table>
                                                                        
                                                                        <table class="form_join">
                                                                        <tbody>
                                                                        <tr><td colspan="2" class="captcha">

                                                                                <div class="left profile_title2" style="color: #2DA5DA;">
                                                                                Questions?
                                                                                </div>
                                                                                <div class="left grey">
                                                                                    See our <a href="#" style="color: #2DA5DA;">FAQ</a> for more info about Kuinkeys accounts.
                                                                                </div>

                                                                            </td></tr>
                                                                        </tbody>
                                                                </table>

{/form}
{/container}

<div style="height: 200px;"></div>
</div> 
</div>
{/canvas}