{* component Change Password *}

{container class="change_password" stylesheet="change_password.style"}


		{form ChangePassword}
                   <div class="row">
                        <div class="label_profile_field span2">{label for="old_password"}</div>
                        <div class="profile_edit_fields span5">
                            <div class="profile_field_row">
                               {input name="old_password"}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="label_profile_field span2">{label for="new_password"}</div>
                        <div class="profile_edit_fields span5">
                            <div class="profile_field_row">
                               {input name="new_password"}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="label_profile_field span2">{label for="new_password_confirm"}</div>
                        <div class="profile_edit_fields span5">
                            <div class="profile_field_row">
                               {input name="new_password_confirm"}
                            </div>
                        </div>
                    </div>
                     <div class="row">
                        <div class="label_profile_field span2"></div>
                        <div class="profile_edit_fields span5">
                            <div class="profile_field_row">
                                {button action="change"}<input type="button" {id="cancel"} value="{text %.forms.change_password.actions.cancel}" class="read_more" style="margin-left: 5px;">
                            </div>
                        </div>
                    </div>
                        
		{*<table class="form change_pass">
			<tr>
				<td class="label">{label for="old_password"}</td><td class="value">{input name="old_password"}</td>
			</tr>
			<tr>
				<td class="label">{label for="new_password"}</td><td class="value">{input name="new_password"}</td>
			</tr>
			<tr>
				<td class="label">{label for="new_password_confirm"}</td><td class="value">{input name="new_password_confirm"}</td>
			</tr>
			<tr>
				<td align="right" colspan="2">{button action="change"}<input type="button" {id="cancel"} value="{text %.forms.change_password.actions.cancel}"></td>
			</tr>
		</table>*}
		{/form}
	
{/container}

