
{container}

{capture name='video_owner'}{text %video_from} {$owner_name}{/capture}
{capture name='more'}<a href="{$view_more_link}">{text %view_all}</a>{/capture}


	{if $video}

 {foreach from=$video item='video' key='it' name='vid'}
        <!--видео галерея список -->
       {if $it == 0}<div class="row-fluid">{/if}
       {if $it % 3 == 0 && $it != 0}</div><div class="line"></div><div class="row-fluid">{/if}
                <div class="span4">
                        <div class="video_name">{$video.title|upper}</div>
                        <div class="video_thumb video_video">
                            {if $video.thumb_img eq 'default'}
                                    <a href="{$video.video_page}"><div class="video_def_thumb"></div></a>
                            {elseif $video.thumb_img eq 'friends_only'}
                                    <div class="video_friends_thumb"></div>
                            {elseif $video.thumb_img eq 'password_protected'}
                                <a href="{$video.video_page}"><div class="video_password_thumb"></div></a>
                            {else}
                                    <a href="{$video.video_page}"><img src="{$video.thumb_img}" class="video_thumb" align="left" /></a>
                            {/if}
                        </div>
                        
                        
                        <div class="stat_video_list  row">
                             <div class="span6">
                                    <div>
                                        {rate rate=$video.rate_score feature='video' small='small'}
                                      {*  <span class="active star_small"></span>
                                        <span class="active star_small"></span>
                                        <span class="active star_small"></span>
                                        <span class="star_small"></span>
                                        <span class="star_small"></span>
                                        <div style="clear:both;"></div>*}
                                    </div>
                             </div>
                             <div class="span6">
                                <div class="row">
                                    <div class="video_like span3">{$video.like}</div>
                                    <div class="video_view span3">{$video.view_count}</div>
                                    <div class="video_comment span3">{$video.comment_count}</div>
                                 </div>
                             </div>
                        </div>
                        
                        <div class="video_desc">{*$video.description|out_format|truncate:150|smile|censor:"video":true*}</div>
                </div>
        {if $smarty.foreach.vid.last}</div>{/if}
        {/foreach}






	{*
		<table class="thumb_text_list">
		{foreach from=$video item='video_item'}
			<tr>
				<td class="thumb">
				<a href="{$video_item.video_page}">
					{if $video_item.thumb_img eq 'default'}
						<div class="video_def_thumb"></div>
					{elseif $video_item.thumb_img eq 'friends_only'}
						<div class="video_friends_thumb"></div>
				    {elseif $video_item.thumb_img == 'password_protected'}
				        <div class="video_password_thumb"></div>	
					{else}
						<img src="{$video_item.thumb_img}" class="video_thumb" align="left" />
					{/if}
				</a>
				</td>
				<td class="listing list_item">
					<a href="{$video_item.video_page}">{$video_item.title|out_format|smile|censor:"video":true}</a>
					<p class="small">{$video_item.upload_stamp|spec_date}<br />
					{text %views} <span class="highlight">{$video_item.view_count}</span><br />
					{rate rate=$video_item.rate_score feature='video'}
					</p>
				</td>
			</tr>
		{/foreach}
		</table>
                *}
	{else}
		<div class="no_content">{text %no_video}</div>
	{/if}

        {literal}
            <script>
                $(document).ready(function(){
                    $('.video_thumb').css('width', '250px');
                });
            </script>
        {/literal}
	
{/container}
