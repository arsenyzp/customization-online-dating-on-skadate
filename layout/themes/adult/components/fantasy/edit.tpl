
{canvas}
{container stylesheet="fantasy.style"}

{component ProfileInfoLeft}

<div class="content_100">

                        {component ProfileInfoHeader}
        
                <div class="content span7">
                    <div class="content_text">

                         <!--Меню пользователя-->
                                <div class="content_menu">
                                    <div class="content_menu_item {if $tabs eq(55)}activ_menu{/if}"><a href="/member/newsfeed.php">NEWS</a></div>
                                    <div class="content_menu_item  {if $tabs eq(1)}activ_menu{/if}"><a href="/member/myfantasy.php">MY FANTASIES</a></div>
                                    <div class="content_menu_item"><a href="/member/view_works.php">MY LIBRARY</a></div>
                                    <div class="content_menu_item  activ_menu"><a href="/member/sent_applications.php">MY JOBS</a></div>
                                    <div class="content_menu_item {if $tabs eq(44)}activ_menu{/if}"><a href="/member/view_review.php?mode=my">MY REVIEWS</a></div>
                                    <div class="content_menu_item {if $tabs eq(3)} activ_menu{/if}"><a href="/member/following.php?tabs=3">FOLLOWING</a></div>
                                    <div class="content_menu_item {if $tabs eq(22)} activ_menu{/if}"><a href="/member/bookmarks.php?tabs=22">FOLLOWERS</a></div>
                                    <div class="content_menu_item" style="float: right; background-color: #412756; margin-right: 10px;"><a href="/member/edit_profile.php">MY ACCOUNT</a></div>
                                </div>
                                <div class="content_tab">
                            {form FantasyEdit}
                                <fieldset>
                                    <table  id="performer_list">
                                        <tr>
                                            <td><label for="title" >Title:</label></td>
                                            <td>
                                                <input type="text" name="title" class="input_create_fantasy" value="{$title}" />
                                                <input type="hidden" name="id" value="{$id}" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><label for="tags" >Tags:</label></td> <td> 
                                                <input type="text" id="demo-input-facebook-theme" name="tags" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><label for="category" >Category:</label></td> <td>{input name='category' labelsection='video_categories'}</td>
                                        </tr>
                                        <tr>
                                            <td  colspan="2"><label for="description" >Description:</label></td>
                                        </tr>
                                         <tr>
                                             <td colspan="2"><textarea name="description" />{$description}</textarea></td>
                                        </tr>
                                        <tr>
                                            <td><label for="perfomers" >Performers:</label></td>
                                            <td><input type="text"  name="perfomers[]" id="performer_tag" />
                                                <input type="button" value="+"  onclick="add_field();" /></td>
                                        </tr>
                                    </table>
                                    <table>
                                        <tr>
                                            <td class="td_label"><label for="budget" >Budget:</label></td>
                                            <td><input type="text" id="budget"  name="budget" class="input_create_fantasy" value="{$budget}"  {if $budget eq '0'}style="display:none;"{/if} />
                                                <label for="buggest_null" >Negotiable:</label><input type="checkbox" name="buggest_null" id="buggest_null" {if $budget eq '0'}checked="checked"{/if}  /></td>
                                        </tr>
                                        <tr>
                                            <td><label for="tags" >Price video view:</label></td>
                                            <td><input type="text" id="price_video" name="price_video" value="5" /></td>
                                        </tr>
                                        <tr>
                                            <td><label for="tags" >Lenght video (minimum 0.5 min):</label></td>
                                            <td><input type="text" id="length_video" name="length_video" value="2" />min</td>
                                        </tr>
                                    </table>

                                    <table id="files">
                                        <tr>
                                             <td class="td_label"><label for="file" >Files:</label></td> 
                                             <td>
                                                 <ul>
                                                     {foreach from=$files item='file' key='i'}
                                                         <li>
                                                             <a href="/$userfiles/upload_fantasy/{$file.name}" title="{$file.name}">
                                                                 file{$i+1}
                                                             </a> 
                                                             {literal}
                                                             <a href="#" onclick="$(this).parent().remove();return false;" >delete</a>
                                                             {/literal}
                                                             <input type="hidden" name="files[]" value="{$file.id}" />
                                                         </li>
                                                     {/foreach}
                                                </ul>
                                             </td>
                                        </tr>
                                    </table>

                                    <table id="file_list">
                                        <tr>
                                             <td class="td_label"><label for="file" >Upload:</label></td> 
                                             <td>{input name="newfile"}</td>
                                        </tr>
                                    </table>
                                    <table>
                                        <tr>
                                            <td class="td_label"><label for="data_fantasy_ex">Expiration Date</label></td>
                                            <td><input type="text"  name="data_fantasy_ex" id="inputDate" value="{$data_ex}" /></td>
                                        </tr>
                                        <tr>
                                            <td width="100px"><label for="fantasy_private">Private Fantasy  (+ $20 Keys)</label></td>
                                            <td><input type="checkbox" {if $private eq(1)}checked="checked"{/if}  name="fantasy_private" /></td>
                                        </tr>
                                        <tr>
                                            <td><input type="submit"  value="Save" /></td> <td><input type="button"  value="Canсel" id="canel_button" /></td>
                                        </tr>
                                    </table>
                                </fieldset>
                           {/form}
                            </div>
                    </div>
                </div>
</div>

  <script>

  var tags = {$tags};
  var performer = '';     
  var catigory_id = {$category_id};
       {literal}
              
(function($) {
    $(document).ready(function() {
    
        
        $('#form_4-category').val(catigory_id);
    
        $('#canel_button').click(function(){
            document.location = '/member/myfantasy.php';
        });
        
       $("#demo-input-facebook-theme").tokenInput(document.location.href, {
            theme: "facebook",
            prePopulate : tags
        });  
            
       {/literal} 
       {foreach from=$performers item='performer' key='k'}
                        {if $k neq(0)}
                        create_field({$performer});
                        {else}
                        performer = {$performer};    
                        {/if}
        {/foreach}   
        {literal}
        $("#performer_tag").tokenInput(document.location.href, {
        theme: "facebook",
        queryParam: 'q_p',
        prePopulate : performer    
    });    
            
    });
})(jQuery);

        {/literal}    
 </script>
{/container}
{/canvas}