{* Component Email Verify *}

{canvas}

	{container}
          <div class="content_100">
    
   <!--чёрная верхняя линия-->
                            <div class="content_head row-fluid">
                                    <div class="span4">
                                          <span class="menu_text" style="font-size:13px"></span>
                                    </div>
                                    <div class="span4">
                                          <span class="menu_text" style="font-size:13px">
                                              WELCOME, {$u}
                                          </span>
                                   </div>
                                    <div class="span4">

                                    </div>
                                
                                    <div style="clear:both;"></div>
                            </div>

                                          <div class="content_profile_info">
                                                 <div class="content_tab email_verified_tab" style="width: 400px;">
                                                   {*block class="block_info"}

                                                             {text %msg}

                                                   {/block*}
                                                   <div style="height: 10px;"></div>
                                                   <div class="video_name" style="height: auto;">
                                                          Thanks for signing up at Kuinkey.com, now there is a few steps you must complete before you can start kuinking:
                                                   </div>
                                                   <div style="height: 10px;"></div>
                                                   <div class="video_name">
Confirm your account                                                   
                                                   </div>
                                                   <div style="height: 10px;"></div>
                                                   <div class="grey">
                                                   You should recelve a welcome email from us. Please click the link within to confirm your account
                                                   </div>
                                                   <div style="height: 10px;"></div>
                                                   <div class="video_name">
                                                   Didn't recive anything?
                                                   </div>
                                                   <div style="height: 10px;"></div>
                                                   <div class="grey">
                                                   Check your spam Inbox or click re-send confirmation:
                                                   </div>
                                                   <div style="height: 10px;"></div>
                                                   <div class="center_block">
                                                        
                                                                       {form EmailVerify}
                                                                                 {input name=email}{button action=send}
                                                                       {/form}
                                                           
                                                   </div>
                                                                       <div class="line"></div>
                                                                       <div>
                                                                              <div class="video_name">
                                                                              Questions?
                                                                              </div>
                                                                       <div class="left grey">
                                                                                    See our <a href="#" style="color: #2DA5DA;">FAQ</a> for more info about Kuinkeys accounts.
                                                                                </div>
                                                                       </div>
                                                                       <div style="height: 100px;"></div>
                                                        </div>
                                          </div>
                              </div>
                              
		
	{/container}

{/canvas}