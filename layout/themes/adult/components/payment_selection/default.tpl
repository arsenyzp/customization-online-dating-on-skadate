{canvas}

{container stylesheet="payment_selection.style"}
{component ProfileInfoLeft}

<div class="content_100">

    {component ProfileInfoHeader}

    <div class="content span7">	
        <div class="content_text">
            <!--Меню пользователя-->
            <div class="content_menu">
                <div class="content_menu_item {if $tabs eq(55)}activ_menu{/if}"><a href="/member/newsfeed.php">NEWS</a></div>
                <div class="content_menu_item  {if $tabs eq(1)}activ_menu{/if}"><a href="/member/myfantasy.php">MY FANTASIES</a></div>
                <div class="content_menu_item {if $tabs eq(11)} activ_menu{/if}"><a href="/member/view_works.php">MY LIBRARY</a></div>
                <div class="content_menu_item {if $tabs eq(77)} activ_menu{/if}"><a href="/member/sent_applications.php">MY JOBS</a></div>
                <div class="content_menu_item {if $tabs eq(44)}activ_menu{/if}"><a href="/member/view_review.php?mode=my">MY REVIEWS</a></div>
                <div class="content_menu_item {if $tabs eq(3)} activ_menu{/if}"><a href="/member/following.php?tabs=3">FOLLOWING</a></div>
                <div class="content_menu_item {if $tabs eq(22)} activ_menu{/if}"><a href="/member/bookmarks.php?tabs=22">FOLLOWERS</a></div>
                <div class="content_menu_item activ_menu" style="float: right; background-color: #412756; margin-right: 10px;"><a href="/member/edit_profile.php">MY ACCOUNT</a></div>
            </div>
            <div style="height: 10px"></div>
            <div class="content_menu">
                                    <div class="content_menu_item"><a href="/member/edit_profile.php">SETTINGS</a></div>
                                    <div class="content_menu_item"><a href="/member/points_purchase.php">MANAGE KEYS</a></div>
                                    <div class="content_menu_item"><a href="/member/block_list.php">BLOCK LIST</a></div>
                                    <div class="content_menu_item activ_menu"><a href="/member/payment_selection.php">KUINKEY+</a></div>
                                </div>

            <div class="content_tab">
                   
                   <div class="line"></div>
                <div class="right" style="line-height: 20px;"><img src="/layout/img/st.png" style="margin-right:5px;" />Secure Transaction</div>
                <div class="kuinkey_title left">
                   Change the billing cycle you want
                </div>
                <div style="height: 20px;"></div>
                
{form PaymentSelection provider_id=$providers.0.fin_payment_provider_id plan_id=$default_plan}
{*
<table class="membership_types_tbl" width="100%" {id="membership_types_tbl"}>
	<tr>
		<td class="first_col">
			<div class="current_status">
				{text %membership_label}: <br /> 
				<b>{text %.membership.types.`$membership.membership_type_id`}</b>
				{if $membership.expiration_time}, <br />
				    {text %expiration_time} <b>{$membership.expiration_time}</b>
				    {if $unsub != ''}<div class="unsubscribe">{$unsub}</div>{/if}
                {/if}
                {if $show_balance}
                    <br />
                    {text %.components.profile_status.credits_left}: <br /><b>{$credits_balance}</b>
                    <span class="small">(<a href="{document_url doc_key='points_purchase'}">{text %.user_points.buy_more}</a>)</span>
                {/if}
			</div>
		</td>
		{foreach from=$types item='type'}
		<td style="width: {$col_width}px" class="{if $type.membership_type_id==$status.membership_type_id}current{/if}">
			{block title=%.membership.types.`$type.membership_type_id`}<div class="membership_desc">{text %.membership.types.desc.`$type.membership_type_id`}</div>{/block}
		</td>
		{/foreach}
	</tr>
	<tr>
		<td>
			{if $sms_services}
				<div class="pay_by_sms">{$sms_services}</div>
			{/if}
		</td>
                    *}
		{foreach from=$type_plans item='type' name='pl'}
			{*<td style="width: {$col_width}px" class="plans {if $type.membership_type_id==$status.membership_type_id}current{/if}">*}
			{* {if $type.membership_type_id != $status.membership_type_id} *}
                              <div class="row">
				{foreach from=$type.plans item='plan'}
                                 <div class="plan_box span">
                                        <div class="membership_content">
                                           {if $plan.membership_type_plan_id eq $default_plan}
                                                     <div class="grey month">{$plan.period}</div>
                                                   <div class="txtm kuinkey_title">Month package</div>
                                                   <div class="kuinkey_title pprice">us$ {$plan.price/$plan.period}/month</div>
                                                   <div style="height:15px"></div>
                                                  {input_item name="plan_id" value=`$type.membership_type_id`_`$plan.membership_type_plan_id` checked='checked'}

                                           {else}
                                                   <div class="grey month">{$plan.period}</div>
                                                   <div class="txtm kuinkey_title">Month package</div>
                                                   <div class="kuinkey_title pprice">us$ {$plan.price/$plan.period}/month</div>
                                                    <div class="grey sale">You save {if $plan.period eq(3)}20%{else}50%{/if}</div>
                                                     {input_item name="plan_id" value=`$type.membership_type_id`_`$plan.membership_type_plan_id`}

                                           {/if}
                                          </div>
                                 </div>
				{/foreach}
                              </div>
			{* {/if} *}
			{*</td>*}
			{assign var="plan_count" value=$smarty.foreach.pl.iteration}
		{/foreach}
	{*</tr>
	
	{if $coupons_enabled}
	<tr>
        <td colspan="{math equation='x + y' x=$plan_count y=1}" align="right">
            <div style="padding: 10px 0px;">{text %coupon} {input name="coupon"}</div>
        </td>	   
	</tr>
	{/if}
	
	<tr>
		<td colspan="{math equation='x + y' x=$plan_count y=1}">
			<div class="providers">
			{if $providers}
				<span {id="provider_select"} style="">{text %pay_with}: {input name="provider_id" labelsection='components.payment_selection'}</span>
				{button action='checkout' class='checkout_btn'}
				<span {id="claim_btn_label"} class="claim_btn">{text %.forms.payment_selection.actions.claim}</span>
				<div class="provider_logo">
					{foreach from=$providers item='provider'}
						{if $provider.icon}<img src="{$smarty.const.URL_USERFILES}{$provider.icon}" alt="{text %.components.payment_selection.`$provider.fin_payment_provider_id`}" />{/if}
					{/foreach}
				</div>
			{else}
				{$col_count}{text %.components.payment_selection.no_providers}
			{/if}
			</div>			
		</td>
	</tr>
	{if $show_chart}
		<tr align="center" class="mem_diagram_header">
			<td class="first_col">{block_cap title=%.membership.permission_diagram_benefit}{/block_cap}</td>
			{foreach from=$types item='type'}
				<td style="width: {$col_width}px" class="other_cols {if $type.membership_type_id==$status.membership_type_id}current{/if}">
					{block_cap title=%.membership.types.`$type.membership_type_id`}{/block_cap}
				</td>
			{/foreach}
		</tr>
		<tr>
			<td class="first_col">
				{foreach from=$diag.services item='serv'}
					<div class="benefit {cycle values=td_1,td_2}" >{text %.membership.services.`$serv.membership_service_key`}</div>
				{/foreach}
			</td>
			{foreach from=$diag.types item='type' key='k'}
			<td {if $k==$status.membership_type_id}class="current"{/if}>
				{foreach from=$type.services item='type_service'}
					{if $type_service.is_promo eq 'yes'}
						<div class="available no_mark">{text %.components.payment_selection.is_promo}</div>
					{elseif $type_service.service_limit}
						<div class="available no_mark"><span class="amount">{$type_service.service_limit}</span> {text %.components.payment_selection.per_day}</div>
					{elseif $type_service.available eq 'yes'}
						<div class="available">{*text %.components.payment_selection.unlimited* }</div>
					{else}
						<div class="unavailable"></div>
					{/if}
					
				{/foreach}
			</td>
			{/foreach}
		</tr>
	{/if}
</table>*}


<table style="display: none;">
       <tr>
		<td colspan="{math equation='x + y' x=$plan_count y=1}">
			<div class="providers">
			{if $providers}
				<span {id="provider_select"} style="">{text %pay_with}: {input name="provider_id" labelsection='components.payment_selection'}</span>
				{*button action='checkout' class='checkout_btn'*}
				<span {id="claim_btn_label"} class="claim_btn">{text %.forms.payment_selection.actions.claim}</span>
				<div class="provider_logo">
					{foreach from=$providers item='provider'}
						{if $provider.icon}<img src="{$smarty.const.URL_USERFILES}{$provider.icon}" alt="{text %.components.payment_selection.`$provider.fin_payment_provider_id`}" />{/if}
					{/foreach}
				</div>
			{else}
				{$col_count}{text %.components.payment_selection.no_providers}
			{/if}
			</div>			
		</td>
	</tr>
</table>

{button action='checkout' class='checkout_btn'}

{/form}
<br clear="all" />
                     </div>
              </div>
       </div>
</div>
{/container}

{/canvas}