{* Component Profile List *}

{canvas}

	{container stylesheet="profile_list.style" class="profile_list_contener"}

 {component ProfileInfoLeft}

<div class="content_100">

                        {component ProfileInfoHeader}
        
                <div class="content span7">
                      <div class="content_text">
            <!--Меню пользователя-->
                                 <div class="content_menu">
                                    <div class="content_menu_item {if $tabs eq(55)}activ_menu{/if}"><a href="/member/newsfeed.php">NEWS</a></div>
                                    <div class="content_menu_item  {if $tabs eq(1)}activ_menu{/if}"><a href="/member/myfantasy.php">MY FANTASIES</a></div>
                                    <div class="content_menu_item {if $tabs eq(0)} activ_menu{/if}"><a href="/member/view_works.php">MY LIBRARY</a></div>
                                    <div class="content_menu_item {if $tabs eq(77)} activ_menu{/if}"><a href="/member/sent_applications.php">MY JOBS</a></div>
                                    <div class="content_menu_item {if $tabs eq(44)}activ_menu{/if}"><a href="/member/view_review.php?mode=my">MY REVIEWS</a></div>
                                    <div class="content_menu_item {if $tabs eq(3)} activ_menu{/if}"><a href="/member/following.php?tabs=3">FOLLOWING</a></div>
                                    <div class="content_menu_item {if $tabs eq(22)} activ_menu{/if}"><a href="/member/bookmarks.php?tabs=22">FOLLOWERS</a></div>
                                    <div class="content_menu_item" style="float: right; background-color: #412756; margin-right: 10px;"><a href="/member/edit_profile.php">MY ACCOUNT</a></div>
                                </div>

                        <div class="content_search">
                                    <div class="input-append">
                                        <input class="span2" id="searc_input" size="16" type="text" placeholder="search">
                                        <span class="add-on"><img src="/layout/themes/adult/img/searchi.png"></span>
                                    </div>
                                </div>
                                <div class="content_tab" style="text-align: left;">
                                    <div class="count_p">{$paging.total} Members</div>
                                </div>
                                <div class="content_tab">
                            
	
		{foreach from=$list item=item}
		<div class="line"></div>
                
                
                
<div class="row">
       <div class="span3" style="width:200px;">
                <div class="row">
                        <div style="width:72px; margin-left: 0px;" class="span1">
                                {profile_thumb profile_id=$item.profile_id redirect_params=$item.url_params size=72}
                        </div>
                        <div style="margin-left: 15px; width: 110px;" class="span3">
                            <a href="/member/profile.php?profile_id={$item.profile_id}" class="list_profile_name">{$item.profile_label}</a>
                                <div class="list_profile_location">
                                        {if $item.location.city}
                                            {$item.location.city},
                                        {elseif !empty($item.location.custom_location)}
                                             {$item.location.custom_location},
                                        {/if}

                                        {if $item.location.state}
                                            {$item.location.state},
                                        {/if}

                                        {if $item.location.country}
                                            {$item.location.country}.
                                        {/if}
                                </div>
                                <div class="list_profile_old">{$item.sex_label}, {$item.old} yers old</div>
                                <div class="list_profile_follower">
                                       <div class="row" style="width: 110px;">
                                        <div class="span1" style="width: 15px;">
                                            {component ProfileReferences profile_id=$item.profile_id mode='follow' type='small'}
                                        </div>
                                         <div class="span2" style="width: 95px;">
                                             <div class="row">
                                                <div style="border-right: solid 1px #2da5da" class="num span1">{$item.video}</div>
                                                <div class="num span1">{$item.follow}</div>
                                        </div>
                                        <div style="line-height: 10px;" class="row">
                                                <div style="border-right: solid 1px #2da5da" class="span1">videos</div>
                                                <div class="span1">followers</div>
                                        </div>
                                         </div>
                                    </div>   
                                </div>
                        </div>
                </div>
        </div>
        <div class="span4" style="margin-left: 0px;">
                {component ProfileVideoListMini profile_id=$item.profile_id except_video=0 limit=3}
        </div>
</div>
                                
		{/foreach}
		<br clear="all" />
		<br clear="all" />
		{if $enable_pagging}
			{paging total=$paging.total on_page=$paging.on_page pages=$paging.pages exclude="new_search"}
		{/if}
                     </div>
    </div>
                    </div> </div> 
                
	{/container}
	
{/canvas}
