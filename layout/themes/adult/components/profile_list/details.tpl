{* Component Profile List *}

{canvas}

	{container stylesheet="profile_list.style" class="profile_list_contener"}

    <div class="content_100">
                        <!--чёрная верхняя линия-->
                        <div class="content_head row">
                                <div class="span2">
                                      {*  <div id="darck_smile"></div>*}
    <span class="menu_text" style="font-size:13px">BROWSE USERS</span>
                                </div>
                                <div class="span4">

                                </div>
                                <div class="span5">

                                </div>
                                <div style="clear:both;"></div>
                        </div>

                        <div class="content_profile_info">
                            <form method="GET">
                                <input type="hidden" name="search" value="1" />
                            <div style="height:15px;"></div>
                            <div class="left">
                                <div class="row form_line">
                                    <div class="span1 video_name sb">Who?</div>
                                    <div class="span6">
                                        <span class="search_label">Male </span><input value="1" type="radio" name="sex" {if $array_s.sex eq(1)}checked="checked"{/if} />
                                        <span class="search_label">Female </span>    <input value="2" type="radio" name="sex" {if $array_s.sex eq(2)}checked="checked"{/if} />
                                        <span class="search_label">Trans </span>    <input value="3" type="radio" name="sex" {if $array_s.sex eq(3)}checked="checked"{/if} />
                                    </div>
                                </div>
                            </div>
                            <div class="search_line line"></div>
                            <div class="left">
                                <div class="row form_line">
                                    <div class="span1 video_name sb">What?</div>
                                    <div class="span6">
                                        <span class="search_label">Performer </span> <input type="checkbox" value="3" name="interested_in" {if $array_s.interested_in eq(3)}checked="checked"{/if}/>
                                    </div>
                                </div>
                            </div>
                            <div class="search_line line"></div>
                            <div class="left">
                                <div class="row form_line">
                                    <div class="span1 video_name sb">Where?</div>
                                    <div class="span4">
                                        <span class="search_label">Country</span>
                                      
                                <select name="country_id">
                                    <option value="">Select country</option>
                                    {foreach from=$select_location item=section}
                                    <option {if $array_s.country_id == $section.Country_str_code } selected="selected"{/if} value="{$section.Country_str_code}">{$section.Country_str_name}</option>
                                    {/foreach}
                                </select>
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="search_line line"></div>
                            <div class="left">
                                <div class="row form_line">
                                    <div class="span1 video_name sb">When?</div>
                                    <div class="span6">
                                        <span class="grey">Online last:</span>
                                        <span class="search_label">Day </span>     <input value="d" type="radio" name="online_last" {if $array_s.online_last eq('d')}checked="checked"{/if}/>
                                        <span class="search_label">Week </span>    <input value="w" type="radio" name="online_last" {if $array_s.online_last eq('w')}checked="checked"{/if}/>
                                        <span class="search_label">Month </span>    <input value="m" type="radio" name="online_last" {if $array_s.online_last eq('m')}checked="checked"{/if}/>
                                    </div>
                                </div>
                            </div>
                            <div class="search_line line"></div>
                            
                            <div class="row ">
                                <div class="left span12">
                                <span class="video_name">Want to complete your search with a tag? go ahead!</span>
                                <div class="grey">type as many specific tags as you like, also you can search by user name here !</div>
            <div class="input-append" style="margin-top: 7px;">
                <input id="searc_input" class="span2" name="tag" size="16" type="text">
                                                        </div>
                                </div>
                            </div>
                                <div class="row">
    <div class="span5" style="text-align: left; margin-top: 10px; margin-bottom: 10px;">
        
                                                        {component TopTag}
    </div>
    <div class="span5">
    </div>
      
    </div>
    <div class="row padding10">
        <input type="submit" style="float: right;" value="Find" />
    </div>
                            </form>
                        </div>
                  

                    <div class="content_text">
    <div class="content_menu">
                                            <div class="this_me">MEMBERS</div>
                                    </div>
                         
        
		{if $tabs}
			{menu type='tabs-small' items=$tabs}
		{/if}
		<br clear="all" />
		{component SearchResultCount}
	
		{counter start=1 skip=1 print=false assign='counter'}
	
		{foreach from=$list item=item}
                    <div class="content_tab">
		<div class="line"></div>
                </div>
                
                
<div class="row">
        <div class="span3">
                <div class="row">
                        <div style="width:72px; margin-left: 0px;" class="span1">
                                {profile_thumb profile_id=$item.profile_id redirect_params=$item.url_params size=72}
                        </div>
                        <div style="margin-left: 15px; width: 130px;" class="span3">
                            <a href="/member/profile.php?profile_id={$item.profile_id}" class="list_profile_name">{$item.profile_label}</a>
                                <div class="list_profile_location">
                                        {if $item.location.city}
                                            {$item.location.city},
                                        {elseif !empty($item.location.custom_location)}
                                             {$item.location.custom_location},
                                        {/if}

                                        {if $item.location.state}
                                            {$item.location.state},
                                        {/if}

                                        {if $item.location.country}
                                            {$item.location.country}.
                                        {/if}
                                </div>
                                <div class="list_profile_old">{$item.sex_label}, {$item.old} yers old</div>
                                <div class="list_profile_follower">
                                    <div class="row">
                                        <div class="span1" style="width: 30px;">
                                            {component ProfileReferences profile_id=$item.profile_id mode='follow' type='small'}
                                        </div>
                                         <div class="span2"  style="width: 95px;">
                                             <div class="row">
                                                <div style="border-right: solid 1px #2da5da" class="num span1">{$item.video}</div>
                                                <div class="num span1">{$item.follow}</div>
                                        </div>
                                        <div style="line-height: 10px;" class="row">
                                                <div style="border-right: solid 1px #2da5da" class="span1">videos</div>
                                                <div class="span1">followers</div>
                                        </div>
                                         </div>
                                    </div>   
                                </div>
                        </div>
                </div>
        </div>
        <div class="span6">
                {component ProfileVideoListMini profile_id=$item.profile_id except_video=0}
        </div>
</div>
                               
		{counter}
			{if ($counter-1) == $ads_pos}
				{capture name=ads}{strip}
					{ads pos='profile_list'}
				{/strip}{/capture}
				{if $smarty.capture.ads}
					<br clear="all" />
					{block title=%.profile.list.ads_label}
						{$smarty.capture.ads}
					{/block}
				{/if}
			{/if}
		{/foreach}
		<br clear="all" />
		<br clear="all" />
		{if $enable_pagging}
			{paging total=$paging.total on_page=$paging.on_page pages=$paging.pages exclude="new_search"}
		{/if}
                     </div>
    </div>
   
        <script> 
            var performer = {if $array_s.tag}{$array_s.tag}{else}""{/if};
             {literal}
        $("#searc_input").tokenInput('/latest_video.php', {
            theme: "facebook",
            queryParam: 'q',
            preventDuplicates: false,
            prePopulate : performer,
            onAdd: function(){
          //  $('#landing_search').submit(); $array_s.tag
            }
         }); 
         $('.token-input-input-token-facebook').append('<span class="add-on"><img src="/static/img/searchi.png"></span>');    
         
             //<span class="add-on"><img src="/static/img/searchi.png"></span>
      </script>
    {/literal}            
                
	{/container}
	
{/canvas}
