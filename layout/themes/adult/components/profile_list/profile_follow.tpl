{* Component Profile List *}

{canvas}

	{container stylesheet="profile_list.style" class="profile_list_contener"}

                                <div class="content_tab">
                            
	
		{foreach from=$list item=item}
		<div class="line"></div>
                
                
                
<div class="row">
        <div class="span3">
                <div class="row">
                        <div style="width:72px; margin-left: 0px;" class="span1">
                                {profile_thumb profile_id=$item.profile_id redirect_params=$item.url_params size=72}
                        </div>
                        <div style="margin-left: 15px; width: 130px;" class="span3">
                                <div class="list_profile_name">{$item.profile_label}</div>
                                <div class="list_profile_location">
                                        {if $item.location.city}
                                            {$item.location.city},
                                        {elseif !empty($item.location.custom_location)}
                                             {$item.location.custom_location},
                                        {/if}

                                        {if $item.location.state}
                                            {$item.location.state},
                                        {/if}

                                        {if $item.location.country}
                                            {$item.location.country}.
                                        {/if}
                                </div>
                                <div class="list_profile_old">{$item.sex_label}, {$item.old} yers old</div>
                                <div class="list_profile_follower">
                                    <div class="row">
                                        <div class="span1" style="width: 30px;">
                                            {component ProfileReferences profile_id=$item.profile_id mode='follow' type='small'}
                                        </div>
                                         <div class="span2"  style="width: 95px;">
                                             <div class="row">
                                                <div style="border-right: solid 1px #2da5da" class="num span1">{$item.video}</div>
                                                <div class="num span1">{$item.follow}</div>
                                        </div>
                                        <div style="line-height: 10px;" class="row">
                                                <div style="border-right: solid 1px #2da5da" class="span1">videos</div>
                                                <div class="span1">followers</div>
                                        </div>
                                         </div>
                                    </div>   
                                </div>
                        </div>
                </div>
        </div>
        <div class="span7">
                {component ProfileVideoListMini profile_id=$item.profile_id except_video=0 limit=4}
        </div>
</div>
                                
		{/foreach}
		<br clear="all" />
		<br clear="all" />
		{if $enable_pagging}
			{paging total=$paging.total on_page=$paging.on_page pages=$paging.pages exclude="new_search"}
		{/if}
                     </div>

                   
                
	{/container}
	
{/canvas}
