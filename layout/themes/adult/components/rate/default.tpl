{* component Rate *}

{container stylesheet="style.style"}

  {if $enanble_rate}  
      <div class="row">
          <div class="span2">
      <span style="float:left;" class="kuinkey_title">
          {if $feature == 'video'}Video rating{/if}
          {if $feature == 'profile'}User rating{/if}
      </span>
          </div>
          <div class="span2">
    <div class="rates_cont">
        {foreach from=$rate_array.items item='rate'}
            <a href="javascript://" class="rate_item" {id="`$rate.id`"}>&nbsp;</a>
        {/foreach}
    </div>
          </div>
</div>
    {else}
        {component $total_rate}
    {/if}



{/container}
