{container}

{if $type eq 'reply'}

		{form SendMessage conversation_id=$conversation_id sender_id=$sender_id recipient_id=$recipient_id type=$type}
			{text_formatter for='text' entity="mailbox" controls="bold,italic,underline,emoticon"}<br clear="all" />
			{*label %.forms.send_message.fields.text for='text'*}
                        
			{input name="text"}
                        <p class="right">{button action='reply'}</p>
		{/form}

{else}
	<a href="javascript://" {id="send_message_link"}>{text %send_label}</a>
	<div style="display: none" {id="send_message_cont"}>
		{form SendMessage sender_id=$sender_id recipient_id=$recipient_id type=$type}
                    
                    
                      <div class="padding10"> 
                                      <div style="height: 10px"></div>
                                      <div class="" style="display: none;">
                                                    {label %.forms.send_message.fields.subject for='subject'}<br />
                                                    {input name="subject"}
                                             </div>
                                             <div class="popup_form">
                                                    {label %.forms.send_message.fields.text for='text'}<br />
                                                    {*text_formatter for='text' entity="mailbox" controls="bold,italic,underline,link,emoticon,image"}{input name="text"*}
                                                    {text_formatter for='text' entity="mailbox" controls="bold,italic,underline,emoticon"}{input name="text"}
                                             </div>
                                            <div class="popup_form">
                                                {button action='send'}
                                                <br clear="all" />
                                            </div>               
                                   </div> 
                    
                    
                    {*
		<table class="form">
			<tr>
				<td class="label">{label %.forms.send_message.fields.subject for='subject'}:</td>
				<td class="value all_row_width">{input name="subject"}</td>
			</tr>
			<tr>
				<td class="label">{label %.forms.send_message.fields.text for='text'}:</td>
				<td class="value">{text_formatter for='text' entity="mailbox"}{input name="text"}</td>
			</tr>
		</table>
		<br />
		<p>{button action='send'}</p>
                    *}
		{/form}
	</div>
{/if}

{/container}