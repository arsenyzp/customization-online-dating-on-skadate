{* component MailboxConversation *}
{canvas index}
{container stylesheet='mailbox_conversation.style' class="conversation" text_ns='components.mailbox_conversation'}

                    <div class="content_text">
                        
                          <div class="content_tab">

	{*block}
                    <div class="row">
                        <div class="span1" style="margin:0; width: 75px;">
                            {profile_thumb profile_id=$conv.initiator_id size=72 }
                        </div>
                        <div class="span6">
                            <div class="row-fluid">
                                <div class="list_profile_name span3">
                                    {if $conv.initiator_username  && $conv.initiator_username  != 'username'}<a href="{if $folder eq 'sentbox'}{document_url doc_key='profile' profile_id=$conv.initiator_id}{else}{document_url doc_key='profile' profile_id=$conv.initiator_id}{/if}">{$conv.initiator_username}</a>{else}{text %.label.deleted_member}{/if}
                                </div>
                                <div class="list_profile_old span2">{$conversation.age} years old</div>
                                <div class="list_profile_location span4">
                                    {if $conversation.location.city}
                                            {$conversation.location.city},
                                        {elseif !empty($conversation.location.custom_location)}
                                             {$conversation.location.custom_location},
                                        {/if}

                                        {if $conversation.location.state}
                                            {$conversation.location.state},
                                        {/if}

                                        {if $conversation.location.country}
                                            {$conversation.location.country}.
                                        {/if}
                                </div>
                                        <div class="span2"><span class="date_create">{$conv.conversation_ts|spec_date}</span></div>
                            </div>
                            <div class="message_list_text">
                                   {$conv.subject}
                           </div>
                        </div>
                    </div>

			{if $msg_count >= 5}
			<div class="right">
				{form ManageConversation conversation_id=$conv.conversation_id mark_for=$profile_id}
					{button action='mark_unread'}
					{button action='remove'}
				{/form}
			</div>
			{/if}
	{/block*}
        <div class="list_mess">

		{foreach from=$messages item='msg'}
                    
                    
                    <div class="line"></div>
                    
                    <div class="row">
                        <div class="span1" style="margin:0; width: 75px;">
                            {profile_thumb profile_id=$msg.sender_id size=72 }
                        </div>
                        <div class="span3">
                            <div class="row">
                                <div class="list_profile_name span1">
                                    {if $msg.username  && $msg.username  != 'username'}<a href="{if $folder eq 'sentbox'}{document_url doc_key='profile' profile_id=$msg.sender_id}{else}{document_url doc_key='profile' profile_id=$msg.sender_id}{/if}">{$msg.username}</a>{else}{text %.label.deleted_member}{/if}
                                </div>
                                </div>
                                <div class="row">
                                 <div class="span2"><span class="date_create">{$msg.time_stamp|spec_date}</span></div>
                                 </div>
                                 <div class="message_list_text">
                                   <span class="mess">{$msg.text|out_format:"mailbox"|censor:"mailbox"|smile}</span>
                           </div>
                            </div>
                        </div>
                    

		{/foreach}	
</div>

<div class="form_list">
			{*<div class="right">
				{form ManageConversation conversation_id=$conv.conversation_id mark_for=$profile_id}
					{button action='mark_unread'}
					{button action='remove'}
				{/form}
			</div>*}
                        <div class="line"></div>
				{if $conv.is_system eq 'no' && $conv.opponent}
					<a name="reply"></a>
					{component SendMessage conversation_id=$conv.conversation_id sender_id=$conv.new_msg_sender recipient_id=$conv.new_msg_recipient type='reply'}
				{/if}

</div>
	

	{*paging total=$paging.total on_page=$paging.on_page pages=$paging.pages*}
        
                </div>
</div>

{literal}
    <script>
    $(document).ready(function(){
        $('.list_mess').scrollTop(9999999);
    });
        </script>
{/literal}
{/container}
{/canvas}