
{canvas}
{container stylesheet="view_works.style"}

{component ProfileInfoLeft}

<div class="content_100">
{*
    <div class="main_menu">
        <ul class="menu-tabs">
            <li class="tab first"><a {if $tabs eq(0)} class="active"{/if} href="?tabs=0" ><span>New Fantasy</span></a></li>
            <li class="tab"><a {if $tabs eq(1)} class="active"{/if} href="?tabs=1" ><span>All Video</span></a></li>
            <li class="tab"><a {if $tabs eq(2)} class="active"{/if} href="?tabs=2" ><span>Uploaded video</span></a></li>
            <li class="tab"><a {if $tabs eq(3)} class="active"{/if} href="?tabs=3" ><span>Fantasy video</span></a></li>
            <li class="tab"><a {if $tabs eq(4)} class="active"{/if} href="?tabs=4" ><span>Purchased video</span></a></li>
            <li class="tab"><a {if $tabs eq(5)} class="active"{/if} href="?tabs=5" ><span>My work</span></a></li>
        </ul>
    </div>
    *}
                        {component ProfileInfoHeader}
                         
        <div class="content span7">	
               <div class="content_text">
            <!--Меню пользователя-->
                                 <div class="content_menu">
                                    <div class="content_menu_item"><a href="/member/newsfeed.php">NEWS</a></div>
                                    <div class="content_menu_item "><a href="/member/myfantasy.php">MY FANTASIES</a></div>
                                    <div class="content_menu_item activ_menu"><a href="/member/view_works.php">MY LIBRARY</a></div>
                                    <div class="content_menu_item"><a href="/member/sent_applications.php">MY JOBS</a></div>
                                    <div class="content_menu_item"><a href="/member/view_review.php?mode=my">MY REVIEWS</a></div>
                                    <div class="content_menu_item"><a href="/member/following.php?tabs=3">FOLLOWING</a></div>
                                    <div class="content_menu_item"><a href="/member/bookmarks.php?tabs=22">FOLLOWERS</a></div>
                                    <div class="content_menu_item" style="float: right; background-color: #412756; margin-right: 10px;"><a href="/member/edit_profile.php">MY ACCOUNT</a></div>
                                </div>
                                <div style="height: 10px"></div>
                                <div class="content_menu">
                                    <div class="content_menu_item {if $tabs eq(3)} activ_menu{/if}" style="max-width: 140px;"><a href="/member/view_works.php?tabs=3">MY COMPLETED FANTASIES</a></div>
                                    <div class="content_menu_item {if $tabs eq(5)} activ_menu{/if}"><a href="/member/view_works.php?tabs=5">MY PERFOMANCES</a></div>
                                    <div class="content_menu_item {if $tabs eq(6)} activ_menu{/if}"><a href="/member/view_works.php?tabs=6">MY FAVORITES</a></div>
                                </div>
            
            
            
                        <div class="content_search">
                               <div class="content_tab">
                                    <div class="input-append">
                                        <input class="span2" id="searc_input" size="16" type="text" placeholder="search">
                                        <span class="add-on"><img src="/layout/themes/adult/img/searchi.png"></span>
                                    </div>
                                </div>
                                <div class="content_tab" style="text-align: left;">
                                    <div class="count_p">{$paging.total} Fantasies</div>
                                </div>
                        </div>
                                <div class="content_tab">
                            

     {if $video}
         {*foreach from=$video item='video' key='it' name='vid'}
        <!--видео галерея список -->
       {if $it == 0}<div class="row-fluid">{/if}
       {if $it % 2 == 0 && $it != 0}</div><div class="line"></div><div class="row-fluid">{/if}
                <div class="span6">
                        <div class="video_name">{$video.title|out_format|smile|censor:"video":true}</div>
                        <div class="video_thumb video_video">
                            {if $video.thumb_img eq 'default'}
                                    <a href="{$video.video_page}"><div class="video_def_thumb"></div></a>
                            {elseif $video.thumb_img eq 'friends_only'}
                                    <div class="video_friends_thumb"></div>
                            {elseif $video.thumb_img eq 'password_protected'}
                                <a href="{$video.video_page}"><div class="video_password_thumb"></div></a>
                            {else}
                                    <a href="{$video.video_page}"><img src="{$video.thumb_img}" class="video_thumb" align="left" /></a>
                            {/if}
                        </div>
                        
                        
                        <div class="stat_video_list  row">
                             <div class="span6">
                                    <div>
                                        {rate rate=$video.rate_score feature='video' small='small'}
                                    </div>
                             </div>
                             <div class="span6">
                                <div class="row">
                                    <div class="video_like span3">{$video.like_count}</div>
                                    <div class="video_view span3">{$video.view_count}</div>
                                    <div class="video_comment span3">{$video.comment_count}</div>
                                 </div>
                             </div>
                        </div>
                        
                        <div class="video_desc">{$video.description|out_format|truncate:150|smile|censor:"video":true}</div>
                </div>
        {if $smarty.foreach.vid.last}</div>{/if}
        {/foreach*}
        
           {foreach from=$video item='video' key='it' name='vid'}
        <!--видео галерея список -->
     <div class="line"></div>
     <div class="row-fluid">
                <div class="span6">
                        <div class="video_thumb video_video">
                            {if $video.thumb_img eq 'default'}
                                    <a href="{$video.video_page}"><div class="video_def_thumb"></div></a>
                            {elseif $video.thumb_img eq 'friends_only'}
                                    <div class="video_friends_thumb"></div>
                            {elseif $video.thumb_img eq 'password_protected'}
                                <a href="{$video.video_page}"><div class="video_password_thumb"></div></a>
                            {else}
                                    <a href="{$video.video_page}"><img src="{$video.thumb_img}" class="video_thumb" align="left" /></a>
                            {/if}
                        </div>
                </div>      
                 
                             <div class="span6">
                                    
                                    <div class="video_name">{$video.title|out_format|smile|censor:"video":true}</div>
                                    
                                     <div class="video_tag">
                                            {foreach from=$video.tag_list item='tag'}
                                                 <div class="tag_item"><a href="?tag={$tag.tag_id}">{$tag.tag}</a></div>
                                             {/foreach}
                                             <div style="clear:both;"></div>
                                   </div>
                                    <div class="row">
                                           <div class="span4 kuinkey_title left" style="padding-top:3px;">video rating</div>
                                           <div class="span3">{rate rate=$video.rate_score feature='video' small='small'}</div>
                                    </div>
                                    <div class="row kuinkey_title" style="width:295px;">
                                          <div class="video_like span3">{$video.like_count}</div>
                                          <div class="video_view span3">{$video.view_count}</div>
                                          <div class="video_comment span3">{$video.comment_count}</div>
                                   </div>
                             
                   
                        
                       {* <div class="video_desc">{$video.description|out_format|truncate:150|smile|censor:"video":true}</div>*}
                        </div>
                </div>
        {/foreach}

        <div style="clear:both;"></div>
        <div class="pagin_footer">
                </div >
               
        {else}
	   <div class="no_content">No video</div>
        {/if}
        
{***********************************************}
<br clear="all" />
{paging total=$paging.total on_page=$paging.on_page pages=$paging.pages}
                         
                                                  
                        </div>
               </div>
        </div>
</div>


{literal}
    <script>
       function Show(id){
            $('#desc_'+id).css('height', '100%');
            $('#show_'+id).css('display', 'none');
            $('#hide_'+id).css('display', 'block');
        }

        function Hide(id){
            $('#desc_'+id).css('height', '5em');
            $('#show_'+id).css('display', 'block');
            $('#hide_'+id).css('display', 'none');
        }
               

       $(document).ready(function(){
              $('.video_thumb').css('width', '300px');
              setTimeout('ks', 1000);
          function ks(){
                 $('.video_thumb').css('width', '300px');
          }
       });
</script>
    </script>
{/literal}
{/container}
{/canvas}