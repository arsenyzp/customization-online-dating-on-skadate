{container stylesheet="profile_status_view.style"}


<div style="height:45px;" class="content_head row-fluid">
        <div class="hm">
                <span style="font-size:13px;" class="menu_text">{$username}</span>
        </div>
        <div class="hm">{$profile.age}  years old</div>
        <div class="hm">{$profile.sexLabel}</div>
        <div style="border:none; max-width: 200px;" class="hm"><div style="margin-top: 5px;" class="map_location"></div> 
            {if $profile.city}
                    {$profile.city},
                {elseif !empty($profile.custom_location)}
                    {$profile.custom_location},
                {/if}                
                {if $profile.state}
                    {$profile.state},
                {/if}
                {if $profile.country}
                    {$profile.country}
                {/if}
        </div>
        {if isset($profile.activity_info.online)} <div class="online_us">online</div>{/if}
        <div style="clear:both;"></div>
</div>

        {**************************}
 
<div class="content_profile_info">
    <div style="padding-left: 15px; text-align: left;" class="padding10 row">
            <div class="profile_big_im span4">
                      {profile_thumb profile_id=$profile.id size=274}
            </div>
            <div class="profile_big_desc span5">
                    <div id="profile_desc_text">
                            <span class="profile_title2">About me</span><br>
                            {$profile.general_description}
                    </div>
                    <div id="profile_tag">

                            <span class="profile_title2">Perfomance tag</span><br>
                            {foreach from=$profile.tag item='tag'}
                            {if $tag}<div class="tag_item">{$tag}</div>{/if}
                            {/foreach}

                            <div style="clear:both;"></div>
                    </div>
                            <div style="width: 280px">      
                     <div class="kuinkey_title">Willing to perform: {if $profile.match_sex eq(1)}Straight{/if}{if $profile.match_sex eq(2)}Gay{/if}{if $profile.match_sex eq(3)}Trans{/if}</div>
                     <div class="margin0 line"></div>
                    <div id="profile_reit">
                            {*<span style="float:left; font-size:12px;" class="profile_title2">User rating</span>*}
                            {component $ProfileRate}
                    </div>
                    <div class="margin0 line"></div>
                    <div id="profile_stat" class="row-fluid" style="height: 21px;">
                            <div class="foto_count span1">{$profile.video}</div>
                            <div class="glass_count span1">{$profile.views}</div>
                            <div class="plus_count span1">{$profile.follow}</div>
                    </div>
                     <div class="margin0 line"></div>
                            </div>
            </div>
    </div>
    <div class="padding10 profile_button row">
            <div class="profile_big_im span4">Interested in:     
                {foreach from=$profile.tag item='tag'}
                            {$tag}
                {/foreach}
            </div>
            <div style="margin:0; width:500px;" class="span6">
                                {component ProfileActions profile_id=$actorId}
                                   {component $invite_fantasy}
                                    </div>

            </div>
    </div>  
{/container}