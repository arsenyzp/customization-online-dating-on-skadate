
{canvas}
	{container stylesheet="video_view.style"}
              
                            <div style="height:45px;" class="content_head row-fluid">
                                <div class="hm" style="margin: 0; border: none;">
                                        {profile_thumb profile_id=$video_info.profile_id size=48}
                                </div>
                                    <div class="hm">
                                            <span style="font-size:13px;" class="menu_text">{$video_info.owner_name|upper}</span>
                                    </div>
                                    <div class="hm">{$video_info.age}  years old</div>
                                    <div class="hm">{$video_info.sexLabel}</div>
                                    <div style="border:none; max-width: 200px;" class="hm"><div style="margin-top: 5px;" class="map_location"></div> 
                                        {if $video_info.location.city}
                                                {$video_info.location.city},
                                            {elseif !empty($video_info.location.custom_location)}
                                                {$video_info.location.custom_location},
                                            {/if}                
                                            {if $video_info.location.state}
                                                {$video_info.location.state},
                                            {/if}
                                            {if $video_info.location.country}
                                                {$video_info.location.country}
                                            {/if}
                                    </div>
                                    {if isset($profile.activity_info.online)} <div class="online_us">online</div>{/if}
                                    <div style="clear:both;"></div>
                            </div>
                                    
                           <div style="height: 15px;"></div>    
                                
                           <div class="content_text">
                                <div class="content_menu">
                                        <div class="this_me">{$video_info.title|upper}</div>
                                </div>
                    
		{if $service_msg}
		  <div class="no_content">{$service_msg}</div>
		{elseif $pass_protection}
		  {block}
		  <div {id="password_unlock"} class="center">
		  <div class="video_password_thumb" style="margin: 30px auto"></div>
		  <form>
		      <input type="password" name="password" />
		      <input type="submit" value="{text %.components.photo_view.unlock}" />
		  </form>
		  </div>
		  {/block}
		{else}
                                
                                <div style="height: 15px;"></div>    
    		<div class="video_box">
    			<div id="video_player_cont">
    				{if $video_info.video_source eq 'file'}
    					{component VideoPlayer video_file_url=$video_info.video_url width='790' }
    				{else}
    					{$video_info.code}
    				{/if}
    			</div>
    		</div>

                        <div class="padding10">
                                <div class="row">
                                    <div class="span6" id="video_desc_text" >
                                        <div class="kuinkey_title">Video description:</div>
                                            {$video_info.description|out_format:"comment"|smile|censor:"video":true}
                                    </div>
                                    <div class="span4" style="text-align: left; color: #7dc5e5;">
                                        <div class="kuinkey_title row-fluid" style="width: 260px; line-height: 19px;">
                                               <div class="video_like span2" id="{$video_info.video_id}">{$like_count}</div>
                                                <div class="video_view span2">{$video_info.view_count}</div>
                                                <div class="video_comment span3">0</div>
                                        </div>
                                        <div class="line"></div>
                                        <div id="kuinkey_title video_reit">
                                            {component $VideoRate}
                                        </div>
                                        <div class="line"></div>
                                        <div class="kuinkey_title">Lenght {if $video_info.video_lenght < 61} 
                                                                                           {$video_info.video_lenght} s
                                                                                           {else}
                                                                                                  {math equation="x/y" x=$video_info.video_lenght y=60.0000 format="%.2d"} min
                                                                                           {/if}</div>
                                        <div class="line"></div>
                                        <div class="soc_share" style="position: relative;">
                                                Share profile: <img src="/layout/img/google.png">
                                                <img src="/layout/img/ttt.png">
                                                <img src="/layout/img/twitter_share.png">
                                                <img src="/layout/img/facebook_share.png">
                                        </div>
                                        <div style="text-align: right; margin-top: 20px;">
                                                 {component Report type='video' reporter_id=$viewer_id entity_id=$video_info.video_id}
                                                 <div class="video_view_button">
                                                 {if $my_fantasy and $status eq(2)}
                                                       <input type="button" value="Approve" onclick="ApproveVideoWorck({$video_info.fantasy_id});" /><br />
                                                       {component $report}
                                                   {/if}
                                                   {if $buy_video}
                                                        <input type="button" value="Buy video" onclick="BuyVideo({$video_info.video_id});" /><br />
                                                   {/if}
                                                 </div>
                                      </div>
                                    </div>
                                </div>
                        </div>
                                        {if $video_info.performer_id}
                        <div class="video_fantasy_description">
                            <div class="padding10">
                                     <div class="row">
                                        <div class="span1" style="margin:0; width: 75px;">
                                            {profile_thumb profile_id=$video_info.performer_id size=72 }
                                        </div>
                                        <div class="span6">
                                            <div class="row-fluid">
                                                <div class="list_profile_name span3">
                                                    {if $video_info.performer.location.username  && $video_info.performer.location.username  != 'username'}
                                                        <a href="{document_url doc_key='profile' profile_id=$conv.performer_id}">{$video_info.performer.location.username}</a>{else}{text %.label.deleted_member}{/if}
                                                </div>
                                                <div class="list_profile_old span2">{$video_info.performer.age} years old</div>
                                                <div class="list_profile_location span4">
                                                    {if $video_info.performer.location.city}
                                                            {$video_info.performer.location.city},
                                                        {elseif !empty($video_info.performer.location.custom_location)}
                                                             {$video_info.performer.location.custom_location},
                                                        {/if}

                                                        {if $video_info.performer.location.state}
                                                            {$video_info.performer.location.state},
                                                        {/if}

                                                        {if $video_info.performer.location.country}
                                                            {$video_info.performer.location.country}.
                                                        {/if}
                                                </div>
                                                        <div class="span2"></div>
                                            </div>
                                            <div class="message_list_text">
                                                {$video_info.fantasy_description.description}
                                           </div>
                                        </div>
                                    </div>
                            </div>
                         </div>
                 {/if}
                       
                           <div class="padding10 total_video kuinkey_title" style="text-align: left;">
                                    {$comment_count} Comments
                           </div>
                           
                           <div class="padding10" >{*style="width: 507px;"*}
                          
                           {if $viewer_id}
                             <div id="invete" style="display: none;">
                                 <div class="padding10">
                                {form Reviews}
                                <textarea id="text_review" style="max-width: 315px; min-width: 315px;"  name="text_review"></textarea>
                                     <input type="hidden" name="reporter_id" value="{$video_info.profile_id}" />
                                     <input type="hidden" name="entity_id" value="{$video_info.video_id}" />
                                     <input type="hidden" name="fantasy_id" value="{$fantasy_id}" />
                                     <input type="hidden" name="type" value="2" />
                                     <input type="submit" value="Add review" />
                                     <input type="button" value="Cancel"  class="close_btn"/>
                                 {/form}
                                 </div>
                             </div>
                            {/if}
                        
                         {if $video_comments}
                              {*<div class="line"></div>*}
                                         {component $video_comments}
                          {/if}
                          </div>
                         
                {*component ContentSocialSharing*}

		{/if}
                
                        <div class="content_tab">
                             <div class="line"></div>
                        </div>
                             <div class="content_menu">
                                        <div class="this_me">RELATED VIDEO</div>
                             </div>
                             <div style="height: 20px;"></div>
                        <div class="content_tab">
                            {component ProfileOtherVideoList profile_id=$video_info.profile_id except_video=$video_info.video_id amount=3}
                        </div>
                 </div>
		{*
		<div class="float_half_right narrow">
		{if !$service_msg && !$pass_protection}
			{block title=%details}
				<table class="thumb_text_list">
					<tr>
						<td class="thumb">
							{profile_thumb profile_id=$video_info.profile_id size=60}
						</td>
						<td class="listing">
						<p class="small">{text %upload_by} <a href="{document_url doc_key='profile' profile_id=$video_info.profile_id}">{$video_info.owner_name}</a><br />
                                                   
                                                    {if $my_fantasy and $status eq(2)}
                                                        <input type="button" value="Approve" onclick="ApproveVideoWorck({$video_info.fantasy_id});" /><br />
                                                        {component $report}
                                                    {/if}
                                                    {if $buy_video}
                                                         <input type="button" value="Buy video" onclick="BuyVideo({$video_info.video_id});" /><br />
                                                    {/if}
                                            <div class="clearfix"></div>
                                                    
                                                    
                        {$video_info.upload_stamp|spec_date}<br />                        
                        {if $enable_categories && $video_info.category_id}{text %.components.video_list.category} {text %.video_categories.cat_`$video_info.category_id`}<br />{/if}						
                        {text %.components.video_list.views} {$video_info.view_count}
						</td>
					</tr>
				</table>
				<p class="small">{text %descr}</p>
				<p>{$video_info.description|out_format:"comment"|smile|censor:"video":true}</p>
				<div class="block_submit right">
					{component Report type='video' reporter_id=$viewer_id entity_id=$video_info.video_id}
				</div>
			{/block}
			{if $show_details}
				{block title=%share_details}
				<p class="all_row_width">
					{text %embeddable_player}
					<input type="text" {id="video_code"} value="{$video_info.share_code}" />
					{text %permalink}
					<input type="text" {id="permalink"} value="{$video_info.permalink}" />
				</p>
				{/block}
			{/if}
			{component $VideoTags}
		{/if}
		{component ProfileOtherVideoList profile_id=$video_info.profile_id except_video=$video_info.video_id}
		</div>
		*}
                {*
                <div id="invete">
                   {form Reviews}
                        <textarea id="text_review"  name="text_review"></textarea>
                        <input type="hidden" name="reporter_id" value="{$video_info.profile_id}" />
                        <input type="hidden" name="entity_id" value="{$entity_id}" />
                        <input type="hidden" name="fantasy_id" value="{$fantasy_id}" />
                        <input type="hidden" name="type" value="2" />
                        <input type="submit" value="Add review" />
                        <input type="button" value="Cancel"  class="close_btn"/>
                    {/form}
                </div>
                *}

                
	{/container}
{/canvas}