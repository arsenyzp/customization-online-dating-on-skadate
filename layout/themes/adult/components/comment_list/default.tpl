{* component Comment Add Form *}

{container stylesheet='comment_list.style'}
{if $comments}

<div {id="comment_list_cont"}>

    	{foreach from=$comments item='comment_item'}
            
                 <div class="line"></div>

                
<div class="row">
    <div class="span4" style="width: 230px;">
                <div class="row">
                        <div style="width:72px; margin-left: 0px;" class="span1">
                                {profile_thumb profile_id=$comment_item.dto->getAuthor_id() size=72}
                        </div>
                        <div style="margin-left: 15px; width: 140px;" class="span3">
                                <div class="list_profile_name">{$comment_item.username}</div>
                                <div class="list_profile_location">
                                        {if $comment_item.location.city}
                                            {$comment_item.location.city},
                                        {elseif !empty($comment_item.location.custom_location)}
                                             {$comment_item.location.custom_location},
                                        {/if}

                                        {if $comment_item.location.state}
                                            {$comment_item.location.state},
                                        {/if}

                                        {if $comment_item.location.country}
                                            {$comment_item.location.country}.
                                        {/if}
                                </div>
                                <div class="list_profile_old">{$comment_item.sex_label}, {$comment_item.old} yers old</div>
                                <div class="list_profile_follower">
                                    <div class="row">
                                        <div class="span1" style="width: 30px;">
                                            {component ProfileReferences profile_id=$comment_item.dto->getAuthor_id() mode='follow'}
                                        </div>
                                         <div class="span2" style="width: 100px;">
                                             <div class="row">
                                                <div style="border-right: solid 1px #2da5da" class="num span1">{$comment_item.video}</div>
                                                <div class="num span1">{$comment_item.follow}</div>
                                        </div>
                                        <div style="line-height: 10px;" class="row">
                                                <div style="border-right: solid 1px #2da5da" class="span1">videos</div>
                                                <div class="span1">followers</div>
                                        </div>
                                         </div>
                                    </div>   
                                </div>
                        </div>
                </div>
        </div>
        <div class="span4">
            <div class="fantasy_list">
               {$comment_item.dto->getCreate_time_stamp()|spec_date}
           </div>  
            
             <div class="description_fantasy">
                  
                 {$comment_item.dto->getText()|out_format:'comment'|censor:'comment'|smile}
             </div>
            
                     
              <div class="informer_fantasy">
                  

                </div>
        </div>
</div>
            
            
            
            
            
    {*        
        <li class="item">
            <div class="list_thumb">
            	{profile_thumb profile_id=$comment_item.dto->getAuthor_id() size='40'}
            </div>
            <div class="list_block">
            	<div class="list_info">
            		<div class="comment_stat">
	                    <div class="item_title">
                        	{if $comment_item.username}
                        		<a href="{$comment_item.profile_url}">{$comment_item.username}</a>
	                        {else}
                            	<span>{text %.label.deleted_member}</span>
    	                    {/if}
                         </div>
						{$comment_item.dto->getCreate_time_stamp()|spec_date}
	                </div>
	                <div class="del_link">
    	            	{if $comment_item.delete}<a href="javascript://" {id="`$comment_item.delete_link_id`"}>{text %.comment.comment_manage_delete}</a>{/if}
        	        </div>
				</div>
                <div class="list_content">
            		{$comment_item.dto->getText()|out_format:'comment'|censor:'comment'|smile}
            	</div
            ></div>
        </li>  *}
        {/foreach}        
 
      
 	{if $pages}
      	<div class="paging">
        	<span>Pages: </span>
			{foreach from=$pages item='page'}
				<a{if $page.active} class="active"{/if} href="javascript://" {id="`$page.id`"}>{$page.label}</a>
			{/foreach}
		</div>
      {/if}
 </div>
{/if}
{/container}
