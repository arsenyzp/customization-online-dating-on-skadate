
{canvas}
{container stylesheet="view_request.style"}


{component ProfileInfoLeft}

<div class="content_100">

                        {component ProfileInfoHeader}
        
                <div class="content span7">
                    <div class="content_text">

                         <!--Меню пользователя-->
                                <div class="content_menu">
                                    <div class="content_menu_item {if $tabs eq(55)}activ_menu{/if}"><a href="/member/newsfeed.php">NEWS</a></div>
                                    <div class="content_menu_item  {if $tabs eq(1)}activ_menu{/if}"><a href="/member/myfantasy.php">MY FANTASIES</a></div>
                                    <div class="content_menu_item"><a href="/member/view_works.php">MY LIBRARY</a></div>
                                    <div class="content_menu_item  activ_menu"><a href="/member/sent_applications.php">MY JOBS</a></div>
                                    <div class="content_menu_item {if $tabs eq(44)}activ_menu{/if}"><a href="/member/view_review.php?mode=my">MY REVIEWS</a></div>
                                    <div class="content_menu_item"><a href="/member/following.php?tabs=3">FOLLOWING</a></div>
                                    <div class="content_menu_item {if $tabs eq(22)} activ_menu{/if}""><a href="/member/bookmarks.php?tabs=22">FOLLOWERS</a></div>
                                    <div class="content_menu_item" style="float: right; background-color: #412756; margin-right: 10px;"><a href="/member/edit_profile.php">MY ACCOUNT</a></div>
                                </div>

                        <div class="content_search">
                                    <div class="input-append">
                                        <input class="span2" id="searc_input" size="16" type="text" placeholder="search">
                                        <span class="add-on"><img src="/layout/themes/adult/img/searchi.png"></span>
                                    </div>
                                </div>
                                <div class="content_tab" style="text-align: left;">
                                    <div class="count_p">{$paging.total} Jobs</div>
                                </div>
                                <div class="content_tab">
                                       
                                       <div class="line"></div>
                                       <div class="profile_menu row">
                                          <div class="span1">
                                              <a href="/member/sent_applications.php" {if $tabs eq(0)} class="active"{/if}>My requests</a>
                                          </div>
                                          <div class="span2">
                                                 <a href="/member/view_request.php?tabs=3" {if $tabs eq(3)} class="active"{/if}>Applications performers</a>
                                          </div>
                                      </div>
                                       
            {foreach from=$list_performer item='fantasy'}
                   
              <div id="div_{$fantasy.id_worck}"  {if $fantasy.status_update eq(2) and $fantasy.worck_status eq('0')}style="background: #BBFFFF"{/if}>
                  <div class="line"></div>
                  <div class="row">
                         <div class="span4">
                                   <div class="row">
                                           <div style="width:72px; margin-left: 0px;" class="span1">
                                                   {profile_thumb profile_id=$fantasy.id_creator redirect_params=$fantasy.url_params size=72}
                                           </div>
                                           <div style="margin-left: 15px; width: 200px;" class="span3">
                                                   <div class="list_profile_name">{$fantasy.username}</div>
                                                   <div class="list_profile_location">
                                                           {if $fantasy.location.city}
                                                               {$fantasy.location.city},
                                                           {elseif !empty($fantasy.location.custom_location)}
                                                                {$fantasy.location.custom_location},
                                                           {/if}

                                                           {if $fantasy.location.state}
                                                               {$fantasy.location.state},
                                                           {/if}

                                                           {if $fantasy.location.country}
                                                               {$fantasy.location.country}.
                                                           {/if}
                                                   </div>
                                                   <div class="list_profile_old">{$fantasy.sex_label}, {$fantasy.old} yers old</div>
                                                   <div class="list_profile_follower">
                                                       <div class="row">
                                                           <div class="span1" style="width: 30px;">
                                                               {component ProfileReferences profile_id=$fantasy.profile_id mode='follow'}
                                                           </div>
                                                            <div class="span2">
                                                                <div class="row">
                                                                   <div style="border-right: solid 1px #2da5da" class="num span1">{$fantasy.video}</div>
                                                                   <div class="num span1">{$fantasy.follow}</div>
                                                           </div>
                                                           <div style="line-height: 10px;" class="row">
                                                                   <div style="border-right: solid 1px #2da5da" class="span1">videos</div>
                                                                   <div class="span1">followers</div>
                                                           </div>
                                                            </div>
                                                       </div>   
                                                   </div>
                                           </div>
                                   </div>
                           </div>
                         
                     <div class="span4">    
                            <div class="fantasy_list">
                                 <span class="list_fantasy_title">  <a href="fantasy_view.php?edit={$fantasy.id_item}">{$fantasy.title|out_format|smile|censor:"video":true}</a></span>
                                 <span class="list_fantasy_price">    
                                 Budget:
                                                             {if $fantasy.budget eq '0'}
                                                 Negotiable
                                             {else}
                                                   {$fantasy.budget}$
                                             {/if}

                                 </span>
                                             <div class="status_worck">
                                               {if $fantasy.worck_status eq('0')}
                                                  {* <a href="offer_details.php?worck_id={$fantasy.worck_id}">View offer details</a>*}
                                                   <span class="sent">Sent</span>
                                               {elseif $fantasy.worck_status eq('1')}
                                                   <span class="accept">Accepted</span>
                                               {elseif $fantasy.worck_status eq('2')}
                                                   <span class="sent">Pending verification</span>
                                                {elseif $fantasy.worck_status eq('3')}
                                                   <span class="accept">Successfully completed</span>
                                               {/if}
                                           </div>
                                     <div style="clear:both"></div>
                            </div>
                     
                      <div>
                              <div id="desc_{$fantasy.id_item}" class="description_fantasy_text">
                               {$fantasy.description|out_format|smile|censor:"video":true}
                              </div>
                              <div id="show_{$fantasy.id_item}"><a onclick="Show('{$fantasy.id_item}'); return false;" href="#">View all</a></div>
                              <div style="display:none;" id="hide_1"><a onclick="Hide('{$fantasy.id_item}'); return false;" href="#">Hide</a></div>
                       </div>
                    

                       <div style="text-align: right;">
                            {if $user and $fantasy.worck_status eq(0)}
                                <input type="button" value="Decline" onclick="SendDecline({$fantasy.id_worck});" style="float:right; margin-left: 5px;" id="button_confirm_{$fantasy.id_worck}" />
                                <input type="button" value="Accept" onclick="SendAccept({$fantasy.id_worck});" style="float:right; margin-left: 5px;" id="button_refuse_{$fantasy.id_worck}" />
                                <input type="button"  value="Edit" onclick="EditRequest({$fantasy.id_worck});" style="float:right; margin-left: 5px;" id="button_edit_{$fantasy.id_worck}" />
                          {else}
                               {if $fantasy.worck_status eq('0')}
                                     <span class="sent">New</span>
                                 {elseif $fantasy.worck_status eq('1')}
                                     <span class="accept">Waiting to perform</span>
                                 {elseif $fantasy.worck_status eq('-1')}
                                     <span class="declined">Declined</span>
                                  {elseif $fantasy.worck_status eq('2')}
                                     <span class="sent">Pending verification</span>
                                     {if $fantasy.video_key}
                                     <input type="button" value="View video" onclick="document.location='/member/video_view.php?videokey={$fantasy.video_key}'"  style="margin-left:5px;"/>
                                     {else}
                                         <span class="sent"> Waiting converting</span>
                                     {/if}
                                 {/if}
                           {/if}
                             <input class="read_more" type="button" value="Read More" onclick="ViewMoreFantasy({$fantasy.id_fantasy})" />
                             </div>
                       </div>
                       </div>
                </div>
             {/foreach}
             
             <br clear="all" />
{paging total=$paging.total on_page=$paging.on_page pages=$paging.pages}
         </div>
     </div>
</div>





</div>


<div id="form_edit_contract" style="display: none">
       {form OrderDetails}
       <div class="padding10">
                    <input type="hidden" name="fantasy_id" id="fantasy_id"/>
                    <input type="hidden" name="worck_id" id="worck_id" /> 
                     <input type="hidden" name="lenght_video" id="lenght_video" /> 
                    
                    <div class="popup_form">
		Budget: <input type="text" name="budget" id="budget_contract"  style="width:47px; margin-left:5px;margin-bottom: 5px;">
                                            <br>
                                            <span  class="budget_popup inf">Kuinkey receives 20%</span>
                     </div>
                     <div class="popup_form">
                                    Completion date: <input type="text" id="datapicker" name="completion_date"  style="background: url('/layout/img/strelka.png') no-repeat scroll 71px 10px transparent; margin-left:5px; width:70px;" >
                     </div>
                     <div class="line"></div>
                      <div class="popup_form">
                            <div class="row">
                                    <div style="line-height:30px; width: 120px;height: 30px;" class="span2">
                                            Public viewing price:
                                    </div>
                                    <div style="width: 120px;height: 30px;" class="span2">
                                            <input type="text" name="price_video" id="price_video" style="width:70px; margin-bottom: 10px;" />
                                    </div>
                            </div>
                            <div style="margin-bottom: 30px;" class="row">
                                    <div style="width: 120px" class="span2">
                                        <input type="text" value="500keys" style="width:70px;" id="performer_r" ><br>
                                            <span class="inf">Performer receives (50%)</span>
                                    </div>
                                    <div style="width: 120px" class="span2">
                                        <input type="text" value="500keys" style="width:70px;" id="user_r"><br>
                                            <span class="inf">Users receives (20%)</span>
                                    </div>
                                    <div style="width: 120px" class="span2">
                                        <input type="text" value="500keys" style="width:70px;" id="kuinkey_r" ><br>
                                            <span class="inf">Kuinkey receives (20%)</span>
                                    </div>
                            </div>
                    </div>
                     
                      <div class="popup_form">
                        <div class="left_cols_form">Messag to Creator</div>
                        <div class="right_cols_form"><textarea name="messag" style="max-width: 360px"></textarea></div>
                        <br clear="all" />
                    </div> 
                     <div class="popup_form">
                        <input type="submit" value="Continue" style="margin-right: 5px;" />   <input type="button" class="read_more" onclick="f" value="Cancel" />
                        <br clear="all" />
                    </div>   
       </div>
       {/form}
</div>

{literal}
{/literal}
{/container}
{/canvas}