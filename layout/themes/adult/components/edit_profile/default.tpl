{*Component Profile Edit*}

{canvas}
{container stylesheet="edit_profile.style" }

{component ProfileInfoLeft}

<div class="content_100">

    {component ProfileInfoHeader}

    <div class="content span7">	
        <div class="content_text">
            <!--Меню пользователя-->
            <div class="content_menu">
                <div class="content_menu_item {if $tabs eq(55)}activ_menu{/if}"><a href="/member/newsfeed.php">NEWS</a></div>
                <div class="content_menu_item  {if $tabs eq(1)}activ_menu{/if}"><a href="/member/myfantasy.php">MY FANTASIES</a></div>
                <div class="content_menu_item {if $tabs eq(11)} activ_menu{/if}"><a href="/member/view_works.php">MY LIBRARY</a></div>
                <div class="content_menu_item {if $tabs eq(77)} activ_menu{/if}"><a href="/member/sent_applications.php">MY JOBS</a></div>
                <div class="content_menu_item {if $tabs eq(44)}activ_menu{/if}"><a href="/member/view_review.php?mode=my">MY REVIEWS</a></div>
                <div class="content_menu_item {if $tabs eq(3)} activ_menu{/if}"><a href="/member/following.php?tabs=3">FOLLOWING</a></div>
                <div class="content_menu_item {if $tabs eq(22)} activ_menu{/if}"><a href="/member/bookmarks.php?tabs=22">FOLLOWERS</a></div>
                <div class="content_menu_item activ_menu" style="float: right; background-color: #412756; margin-right: 10px;"><a href="/member/edit_profile.php">MY ACCOUNT</a></div>
            </div>
            <div style="height: 10px"></div>
            <div class="content_menu">
                                    <div class="content_menu_item"><a href="/member/edit_profile.php">SETTINGS</a></div>
                                    <div class="content_menu_item"><a href="/member/points_purchase.php">MANAGE KEYS</a></div>
                                    <div class="content_menu_item"><a href="/member/block_list.php">BLOCK LIST</a></div>
                                    <div class="content_menu_item"><a href="/member/payment_selection.php">KUINKEY+</a></div>
                                </div>

            <div class="content_tab">

                <div class="line"></div>
                <div class="profile_menu row">
                    <div class="span1">
                        <a {if $form_tab eq(0)}class="active"{/if} href="/member/edit_profile.php">Profile</a>
                    </div>
                    <div class="span1">
                        <a {if $form_tab eq(1)}class="active"{/if} href="/member/edit_profile.php?form_tab=1">This is me</a>
                    </div>
                    <div class="span1">
                        <a {if $form_tab eq(2)}class="active"{/if} href="/member/edit_profile.php?form_tab=2">Password</a>
                    </div>
                    <div class="span1">
                        <a {if $form_tab eq(3)}class="active"{/if} href="/member/edit_profile.php?form_tab=3">Notifications</a>
                    </div>
                </div>
                <div class="line"></div>

                {* Edit profile *}   
                {if $form_tab eq(0)}
                <div class="count_p">Edit your name, age, profile picture, etc</div>

                <br clear="all" />

                <div class="content_tab">
                    {form EditProfileKuinkey}


                    <div class="row">
                        <div class="label_profile_field span2">First name: </div>
                        <div class="profile_edit_fields span5">
                            <input type="text" name="real_name" value="{$real_name}" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="label_profile_field span2">Last name: </div>
                        <div class="profile_edit_fields span5">
                            <input type="text" name="last_name" value="{$last_name}"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="label_profile_field span2">Username: </div>
                        <div class="profile_edit_fields span5">
                            <input type="text" name="username" value="{$username}" />
                        </div>
                    </div>

                    <div class="row">
                        <div class="label_profile_field span2">Email: </div>
                        <div class="profile_edit_fields span5">
                            <input type="text" name="email" value="{$email}" />
                        </div>
                    </div>

                    <div class="row">
                        <div class="label_profile_field span2">Age: </div>
                        <div class="profile_edit_fields span5">
                            <input type="text" name="age" style="width:40px;" value="{$age}" />
                        </div>
                    </div>

                    <div class="row">
                        <div class="label_profile_field span2">I am: </div>
                        <div class="profile_edit_fields span5">
                            <div class="profile_edit_radio row-fluid">
                                <div class="span4">
                                    Male <input {if $sex eq(1)}checked="checked"{/if} type="radio" value="1" name="sex">
                                </div>
                                <div class="span4">
                                    Female  <input {if $sex eq(2)}checked="checked"{/if} type="radio" value="2" name="sex">
                                </div>
                                <div class="span4">
                                    Trans  <input {if $sex eq(3)}checked="checked"{/if} type="radio" value="3" name="sex">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="label_profile_field span2">Country: </div>
                        <div class="profile_edit_fields span5">
                            <div>
                                <select name="location[сountry_id]">
                                    {foreach from=$select_location item=section}
                                    <option {if $location.country_id == $section.Country_str_code } selected="selected"{/if} value="{$section.Country_str_code}">{$section.Country_str_name}</option>
                                    {/foreach}
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="label_profile_field span2">Interested in: </div>
                        <div class="profile_edit_fields span5">
                            <div class="profile_edit_radio row-fluid">
                                <div class="span4">
                                    Broesing <input {if $interested_in eq(1)}checked="checked"{/if} type="radio" value="1" name="interested_in">
                                </div>
                                <div class="span4">
                                    Fanaticizing  <input {if $interested_in eq(2)}checked="checked"{/if} type="radio" value="2" name="interested_in">
                                </div>
                                <div class="span4">
                                    Perfoming  <input {if $interested_in eq(3)}checked="checked"{/if} type="radio" value="3" name="interested_in">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="label_profile_field span2">Willing to performer only: </div>
                        <div class="profile_edit_fields span5">
                            <div class="profile_edit_radio row-fluid">
                                <div class="span4">
                                    Stratight <input {if $match_sex eq(1)}checked="checked"{/if} type="radio" value="1" name="match_sex">
                                </div>
                                <div class="span4">
                                    Gay  <input {if $match_sex eq(2)}checked="checked"{/if} type="radio" value="2" name="match_sex">
                                </div>
                                <div class="span4">
                                    Trans  <input {if $match_sex eq(3)}checked="checked"{/if} type="radio" value="3" name="match_sex">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="label_profile_field span2">Perfomance tags</div>
                        <div class="profile_edit_fields span5">
                            <div class="profile_field_row">
                                <input type="text" name="tag"  id="form_tag" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="label_profile_field span2">About me</div>
                        <div class="profile_edit_fields span5">
                            <textarea style="max-width:313px" name="general_description">{$general_description}</textarea>
                        </div>
                    </div>
                        <input type="hidden" name="pass" id="pass" />
                    {/form}
                    {*foto*}
                    <div class="row">
                        <div class="label_profile_field span2">Profile picture</div>
                        <div class="profile_foto span5">
                            <div class="span2" style="width:110px;">
                                {profile_thumb profile_id=$profile_id  size=90}
                                <iframe id=hiddenframe name=hiddenframe style="overflow: hidden !important; width: 110px; border:0px; display: none;"></iframe>
                            </div>
                            <div class="span2" style="padding:10px; width: 145px;">
                                <div>
                                    <input type="hidden" value="{$profile_id}" id="photo_id" />
                                    <input type="button" value="delete" class="read_more" id="delete_foto" />
                                </div>
                                <div class="count_p">Change Profile Picture</div>
                                <form enctype=multipart/form-data action="ajax_user_update.php" method="post" id="loadavatar" target=hiddenframe>
                                    <input type="file" name="file" />
                                </form>
                                <div class="grey">JPG, GIF or PNG. Max size of 700K</div>
                                <input type="button" value="upload now" id="upload_now" /> <input type="button" value="cancel" class="read_more" id="cancel_foto"/>
                            </div>
                        </div>    
                    </div>
                    {**//////////////////////**}

                    
                    <div class="profile_password">		
                        <div class="line"></div>
                        <div class="form_block_user">To save these settings, please enter your kuinkey password</div>
                        <div class="form_block_user">
                            Password:   <input type="password" name="pass2" id="pass2" />
                        </div>
                        <div class="form_block_user">
                            <input type="button" value="save changes" onclick="SaveChange();" />
                            <input type="button" value="Cancel" class="read_more" />
                            <a class="profile_unregister" href="{document_url doc_key="profile_unregister"}">{text %.nav_doc_item.profile_unregister}</a>
                        </div>
                    </div>
                    <div style="height: 40px;"></div>
                </div>

                {/if}
                {if $form_tab eq(1)}{component ThisIsMe }{/if}


                {if $form_tab eq(2)}

                {*password*}
                <div class="content_tab">


                    {component ChangePassword}
                    <br clear="all">


                </div>

                {*if $fbcButton}
                <div class="fbc_synch_c">
                    {component $fbcButton}
                </div>
                {/if*}


                {/if}

                {if $form_tab eq(3)}
                <div class="count_p">Email me, when ...</div>

                <br clear="all" />
                <div class="content_tab">
                    {form Notifications}
                  <div class="profile_password">	  
                    <div class="row">
                        <div class="label_profile_notice">
                            You have a new follower 
                            <input type="checkbox" value="1" {if $new_follower eq(1)}checked="checked"{/if} name="new_follower" >
                        </div>
                    </div>
                    <div class="row">
                        <div class="label_profile_notice">
                            You have been invited to fantasize 
                            <input type="checkbox" value="1" {if $invited_to_fantasize eq(1)}checked="checked"{/if} name="invited_to_fantasize" >
                        </div>
                    </div>
                    <div class="row">
                        <div class="label_profile_notice">
                           Your contract has been updated
                            <input type="checkbox" value="1" {if $contract_updated eq(1)}checked="checked"{/if} name="contract_updated" >
                        </div>
                    </div>
                    <div class="row">
                        <div class="label_profile_notice">
                            You received a payment 
                            <input type="checkbox" value="1" {if $received_keys eq(1)}checked="checked"{/if} name="received_keys">
                        </div>
                    </div>
                    <div class="row">
                        <div class="label_profile_notice">
                            Your fanasy has been rated by User
                            <input type="checkbox" value="1" {if $you_rated eq(1)}checked="checked"{/if} name="you_rated">
                        </div>
                    </div>
                    <div class="row">
                        <div class="label_profile_notice">
                            Follower just posted a new fantasy
                            <input type="checkbox" value="1" {if $posted_fantasy eq(1)}checked="checked"{/if} name="posted_fantasy" >
                        </div>
                    </div>
                    <div class="row">
                        <div class="label_profile_notice">
                            I get a new message
                            <input type="checkbox" value="1" {if $new_message eq(1)}checked="checked"{/if} name="new_message">
                        </div>
                    </div>
                      <input type="hidden" id="pass" name="pass" />
                          
                      {/form}
                </div>
                <div class="profile_password">		
                    <div class="line"></div>
                    <div class="form_block_user">To save these settings, please enter your kuinkey password</div>
                    <div class="form_block_user">
                        Password:   <input type="password" name="pass2" id="pass2" />
                    </div>
                    <div class="form_block_user">
                        <input type="button" value="save changes" onclick="SaveChange();" />
                        <input type="button" value="Cancel" class="read_more" />
                        <a class="profile_unregister" href="{document_url doc_key="profile_unregister"}">{text %.nav_doc_item.profile_unregister}</a>
                    </div>
                </div>
                </div>
                <div style="height: 40px;"></div>
                {/if}



            </div> 
        </div>
    </div>
</div>

{*выводит теги*}
{literal}   
<script>

    var performer = {/literal}{$tag}{literal};
    // $("input:regex(id, ^form_\\d{1}-tag)").tokenInput(document.location.href, {
    $("#form_tag").tokenInput(document.location.href, {
        theme: "facebook",
        queryParam: 'q_p',
        prePopulate : performer
    });
    function SaveChange(){
        $('#pass').val($('#pass2').val());
        $('#form_1').submit();
         $('#form_2').submit();
          $('#form_3').submit();
           $('#form_4').submit();
    }
</script>     
{/literal}

{/container}
{/canvas}