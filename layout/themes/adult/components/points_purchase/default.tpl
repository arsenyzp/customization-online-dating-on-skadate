
{canvas}

    {container class="points_purchase" stylesheet="points_purchase.style"}
{component ProfileInfoLeft}

<div class="content_100">

    {component ProfileInfoHeader}

    <div class="content span7">	
        <div class="content_text">
            <!--Меню пользователя-->
            <div class="content_menu">
                <div class="content_menu_item {if $tabs eq(55)}activ_menu{/if}"><a href="/member/newsfeed.php">NEWS</a></div>
                <div class="content_menu_item  {if $tabs eq(1)}activ_menu{/if}"><a href="/member/myfantasy.php">MY FANTASIES</a></div>
                <div class="content_menu_item {if $tabs eq(11)} activ_menu{/if}"><a href="/member/view_works.php">MY LIBRARY</a></div>
                <div class="content_menu_item {if $tabs eq(77)} activ_menu{/if}"><a href="/member/sent_applications.php">MY JOBS</a></div>
                <div class="content_menu_item {if $tabs eq(44)}activ_menu{/if}"><a href="/member/view_review.php?mode=my">MY REVIEWS</a></div>
                <div class="content_menu_item {if $tabs eq(3)} activ_menu{/if}"><a href="/member/following.php?tabs=3">FOLLOWING</a></div>
                <div class="content_menu_item {if $tabs eq(22)} activ_menu{/if}"><a href="/member/bookmarks.php?tabs=22">FOLLOWERS</a></div>
                <div class="content_menu_item activ_menu" style="float: right; background-color: #412756; margin-right: 10px;"><a href="/member/edit_profile.php">MY ACCOUNT</a></div>
            </div>
            <div style="height: 10px"></div>
            <div class="content_menu">
                                    <div class="content_menu_item"><a href="/member/edit_profile.php">SETTINGS</a></div>
                                    <div class="content_menu_item activ_menu"><a href="/member/points_purchase.php">MANAGE KEYS</a></div>
                                    <div class="content_menu_item"><a href="/member/block_list.php">BLOCK LIST</a></div>
                                    <div class="content_menu_item"><a href="/member/payment_selection.php">KUINKEY+</a></div>
                                </div>

            <div class="content_tab">
                   
                     <div class="line"></div>
                <div class="profile_menu row">
                    <div class="span1">
                        <a {if $form_tab eq(1)}class="active"{/if} href="/member/my_credits.php">Summary</a>
                    </div>
                    <div class="span1">
                        <a {if $form_tab eq(0)}class="active"{/if} href="/member/points_purchase.php">Add Keys</a>
                    </div>
                    <div class="span2">
                        <a {if $form_tab eq(2)}class="active"{/if} href="/member/my_credits.php?tabs=1">Withdraw keys</a>
                    </div>
                </div>
                <div class="line"></div>
                <div class="right" style="line-height: 20px;"><img src="/layout/img/st.png" style="margin-right:5px;" />Secure Transaction</div>
                <div class="kuinkey_title left">
                   Keys are just like real money, except it is only for use on Kuikey
                </div>
                <div style="height: 20px;"></div>
            {form PointsPurchase}
                <div class="row">
                {foreach from=$packages item='p' name='pack'}
                    <div class="span package">
                           <div class="key_content">
                                  <div class="row">
                                         <div class="blue_keys span1">
                                         </div>
                                         <div class="keys span1">
                                                <div class="grey points">{$p.points}</div>
                                                <div class="kuinkey_title key_t">Keys</div>
                                                <div class="kuinkey_title pprice">
                                                 us$  {$p.price}
                                                 </div>
                                         </div>
                                  </div>
                                                 <div class="kuinkey_title"><span class="grey">5</span> bonus keys</div>
                                                 
                                                 <div class="key_buy">
                                                        <input type="button" value="buy now" class="key_buy_button" id="{$p.package_id}" />
                                                 </div>
                                                 {*
                                                 <div style="display: none;">
                                                        {if $smarty.foreach.pack.first}
                                                            {input_item name="package_id" value=$p.package_id checked="checked"}
                                                        {else}
                                                            {input_item name="package_id" value=$p.package_id }
                                                        {/if}  
                                                 </div>
                                                 *}
                                                
                        </div>
                    </div>
                {/foreach}
                 <div style="display:none;">{input_item name="package_id" value="0"  checked="checked"}</div>
                </div>
                <div class="providers" style="display:none;">
                {if $providers}
                    <span {id="provider_select"}>
                        {text %.components.payment_selection.pay_with}: {input name="provider_id" labelsection='components.payment_selection'}
                    </span>
                    <br />
                    {button action='purchase' class='checkout_btn'}
                    <div class="provider_logos">
                        {foreach from=$providers item='provider'}
                            {if $provider.icon}<img src="{$smarty.const.URL_USERFILES}{$provider.icon}" alt="{text %.components.payment_selection.`$provider.fin_payment_provider_id`}" />{/if}
                        {/foreach}
                    </div>
                {else}
                    {text %.components.payment_selection.no_providers}
                {/if}
                </div>
                
            {/form}
    
            <table width="100%">
            {foreach from=$services item='serv'}
                <tr class="{cycle values='tr_1,tr_2'}">
                    <td>{text %.membership.services.`$serv.membership_service_key`}</td>
                    <td class="scost"><b>{$serv.credits}</b> <span class="small">{text %.components.profile_status.credits}</span></td>
                </tr>
            {/foreach}
            </table>

            
        </div></div></div></div>
        
            {literal}
                   <script>
                            $('.key_buy_button').on('click', function(){
                                          $('input[name=package_id]').val($(this).attr('id'));
                                                 $('#form_1').submit();
                            });
                   </script>
            {/literal}
            
            
    {/container}

{/canvas}