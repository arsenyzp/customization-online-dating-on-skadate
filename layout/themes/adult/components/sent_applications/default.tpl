
{canvas}
{container stylesheet="sent_applications.style"}

{*
    <div class="main_menu">
        <ul class="menu-tabs">
            <li class="tab first"><a {if $tabs eq(0)} class="active"{/if} href="?tabs=0" ><span>Sent Aplication</span></a></li>
            <li class="tab"><a {if $tabs eq(3)} class="active"{/if} href="?tabs=3" ><span>Declined</span></a></li>
            <li class="tab last"><a {if $tabs eq(4)} class="active"{/if} href="?tabs=4" ><span>Interview</span></a></li>
            <li class="tab"><a {if $tabs eq(2)} class="active"{/if} href="?tabs=2" ><span>Contract</span></a></li>
            <li class="tab"><a {if $tabs eq(1)} class="active"{/if} href="?tabs=1" ><span>Completed contracts</span></a></li>     
        </ul>
    </div>
  *}      


{component ProfileInfoLeft}

<div class="content_100">

                        {component ProfileInfoHeader}
        
                <div class="content span7">
                    <div class="content_text">

                         <!--Меню пользователя-->
                                <div class="content_menu">
                                    <div class="content_menu_item {if $tabs eq(55)}activ_menu{/if}"><a href="/member/newsfeed.php">NEWS</a></div>
                                    <div class="content_menu_item  {if $tabs eq(1)}activ_menu{/if}"><a href="/member/myfantasy.php">MY FANTASIES</a></div>
                                    <div class="content_menu_item"><a href="/member/view_works.php">MY LIBRARY</a></div>
                                    <div class="content_menu_item  activ_menu"><a href="/member/sent_applications.php">MY JOBS</a></div>
                                    <div class="content_menu_item {if $tabs eq(44)}activ_menu{/if}"><a href="/member/view_review.php?mode=my">MY REVIEWS</a></div>
                                    <div class="content_menu_item {if $tabs eq(3)} activ_menu{/if}"><a href="/member/following.php?tabs=3">FOLLOWING</a></div>
                                    <div class="content_menu_item {if $tabs eq(22)} activ_menu{/if}"><a href="/member/bookmarks.php?tabs=22">FOLLOWERS</a></div>
                                    <div class="content_menu_item" style="float: right; background-color: #412756; margin-right: 10px;"><a href="/member/edit_profile.php">MY ACCOUNT</a></div>
                                </div>

                        <div class="content_search">
                                    <div class="input-append">
                                        <input class="span2" id="searc_input" size="16" type="text" placeholder="search">
                                        <span class="add-on"><img src="/layout/themes/adult/img/searchi.png"></span>
                                    </div>
                                </div>
                                <div class="content_tab" style="text-align: left;">
                                    <div class="count_p">{$paging.total} Jobs</div>
                                </div>
                                <div class="content_tab">
                                       
                                       <div class="line"></div>
                                       <div class="profile_menu row">
                                          <div class="span1">
                                              <a href="/member/sent_applications.php" {if $tabs eq(0)} class="active"{/if}>My requests</a>
                                          </div>
                                          <div class="span2">
                                                 <a href="/member/view_request.php?tabs=3" {if $tabs eq(3)} class="active"{/if}>Applications performers</a>
                                          </div>
                                      </div>
                                       <div class="line"></div>
                                       
            {foreach from=$list_fantasy item='fantasy'}
                
               <div id="div_{$fantasy.id_worck}"  {if $fantasy.status_update eq(1) and $fantasy.worck_status eq('0')}style="background: #BBFFFF"{/if}>
                  <div class="line"></div>
                    <div>
                     <div class="fantasy_list">
                          <span class="list_fantasy_title">  <a href="fantasy_view.php?edit={$fantasy.id_item}">{$fantasy.title}</a></span>
                          <span class="list_fantasy_price">
                          Budget:
                                                      {if $fantasy.budget eq '0'}
                                          Negotiable
                                      {else}
                                            {$fantasy.budget}$
                                      {/if}

                          </span>
                           
                                      <div class="status_worck">
                                        {if $fantasy.worck_status eq('0')}
                                           {* <a href="offer_details.php?worck_id={$fantasy.worck_id}">View offer details</a>*}
                                            <span class="sent">Sent</span>
                                        {elseif $fantasy.worck_status eq('1')}
                                            <span class="accept">Accepted</span>
                                        {elseif $fantasy.worck_status eq('2')}
                                            <span class="sent">Pending verification</span>
                                         {elseif $fantasy.worck_status eq('3')}
                                            <span class="accept">Successfully completed</span>
                                        {/if}
                                    </div>
                                      
                                      

                              <div style="clear:both"></div>
                     </div>
                      <div>
                              <div id="desc_{$fantasy.fantasy_id}" class="description_fantasy_text">
                               {$fantasy.description}
                              </div>
                              <div id="show_{$fantasy.id_item}"><a onclick="Show('{$fantasy.id_fantasy}'); return false;" href="#">View all</a></div>
                              <div style="display:none;" id="hide_1"><a onclick="Hide('{$fantasy.id_fantasy}'); return false;" href="#">Hide</a></div>
                       </div>
                    </div>

                       <div style="text-align: right;">
                            {if $fantasy.worck_status eq('0')}
                                <input type="button" onclick="CancelRequest({$fantasy.worck_id});" class="read_more" value="Cancel" />
                           <input type="button" onclick="EditRequest({$fantasy.worck_id});" value="Edit" />
                           {elseif $fantasy.worck_status eq('1')}
                               <input type="button" onclick="SentResult({$fantasy.id_fantasy});" value="Sent" />
                           {*<input type="button" onclick="AcceptRequest({$fantasy.worck_id});" value="Accept" />*}
                           {elseif $fantasy.worck_status eq('5')}
                                  Canceled
                           {/if}
                             <input class="read_more" type="button" value="Read More" onclick="ViewMoreFantasy({$fantasy.id_fantasy})" />
                       </div>
                </div>
             {/foreach}

<br clear="all" />
{paging total=$paging.total on_page=$paging.on_page pages=$paging.pages}

</div>
</div>
</div>
</div>

<div id="form_edit_contract" style="display: none">
       {form OrderDetails}
       <div class="padding10">
                    <input type="hidden" name="fantasy_id" id="fantasy_id"/>
                    <input type="hidden" name="worck_id" id="worck_id" /> 
                     <input type="hidden" name="lenght_video" id="lenght_video" /> 
                    
                    <div class="popup_form">
		Budget: <input type="text" name="budget" id="budget_contract"  style="width:47px; margin-left:5px;margin-bottom: 5px;">
                                            <br>
                                            <span style="margin-left:41px;" class="inf">Kuinkey receives 20%</span>
                     </div>
                     <div class="popup_form">
                                    Completion date: <input type="text" id="datapicker" name="completion_date"  style="background: url('/layout/img/strelka.png') no-repeat scroll 71px 10px transparent; margin-left:5px; width:70px;" >
                     </div>
                     <div class="line"></div>
                      <div class="popup_form">
                            <div class="row">
                                    <div style="line-height:30px; width: 120px;height: 30px;" class="span2">
                                            Public viewing price:
                                    </div>
                                    <div style="width: 120px;height: 30px;" class="span2">
                                            <input type="text" name="price_video" id="price_video" style="width:70px; margin-bottom: 10px;" />
                                    </div>
                            </div>
                            <div style="margin-bottom: 30px;" class="row">
                                    <div style="width: 120px" class="span2">
                                        <input type="text" value="500keys" style="width:70px;" id="performer_r" ><br>
                                            <span class="inf">Performer receives (50%)</span>
                                    </div>
                                    <div style="width: 120px" class="span2">
                                        <input type="text" value="500keys" style="width:70px;" id="user_r"><br>
                                            <span class="inf">Users receives (20%)</span>
                                    </div>
                                    <div style="width: 120px" class="span2">
                                        <input type="text" value="500keys" style="width:70px;" id="kuinkey_r" ><br>
                                            <span class="inf">Kuinkey receives (20%)</span>
                                    </div>
                            </div>
                    </div>
                     
                      <div class="popup_form">
                        <div class="left_cols_form">Messag to Creator</div>
                        <div class="right_cols_form"><textarea name="messag" style="max-width: 360px"></textarea></div>
                        <br clear="all" />
                    </div> 
                     <div class="popup_form">
                        <input type="submit" value="Continue" style="margin-right: 5px;" />   <input type="button" class="read_more" onclick="f" value="Cancel" />
                        <br clear="all" />
                    </div>   
       </div>
       {/form}
</div>

<div id="up_fantasy" style="display: none;">
    
        {form UploadFantasy}
        <input type="hidden" name="fantasy_id" id="fantasy_id_up" value="0" />
        <div class="row">
       <div class="pop_up_title span1" style="width: 145px;">UPLOAD FANTASY</div>
        <div class="span1">
               <div style="height: 20px;"></div>
        </div>
        <a id="up_fantasy_add_close"></a>
    </div>
    <div class="padding10">
        <div class="row popup_form">
            <div class="performer_list span1" style="margin-left: 0; width: 42px;">File: </div>
            <div class="span3" style="margin-top: 4px;">
                     {input name="fantasy"}
            </div>
        </div>
            <div class="row popup_form">
                <div class="span2" style="margin-left: 0;">Messag to creator:</div>
            </div>
            <div class="row popup_form">
                <textarea name="description"></textarea>
            </div>
            <div class="row popup_form">
                <input type="submit" value="Upload" />
                <input type="button" value="Cancel" class="read_more" id="up_fantasy_cancel" />
            </div>
    </div>
    {/form}
    
</div>





{literal}
    <script>
        //запрос на отзыв
            function Review(id){
                 var report_fl_box = new SK_FloatBox({
                    $title: 'Add review',
                    $contents: $('#invete'+id).children(),
                    width: 350 
                    });
               }
    </script>
{/literal}
{/container}
{/canvas}