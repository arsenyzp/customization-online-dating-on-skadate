{* component Comment Add Form *}

{container stylesheet='add_comment.style'}

{if $enanble_rate}
            
        {if !$mode}

        {if $err_message}
        	<div class="no_content">{$err_message}</div>
        {else}
               <div class="line"></div>
        {form CommentAdd entity_id=$entity_id feature=$feature entityType=$entityType}
        	{*text_formatter for="comment_text" entity="comment"*}<br />
        	<div style="display: none">{label %.comment.cap_add_comment_label for=comment_text}</div>
            {input name='comment_text'}<br />
            <div class="block_submit right">{*button action='add_comment'*}<input type="submit" value="post" /></div>
        {/form}
 		{/if}
        {/if}
        
      
  
        {if $mode}

        {if $err_message}
        	<div class="no_content">{$err_message}</div>
        {else}
        {form CommentAdd entity_id=$entity_id feature=$feature}
        	{text_formatter for="comment_text" entity="comment"}<br />
            <div style="display: none">{label %.comment.cap_add_comment_label for=comment_text}</div>
            {input name='comment_text'}<br />
            <div class="block_submit right">{*button action='add_comment'*}<input type="submit" value="Post" /></div>
        {/form}
        {/if}

        {/if}
  {/if}   
  
  
    <div {id='comment_list_cont'}>{component $comment_list}</div>
{/container}
