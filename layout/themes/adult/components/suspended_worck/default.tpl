
{canvas}
{container stylesheet="sent_applications.style"}

<div class="float_half_right narrower">
<input type="button" value="Create Fantasy" id="create_fantasy"  onclick="document.location = 'fantasy.php'" />
</div>
<br clear="all" />



{paging total=$paging.total on_page=$paging.on_page pages=$paging.pages}
{*capture name='list_title'}
	{if $list_type eq 'tags'}{text %tags_label} '{$tag_words}'{elseif $list_type eq 'profile'}{text %music_by} {$username}{else}{text %list.`$list_type`}{/if}
{/capture*}
    <div class="main_menu">
        <ul class="menu-tabs">
            <li class="tab first"><a {if $tabs eq(0)} class="active"{/if} href="?tabs=0" ><span>Sent Aplication</span></a></li>
            <li class="tab"><a {if $tabs eq(1)} class="active"{/if} href="?tabs=1" ><span>Completed contracts</span></a></li>
            <li class="tab"><a {if $tabs eq(2)} class="active"{/if} href="?tabs=2" ><span>Accept</span></a></li>
            <li class="tab last"><a {if $tabs eq(3)} class="active"{/if} href="?tabs=3" ><span>Declain</span></a></li>
        </ul>
    </div>
<div class="float_half_left wider">

{block title="Job Applications"}

<div class="block_body">

    <div class="block_body_r">
        <div class="block_body_c clearfix">
            {foreach from=$list_fantasy item='fantasy'}
                <ul>
                    <li class="list_item">
                        <h2>{$fantasy.title}</h2>
                        <div class="tag_list">
                            <ul >
                                    {foreach from=$fantasy.fantasy_tag item='tag'}
                                       
                                        <li class="item_tag"><p>{$tag.tag}</p></li>
                                       
                                    {/foreach}   
                                    </ul>
                               <br clear="all">     
                        </div>
                                    <div class="status">
                                        {if $fantasy.worck_status eq('0')}
                                            <span class="sent">Sent</span>
                                        {elseif $fantasy.worck_status eq('1')}
                                            <span class="accept">Accepted</span>
                                            <input type="button" value="Send fantasy" onclick="" />
                                        {elseif $fantasy.worck_status eq('-1')}
                                            <span class="declined">Declined</span>
                                        {elseif $fantasy.worck_status eq('2')}
                                            <span class="sent">Pending verification</span>
                                        {/if}
                                    </div>
                        <div class="description_fantasy">
                            {$fantasy.description}
                        </div>
                        <div class="informer_fantasy">
                            {if $fantasy.budget eq '0'}
                                Negotiable
                            {else}
                                  {$fantasy.budget}$
                            {/if}
                           
                            {if $user}
                                <input type="button" value="Delete" onclick="SendDelete({$fantasy.id});" style="float:right;" id="button_{$fantasy.id}" />
                            {/if}
                             <br clear="all">
                            <ul>
                                {foreach from=$fantasy.fantasy_file item='file' key='i'}
                                    <li><a href="/$userfiles/upload_fantasy/{$file.name}" title="{$file.name}">file{$i+1}</a></li>
                                {/foreach}   
                            </ul>
                        </div>
                        <br clear="all">
                        <div>
                            <ul>
                            {foreach from=$fantasy.performer item='performer'  key='k'}
                                <li class="item_performer">
                                    Performer {$k+1}:
                                    <ul class="item_ul">
                                    {foreach from=$performer.list_tag item='tags'}
                                       
                                        <li class="item_tag"><p>{$tags.tag}</p></li>
                                       
                                    {/foreach}   
                                    </ul>
                                </li>
                            {/foreach}
                            </ul>
                        </div>
                    </li>
                </ul>
             {/foreach}
         </div>
     </div>
</div>


<br clear="all" />
{paging total=$paging.total on_page=$paging.on_page pages=$paging.pages}

{/block}

</div>


{literal}
{/literal}
{/container}
{/canvas}