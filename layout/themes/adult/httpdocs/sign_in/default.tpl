{* Httpdoc Sign In component *}

{canvas centered}
	{include_style file="sign_in.style"}
    	{* <div class="sign_in_open"></div> *}
<div  class="content_100">
<div class="content_head row-fluid">
        <div class="span5">

<span class="menu_text" style="font-size:13px"></span>
        </div>
        <div class="span2">
            
<span class="menu_text" style="font-size:13px">
SIGN IN
</span>
</div>
        <div class="span4">

        </div>

        <div style="clear:both;"></div>
</div>
    
<div class="content_text">



<div class="sign_in_document clearfix">

                <div class="center_block">
                        {component SignIn}
                </div>

</div>
                <div class="text_info">Not a member yet?, no worries, <a href="/member/join.php">sign up now</a></div>
                 <div style="height: 100px"></div>
 </div>
                
</div>
                       
{/canvas}
