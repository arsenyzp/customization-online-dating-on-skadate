{if $style}
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xml:lang="{$language_tag}" lang="{$language_tag}" {if $themeDirection=='rtl'}dir="rtl"{/if} xmlns="http://www.w3.org/1999/xhtml" itemscope itemtype="http://schema.org/">
<head>
	<link rel="shortcut icon" href="{$smarty.const.SITE_URL}favicon.ico" type="image/x-icon">
	<link rel="icon" href="{$smarty.const.SITE_URL}favicon.ico" type="image/x-icon">
                <link href="/static/bootstrap.min.css" rel="stylesheet">
                    <link href="/font.css" rel="stylesheet">
                        {$html_head}
                        {$script}
</head>
        <body>
    {/if}
{$html_body}
{if $style}
	  </body>
</html>
         {/if}