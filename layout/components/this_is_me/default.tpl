
{canvas}
{container stylesheet="this_is_me.style"}
    <div class="count_p">Edit "This is me" part of your profile</div>

                <br clear="all" />

                <div class="content_tab">
                 
                    {if $list}
                           {form ThisIsMeEdit}
                       {foreach from=$list item='media'}
                           {if $media.photo_id}
                               <div class="photo">
                               <div class="left row">
                                   <div class="span3"> <img width="200" src="{$media.photo_url}" /></div>
                                   <div class="span4">
                                          <div><input name="title[]" type="text" value="{$media.name}" /></div>
                                       <div class="right">
                                              <textarea name="description[]">{$media.description}</textarea>
                                            <div class="grey">150 Characters left</div>
                                       </div>
                                   </div>
                               </div>
                                            <div class="close_pop" id="{$media.photo_id}" type="photo"></div>
                               <div class="line"></div>
                               </div>
                               <input type="hidden" name="type[]" value="photo" />
                               <input type="hidden" name="id_media[]" value="{$media.photo_id}" />
                           {/if}
                           {if $media.video_id}
                               {*component VideoPlayer video_file_url=$media.url*}
                               <div class="video">
                               <div class="left row">
                                   <div class="span3"> 
                                        <div class="video_thumb">
                                        <a href="{$media.video_page}"><img src="{$media.thumb_img}" class="video_thumb" align="left" /></a>
                                        </div>
                                   </div>
                                   <div class="span4">
                                       <div><input name="title[]" type="text"  type="text" value="{$media.title}" /></div>
                                       <div class="right">
                                           <textarea name="description[]">{$media.description}</textarea>
                                           <div class="grey">250 Characters left</div>
                                       </div>
                                   </div>
                               </div>
                                           <div class="close_pop" id="{$media.video_id}"  type="video"></div>
                               <div class="line"></div>
                               </div>
                               <input type="hidden" name="type[]" value="video" />
                               <input type="hidden" name="id_media[]" value="{$media.video_id}" />
                           {/if}
                       {/foreach}
                       <input type="hidden" name="password" id="pass" />
                       <input type="hidden" name="profile_id" id="profile_id" value="{$profile_id}" />
                       {/form}
                    {/if}
                <br clear="all" />
                    {paging total=$paging.total on_page=$paging.on_page pages=$paging.pages}
                </div>
                   <div class="profile_password">		
                    <div class="line"></div>
                    <div class="form_block_user">To save these settings, please enter your kuinkey password</div>
                    <div class="form_block_user">
                        Password:   <input type="password" name="pass2" id="pass2" />
                    </div>
                    <div class="form_block_user">
                        <input type="button" value="save changes" onclick="SaveChange();" />
                        <input type="button" value="Cancel" class="read_more" />
                      
                    </div>
                    <div style="height: 30px;"></div>
                </div>
        {literal}
        <script>
            $(document).ready(function(){
                $('.video_thumb').css('width', '230px');
            });
        </script>
        {/literal}
{/container}
{/canvas}