
{canvas}
{container stylesheet="profile_reviews.style"}

<div style="float: left;">

</div>
<br clear="all" />

{paging total=$paging.total on_page=$paging.on_page pages=$paging.pages}
    <div class="main_menu">
        <ul class="menu-tabs">
            <li class="tab first"><a {if $tabs eq(0)} class="active"{/if} href="?tabs=0" ><span>Reviews by me</span></a></li>
            <li class="tab"><a {if $tabs eq(1)} class="active"{/if} href="?tabs=1" ><span>Reviews of me</span></a></li>  
        </ul>
    </div>
<div class="float_half_left wider">

{block title="Reviews"}


<div class="block_body">
    <div class="block_body_r">
        <div class="block_body_c clearfix">
            {foreach from=$list_reviews item='review'}
                <ul>
                    <li class="list_item">
                        <div>{$review.username}</div>
                        <div>{$review.review}</div>
                    </li>
                </ul>
             {/foreach}
         </div>
     </div>
</div>


<br clear="all" />
{paging total=$paging.total on_page=$paging.on_page pages=$paging.pages}

{/block}

</div>


{literal}
{/literal}
{/container}
{/canvas}