{* component TopVideo *}
{container stylesheet="top_video.style"}
{if $video_id}
<div class="top_video">
        <div class="padding10">
                <div class="row">
                    <div class="top_video_player span6">
                            {if $video_info.video_source eq 'file'}
                                    {component VideoPlayer video_file_url=$video_info.video_url width=455 height=300 style="color: red"}
                            {else}
                                    {$video_info.code}
                            {/if}
                        </div>
                                <div class="video_desc_top span3">
                                        <div id="video_desc_text">
                                                <span class="video_title">{$video_info.title|smile|censor:"video":true|out_format}</span><br>
                                               {$video_info.description|out_format:"comment"|smile|censor:"video":true}
                                        </div>
                                                <div>
                                                         {foreach from=$list_tag item=tag}
                                                            <div class="tag_item"><a href="?tag={$tag.tag_id}">{$tag.tag}</a></div>
                                                        {/foreach}      
                                                        <div style="clear:both;"></div>
                                                </div>
                                            <div class="top_video_stat">                
                                            <div class="line"></div>               
                                            <div id="video_reit">
                                                   {component $VideoRate}
                                            </div>
                                            <div class="line"></div> 
                                            <div style="color: #7dc5e5; height: 17px;" class="row-fluid">
                                                    <div class="video_like span2">{$video_like}</div>
                                                    <div class="video_view span2">{$video_views}</div>
                                                    <div class="video_comment span3">{$video_comments_count}</div>
                                            </div>
                                            <div class="line"></div> 
                                        </div>
                                </div>
                </div>
        </div>
</div>
                                            <div class="content_tab">
                                                <div class="line"></div>
                                            </div>
                                                {/if}
{/container}