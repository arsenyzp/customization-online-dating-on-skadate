{canvas index}

<div id="desc_prof_creator">
        <div class="row">
                <div style="font-famaly: ProximaNova-Semibold;" class="name span3">
                    {$title}
                    |  Budget:
                     {if $budget eq '0'}
                        Negotiable
                    {else}
                          {$budget}$
                    {/if}
                </div>
                <div class="grey span2">Posted:  {$creator_info.date_create|date_format}</div>
        </div>
        <div class="row">
                <div class="span1" style="margin: 0;">
                                {profile_thumb profile_id=$creator_info.profile_id redirect_params=$creator_info.url_params size=49}
                </div>
                <div class="span3" style="margin-left:5px">
                        <div style="font-size: 11px; line-height: 22px; margin-top: -5px;" class="grey">Invited by</div>
                        <div style="font-famaly: ProximaNova-Semibold;" class="performer_invite_name">
                            {$creator_info.username} | {$creator_info.old} years old | {$creator_info.sex_label}
                        </div>
                        <div class="list_profile_location">
                                  {if $creator_info.location.city}
                                            {$creator_info.location.city},
                                        {elseif !empty($creator_info.location.custom_location)}
                                             {$creator_info.location.custom_location},
                                        {/if}

                                        {if $creator_info.location.state}
                                            {$creator_info.location.state},
                                        {/if}

                                        {if $creator_info.location.country}
                                            {$creator_info.location.country}.
                                        {/if}
                        </div>
                </div>
        </div> 
</div>
<div id="form_create" class="padding10">
   <div class="performer_invite_name">
                            Description:
     </div>               
     <div style="font-size: 11px;" class="grey">
            {$description}
     </div>  
     <div class="performer_invite_name">
                            Tags:
     </div>               
     <div>
                {foreach from=$fantasy_tag item='tag'}
                          {$tag.tag}     
                          {if not  $smarty.foreach.tag.last},{/if}
                {/foreach}  
     </div>
      <div class="performer_invite_name">
                            Fantasy performers:
     </div>
     <div style="font-size: 11px;" class="grey">
            Here you describe
     </div>  
     <div class="performer_invite_name">
                 {foreach from=$performers item='performer'  key='k'}
                     <div class="line"></div>
                        Performer {$k+1}:
                      
                        {foreach from=$performer.list_tag item='tags'}                       
                       {$tags.tag} 
                        {if not  $smarty.foreach.tags.last},{/if}
                        {/foreach}   

                {/foreach}
     </div>
     {if $files}
        <div class="performer_invite_name">
            Inspiration Media
        </div>
        <div class="performer_invite_name">

                     {foreach from=$files item='file' key='i'}

                                        <a href="/userfiles/upload_fantasy/{$file.name}" title="{$file.name}">
                                            file{$i+1}
                                        </a> 

                                {/foreach}
        </div>
     {/if}

     <div>
                            {if $user}
                                {if $my_fantasy}
                                   {* <input type="button" value="Edit" onclick="document.location = 'fantasy.php?edit={$id}'" style="float:right;" />*}
                                {elseif !$send}
                                    {*<input type="button" value="Delete" onclick="SendDelete({$id});" style="float:right;" id="button_{$id}" />*}
                                {else}
                                    <input type="button" value="cancel" class="read_more" onclick="cl_p();" style="float:right; margin-left: 5px;" />    
                                    <input type="button" value="see contract details" onclick="SendRequest({$id});" style="float:right;" id="button_{$id}" />    
                                    
                                {/if}
                            {/if}
     </div>
     
                        {*  Expiration Date: {$data_ex}*}
                        <div style="height: 10px;"></div>
</div>
                      

{/container}
{/canvas}