
{canvas}
{container stylesheet="offer_details.style"}

<div class="float_half_left wider">

{block title="Offer Details"}

<div class="block_body">
    <div class="block_body_r">
        <div class="block_body_c clearfix">
  
            <table  id="performer_list">
                <tr>
                    <td>
                       <h2>{$title|out_format|smile|censor:"video":true}</h2>
                    </td>
                </tr>
                <tr>
                    <td> 
                       <div class="tag_lis">
                            <ul >
                                    {foreach from=$fantasy_tag item='tag'}
                                       
                                        <li class="item_tag"><p>{$tag.tag}</p></li>
                                       
                                    {/foreach}   
                                    </ul>
                                    
                                {if $user}
                                {if $my_fantasy}
                                    <input type="button" value="Edit" onclick="document.location = 'fantasy.php?edit={$id}'" style="float:right;" />
                                {elseif !$send}
                                  {*  <input type="button" value="Delete" onclick="SendDelete({$id});" style="float:right;" id="button_{$id}" />*}
                                {else}
                                    {*<input type="button" value="Send" onclick="SendRequest({$id});" style="float:right;" id="button_{$id}" />    *}
                                {/if}
                            {/if}
                                    
                               <br clear="all">     
                        </div>
                    </td>
                </tr>
                 <tr>
                     <td >{$description|out_format|smile|censor:"video":true}</td>
                </tr>
                <tr>
                    <td>
                        <div>
                            <ul>
                            {foreach from=$performers item='performer'  key='k'}
                                <li class="item_performer">
                                    Performer {$k+1}:
                                    <ul class="item_ul">
                                    {foreach from=$performer.list_tag item='tags'}                       
                                        <li class="item_tag"><p>{$tags.tag}</p></li>
                                    {/foreach}   
                                    </ul>
                                </li>
                            {/foreach}
                            </ul>
                        </div>
                     </td>
                </tr>
                <tr>
                    <td>Creator:</td>
                    <td><a href="/member/profile.php?profile_id={$creator_info.profile_id}">{$creator_info.username}</a></td>
                </tr>
                <tr>
                    <td>
                    {if $budget eq '0'}
                                Negotiable
                            {else}
                                  {$budget}$
                            {/if}
                     </td>
                </tr>
                <tr>
                     <td>
                         <ul>
                             {foreach from=$files item='file' key='i'}
                                 <li>
                                     <a href="/$userfiles/upload_fantasy/{$file.name}" title="{$file.name}">
                                         file{$i+1}
                                     </a> 
                                 </li>
                             {/foreach}
                        </ul>
                     </td>
                </tr>
                <tr>
                    <td>
                         Expiration Date: {$data_ex}
                    </td>
                </tr>
            </table>
                    <br clear="all" />
                    <div class="rows_form">
                        <div class="left_cols_form">Price proposed by performer</div>
                        <div class="right_cols_form">
                            {$performer_budget}
                        </div>  
                        <br clear="all" />
                    </div>
                   <br clear="all" />
          <ul>
              {foreach from=$worck_messag item='messag'}
                  <li class="list_item">
                      <h2>{$messag.subject}</h2>
                          {$messag.text}
                  </li>   
              {/foreach}
          </ul>
         {if $form_enable}                  
        {form OrderDetails}
        <input type="hidden" name="fantasy_id" value="{$fantasy_id}" />
        <input type="hidden" name="worck_id" value="{$worck_id}" />
                    <div class="rows_form">
                        <div class="left_cols_form">Performer</div>
                        <div class="right_cols_form"></div>
                        <br clear="all" />
                    </div>  
                    <div class="rows_form">
                        <div class="left_cols_form">Price offer:</div>
                        <div class="right_cols_form"><input type="text" value="{if $performer_budget neq(0)}{$performer_budget}{else}{$budget}{/if}" name="budget" id="budget" /></div>
                        <br clear="all" />
                        <div class="left_cols_form">Kuinkey.com fee:</div>
                        <div class="right_cols_form">{$kuinkey_fee}</div>
                        <br clear="all" />
                        <div class="left_cols_form">Paid to Performer:</div>
                        <div class="right_cols_form">{$paid_to_performer}</div>
                        <br clear="all" />
                    </div>
                     <div class="rows_form">
                        <div class="left_cols_form">Price video view:</div>
                        <div class="right_cols_form">
                            <input type="text" value="{$performer_price_video}" name="price_video" id="price_video" />
                        </div>
                        <br clear="all" />
                    </div>
                    <div class="rows_form">
                        <div class="left_cols_form">Lenght video:</div>
                      <div class="right_cols_form"><input type="text" value="{$lenght_video_per}" name="lenght_video" /> min</div>
                        <br clear="all" />
                    </div>
                    <div class="rows_form">
                        <div class="left_cols_form">Completion Date</div>
                        <div class="right_cols_form"><input type="text" name="completion_date" id="datapicker" value="{$worck_info.completion_date}" /></div>
                        <br clear="all" />
                    </div>
                    <div class="rows_form">
                        <div class="left_cols_form">Messag to Creator</div>
                        <div class="right_cols_form"><textarea name="messag"></textarea></div>
                        <br clear="all" />
                    </div> 
                    <div class="rows_form">
                        <input type="submit" value="Resubmit" />
                        {*Cancel*}
                        <input type="button" value="Cancel" onclick="CanelRequest({$worck_id});" style="float:right;" id="button_confirm_{$worck_id}" />
                        <br clear="all" />
                    </div>   
                        
                        {/form}
     {/if}
        </div>
    </div>
</div>
     
                        {/block}

</div>
{/container}
{/canvas}