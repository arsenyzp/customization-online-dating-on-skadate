
{canvas}
{container stylesheet="sent_applications.style"}

<div class="float_half_right narrower">
<input type="button" value="Create Fantasy" id="create_fantasy"  onclick="document.location = 'fantasy.php'" />
</div>
<br clear="all" />



{paging total=$paging.total on_page=$paging.on_page pages=$paging.pages}
{*capture name='list_title'}
	{if $list_type eq 'tags'}{text %tags_label} '{$tag_words}'{elseif $list_type eq 'profile'}{text %music_by} {$username}{else}{text %list.`$list_type`}{/if}
{/capture*}
    <div class="main_menu">
        <ul class="menu-tabs">
            <li class="tab first"><a {if $tabs eq(0)} class="active"{/if} href="?tabs=0" ><span>Sent Aplication</span></a></li>
            <li class="tab"><a {if $tabs eq(3)} class="active"{/if} href="?tabs=3" ><span>Declined</span></a></li>
            <li class="tab last"><a {if $tabs eq(4)} class="active"{/if} href="?tabs=4" ><span>Interview</span></a></li>
            <li class="tab"><a {if $tabs eq(2)} class="active"{/if} href="?tabs=2" ><span>Contract</span></a></li>
            <li class="tab"><a {if $tabs eq(1)} class="active"{/if} href="?tabs=1" ><span>Completed contracts</span></a></li>     
        </ul>
    </div>
<div class="float_half_left wider">

{block title="Job Applications"}

<div class="block_body">

    <div class="block_body_r">
        <div class="block_body_c clearfix">
            {foreach from=$list_fantasy item='fantasy'}
                <ul>
                    <li class="list_item">
                        <h2>{$fantasy.title}</h2>
                        <div class="tag_list">
                            <ul >
                                    {foreach from=$fantasy.fantasy_tag item='tag'}
                                       
                                        <li class="item_tag"><p>{$tag.tag}</p></li>
                                       
                                    {/foreach}   
                                    </ul>
                               <br clear="all">     
                        </div>
                                    <div class="status">
                                        {if $fantasy.worck_status eq('0')}
                                            <a href="offer_details.php?worck_id={$fantasy.worck_id}">View offer details</a>
                                            <span class="sent">Sent</span>
                                        {elseif $fantasy.worck_status eq('1')}
                                            <span class="accept">Accepted</span>
                                            <input type="button" value="Send fantasy" onclick="document.location = 'upload_fantasy.php?id_fantasy={$fantasy.id}'" />
                                        {elseif $fantasy.worck_status eq('-1')}
                                            <span class="declined">Declined</span>
                                        {elseif $fantasy.worck_status eq('2')}
                                            <span class="sent">Pending verification</span>
                                         {elseif $fantasy.worck_status eq('3')}
                                            <span class="accept">Successfully completed</span>
                                            {if $fantasy.review}
                                                <input type="button" value="Send review" onclick="Review({$fantasy.id})" />
                                                <div id="invete{$fantasy.id}" class="invete">
                                                    {form Reviews}
                                                         <textarea id="text_review"  name="text_review"></textarea>
                                                         <input type="hidden" name="reporter_id" value="{$user}" />
                                                         <input type="hidden" name="entity_id" value="{$fantasy.id_creator}" />
                                                         <input type="hidden" name="fantasy_id" value="{$fantasy.id}" />
                                                         <input type="hidden" name="type" value="1" />
                                                         <input type="submit" value="Add review" />
                                                         <input type="button" value="Cancel"  class="close_btn"/>
                                                     {/form}
                                                 </div>
                                             {/if}
                                        {/if}
                                    </div>
                        <div class="description_fantasy">
                            <div class="description_fantasy_text" id="desc_{$fantasy.id}">
                                 {$fantasy.description}
                            </div>
                            <div id="show_{$fantasy.id}"><a href="#" onclick="Show('{$fantasy.id}'); return false;">View all</a></div>
                            <div  id="hide_{$fantasy.id}" style="display:none;"><a href="#" onclick="Hide('{$fantasy.id}'); return false;">Hide</a></div>
                        </div>
                           
                        <div class="informer_fantasy">
                            {if $fantasy.budget eq '0'}
                                Negotiable
                            {else}
                                  {$fantasy.budget}$
                            {/if}
                             <br clear="all">
                            <ul>
                                {foreach from=$fantasy.fantasy_file item='file' key='i'}
                                    <li><a href="/$userfiles/upload_fantasy/{$file.name}" title="{$file.name}">file{$i+1}</a></li>
                                {/foreach}   
                            </ul>
                        </div>
                        <br clear="all">
                        <div>
                            <ul>
                            {foreach from=$fantasy.performer item='performer'  key='k'}
                                <li class="item_performer">
                                    Performer {$k+1}:
                                    <ul class="item_ul">
                                    {foreach from=$performer.list_tag item='tags'}
                                       
                                        <li class="item_tag"><p>{$tags.tag}</p></li>
                                       
                                    {/foreach}   
                                    </ul>
                                </li>
                            {/foreach}
                            </ul>
                        </div>
                    </li>
                </ul>
             {/foreach}
         </div>
     </div>
</div>


<br clear="all" />
{paging total=$paging.total on_page=$paging.on_page pages=$paging.pages}

{/block}

</div>


{literal}
    <script>
        //запрос на отзыв
            function Review(id){
                 var report_fl_box = new SK_FloatBox({
                    $title: 'Add review',
                    $contents: $('#invete'+id).children(),
                    width: 350 
                    });
               }
    </script>
{/literal}
{/container}
{/canvas}