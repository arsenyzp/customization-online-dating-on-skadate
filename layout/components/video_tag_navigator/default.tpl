{* component Entity Item Tag Navigator *}

{container stylesheet="style.style"}
{block title="Tag"}
	{if $no_tags}
	   {$no_tags}
	{else}
		{foreach from=$tags item='tag'}
	    	<a href="{$tag.url}">{$tag.tag}</a>&nbsp;&nbsp;
	    {/foreach}
    {/if}
{/block}
{/container}
