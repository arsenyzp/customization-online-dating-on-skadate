
{canvas}
{container stylesheet="fantasy_list.style"}


<div class="float_half_right narrower">
<form method="GET" id="filter_list">
<input type="button" value="Create Fantasy" id="create_fantasy" onclick="document.location = 'fantasy.php'" />
    <select name="cat_id" onchange="$('#filter_list').submit();">
    {foreach from=$catArray item=option}
        <option value="{$option.id}">{$option.label}</option>
    {/foreach}
    </select>
</form>
</div>
<br clear="all" />

    <div class="main_menu">
        <ul class="menu-tabs">
            <li class="tab first"><a {if $tabs eq(0)} class="active"{/if} href="?tabs=0" ><span>My fantasy</span></a></li>
            <li class="tab"><a {if $tabs eq(1)} class="active"{/if} href="view_works.php??tabs=1" ><span>My fantasy Video</span></a></li>
            <li class="tab last"><a {if $tabs eq(3)} class="active"{/if} href="view_request.php?tabs=3" ><span>Performer request</span></a></li>
            <li class="tab last"><a {if $tabs eq(3)} class="active"{/if} href="view_request.php?tabs=4" ><span>Update contract</span></a></li>
            <li class="tab last"><a {if $tabs eq(3)} class="active"{/if} href="view_review.php" ><span>My reviews</span></a></li>
        </ul>
    </div>

{paging total=$paging.total on_page=$paging.on_page pages=$paging.pages}


<div class="float_half_left wider">

{block title="My Fantasy"}

     


<div class="block_body">
    <div class="block_body_r">
        <div class="block_body_c clearfix">
            {foreach from=$list_fantasy item='fantasy'}
                <ul>
                    <li class="list_item">
                        <h2>{$fantasy.title}</h2>
                        <div class="tag_lis">
                            <ul >
                                    {foreach from=$fantasy.fantasy_tag item='tag'}
                                       
                                        <li class="item_tag"><p><a href="?tag={$tag.tag}">{$tag.tag}</a></p></li>
                                       
                                    {/foreach}   
                                    </ul>
                               <br clear="all">     
                        </div>
                         <div class="description_fantasy">
                            <div class="description_fantasy_text" id="desc_{$fantasy.id_item}">
                            {$fantasy.description}
                            </div>
                            <div id="show_{$fantasy.id_item}"><a href="#" onclick="Show('{$fantasy.id_item}'); return false;">View all</a></div>
                            <div  id="hide_{$fantasy.id_item}" style="display:none;"><a href="#" onclick="Hide('{$fantasy.id_item}'); return false;">Hide</a></div>
                        </div>
                        <div class="informer_fantasy">
                            {if $fantasy.budget eq '0'}
                                Negotiable
                            {else}
                                  {$fantasy.budget}$
                            {/if}
                           
                            {if $user}
                                <input type="button" value="Edit" onclick="document.location = 'fantasy.php?edit={$fantasy.id_item}'" style="float:right;" />
                            {/if}
                             <br clear="all">
                            <ul>
                                {foreach from=$fantasy.fantasy_file item='file' key='i'}
                                    <li><a href="/$userfiles/upload_fantasy/{$file.name}" title="{$file.name}">file{$i+1}</a></li>
                                {/foreach}   
                            </ul>
                        </div>
                        <br clear="all">
                        <div>
                            <ul>
                            {foreach from=$fantasy.performer item='performer'  key='k'}
                                <li class="item_performer">
                                    Performer {$k+1}:
                                    <ul class="item_ul">
                                    {foreach from=$performer.list_tag item='tags'}
                                       
                                        <li class="item_tag"><p>{$tags.tag}</p></li>
                                       
                                    {/foreach}   
                                    </ul>
                                </li>
                            {/foreach}
                            </ul>
                        </div>
                    </li>
                </ul>
             {/foreach}
         </div>
     </div>
</div>


<br clear="all" />
{paging total=$paging.total on_page=$paging.on_page pages=$paging.pages}

{/block}

</div>


{literal}
{/literal}
{/container}
{/canvas}