
{canvas}
{container stylesheet="fantasy_list.style"}

<div style="float: left;">
{*<input type="button" value="Create Fantasy" id="create_fantasy"  onclick="document.location = 'fantasy.php'" />*}
<form method="GET" id="filter_list">
    <input type="text" name="search" id="search_fantasy"  /><input type="submit" value="Search" />
    <br>
    <select name="cat_id" onchange="$('#filter_list').submit();">
    {foreach from=$catArray item=option}
        <option value="{$option.id}">{$option.label}</option>
    {/foreach}
    </select>
</form>
</div>
<br clear="all" />

  <div class="main_menu">
        <ul class="menu-tabs">
            <li class="tab first"><a {if $tabs eq(0)} class="active"{/if} href="?tabs=0" ><span>Preference</span></a></li>
            <li class="tab"><a {if $tabs eq(1)} class="active"{/if} href="?tabs=1" ><span>New</span></a></li>
            <li class="tab last"><a {if $tabs eq(2)} class="active"{/if} href="?tabs=2" ><span>Top price</span></a></li>
        </ul>
    </div>

{paging total=$paging.total on_page=$paging.on_page pages=$paging.pages}

<div class="float_half_left wider">

{block title=%.fantasy.list_title}


<div class="block_body">
    <div class="block_body_r">
        <div class="block_body_c clearfix">
            {foreach from=$list_fantasy item='fantasy'}
                <ul>
                    <li class="list_item">
                        <a href="fantasy_view.php?edit={$fantasy.id_item}"><h2>{$fantasy.title}</h2></a>
                        <div class="tag_lis">
                            <ul >
                                    {foreach from=$fantasy.fantasy_tag item='tag'}
                                       
                                        <li class="item_tag"><p><a href="?tag={$tag.tag}">{$tag.tag}</a></p></li>
                                       
                                    {/foreach}   
                                    </ul>
                               <br clear="all">     
                        </div>
                        <div class="description_fantasy">
                            <div class="description_fantasy_text" id="desc_{$fantasy.id_item}">
                            {$fantasy.description}
                            </div>
                            <div id="show_{$fantasy.id_item}"><a href="#" onclick="Show('{$fantasy.id_item}'); return false;">View all</a></div>
                            <div  id="hide_{$fantasy.id_item}" style="display:none;"><a href="#" onclick="Hide('{$fantasy.id_item}'); return false;">Hide</a></div>
                        </div>
                         
                        <div class="informer_fantasy">
                            {if $fantasy.budget eq '0'}
                                Negotiable
                            {else}
                                  {$fantasy.budget}$
                            {/if}
                           
                            {if $user}
                                {if $fantasy.my_fantasy}
                                    <input type="button" value="Edit" onclick="document.location = 'fantasy.php?edit={$fantasy.id_item}'" style="float:right;" />
                                {elseif !$fantasy.send}
                                    {*<input type="button" value="Delete" onclick="SendDelete({$fantasy.id_item});" style="float:right;" id="button_{$fantasy.id_item}" />*}
                                     <div class="status">
                                        {if $fantasy.status eq('0')}
                                            <span class="sent">Sent</span>
                                        {elseif $fantasy.status eq('1')}
                                            <span class="accept">Accepted</span>
                                            {*<input type="button" value="Send fantasy" onclick="document.location = 'upload_fantasy.php?id_fantasy={$fantasy.id}'" />*}
                                        {elseif $fantasy.status eq('-1')}
                                            <span class="declined">Declined</span>
                                        {elseif $fantasy.status eq('2')}
                                            <span class="sent">Pending verification</span>
                                        {/if}
                                    </div>
                                {else}
                                    <input type="button" value="Apply contract" onclick="SendRequest({$fantasy.id_item});" style="float:right;" id="button_{$fantasy.id_item}" />    
                                {/if}
                            {/if}
                             <br clear="all">
                            <ul>
                                {foreach from=$fantasy.fantasy_file item='file' key='i'}
                                    <li><a href="/$userfiles/upload_fantasy/{$file.name}" title="{$file.name}">file{$i+1}</a></li>
                                {/foreach}   
                            </ul>
                        </div>
                        <br clear="all">
                        <div>
                            <ul>
                            {foreach from=$fantasy.performer item='performer'  key='k'}
                                <li class="item_performer">
                                    Performer {$k+1}:
                                    <ul class="item_ul">
                                    {foreach from=$performer.list_tag item='tags'}
                                       
                                        <li class="item_tag"><p>{$tags.tag}</p></li>
                                       
                                    {/foreach}   
                                    </ul>
                                </li>
                            {/foreach}
                            </ul>
                        </div>
                    </li>
                </ul>
             {/foreach}
         </div>
     </div>
</div>


<br clear="all" />
{paging total=$paging.total on_page=$paging.on_page pages=$paging.pages}

{/block}

</div>


{literal}
{/literal}
{/container}
{/canvas}