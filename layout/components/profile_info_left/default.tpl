{* component ProfileInfoLeft *}
{if $profile.id}	
<div class="left_profil span2">
   {profile_thumb profile_id=$profile.id href="/member/edit_profile.php" size=155 sexLine=false}

    <div class="profile_info">
        <div><span class="profile_title">{$profile.username|truncate:18:"...":true toolbar=$smarty.capture.toolbar}</span></div>
        <div><a href="/member/edit_profile.php">edit profile</a></div>
    </div>
    <div style="background: url('/layout/img/222526.png') #333333; padding-bottom: 2px; padding-top: 2px;">
        <div class="profile_info">
            <p>
                Peope Morgendorfer <br>
                {$age} years old <br>
                {$profile_info.sex_label} <br>
            </p>
            <div class="padding2">
                <div class="map_location"></div>
                {if $profile_info.location.city}
                    {$profile_info.location.city},
                {elseif !empty($profile_info.location.custom_location)}
                     {$profile_info.location.custom_location},
                {/if}

                {if $profile_info.location.state}
                    {$profile_info.location.state},
                {/if}

                {if $profile_info.location.country}
                    {$profile_info.location.country}.
                {/if}
            </div>
            <div style="margin-top:5px; margin-bottom:5px;" class="padding2">
                <span class="profile_title2">About me</span>	<br>
              {$profile_info.general_description}
            </div>

            <div class="padding2">
                <span class="profile_r">my rating</span>
                {*<div id="prof_reit"></div>*}
                  {component $total_rate}
                <div style="clear:both;"></div>
            </div>
            <div style="height:10px;"></div>
            <div class="padding2 row">
                <div id="cam">
                   {$video}
                </div>
                <div id="smile">
                    {$follow}
                </div>
                <div id="glass">
                    {$views}
                </div>
            </div>
        </div>
    </div>
</div>
{/if}
