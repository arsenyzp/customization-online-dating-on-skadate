
{canvas}
{container stylesheet="notice.style"}

<div style="float: left;">

</div>
<br clear="all" />

{paging total=$paging.total on_page=$paging.on_page pages=$paging.pages}

<div class="float_half_left wider">

{block title="Notice"}


<div class="block_body">
    <div class="block_body_r">
        <div class="block_body_c clearfix">
            {foreach from=$list_notice item='notice'}
                <ul>
                    <li class="list_item">
                        <div>{$notice.notice_type}</div>
                        <div>{$notice.notice_description}</div>
                    </li>
                </ul>
             {/foreach}
         </div>
     </div>
</div>


<br clear="all" />
{paging total=$paging.total on_page=$paging.on_page pages=$paging.pages}

{/block}

</div>


{literal}
{/literal}
{/container}
{/canvas}