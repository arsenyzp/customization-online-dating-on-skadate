
{canvas}
{container stylesheet="view_request.style"}

<div class="float_half_right narrower">
<input type="button" value="Create Fantasy" id="create_fantasy"  onclick="document.location = 'fantasy.php'" />
</div>
<br clear="all" />

  <div class="main_menu">
        <ul class="menu-tabs">
            <li class="tab first"><a {if $tabs eq(0)} class="active"{/if} href="myfantasy.php?tabs=0" ><span>My fantasy</span></a></li>
            <li class="tab"><a {if $tabs eq(1)} class="active"{/if} href="myfantasy.php?tabs=1" ><span>My fantasy Video</span></a></li>
            <li class="tab last"><a {if $tabs eq(3)} class="active"{/if} href="myfantasy.php?tabs=3" ><span>Performer request</span></a></li>
            <li class="tab last"><a {if $tabs eq(4)} class="active"{/if} href="view_request.php?tabs=4" ><span>Update contract</span></a></li>
        </ul>
    </div>

{paging total=$paging.total on_page=$paging.on_page pages=$paging.pages}


<div class="float_half_left wider">

{block title="Performer request"}

<div class="block_body">
    <div class="block_body_r">
        <div class="block_body_c clearfix">
            {foreach from=$list_performer item='fantasy'}
                <ul>
                    <li class="list_item">
                        <div class="performer_{if $fantasy.status eq(0)}new{/if}">
                            <a href="profile.php?profile_id={$fantasy.profile_id}">
                                <h2>{$fantasy.username}</h2>
                            </a>     
                            
                            <a href="offer_details.php?worck_id={$fantasy.id_worck}">
                                View offer details
                            </a>
                            
                            
                            {if $user and $fantasy.worck_status eq(0)}
                                {component $fantasy.SendMessage}
                                <input type="button" value="Decline" onclick="SendDecline({$fantasy.id_worck});" style="float:right;" id="button_confirm_{$fantasy.id_worck}" />
                                 <input type="button" value="Accept" onclick="SendAccept({$fantasy.id_worck});" style="float:right;" id="button_refuse_{$fantasy.id_worck}" />
                                 {else}
                                      {if $fantasy.worck_status eq('0')}
                                            <span class="sent">Sent</span>
                                        {elseif $fantasy.worck_status eq('1')}
                                            <span class="accept">Accepted</span>
                                        {elseif $fantasy.worck_status eq('-1')}
                                            <span class="declined">Declined</span>
                                         {elseif $fantasy.worck_status eq('2')}
                                            <span class="sent">Pending verification</span>
                                        {/if}
                            {/if}
                            <br clear="all"> 
                        </div>
                        <br clear="all"> 
                        <a href="fantasy.php?edit={$fantasy.id}"><h3>{$fantasy.title}</h3></a>
                        <div class="tag_lis">
                            <ul >
                                    {foreach from=$fantasy.fantasy_tag item='tag'}
                                       
                                        <li class="item_tag"><p>{$tag.tag}</p></li>
                                       
                                    {/foreach}   
                                    </ul>
                               <br clear="all">     
                        </div>
                         <div class="description_fantasy">
                            <div class="description_fantasy_text" id="desc_{$fantasy.id}">
                            {$fantasy.description}
                            </div>
                            <div id="show_{$fantasy.id}"><a href="#" onclick="Show('{$fantasy.id}'); return false;">View all</a></div>
                            <div  id="hide_{$fantasy.id}" style="display:none;"><a href="#" onclick="Hide('{$fantasy.id}'); return false;">Hide</a></div>
                        </div>
                        <div class="informer_fantasy">
                            {if $fantasy.budget eq '0'}
                                Negotiable
                            {else}
                                  {$fantasy.budget}$
                            {/if}
                           
                            
                             <br clear="all">
                            <ul>
                                {foreach from=$fantasy.fantasy_file item='file' key='i'}
                                    <li><a href="/$userfiles/upload_fantasy/{$file.name}" title="{$file.name}">file{$i+1}</a></li>
                                {/foreach}   
                            </ul>
                        </div>
                        <br clear="all">
                        <div>
                            <ul>
                            {foreach from=$fantasy.performer item='performer'  key='k'}
                                <li class="item_performer">
                                    Performer {$k+1}:
                                    <ul class="item_ul">
                                    {foreach from=$performer.list_tag item='tags'}
                                       
                                        <li class="item_tag"><p>{$tags.tag}</p></li>
                                       
                                    {/foreach}   
                                    </ul>
                                </li>
                            {/foreach}
                            </ul>
                        </div>
                    </li>
                </ul>
             {/foreach}
         </div>
     </div>
</div>


<br clear="all" />
{paging total=$paging.total on_page=$paging.on_page pages=$paging.pages}

{/block}

</div>


{literal}
{/literal}
{/container}
{/canvas}