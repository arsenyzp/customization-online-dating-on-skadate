
{canvas}
{container stylesheet=" fantasy_view.style"}

<div class="float_half_left wider">

{block title="View Fantasy"}

<div class="block_body">
    <div class="block_body_r">
        <div class="block_body_c clearfix">
<div id="form_create">
  
            <table  id="performer_list">
                <tr>
                    <td>
                       <h2>{$title}</h2>
                    </td>
                </tr>
                <tr>
                    <td> 
                       <div class="tag_lis">
                            <ul >
                                    {foreach from=$fantasy_tag item='tag'}
                                       
                                        <li class="item_tag"><p>{$tag.tag}</p></li>
                                       
                                    {/foreach}   
                                    </ul>
                                    
                                {if $user}
                                {if $my_fantasy}
                                    <input type="button" value="Edit" onclick="document.location = 'fantasy.php?edit={$id}'" style="float:right;" />
                                {elseif !$send}
                                    {*<input type="button" value="Delete" onclick="SendDelete({$id});" style="float:right;" id="button_{$id}" />*}
                                {else}
                                    <input type="button" value="Apply contract" onclick=" document.location = '/member/offer_details?fantasy_id={$id}'" style="float:right;" id="button_{$id}" />    
                                {/if}
                            {/if}
                                    
                               <br clear="all">     
                        </div>
                    </td>
                </tr>
                 <tr>
                     <td >{$description}</td>
                </tr>
                <tr>
                    <td>
                        <div>
                            <ul>
                            {foreach from=$performers item='performer'  key='k'}
                                <li class="item_performer">
                                    Performer {$k+1}:
                                    <ul class="item_ul">
                                    {foreach from=$performer.list_tag item='tags'}                       
                                        <li class="item_tag"><p>{$tags.tag}</p></li>
                                    {/foreach}   
                                    </ul>
                                </li>
                            {/foreach}
                            </ul>
                        </div>
                     </td>
                </tr>
                <tr>
                    <td>
                    {if $budget eq '0'}
                                Negotiable
                            {else}
                                  {$budget}$
                            {/if}
                     </td>
                </tr>
                <tr>
                     <td>
                         <ul>
                             {foreach from=$files item='file' key='i'}
                                 <li>
                                     <a href="/userfiles/upload_fantasy/{$file.name}" title="{$file.name}">
                                         file{$i+1}
                                     </a> 
                                 </li>
                             {/foreach}
                        </ul>
                     </td>
                </tr>
                <tr>
                    <td>
                         Expiration Date: {$data_ex}
                    </td>
                </tr>
                <tr>
                    <td>
                        Creator: <a href="profile.php?profile_id={$creator_info.profile_id}"> {$creator_info.username}</a>
                    </td>
                </tr>
            </table>
</div>
</div></div></div>
                        
                        {/block}

</div>

{/container}
{/canvas}