
{canvas}
{container stylesheet="fantasy.style"}


<div class="float_half_left wider">

{block title="Create Fantasy"}

<div id="form_create">
    {form Fantasy}
        <fieldset>
            <table  id="performer_list">
                <tr>
                    <td><label for="title" >Title:</label></td> <td><input type="text" name="title" class="input_create_fantasy" /></td>
                </tr>
                <tr>
                    <td><label for="tags" >Tags:</label></td> <td> <input type="text" id="demo-input-facebook-theme" name="tags" /></td>
                </tr>
                <tr>
                    <td><label for="category" >Category:</label></td> <td>{input name='category' labelsection='video_categories'}</td>
                </tr>
                <tr>
                    <td  colspan="2"><label for="description" >Description:</label></td>
                </tr>
                 <tr>
                     <td colspan="2"><textarea name="description" /></textarea></td>
                </tr>
                <tr>
                    <td><label for="perfomers" >Performers:</label></td> <td><input type="text"  name="perfomers[]" id="performer_tag" /><input type="button" value="+"  onclick="add_field();" /></td>
                </tr>
            </table>
            <table>
                <tr>
                    <td class="td_label"><label for="budget" >Budget:</label></td>
                    <td><input type="text" id="budget"  name="budget" value="0" class="input_create_fantasy"  /><label for="buggest_null" >Negotiable:</label><input type="checkbox" name="buggest_null" id="buggest_null" /></td>
                </tr>
                 <tr>
                    <td><label for="tags" >Price video view:</label></td>
                    <td><input type="text" id="price_video" name="price_video" value="5" class="input_create_fantasy" /></td>
                </tr>
                <tr>
                    <td><label for="tags" >Lenght video (minimum 0.5 min):</label></td>
                    <td><input type="text" id="length_video" name="length_video" value="2" class="input_create_fantasy" />min</td>
                </tr>
            </table>
            <table id="file_list">
                <tr>
                     <td class="td_label"><label for="file" >Upload:</label></td> 
                     <td>{input name="file"}{*<input type="file"  name="file[]" id="file_fantasy" /><input type="button" value="+"  class="file_list_add" />*}</td>
                </tr>
            </table>
            <table>
                <tr>
                    <td class="td_label"><label for="data_fantasy_ex">Expiration Date</label></td>
                    <td><input type="text"  name="data_fantasy_ex" id="inputDate" /></td>
                </tr>
                {if $private}
                <tr>
                    <td class="td_label"><label for="completion_date">Completion Date</label></td>
                    <td><input type="text"  name="completion_date" id="completion_date" /></td>
                </tr>
                {/if}
                <tr>
                    <td width="100px"><label for="fantasy_private">Private Fantasy  (+ $20 Keys)</label></td>
                    <td><input type="checkbox"  name="fantasy_private" {if $private}checked="checked"{/if} /></td>
                </tr>
                <tr>
                    <td><input type="submit"  value="Create" /></td> <td><input type="button"  value="Canсel" id="canel_create" /></td>
                </tr>
            </table>
        </fieldset>
   {/form}
</div>
{/block}

</div>
{/container}
{/canvas}