
{canvas}
{container stylesheet="upload_fantasy.style"}
{*
<div class="float_half_right narrower">
<input type="button" value="Create Fantasy" id="create_fantasy"  onclick="document.location = 'fantasy.php'" />
</div>
*}
<br clear="all" />

<div class="float_half_left wider">

{block title="Upload Fantasy"}

<div id="form_create">
  
            <table  id="performer_list">
                <tr>
                    <td>
                       <h2>{$title}</h2>
                    </td>
                </tr>
                <tr>
                    <td> 
                       <div class="tag_lis">
                            <ul >
                                    {foreach from=$fantasy_tag item='tag'}
                                       
                                        <li class="item_tag"><p>{$tag.tag}</p></li>
                                       
                                    {/foreach}   
                                    </ul>
                                    
                               <br clear="all">     
                        </div>
                    </td>
                </tr>
                 <tr>
                     <td >
                         <div class="description_fantasy_text"  id="desc_{$fantasy_id}" >
                         {$description}
                         </div>
                         <div id="show_{$fantasy_id}">
                             <a href="#" onclick="Show('{$fantasy_id}'); return false;">View all</a>
                         </div>
                         <div  id="hide_{$fantasy_id}" style="display:none;">
                             <a href="#" onclick="Hide('{$fantasy_id}'); return false;">Hide</a>
                         </div>
                     </td>
                </tr>
                <tr>
                    <td>
                        <div>
                            <ul>
                            {foreach from=$performers item='performer'  key='k'}
                                <li class="item_performer">
                                    Performer {$k+1}:
                                    <ul class="item_ul">
                                    {foreach from=$performer.list_tag item='tags'}                       
                                        <li class="item_tag"><p>{$tags.tag}</p></li>
                                    {/foreach}   
                                    </ul>
                                </li>
                            {/foreach}
                            </ul>
                        </div>
                     </td>
                </tr>
                <tr>
                    <td>
                    {if $budget eq '0'}
                                Negotiable
                            {else}
                                  {$budget}$
                            {/if}
                     </td>
                </tr>
                <tr>
                     <td>
                         <ul>
                             {foreach from=$files item='file' key='i'}
                                 <li>
                                     <a href="/$userfiles/upload_fantasy/{$file.name}" title="{$file.name}">
                                         file{$i+1}
                                     </a> 
                                 </li>
                             {/foreach}
                        </ul>
                     </td>
                </tr>
                <tr>
                    <td>
                         Expiration Date: {$data_ex}
                    </td>
                </tr>
            </table>
</div>

<br clear="all" />
<div>
    {form UploadFantasy}
        
       <div>
           <input type="hidden" name="fantasy_id" value="{$fantasy_id}" />
           <input type="hidden" name="profile_id" value="{$profile_id}" />
       </div>
       <div>{input name="fantasy"}</div>
       <div>
           <textarea name="description"></textarea>
       </div>
       <div>
           <input type="submit" value="Send" />
       </div>
    {/form}
</div>

{/block}

</div>


{literal}
{/literal}
{/container}
{/canvas}