{* Component ProfileFantasy *}
{paging total=$paging.total on_page=$paging.on_page pages=$paging.pages}


<div class="float_half_left wider">

{block title="Profile Fantasy"}

<div class="block_body">

	<ul class="video_list">
	{foreach from=$video item='video_item'}
		<li class="list_item">
		{if $video_item.is_converted eq 'no'}
			<div class="video_info">
				<div class="small">
                                                                            {$video_item.upload_stamp|spec_date}<br/> hours ago <br/> Status: not converted yet<br />
                                                                        </div>
			</div>
			<div class="video_thumb">
				<div class="video_def_thumb"></div>
			</div>
			<div class="video_body">
				<span class="a_fake">{$video_item.title|out_format|smile|censor:"video":true}</span>
                                
                            <div class="description_fantasy">
                                <div class="description_fantasy_text" id="desc_{$video_item.video_id}">
                               {$video_item.description|out_format:"comment"|smile|censor:"video":true}
                                </div>
                                <div id="show_{$video_item.video_id}"><a href="#" onclick="Show('{$video_item.video_id}'); return false;">View all</a></div>
                                <div  id="hide_{$video_item.video_id}" style="display:none;">
                                    <a href="#" onclick="Hide('{$video_item.video_id}'); return false;">Hide</a>
                                </div>
                            </div>
                                
				
			</div>
		{else}
			<div class="video_info">
				<div class="small">
					{rate rate=$video_item.rate_score feature='video'}
					{$video_item.upload_stamp|spec_date}<br/>
				</div>
				<div class="small">
					Views: <span class="highlight">{$video_item.view_count}</span><br />
					Comments: <span class="highlight">{$video_item.comment_count}</span><br />
					Rates: <span class="highlight">{$video_item.rate_count}</span><br />
					<hr />
					Status: {$video_item.status}
				</div>
			</div>
			<div class="video_thumb">
				<a href="{$video_item.video_page}">
					{if $video_item.thumb_img eq 'default'}
						<div class="video_def_thumb"></div>
					{else}
						<img src="{$video_item.thumb_img}" class="video_thumb" align="left" />
					{/if}
				</a><br clear="all" />
				<div class="small center">{component VideoControls video_id=$video_item.video_id}</div>
			</div>
			<div class="video_body">
				<a href="{$video_item.video_page}">{$video_item.title|out_format|smile|censor:"video":true}</a>
				<div class="description_fantasy">
                                                                            <div class="description_fantasy_text" id="desc_{$video_item.video_id}">
                                                                           {$video_item.description|out_format:"comment"|smile|censor:"video":true}
                                                                            </div>
                                                                            <div id="show_{$video_item.video_id}"><a href="#" onclick="Show('{$video_item.video_id}'); return false;">View all</a></div>
                                                                            <div  id="hide_{$video_item.video_id}" style="display:none;">
                                                                                <a href="#" onclick="Hide('{$video_item.video_id}'); return false;">Hide</a>
                                                                            </div>
                                                                        </div>
			</div>
		{/if}
			<div class="clr"></div>
		</li>
	{/foreach}
	</ul>


</div>


<br clear="all" />
{paging total=$paging.total on_page=$paging.on_page pages=$paging.pages}

{/block}

</div>


{literal}
    <script>
       function Show(id){
            $('#desc_'+id).css('height', '100%');
            $('#show_'+id).css('display', 'none');
            $('#hide_'+id).css('display', 'block');
        }

        function Hide(id){
            $('#desc_'+id).css('height', '5em');
            $('#show_'+id).css('display', 'block');
            $('#hide_'+id).css('display', 'none');
        }
    </script>
{/literal}
