
{canvas}
{container stylesheet="landing.style"}

{*if $list_type eq 'tags' && !$tag_words}
	{component $VideoTagNavigator}
{else*}

{*capture name='list_title'}
	{if $list_type eq 'tags'}{text %tags_label} '{$tag_words}'{elseif $list_type eq 'profile'}{text %video_by} {$username}{else}{text %list.`$list_type`}{/if}
{/capture*}



<div class="line_page"></div>
    <!--Меню пользователя-->
    <div class="content_menu">
            <div class="content_menu_item {if $tabs==0}activ_menu{/if}"><a href="?tabs=0">LATEST</a></div>
            <div class="content_menu_item {if $tabs==1}activ_menu{/if}"><a href="?tabs=1">TOP LENGTH</a></div>
            <div class="content_menu_item {if $tabs==2}activ_menu{/if}"><a href="?tabs=2">HIGHEST RATED</a></div>
            <div class="content_menu_item {if $tabs==3}activ_menu{/if}"><a href="?tabs=3">MOST DISCUSSED</a></div>
            <div class="content_menu_item {if $tabs==4}activ_menu{/if}"><a href="?tabs=4">MOST LIKED</a></div>
            <div class="content_menu_item {if $tabs==5}activ_menu{/if}"><a href="?tabs=5">RANDOM</a></div>
            <div style="clear:both;"></div>
    </div>
    <div class="content_search">
            <div class="input-append">
                <form id="landing_search">
                    <input id="searc_video_input" class="span2" size="16" type="text" name="tag">
                </form>
                                    <span class="add-on"><img src="/static/img/searchi.png"></span>
            </div>
    <div class="padding10 total_video">
            {$paging.total} performers video
    </div>
    </div>



{**********************************************}

<div class="line"></div>
<div class="content_tab">
     {if $list}
         {foreach from=$list item='video' key='it' name='vid'}
        <!--видео галерея список -->
       {if $it == 0}<div class="row-fluid">{/if}
       {if $it % 3 == 0 && $it != 0}</div><div class="line"></div><div class="row-fluid">{/if}
                <div class="span4">
                        <div class="video_name">{$video.title|out_format|smile|censor:"video":true}</div>
                        <div class="video_thumb video_video">
                            {if $video.thumb_img eq 'default'}
                                    <a href="{$video.video_page}"><div class="video_def_thumb"></div></a>
                            {elseif $video.thumb_img eq 'friends_only'}
                                    <div class="video_friends_thumb"></div>
                            {elseif $video.thumb_img eq 'password_protected'}
                                <a href="{$video.video_page}"><div class="video_password_thumb"></div></a>
                            {else}
                                    <a href="{$video.video_page}"><img src="{$video.thumb_img}" class="video_thumb" align="left" /></a>
                            {/if}
                        </div>
                     
                        <div class="row">
                             <div class="span6">
                                    <div>
                                        {rate rate=$video.rate_score feature='video' small='small'}
                                    </div>
                             </div>
                             <div class="span6">
                                <div class="stat_video_list row">
                                    <div class="span3"></div>
                                    <div class="video_like span3">{$video.like_count}</div>
                                    <div class="video_view span3">{$video.view_count}</div>
                                    <div class="video_comment span3">{$video.comment_count}</div>
                                 </div>
                             </div>
                        </div>
                        
                        <div class="video_desc">{$video.description|out_format|truncate:150|smile|censor:"video":true}</div>
                </div>
        {if $smarty.foreach.vid.last}</div>{/if}
        {/foreach}

        <div style="clear:both;"></div>
        <div class="pagin_footer">
                </div >
                
        {else}
	   <div class="no_content">No video</div>
        {/if}
        </div>
{***********************************************}


  
{literal}
<script>
    $(document).ready(function(){
        $('.video_thumb').css('width', '250px');
        $("#searc_video_input").tokenInput('/latest_video.php', {
        theme: "facebook",
        queryParam: 'q',
        onAdd: function(){
                $('#landing_search').submit();
            }
        });
    });
</script>
{/literal}


<br clear="all" />
{paging total=$paging.total on_page=$paging.on_page pages=$paging.pages}
{*/if*}
{/container}
{/canvas}