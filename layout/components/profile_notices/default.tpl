{* component Attention *}

{container class="profile_notice_cont"}
	{block title=%title class="indicator"}
		<ul class="simple">
			{if $friend_requests}
				<li>
					{text %new}
					<a href="{document_url doc_key='profile_friend_list' tab='got_requests'}">
						<b>{text %requests}</b>
					</a>
					({$friend_requests})
				</li>
			{/if}
			{if $new_messages}
				<li>
					{text %new}
					<a href="{document_url doc_key='mailbox' folder='inbox'}"><b>{text %messages}</b></a>
					({$new_messages})
				</li>
			{/if}
			{if $invites_count}
				<li>
					{text %new}
					{text %invitations}
					({$invites_count}):
					<ul>
					{foreach from=$invites item=invite name=i}
						<li> - <a href="{document_url doc_key='group' group_id=$invite.group_id}"><b>{$invite.title|out_format|truncate:50|censor:'group':true}</b></a></li>
					{/foreach}
					<ul>
				</li>
			{/if}
                        
                        {*********}
                        
                        {if $request_count != 0}
				<li>
					{text %new}
					{*text %invitations*}
                                         
                                        <a href="{document_url doc_key='view_request'}"><b>Request</b></a>
					({$request_count}):
					
				</li>
			{/if}
                        
                        {if $fantasy_count != 0}
				<li>
					You have 
                                         
                                        <a href="{document_url doc_key='view_works'}"><b>  completed fantasies</b></a>
					({$fantasy_count})
				</li>
			{/if}
                        
                        {if $update_contract != 0}
				<li>
					New 
                                         
                                        <a href="{document_url doc_key='sent_aplication'}?tabs=4"><b>  update contract</b></a>
					({$update_contract})
				</li>
			{/if}
                        
                        {if $update_request != 0}
				<li>
					New 
                                         
                                        <a href="{document_url doc_key='view_request'}?tabs=4"><b>  update request</b></a>
					({$update_request})
				</li>
			{/if}
                        
                        {if $new_invate != 0}
				<li>
					New 
                                         
                                        <a href="{document_url doc_key='sent_aplication'}?tabs=4"><b>  invite</b></a>
					({$new_invate})
				</li>
			{/if}
                        
                        {if $new_contract != 0}
				<li>
					Open 
                                         
                                        <a href="{document_url doc_key='sent_aplication'}?tabs=2"><b>  contract</b></a>
					({$new_contract})
				</li>
			{/if}
                        
                        {*********}
                        
		</ul>
	{/block}
{/container}