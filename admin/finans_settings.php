<?php

$file_key = 'fantasy';
$active_tab = 'statistic';

require_once( '../internals/Header.inc.php' );

// Admin auth
require_once( DIR_ADMIN_INC.'inc.auth.php' );

require_once( DIR_ADMIN_INC.'class.admin_frontend.php' );
require_once( DIR_ADMIN_INC.'class.admin_profile_field.php' );
require_once( DIR_ADMIN_INC.'class.admin_configs.php' );

require_once( 'inc/fnc.profile_list.php' );
require_once( DIR_ADMIN_INC.'class.admin_profile.php' );

// requiring applications
require_once( DIR_APPS.'Tags.app.php' );

$frontend = new AdminFrontend( );

adminConfig::SaveConfigs($_POST);
adminConfig::getResult($frontend);

$_page['title'] = 'Finance Setting';

require_once( 'inc.admin_menu.php' );


function getURL($params, $query)
{
	return sk_make_url(null, $query);
}

$sections = adminConfig::getChildSections('finance_key');
array_unshift( $sections, 'finance_key');
$frontend->assign_by_ref('sections', $sections);

//SK_Config::section('finance_key')->get('set_active_video_on_upload')

$frontend->display( 'finans_settings.html' );