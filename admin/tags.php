<?php

$file_key = 'tags';
$active_tab = 'tags_fantasy';

require_once( '../internals/Header.inc.php' );

// Admin auth
require_once( DIR_ADMIN_INC.'inc.auth.php' );

require_once( DIR_ADMIN_INC.'class.admin_frontend.php' );
require_once( DIR_ADMIN_INC.'class.admin_profile_field.php' );
require_once( DIR_ADMIN_INC.'class.admin_configs.php' );

require_once( 'inc/fnc.profile_list.php' );
require_once( DIR_ADMIN_INC.'class.admin_profile.php' );

// requiring applications
require_once( DIR_APPS.'Tags.app.php' );

$frontend = new AdminFrontend( );

$_page['title'] = 'Tags List';

require_once( 'inc.admin_menu.php' );

if ( isset($_POST['action']) )
{
    switch (trim($_POST['action'])){
        case 'create':
            app_Tags::InsertTag(trim($_POST['tag']));
            break;
        case 'delete':
             app_Tags::DeleteTag(trim($_POST['id']));
            break;
        case 'update':
             app_Tags::UpdateTag(trim($_POST['id']), trim($_POST['tag']));
            break;
    }    
}

if ( isset($_GET['action']) )
{
   $frontend->assign( 'cerate_form', true);    
}

$res_per_page = profileList_getResultsPerPageValue();

if( !(int)@$_GET['_page'] )
	$_GET['_page'] = 1;

$limit = navigationDBLimit( $res_per_page, $_GET['_page'] );//$limit['sql']

$tags = app_Tags::GetAllTags($limit['sql'], @$_GET['_sf'], @$_GET['_so']);


function getURL($params, $query)
{
	return sk_make_url(null, $query);
}


$total_results_num = app_Tags::GetAllTagsCount();

$frontend->assign( 'db_results', $limit['begin'].'-'.($limit['begin']+$results_num-1) );

$frontend->assign_by_ref( 'total_results_num', $total_results_num );

$frontend->assign_by_ref( 'tags', $tags );

$sort_order = @$_GET['_so'];
	$frontend->assign_by_ref( 'sort_order', $sort_order );

$sort_field = @$_GET['_sf'];
	$frontend->assign_by_ref( 'sort_field', $sort_field );

$field = array('id'=>'id', 'tag'=>'tag');
        
$frontend->assign_by_ref( 'checked_fields', $field);

$frontend->assign( 'rpp_select', ResPerPageSelect( array(10,30,50,100), $res_per_page ) );

$frontend->assign( 'navigation_pages', navigationPages( ceil( $total_results_num/$res_per_page ) ) );

$frontend->register_block('make_url', 'getURL');

//$frontend->IncludeJsFile( URL_ADMIN_JS.'opacity.js' );
//$frontend->IncludeJsFile( URL_ADMIN_JS.'form.js' );
//$frontend->IncludeJsFile( URL_ADMIN_JS.'profile_list.js' );

$frontend->display( 'tags.html' );