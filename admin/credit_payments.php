<?php

$file_key = 'user_points';
$active_tab = 'credits_payments';

require_once( '../internals/Header.inc.php' );

// Admin auth
require_once( DIR_ADMIN_INC.'inc.auth.php' );

require_once( DIR_ADMIN_INC.'class.admin_frontend.php' );
require_once( DIR_ADMIN_INC.'class.admin_profile_field.php' );
require_once( DIR_ADMIN_INC.'class.admin_configs.php' );

require_once( 'inc/fnc.profile_list.php' );
require_once( DIR_ADMIN_INC.'class.admin_profile.php' );

if(isset($_POST['action'])){
    switch ($_POST['action']){
        case 'pay':
            if(intval($_POST['id']))
                app_KuinKeys::PayAdmin(intval($_POST['id']));
            break;
    }
}

$frontend = new AdminFrontend( );

$_page['title'] = 'Credits Payments';

require_once( 'inc.admin_menu.php' );


$res_per_page = profileList_getResultsPerPageValue();

if( !(int)@$_GET['_page'] )
	$_GET['_page'] = 1;

$limit = navigationDBLimit( $res_per_page, $_GET['_page'] );//$limit['sql']

$fantasy = app_KuinKeys::GetRequestsPayment($limit['sql'], @$_GET['_sf'], @$_GET['_so']);


function getURL($params, $query)
{
	return sk_make_url(null, $query);
}


$total_results_num = app_KuinKeys::GetRequestsPaymentCount();

$frontend->assign( 'db_results', $limit['begin'].'-'.($limit['begin']+$results_num-1) );

$frontend->assign_by_ref( 'total_results_num', $total_results_num );

$frontend->assign_by_ref( 'fantasy', $fantasy );

$sort_order = @$_GET['_so'];
	$frontend->assign_by_ref( 'sort_order', $sort_order );

$sort_field = @$_GET['_sf'];
	$frontend->assign_by_ref( 'sort_field', $sort_field );

$field = array('id'=>'id', 'username'=>'username', 'amount'=>'amount', 'date_request'=>'date_request', 'status_operation'=>'status_operation');

$frontend->assign_by_ref( 'count_colum', count($field));

$frontend->assign_by_ref( 'checked_fields', $field);

$frontend->assign( 'rpp_select', ResPerPageSelect( array(10,30,50,100), $res_per_page ) );

$frontend->assign( 'navigation_pages', navigationPages( ceil( $total_results_num/$res_per_page ) ) );

$frontend->register_block('make_url', 'getURL');

$frontend->display( 'credits_payments.html' );