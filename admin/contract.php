<?php

$file_key = 'fantasy';
$active_tab = 'contract';

require_once( '../internals/Header.inc.php' );

// Admin auth
require_once( DIR_ADMIN_INC.'inc.auth.php' );

require_once( DIR_ADMIN_INC.'class.admin_frontend.php' );
require_once( DIR_ADMIN_INC.'class.admin_profile_field.php' );
require_once( DIR_ADMIN_INC.'class.admin_configs.php' );

require_once( 'inc/fnc.profile_list.php' );
require_once( DIR_ADMIN_INC.'class.admin_profile.php' );

// requiring applications
require_once( DIR_APPS.'Tags.app.php' );

$frontend = new AdminFrontend( );

$_page['title'] = 'Contract List';

require_once( 'inc.admin_menu.php' );

if ( isset($_POST['action']) )
{
   
    
}

if ( isset($_GET['action']) )
{
    switch ($_GET['action']){
        case 'del':
          //  app_Fantasy::Delete(intval($_GET['fantasy_id']));
            break;
    }
    
}

$res_per_page = profileList_getResultsPerPageValue();

if( !(int)@$_GET['_page'] )
	$_GET['_page'] = 1;

$limit = navigationDBLimit( $res_per_page, $_GET['_page'] );//$limit['sql']

$fantasy = app_FantasyWorck::GetAllContract($limit['sql'], @$_GET['_sf'], @$_GET['_so']);

foreach ($fantasy as $k=>$v){
    $fantasy[$k]['date_create'] = date("Y/m/d", $v['date_create']);
    if($fantasy[$k]['completion_date'] < time())
        $fantasy[$k]['completion_date'] = "<font color=red>".date("Y/m/d", $v['completion_date'])."</font>";
    else
         $fantasy[$k]['completion_date'] = date("Y/m/d", $v['completion_date']);
    $fantasy[$k]['private'] = ($v['private']>0)?'Yes':'No';
    $fantasy[$k]['worck_status'] =  app_FantasyWorck::GetStatusName($v['worck_status']);
}


function getURL($params, $query)
{
	return sk_make_url(null, $query);
}


$total_results_num = app_FantasyWorck::GetTotalContract();

$frontend->assign( 'db_results', $limit['begin'].'-'.($limit['begin']+$results_num-1) );

$frontend->assign_by_ref( 'total_results_num', $total_results_num );

$frontend->assign_by_ref( 'fantasy', $fantasy );

$sort_order = @$_GET['_so'];
	$frontend->assign_by_ref( 'sort_order', $sort_order );

$sort_field = @$_GET['_sf'];
	$frontend->assign_by_ref( 'sort_field', $sort_field );

$field = array('worck_status'=>'worck_status', 'title'=>'title', 'date_create'=>'date_create', 'completion_date'=>'completion_date', 'private'=>'private');

$frontend->assign_by_ref( 'count_colum', count($field));

$frontend->assign_by_ref( 'checked_fields', $field);

$frontend->assign( 'rpp_select', ResPerPageSelect( array(10,30,50,100), $res_per_page ) );

$frontend->assign( 'navigation_pages', navigationPages( ceil( $total_results_num/$res_per_page ) ) );

$frontend->register_block('make_url', 'getURL');

//$frontend->IncludeJsFile( URL_ADMIN_JS.'opacity.js' );
//$frontend->IncludeJsFile( URL_ADMIN_JS.'form.js' );
//$frontend->IncludeJsFile( URL_ADMIN_JS.'profile_list.js' );

$frontend->display( 'contract.html' );