<?php

$file_key = 'fantasy';
$active_tab = 'statistic';

require_once( '../internals/Header.inc.php' );

// Admin auth
require_once( DIR_ADMIN_INC.'inc.auth.php' );

require_once( DIR_ADMIN_INC.'class.admin_frontend.php' );
require_once( DIR_ADMIN_INC.'class.admin_profile_field.php' );
require_once( DIR_ADMIN_INC.'class.admin_configs.php' );

require_once( 'inc/fnc.profile_list.php' );
require_once( DIR_ADMIN_INC.'class.admin_profile.php' );

// requiring applications
require_once( DIR_APPS.'Tags.app.php' );

$frontend = new AdminFrontend( );

$_page['title'] = 'Fantasy List';

require_once( 'inc.admin_menu.php' );

if ( isset($_POST['action']) )
{
   
    
}


$fantasy = app_Fantasy::GetAllListAdmin($limit['sql'], @$_GET['_sf'], @$_GET['_so']);


function getURL($params, $query)
{
	return sk_make_url(null, $query);
}


$frontend->assign_by_ref( 'fantasy', $fantasy );

$frontend->assign( 'rpp_select', ResPerPageSelect( array(10,30,50,100), $res_per_page ) );

$frontend->register_block('make_url', 'getURL');

//$frontend->IncludeJsFile( URL_ADMIN_JS.'opacity.js' );
//$frontend->IncludeJsFile( URL_ADMIN_JS.'form.js' );
//$frontend->IncludeJsFile( URL_ADMIN_JS.'profile_list.js' );

$frontend->display( 'fantasy_edit.html' );